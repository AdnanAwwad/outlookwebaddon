﻿namespace OTAWebAppWeb
{
    public class outlookItem
    {
        public string url { get; set; }
        public string userKey { get; set; }
        public string attachmentIds { get; set; }
        public string messageId { get; set; }
        public string outlookToken { get; set; }
        public string outlookRestUrl { get; set; }
    }
}