﻿using System.Text;
using System.IO;
using System.Net;
using System.Collections.Specialized;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Threading.Tasks;

namespace OTAWebAppWeb
{
    class Web
    {
        #region UploadFilesAttachments
        //public static string sentRequestToPostFormDataANDMultipleFiles(string urlApp4Legal, string pathAttachmentsFiles, string dataToPOST,string userKey)
        //{
        //    try
        //    {
        //        string[] filesArray = Directory.GetFiles(pathAttachmentsFiles);
        //        return PostFormDataANDMultipleFilesProcedure(urlApp4Legal, filesArray, dataToPOST, userKey);
        //    }
        //    catch (Exception ex)
        //    {
        //        return ex.Message;
        //    }
        //}

       
        #endregion
        public static string getdata(string url)
        {
            string valueOriginal = string.Empty;
            using (WebClient webClient = new System.Net.WebClient())
            {
                WebClient n = new WebClient();
                //this is how you authenticate to jira
                System.Net.ServicePointManager.ServerCertificateValidationCallback = (senderX, certificate, chain, sslPolicyErrors) => { return true; };
                n.Headers.Add("Content-Type", "application/json");
                //to get arabic values
                n.Encoding = System.Text.Encoding.UTF8;
                var json = n.DownloadString(url);
                valueOriginal = Convert.ToString(json);
                return valueOriginal;
            }
        }
        public static Task<string> sendRequestToPostData(string url, string dataToPost,string userKey)
        {
            string valueToReturnFromRequest = string.Empty;
            try
            {
                byte[] buffer = Encoding.UTF8.GetBytes(dataToPost);
                HttpWebRequest WebReq = (HttpWebRequest)WebRequest.Create(url);
                WebReq.Method = "POST";
                ServicePointManager.ServerCertificateValidationCallback = (senderX, certificate, chain, sslPolicyErrors) => { return true; };
                WebReq.ContentType = "application/x-www-form-urlencoded";
                WebReq.Headers["X-api-key"] = userKey;
                WebReq.Headers["x-api-channel"] = "outlook";
                WebReq.ContentLength = buffer.Length;
                Stream dataStream = WebReq.GetRequestStream();
                dataStream.Write(buffer, 0, buffer.Length);
                dataStream.Close();
                WebResponse WebResp = WebReq.GetResponse();
                using (dataStream = WebResp.GetResponseStream())
                {
                    // Open the stream using a StreamReader for easy access.
                    StreamReader reader = new StreamReader(dataStream);
                    // Read the content.
                    string responseFromServer = reader.ReadToEnd();
                    // Display the content.
                    valueToReturnFromRequest = responseFromServer.ToString();
                }
            }
            catch (Exception ex)
            {
                //Console.WriteLine(ex.Message);
                valueToReturnFromRequest = ex.Message;
            }
            return Task.FromResult(valueToReturnFromRequest); 
        }
        public static Task<string> login(string url, string dataToPost)
        {
            string valueToReturnFromRequest = string.Empty;
            try
            {
                byte[] buffer = Encoding.UTF8.GetBytes(dataToPost);
                HttpWebRequest WebReq = (HttpWebRequest)WebRequest.Create(url);
                WebReq.Method = "POST";
                System.Net.ServicePointManager.ServerCertificateValidationCallback = (senderX, certificate, chain, sslPolicyErrors) => { return true; };
                WebReq.ContentType = "application/x-www-form-urlencoded";
                WebReq.Headers["x-api-channel"] = "outlook";
                WebReq.ContentLength = buffer.Length;
                Stream PostData = WebReq.GetRequestStream();
                PostData.Write(buffer, 0, buffer.Length);
                PostData.Close();
                HttpWebResponse WebResp = (HttpWebResponse)WebReq.GetResponse();
                Stream Answer = WebResp.GetResponseStream();
                StreamReader _Answer = new StreamReader(Answer);
                valueToReturnFromRequest = _Answer.ReadToEnd().ToString();
            }
            catch (Exception ex)
            {
                //Console.WriteLine(ex.Message);
                valueToReturnFromRequest = ex.Message;
            }
            return Task.FromResult(valueToReturnFromRequest);
        }

        public static String sendRequestToGETtData(string url,string userKey)
        {
            string valueToReturnFromRequest = string.Empty;

            try
            {
                // converting the data string to array bytes

                //Initialization
                WebRequest WebReq = WebRequest.Create(url);
                // WebReq.Proxy = null;
                // Method is post
                //WebReq.Method = "POST";
                System.Net.ServicePointManager.ServerCertificateValidationCallback = (senderX, certificate, chain, sslPolicyErrors) => { return true; };
                // We use form contentType, for the dataToPost.
                WebReq.ContentType = "application/x-www-form-urlencoded";
                WebReq.Headers["X-api-key"] = userKey;
                WebReq.Headers["x-api-channel"] = "outlook";
                // The length of the buffer (dataToPost) is used as contentlength.
                //   WebReq.ContentLength = buffer.Length;

                // We open a stream for writing the dataToPost
                Stream PostData = WebReq.GetResponse().GetResponseStream();

                // Now we write 
                // PostData.Write(buffer, 0, buffer.Length);
                // And afterwards, we close. Closing is always important!
                //  PostData.Close();

                //Get the response handle
                //   HttpWebResponse WebResp = (HttpWebResponse)WebReq.GetResponse();

                //Now, we read the response (the string), and output it.
                //   Stream Answer = WebResp.GetResponseStream();

                StreamReader _Answer = new StreamReader(PostData);

                valueToReturnFromRequest = _Answer.ReadToEnd().ToString();

            }
            catch (Exception ex)
            {
                //Console.WriteLine(ex.Message);
                valueToReturnFromRequest = ex.Message;
            }

            return valueToReturnFromRequest;
        }
        //Returns the success part of json data returned from app4legal
        public static string serializeJson(string json)
        {
            var dyn = JsonConvert.DeserializeObject<dynamic>(json);
            var p = dyn.success;
            var jObject = (JObject)p;
            return jObject.ToString();


        }
        //Returns error
        public static string deserializeJson(string json)
        {
            var dyn = JsonConvert.DeserializeObject<dynamic>(json);
            var p = dyn.error;
            var jObject = (JObject)p;
            return jObject.ToString();
        }



        public static bool isValidConnection(string url, string user, string password)
        {
            try
            {
                FtpWebRequest request = (FtpWebRequest)WebRequest.Create(url);
                request.Method = WebRequestMethods.Ftp.ListDirectory;
                request.Credentials = new NetworkCredential(user, password);
                request.GetResponse();
            }
            catch (WebException ex)
            {
                return false;
            }
            return true;
        }


        //public static string CLoudApiPost(string url, string dataToPost)
        //{
        //    string valueToReturnFromRequest = string.Empty;

        //    try
        //    {
        //        // converting the data string to array bytes
        //        byte[] buffer = Encoding.ASCII.GetBytes(dataToPost);

        //        //Initialization
        //        HttpWebRequest WebReq = (HttpWebRequest)WebRequest.Create(url);
        //        // WebReq.Proxy = null;
        //        // Method is post
        //        WebReq.Method = "POST";
        //        System.Net.ServicePointManager.ServerCertificateValidationCallback = (senderX, certificate, chain, sslPolicyErrors) => { return true; };
        //        // We use form contentType, for the dataToPost.
        //        WebReq.ContentType = "application/x-www-form-urlencoded";
        //        WebReq.Headers["X-api-key"] = ThisAddIn.userKey;
        //        // The length of the buffer (dataToPost) is used as contentlength.
        //        WebReq.ContentLength = buffer.Length;
        //        // We open a stream for writing the dataToPost
        //        Stream PostData = WebReq.GetRequestStream();
        //        // Now we write 
        //        PostData.Write(buffer, 0, buffer.Length);
        //        // And afterwards, we close. Closing is always important!
        //        PostData.Close();
        //        //Get the response handle
        //        HttpWebResponse WebResp = (HttpWebResponse)WebReq.GetResponse();

        //        //Now, we read the response (the string), and output it.
        //        Stream Answer = WebResp.GetResponseStream();

        //        StreamReader _Answer = new StreamReader(Answer);

        //        valueToReturnFromRequest = _Answer.ReadToEnd().ToString();

        //    }
        //    catch (Exception ex)
        //    {
        //        Console.WriteLine(ex.Message);
        //    }

        //    return valueToReturnFromRequest;
        //}
    }
}