﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections.Specialized;
using System.Net;
using System.IO;
using System.Threading.Tasks;
using System.Linq;

namespace OTAWebAppWeb
{
    public class HttpUploadHelper
    {
        private HttpUploadHelper()
        { }

        public static Task<string> Upload(string url, UploadFile[] files, NameValueCollection form, string apikey)
        {
            HttpWebResponse resp = Upload((HttpWebRequest)WebRequest.Create(url), files, form, apikey);

            using (Stream s = resp.GetResponseStream())
            using (StreamReader sr = new StreamReader(s))
            {
                return Task.FromResult(sr.ReadToEnd());
            }
        }

        public static HttpWebResponse Upload(HttpWebRequest req, UploadFile[] files, NameValueCollection form, string apiKey)
        {
            List<MimePart> mimeParts = new List<MimePart>();

            try
            {
                foreach (string key in form.AllKeys)
                {
                    StringMimePart part = new StringMimePart();
                    System.Net.ServicePointManager.ServerCertificateValidationCallback = (senderX, certificate, chain, sslPolicyErrors) => { return true; };
                    part.Headers["Content-Disposition"] = "form-data; name=\"" + key + "\"";
                    part.StringData = form[key];

                    mimeParts.Add(part);
                }

                int nameIndex = 0;

                foreach (UploadFile file in files)
                {
                    StreamMimePart part = new StreamMimePart();
                    if (file != null)
                    {
                        if (string.IsNullOrEmpty(file.FieldName))
                        {
                            if(file.IsEmail)
                            {
                                file.FieldName = "email_file";
                            }
                            else
                            {
                                file.FieldName = "file" + nameIndex++;
                            }
                        }
                        System.Net.ServicePointManager.ServerCertificateValidationCallback = (senderX, certificate, chain, sslPolicyErrors) => { return true; };
                        part.Headers["Content-Disposition"] = "form-data; name=\"" + file.FieldName + "\"; filename=\"" + file.FileName + "\"";
                        part.Headers["Content-Type"] = file.ContentType;

                        part.SetStream(file.Data);

                        mimeParts.Add(part);
                    }
                }

                string boundary = "----------" + DateTime.Now.Ticks.ToString("x");
                System.Net.ServicePointManager.ServerCertificateValidationCallback = (senderX, certificate, chain, sslPolicyErrors) => { return true; };
                req.ContentType = "multipart/form-data; boundary=" + boundary;
                req.Method = "POST";
                //header is added for app4legal to check that the request is secured
                req.Headers["X-api-key"] = apiKey;
                req.Headers["x-api-channel"] = "outlook";
                //req.Proxy = null;
                long contentLength = 0;

                byte[] _footer = Encoding.UTF8.GetBytes("--" + boundary + "--\r\n");

                foreach (MimePart part in mimeParts)
                {
                    contentLength += part.GenerateHeaderFooterData(boundary);
                }

                req.ContentLength = contentLength + _footer.Length;

                byte[] buffer = new byte[8192];
                byte[] afterFile = Encoding.UTF8.GetBytes("\r\n");
                int read;

                using (Stream s = req.GetRequestStream())
                {
                    foreach (MimePart part in mimeParts)
                    {
                        s.Write(part.Header, 0, part.Header.Length);

                        while ((read = part.Data.Read(buffer, 0, buffer.Length)) > 0)
                            s.Write(buffer, 0, read);

                        part.Data.Dispose();

                        s.Write(afterFile, 0, afterFile.Length);
                    }

                    s.Write(_footer, 0, _footer.Length);
                }

                return (HttpWebResponse)req.GetResponse();
            }
            catch
            {
                foreach (MimePart part in mimeParts)
                    if (part.Data != null)
                        part.Data.Dispose();

                throw;
            }
        }
    }
}