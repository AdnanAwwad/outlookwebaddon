﻿namespace OTAWebAppWeb
{
    public class UserData
    {
        public string url { get; set; }
        public string userKey { get; set; }
        public string caseId { get; set; }
        public bool isContract { get; set; }
    }
}