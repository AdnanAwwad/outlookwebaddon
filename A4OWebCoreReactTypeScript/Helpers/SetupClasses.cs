﻿namespace OTAWebAppWeb
{
    public class Success
    {
        public data data { get; set; }
        public string msg { get; set; }
    }
    public class data
    {
        public string key { get; set; }
        public string userId { get; set; }
        public string case_id { get; set; }
        public string contract_id { get; set; }
        public string hearing_id { get; set; }
        public ProfilePicture profilePicture { get; set; }
    }
    public class ProfilePicture
    {
        public string type { get; set; }
        public string content { get; set; }
    }
    public class errors
    {
        public string error { get; set; }
    }
    public class addedIP
    {
        public IPAdd data { get; set; }
        public string msg { get; set; }
    }
    public class IPAdd
    {
        public string id { get; set; }
    }
}