﻿using System.IO;


namespace OTAWebAppWeb
{
    public class UploadFile
    {
        Stream _data;
        string _fieldName;
        string _fileName;
        string _contentType;
        bool _isEmail;

        public UploadFile(Stream data, string fieldName, string fileName, string contentType)
        {
            _data = data;
            _fieldName = fieldName;
            _fileName = fileName;
            _contentType = contentType;
        }

        public UploadFile(MemoryStream data, string fileName, string contentType, bool isEmail = false)
        {
            _data = data;
            _fileName = fileName;
            _contentType = contentType;
            _isEmail = isEmail;
        }

        public UploadFile(string fileName, string fieldName, string contentType)
            : this(File.OpenRead(fileName), fieldName, Path.GetFileName(fileName), contentType)
        { }

        public UploadFile(string fileName, string contentType)
            : this(fileName, null, contentType)
        { }

        public Stream Data
        {
            get { return _data; }
            set { _data = value; }
        }

        public string FieldName
        {
            get { return _fieldName; }
            set { _fieldName = value; }
        }

        public string FileName
        {
            get { return _fileName; }
            set { _fileName = value; }
        }

        public string ContentType
        {
            get { return _contentType; }
            set { _contentType = value; }
        }
        public bool IsEmail
        {
            get { return _isEmail; }
            set { _isEmail = value; }
        }
    }
}