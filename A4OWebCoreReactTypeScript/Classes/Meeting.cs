﻿namespace OTAWebAppWeb
{
    public class Meeting
    {
        public string[] attachmentIds { get; set; }
        public string messageId { get; set; }
        public string outlookToken { get; set; }
        public string outlookRestUrl { get; set; }
        public string url { get; set; }
        public string userKey { get; set; }
        public string title { get; set; }
        public string from { get; set; }
        public string fromTime { get; set; }
        public string to { get; set; }
        public string toTime { get; set; }
        public string description { get; set; }
        public string relatedMatter { get; set; }
        public string privateVal { get; set; }
        public string[] attendees { get; set; }
        public string location { get; set; }
        public string priority { get; set; }
    }
}