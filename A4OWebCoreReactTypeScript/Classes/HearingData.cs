﻿namespace OTAWebAppWeb
{
    public class HearingData
    {
        public string url { get; set; }
        public string userKey { get; set; }
        public string caseId { get; set; }
    }
}