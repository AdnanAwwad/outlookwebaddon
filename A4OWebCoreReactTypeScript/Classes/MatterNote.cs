﻿using OTAWebAppWeb.Classes;

namespace OTAWebAppWeb
{
    public class MatterNote
    {
        public string[] attachmentIds { get; set; }
        public string messageId { get; set; }
        public string outlookToken { get; set; }
        public string outlookRestUrl { get; set; }
        public string url { get; set; }
        public string userKey { get; set; }
        public string matter { get; set; }
        public string note { get; set; }
        public string customerPortal { get; set; }
        public string mailProps { get; set; }
    }
}