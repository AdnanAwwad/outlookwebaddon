﻿namespace OTAWebAppWeb.Classes
{
    public class AnyDataClass
    {
        public string[] attachmentIds { get; set; }
        public string messageId { get; set; }
        public string outlookToken { get; set; }
        public string outlookRestUrl { get; set; }
        public string url { get; set; }
        public string userKey { get; set; }

        public string dataToPost { get; set; }
    }
}