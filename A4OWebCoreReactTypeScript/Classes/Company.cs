﻿namespace OTAWebAppWeb
{
    public class Company
    {
        public string url { get; set; }
        public string userKey { get; set; }
        public string name { get; set; }
        public string shortName { get; set; }
        public string category { get; set; }
        public string subCategory { get; set; }
        public string companyLegalType { get; set; }
        public string companyGroup { get; set; }
        public string nationality { get; set; }
        public string addresses { get; set; }
        public string capital { get; set; }
        public string currency { get; set; }
        public string referenceNumber { get; set; }
        public string objectVal { get; set; }
        public string commercialRegReleaseDate { get; set; }
        public string commercialRegNb { get; set; }
        public string cbPrivate { get; set; }
        public string[] sharedWith { get; set; }
        public string comments { get; set; }
    }
}