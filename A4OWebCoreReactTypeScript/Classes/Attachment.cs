﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace A4OWebCoreReactWebTs.Classes
{
    public class FileObject
    {
        public string Data { get; set; }
        public SingleFile File { get; set; }
    }
    public class SingleFile
    {
        public string ContentType { get; set; }
        public string FileName { get; set; }
        public int Length { get; set; }
        public Boolean isEmail { get; set; }
    }
    public class AttachmentsModel
    {
        public string Url { get; set; }
        public IList<string> Files { get; set; }
        public string DataToPost { get; set; }
        public string UserKey { get; set; }

    }
}
