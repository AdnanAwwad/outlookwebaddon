﻿namespace OTAWebAppWeb
{ 
    public class IntellectualProperty
    {
        public string[] attachmentIds { get; set; }
        public string messageId { get; set; }
        public string outlookToken { get; set; }
        public string outlookRestUrl { get; set; }
        public string url { get; set; }
        public string userKey { get; set; }
        public string ipRights { get; set; }
        public string iPClass { get; set; }
        public string iPSubcategory { get; set; }
        public string iPName { get; set; }
        public string subject { get; set; }
        public string description { get; set; }
        public string clientCompanyGroup { get; set; }
        public string clientName { get; set; }
        public string country { get; set; }
        public string assignedteam { get; set; }
        public string assignedmember { get; set; }
        public string filedon { get; set; }
        public string filingNumber { get; set; }
        public string registrationDate { get; set; }
        public string registrationRef { get; set; }
        public string agentCompanyGroup { get; set; }
        public string agent { get; set; }
    }
}