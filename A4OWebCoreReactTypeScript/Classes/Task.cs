﻿namespace OTAWebAppWeb
{
    public class NewTask
    {
        public string[] attachmentIds { get; set; }
        public string messageId { get; set; }
        public string outlookToken { get; set; }
        public string outlookRestUrl { get; set; }
        public string url { get; set; }
        public string userKey { get; set; }
        public string taskType { get; set; }
        public string description { get; set; }
        public string relatedMatter { get; set; }
        public string stageId { get; set; }
        public string assignee { get; set; }
        public string reporter { get; set; }
        public string priority { get; set; }
        public string dueDate { get; set; }
        public string status { get; set; }
        public string estEffort { get; set; }
        public string location { get; set; }
        public string cbprivate { get; set; }
        public string[] sharedwith { get; set; }
    }
}