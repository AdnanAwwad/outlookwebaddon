﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OTAWebAppWeb.Classes
{
    public class EmailProps
    {
        public string to { get; set; }
        public string from { get; set; }
        public string fromName { get; set; }
        public string date { get; set; }
        public string subject { get; set; }
    }
}