﻿namespace OTAWebAppWeb
{
    public class NewHearing
    {
        public string[] attachmentIds { get; set; }
        public string messageId { get; set; }
        public string outlookToken { get; set; }
        public string outlookRestUrl { get; set; }
        public string url { get; set; }
        public string userKey { get; set; }
        public string matter { get; set; }
        public string subject { get; set; }
        public string ddate { get; set; }
        public string ddateTime { get; set; }
        public string hearingReference { get; set; }
        public string judgment { get; set; }
        public string comments { get; set; }
        public string[] assigneeResults { get; set; }
        public string[] clientsResults { get; set; }
        public string clientPosition { get; set; }
        public string[] judgesResults { get; set; }
        public string courtType { get; set; }
        public string courtDegree { get; set; }
        public string courtRegion { get; set; }
        public string court { get; set; }
        public string[] extLawyerResults { get; set; }
        public string[] opponentResults { get; set; }
        public string[] opponentLawyerResults { get; set; }
        public string summary { get; set; }
    }
}