﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OTAWebAppWeb.Classes
{
    public class Contract
    {
        public string[] attachmentIds { get; set; }
        public string messageId { get; set; }
        public string outlookToken { get; set; }
        public string outlookRestUrl { get; set; }
        public string url { get; set; }
        public string userKey { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public string contractType { get; set; } // Area of practice
        public string parties { get; set; }
        public string assignedteam { get; set; }
        public string assignedmember { get; set; }
        public string contractDate { get; set; }
        public string startDate { get; set; }
        public string endDate { get; set; }
        public string referenceNumber { get; set; }
        public string renewal { get; set; }
        public string priority { get; set; }
        public string requester { get; set; }
        public String[] contributors { get; set; }
        public string value { get; set; }
        public string currency { get; set; }
    }
}