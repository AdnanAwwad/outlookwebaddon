﻿namespace OTAWebAppWeb
{
    public class Contact
    {
        public string url { get; set; }
        public string userKey { get; set; }
        public string title { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string middleName { get; set; }
        public string category { get; set; }
        public string subCategory { get; set; }
        public string[] companyGroupResults { get; set; }
        public string jobTitle { get; set; }
        public string email { get; set; } 
        public string phone { get; set; }
        public string mobile { get; set; }
        public string isLawyer { get; set; }
        public string inHouseLawyer { get; set; }
        public string website { get; set; }
        public string fax { get; set; }
        public string addressOne { get; set; }
        public string addressTwo { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string country { get; set; }
        public string zip { get; set; }
        public string cbPrivate { get; set; }
        public string[] sharedwith { get; set; }
        public string gender { get; set; }
        public string motherName { get; set; }
        public string referenceNumber { get; set; }
        public string[] nationality { get; set; }
        public string foreignFirstName { get; set; }
        public string foreignLastName { get; set; }
        public string dateOfBirth { get; set; }
        public string comments { get; set; }
    }
}