﻿namespace OTAWebAppWeb
{
    public class Hearing
    {
        public string[] attachmentIds { get; set; }
        public string messageId { get; set; }
        public string outlookToken { get; set; }
        public string outlookRestUrl { get; set; }
        public string url { get; set; }
        public string userKey { get; set; }
        public string matter { get; set; }
        public string stageId { get; set; }
        public string types { get; set; }
        public string ddate { get; set; }
        public string ddateTime { get; set; }
        public string postponedUntilDate { get; set; }
        public string postponedUntilTime { get; set; }
        public string reasonsOfPostponement { get; set; }
        public string[] assigneeResults { get; set; }
        public string comments { get; set; }
        public string summary { get; set; }
        public string chkbxJudgment { get; set; }
        public string judgment { get; set; }
        public string stageStatus { get; set; }
        public string judgmentValue { get; set; }
        

    }
}