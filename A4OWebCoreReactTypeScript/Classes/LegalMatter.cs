﻿namespace OTAWebAppWeb
{
    public class LegalMatter
    {
        public string matterType { get; set; }
        public string[] attachmentIds { get; set; }
        public string messageId { get; set; }
        public string accessToken { get; set; }
        public string isEmailIncluded { get; set; }
        public string url { get; set; }
        public string userKey { get; set; }
        public string subject { get; set; }
        public string casedescription { get; set; }
        public string casetype { get; set; } // Area of practice
        public string contactcompany { get; set; }
        public string clientname { get; set; }
        public string assignedteam { get; set; }
        public string assignedmember { get; set; }
        public string arrivaldate { get; set; }
        public string filedon { get; set; }
        public string duedate { get; set; }
        public string fileref { get; set; }
        public string casepriority { get; set; }
        public string caseStageSelect { get; set; }
        public string requestedby { get; set; }
        public string referredBy { get; set; }
        public string casevalue { get; set; }
        public string estimatedeffort { get; set; }
        public string cbprivate { get; set; }
        public string[] sharedwith { get; set; }
    }
}