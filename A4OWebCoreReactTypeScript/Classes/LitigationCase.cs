﻿namespace OTAWebAppWeb
{
    public class LitigationCase
    {
        public string[] attachmentIds { get; set; }
        public string messageId { get; set; }
        public string outlookToken { get; set; }
        public string outlookRestUrl { get; set; }
        public string url { get; set; }
        public string userKey { get; set; }
        public string subject { get; set; }
        public string casedescription { get; set; }
        public string casetype { get; set; } // Area of practice
        public string contactcompany { get; set; }
        public string clientname { get; set; }
        public string clientposition { get; set; }
        public string[] oppnenet { get; set; }
        public string[] opponentname { get; set; }
        public string assignedteam { get; set; }
        public string assignedmember { get; set; }
        public string arrivaldate { get; set; }
        public string filedon { get; set; }
        public string duedate { get; set; }
        public string fileref { get; set; }
        public string casepriority { get; set; }
        public string caseStageSelect { get; set; }
        public string requestedby { get; set; }
        public string casevalue { get; set; }
        public string estimatedeffort { get; set; }
        public string cbprivate { get; set; }
        public string[] sharedwith { get; set; }
    }
}