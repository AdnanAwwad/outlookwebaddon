﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OTAWebAppWeb.Classes
{
    public class UserInfo
    {
        public string url { get; set; }
        public string token { get; set; }
    }
}