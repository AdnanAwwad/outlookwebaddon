﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using OTAWebAppWeb;
using OTAWebAppWeb.Classes;
using A4OWebCoreReactWebTs.Services;
using Microsoft.Graph;
using System.Web;
using System.Net;
using System.Text;
using A4OWebCoreReactWebTs.Classes;
using Microsoft.AspNetCore.Http;

namespace A4OWebCoreReactWebTs.Controllers
{
    public class PostDataController : Controller
    {
        private readonly IHttpClientFactory _httpClientFactory;
        public PostDataController(IHttpClientFactory httpClientFactory)
        {
            _httpClientFactory = httpClientFactory;
        }
        [HttpPost]
        public async Task<IActionResult> AddLitigationCase([FromBody] LitigationCase request)
        {
            string userKey = string.Empty;
            string url = string.Empty;
            string jsonRespo = string.Empty;
            string cbPrivate = string.Empty;
            string dataToPost = string.Empty;
            string opponentname = string.Empty;
            if (request != null)
            {
                userKey = request.userKey;
                url = request.url;
            }
            if (!string.IsNullOrEmpty(userKey) && !string.IsNullOrEmpty(url))
            {
                string fullUrl = url + @"modules/api/cases/add_litigation";
                string[] array1 = request.oppnenet;
                string[] array2 = request.opponentname;
                if (array1.Length > 0 && array2.Length > 0 && array1.Length == array2.Length)
                {
                    for (int i = 0; i < array1.Length; i++)
                    {
                        if (!string.IsNullOrEmpty(array1[i]) && !string.IsNullOrEmpty(array2[i]))
                            opponentname += "&opponent_member_type[" + i + "]=" + array1[i] + "&opponent_member_id[" + i + "]=" + array2[i];
                    }
                }
                string privateArray = string.Empty;
                if (request.cbprivate != null)
                {
                    if (request.cbprivate.ToLower() == "true")
                    {
                        cbPrivate = "private=yes";

                        if (request.sharedwith != null)
                        {
                            for (int i = 0; i < request.sharedwith.Length; i++)
                            {
                                privateArray += "&Legal_Case_Watchers_Users[" + i + "]=" + request.sharedwith[i];
                            }
                        }
                    }
                }
                dataToPost = "subject=" + request.subject + "&description=" + request.casedescription +
                "&case_type_id=" + request.casetype +
                "&provider_group_id=" + request.assignedteam +
                "&priority=" + request.casepriority.ToLower() +
                "&lawSuit=no&externalizeLawyers=no&archived=no&legal_case_stage_id=" + request.caseStageSelect +
                "&assignedTo=" + request.assignedmember +
                "&caseArrivalDate=" + request.arrivaldate +
                "&arrivalDate=" + request.filedon + "&internalReference=" + request.fileref +
                "&" + cbPrivate + privateArray +
                "&dueDate=" + request.duedate +
                "&requestedBy=" + request.requestedby +
                "&estimated_effort=" + request.estimatedeffort +
                "&clientType=" + request.contactcompany +
                "&contact_company_id=" + request.contactcompany + opponentname +
                "&caseValue=" + request.casevalue +
                "&legal_case_client_position_id=" + request.clientposition;

                var client = _httpClientFactory.CreateClient();
                var data = new StringContent(dataToPost, Encoding.UTF8, "application/x-www-form-urlencoded");
                client.DefaultRequestHeaders.Add("x-api-channel", "outlook");
                client.DefaultRequestHeaders.Add("x-api-key", userKey);
                var response = await client.PostAsync(fullUrl, data);

                jsonRespo = await Web.sendRequestToPostData(fullUrl, dataToPost, userKey);

            }

            string baseAttachmentsUri = request.outlookRestUrl;
            if (!baseAttachmentsUri.EndsWith("/"))
                baseAttachmentsUri += "/";
            baseAttachmentsUri += "v2.0/me/messages/" + request.messageId + "/attachments/";
            try
            {
                using (var client = new HttpClient())
                {
                    UploadFile[] allFiles = new UploadFile[request.attachmentIds.Length];
                    for (int i = 0; i < request.attachmentIds.Length; i++)
                    {
                        var getAttachmentReq = new HttpRequestMessage(HttpMethod.Get, baseAttachmentsUri + request.attachmentIds[i]);
                        getAttachmentReq.Headers.Authorization = new AuthenticationHeaderValue("Bearer", request.outlookToken);
                        getAttachmentReq.Headers.UserAgent.Add(new ProductInfoHeaderValue("OTAOutlookAddin", "1.0"));
                        getAttachmentReq.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                        var result = await client.SendAsync(getAttachmentReq);
                        string json = await result.Content.ReadAsStringAsync();
                        OutlookAttachment attachment = JsonConvert.DeserializeObject<OutlookAttachment>(json);
                        if (attachment.Type.ToLower().Contains("itemattachment"))
                        {
                            var getAttachedItemJsonReq = new HttpRequestMessage(HttpMethod.Get, baseAttachmentsUri +
                                request.attachmentIds[i] + "?$expand=Microsoft.OutlookServices.ItemAttachment/Item");
                            getAttachedItemJsonReq.Headers.Authorization = new AuthenticationHeaderValue("Bearer", request.outlookToken);
                            getAttachedItemJsonReq.Headers.UserAgent.Add(new ProductInfoHeaderValue("AttachmentsDemoOutlookAddin", "1.0"));
                            getAttachedItemJsonReq.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                            var getAttachedItemResult = await client.SendAsync(getAttachedItemJsonReq);
                            Stream jsonAttachedItem = await getAttachedItemResult.Content.ReadAsStreamAsync();
                        }
                        else
                        {
                            if (attachment.Size < (4 * 1024 * 1024))
                            {
                                MemoryStream fileStream = new MemoryStream(Convert.FromBase64String(attachment.ContentBytes));
                                allFiles[i] = new UploadFile(fileStream, attachment.Name, attachment.ContentType);
                            }
                            else
                            {
                            }
                        }
                    }
                    
                    errors data = JsonConvert.DeserializeObject<errors>(jsonRespo);
                    if (data.error.Equals(""))
                    {
                        string result = Web.serializeJson(jsonRespo);
                        Success res = JsonConvert.DeserializeObject<Success>(result);
                        string caseId = res.data.case_id.Remove(0, 1);
                        bool success = false;
                        //= await UploadFileToApp4legal(request.url + @"modules/api/cases/attachment_add", allFiles, "legal_case_id=" + caseId, request.userKey);
                        if (!success)
                        {
                            return BadRequest(string.Format("Could not save {0} to Attachment ", "attachment.Name"));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
            }
            return Ok(jsonRespo);
        }
        [HttpPost]
        public async Task<IActionResult> UploadFileToApp4legal([FromForm]AttachmentsModel attachmentsModel)
        {
            try
            {
                var files = attachmentsModel.Files;
                UploadFile[] allFiles = new UploadFile[files != null ? files.Count : 0];
                if (files != null)
                {
                    if (files.Count > 0)
                    {
                        for (int i = 0; i < files.Count; i++)
                        {
                            try
                            {
                                var fileObject = JsonConvert.DeserializeObject<FileObject>(files[i]);
                                var file = fileObject.File;
                                var totalSize = file.Length;
                                var fileBytes = new byte[file.Length];
                                if (file.Length < (50 * 1024 * 1024))
                                {
                                    var base64String = fileObject.Data.Substring(fileObject.Data.IndexOf(',') + 1);
                                    MemoryStream memoryStream = new MemoryStream(Convert.FromBase64String(base64String));
                                    allFiles[i] = new UploadFile(memoryStream, file.FileName, file.ContentType, file.isEmail);
                                }
                                else
                                {
                                    return BadRequest("File is too large for simple upload.");
                                }
                            }
                            catch (Exception ex)
                            {

                            }
                        }
                    }
                }
                NameValueCollection form = new NameValueCollection();
                string[] ArrayForSeparatePOSTData = attachmentsModel.DataToPost?.Split('&');
                for (int j = 0; j < ArrayForSeparatePOSTData.Length; j++)
                {
                    string[] ArrayForSeparateKeyANDValueForEachData = ArrayForSeparatePOSTData[j].Split('=');
                    if (ArrayForSeparateKeyANDValueForEachData.Length == 2)
                    {
                        form[ArrayForSeparateKeyANDValueForEachData[0].Trim().Replace("\"", string.Empty).Replace("+", string.Empty)] = ArrayForSeparateKeyANDValueForEachData[1].Trim().Replace("\"", string.Empty).Replace("+", string.Empty);
                    }
                }
                var responseJson = await HttpUploadHelper.Upload(attachmentsModel.Url, allFiles, form, attachmentsModel.UserKey);
                return Content(responseJson);
            }
            catch (Exception)
            {
                return BadRequest("Error");
            }            
        }

        private async Task<string> addMatterNoteWithAttach(string urlApp4Legal, UploadFile[] files, string dataToPOST, string userKey)
        {
            string responseJson = string.Empty;
            try
            {
                NameValueCollection form = new NameValueCollection();
                string[] ArrayForSeparatePOSTData = dataToPOST.Split('&');
                for (int j = 0; j < ArrayForSeparatePOSTData.Length; j++)
                {
                    string[] ArrayForSeparateKeyANDValueForEachData = ArrayForSeparatePOSTData[j].Split('=');

                    if (ArrayForSeparateKeyANDValueForEachData.Length == 2)
                    {
                        form[ArrayForSeparateKeyANDValueForEachData[0].Trim().Replace("\"", string.Empty).Replace("+", string.Empty)] = ArrayForSeparateKeyANDValueForEachData[1].Trim().Replace("\"", string.Empty).Replace("+", string.Empty);
                    }
                }
                responseJson = await HttpUploadHelper.Upload(urlApp4Legal, files, form, userKey);
            }
            catch (Exception)
            {
            }
            return responseJson;
        }
        [HttpPost]
        public async Task<string> UploadExternalAttachment()
        {
            //try
            //{
            //    var blob = HttpContext.Request.Form["blob"];
            //    UserData FormDataRequest = new UserData();
            //    var provider = new MultipartMemoryStreamProvider();
            //    await Request.Content.ReadAsMultipartAsync(provider);
            //    UploadFile[] allFiles = new UploadFile[provider.Contents.Count - 1];
            //    if (Request.Content.IsMimeMultipartContent())
            //    {
            //        for (int i = 0; i < provider.Contents.Count; i++)
            //        {
            //            var requestJson = await provider.Contents[i].ReadAsStringAsync();
            //            try
            //            {
            //                FormDataRequest = JsonConvert.DeserializeObject<UserData>(requestJson);
            //            }
            //            catch (Exception)
            //            {
            //                var fileNameParam = provider.Contents[i].Headers.ContentDisposition.Parameters.FirstOrDefault(p => p.Name.ToLower() == "filename");
            //                var fileType = provider.Contents[i].Headers.ContentType.ToString();
            //                string type = string.Empty;
            //                if (fileType != null)
            //                    type = fileType;
            //                else
            //                    type = "image/png";
            //                string fileName = (fileNameParam == null) ? "" : fileNameParam.Value.Trim('"');
            //                byte[] file = await provider.Contents[i].ReadAsByteArrayAsync();
            //                MemoryStream fileStream = new MemoryStream(file);
            //                allFiles[i] = new UploadFile(fileStream, fileName, type);
            //            }
            //        }
            //    }
            //    string caseId = "";
            //    if (!FormDataRequest.isContract)
            //        caseId = FormDataRequest.caseId.Remove(0, 1);
            //    else
            //        caseId = FormDataRequest.caseId;
            //    string caseOrContractUrl = FormDataRequest.isContract ? "/documents/contract_upload_file" : "/cases/attachment_add";
            //    string caseOrContractValue = FormDataRequest.isContract ? "module_record_id=" : "legal_case_id=";
            //    bool success = await UploadFileToApp4legal(FormDataRequest.url + @"modules/api" + caseOrContractUrl, allFiles, caseOrContractValue + caseId, FormDataRequest.userKey);
            //}
            //catch (Exception ex)
            //{
            //}
            return "";
        }

        [HttpPost]
        public async Task<string> UploadHearingAttachment()
        {
            //try
            //{
            //    UserData FormDataRequest = new UserData();
            //    var provider = new MultipartMemoryStreamProvider();
            //    await Request.Content.ReadAsMultipartAsync(provider);
            //    UploadFile[] allFiles = new UploadFile[provider.Contents.Count - 1];
            //    if (Request.Content.IsMimeMultipartContent())
            //    {
            //        for (int i = 0; i < provider.Contents.Count; i++)
            //        {
            //            var requestJson = await provider.Contents[i].ReadAsStringAsync();
            //            try
            //            {
            //                FormDataRequest = JsonConvert.DeserializeObject<UserData>(requestJson);
            //            }
            //            catch (Exception)
            //            {
            //                var fileNameParam = provider.Contents[i].Headers.ContentDisposition.Parameters.FirstOrDefault(p => p.Name.ToLower() == "filename");
            //                var fileType = provider.Contents[i].Headers.ContentType.ToString();
            //                string type = string.Empty;
            //                if (fileType != null)
            //                    type = fileType;
            //                else
            //                    type = "image/png";
            //                string fileName = (fileNameParam == null) ? "" : fileNameParam.Value.Trim('"');
            //                byte[] file = await provider.Contents[i].ReadAsByteArrayAsync();
            //                MemoryStream fileStream = new MemoryStream(file);
            //                allFiles[i] = new UploadFile(fileStream, fileName, type);
            //            }
            //        }
            //    }
            //    string caseId = FormDataRequest.caseId;
            //    bool success = await UploadFileToApp4legal(FormDataRequest.url + @"modules/api/cases/attachment_add", allFiles, "legal_case_id=" + caseId, FormDataRequest.userKey);
            //}
            //catch (Exception ex)
            //{
            //}
            return "";
        }

        [HttpPost]
        public async Task<IActionResult> addNewMeeting([FromBody] Meeting request)
        {
            string userKey = string.Empty;
            string url = string.Empty;
            string jsonRespo = string.Empty;
            string cbPrivate = string.Empty;
            string dataToPost = string.Empty;
            string opponentname = string.Empty;
            if (request != null)
            {
                userKey = request.userKey;
                url = request.url;
            }
            string fullUrl = url + @"modules/api/calendars/add/";
            if (!string.IsNullOrEmpty(userKey) && !string.IsNullOrEmpty(url))
            {
                string privateArray = string.Empty;
                if (request.privateVal != null)
                {
                    if (request.privateVal.ToLower() == "true")
                    {
                        cbPrivate = "private=yes";
                    }
                }
                if (request.attendees != null)
                {
                    for (int i = 0; i < request.attendees.Length; i++)
                    {
                        privateArray += "&attendees[" + i + "]=" + request.attendees[i];
                    }
                }
                dataToPost = "title=" + request.title +
                    "&start_date=" + request.from +
                    "&start_time=" + request.fromTime +
                    "&end_date=" + request.to +
                    "&end_time=" + request.toTime +
                    "&priority=" + request.priority.ToLower() +
                    "&legal_case_id=" + request.relatedMatter +
                    "&description=" + request.description +
                    "&task_location_id=" + request.location +
                   "&" + cbPrivate + "&" + privateArray;

                jsonRespo = await Web.sendRequestToPostData(fullUrl, dataToPost, userKey);
            }

            return Ok(jsonRespo);
        }

        [HttpPost]
        public async Task<IActionResult> AddMatter([FromBody] LegalMatter request)
        {
            string userKey = string.Empty;
            string url = string.Empty;
            string jsonRespo = string.Empty;
            string cbPrivate = string.Empty;
            string dataToPost = "";
            if (request != null)
            {
                userKey = request.userKey;
                url = request.url;
            }
            if (!string.IsNullOrEmpty(userKey) && !string.IsNullOrEmpty(url))
            {
                string fullUrl = url + @"modules/api/cases/add_legal_matter/";
                string privateArray = string.Empty;
                if (request.cbprivate != null)
                {
                    if (request.cbprivate.ToLower() == "true")
                    {
                        cbPrivate = "private=yes";
                        if (request.sharedwith != null)
                        {
                            for (int i = 0; i < request.sharedwith.Length; i++)
                            {
                                privateArray += "&Legal_Case_Watchers_Users[" + i + "]=" + request.sharedwith[i];
                            }
                        }
                    }
                    else
                    {
                        cbPrivate = "private=no";
                    }
                }
                if (request.subject != null)
                    dataToPost += "subject=" + request.subject + "&lawSuit=no&externalizeLawyers=no&archived=no";
                if (request.casetype != null)
                    dataToPost += "&case_type_id=" + request.casetype;
                if (!string.IsNullOrEmpty(request.casedescription))                    
                    dataToPost += "&description=" + HttpUtility.UrlEncode(request.casedescription);
                if (request.assignedteam != null)
                    dataToPost += "&provider_group_id=" + request.assignedteam;
                if (request.assignedmember != null)
                    dataToPost += "&assignedTo=" + request.assignedmember;
                if (request.fileref != null)
                    dataToPost += "&internalReference=" + request.fileref;
                if (request.referredBy != null)
                    dataToPost += "&referredBy=" + request.referredBy;
                if (request.requestedby != null)
                    dataToPost += "&requestedBy=" + request.requestedby;
                if (request.contactcompany != null)
                    dataToPost += "&clientType=" + request.contactcompany;
                if (request.clientname != null)
                    dataToPost += "&contact_company_id=" + request.clientname;
                if (request.caseStageSelect != null)
                    dataToPost += "&legal_case_stage_id=" + request.caseStageSelect;
                if (request.casepriority != null)
                    dataToPost += "&priority=" + request.casepriority.ToLower();
                if (request.arrivaldate != null)
                    dataToPost += "&caseArrivalDate=" + request.arrivaldate;
                if (request.filedon != null)
                    dataToPost += "&arrivalDate=" + request.filedon;
                if (request.duedate != null)
                    dataToPost += "&dueDate=" + request.duedate;
                if (request.casevalue != null)
                    dataToPost += "&caseValue=" + request.casevalue;
                if (request.estimatedeffort != null)
                    dataToPost += "&estimatedEffort=" + request.estimatedeffort;
                if (privateArray.Length > 0)
                    dataToPost += privateArray;

                try
                {
                    var client = _httpClientFactory.CreateClient();
                    var data = new StringContent(dataToPost, Encoding.UTF8, "application/x-www-form-urlencoded");
                    client.DefaultRequestHeaders.Add("x-api-channel", "outlook");
                    client.DefaultRequestHeaders.Add("x-api-key", userKey);
                    var response = await client.PostAsync(fullUrl, data);
                }
                catch(Exception ex)
                {

                }

                jsonRespo = await Web.sendRequestToPostData(fullUrl, dataToPost, userKey);
            }
            try
            {                
                var attachments = await GraphService.GetEmailAttachments(request.messageId, request.accessToken);
                if(attachments?.Count > 0)
                {
                    using var client = new HttpClient();
                    List<UploadFile> allFiles = new List<UploadFile>();
                    foreach (var attachment in attachments)
                    {
                        try
                        {
                            if (attachment is FileAttachment)
                            {
                                FileAttachment file = (FileAttachment)attachment;
                                if (file.Size < (4 * 1024 * 1024))
                                {
                                    MemoryStream fileStream = new MemoryStream(file.ContentBytes);
                                    allFiles.Add(new UploadFile(fileStream, file.Name, file.ContentType));
                                }
                                else
                                {
                                    return BadRequest("File is too large for simple upload.");
                                }
                            }
                        }
                        catch (Exception ex)
                        {

                        }
                    }
                    UploadFile[] convertedFiles = allFiles.ToArray();

                    //errors data = JsonConvert.DeserializeObject<errors>(jsonRespo);
                    if (true)
                    {
                        //string result = Web.serializeJson(jsonRespo);
                        //Success res = JsonConvert.DeserializeObject<Success>(result);
                        //string caseId = res.data.case_id.Remove(0, 1);
                        bool success = false;
                        //= await UploadFileToApp4legal(request.url + @"modules/api/cases/attachment_add", convertedFiles, "legal_case_id=" + "6012", request.userKey);
                        if (!success)
                        {
                            return BadRequest(string.Format("Could not save {0} to Attachment ", "attachment.Name"));
                        }
                    }
                }
                
            }
            catch (Exception ex)
            {
            }
            return Ok(jsonRespo);
        }

        [HttpPost]
        public async Task<IActionResult> addContract([FromBody] OTAWebAppWeb.Classes.Contract request)
        {
            string userKey = string.Empty;
            string url = string.Empty;
            string jsonRespo = string.Empty;
            string cbPrivate = string.Empty;
            string dataToPost = string.Empty;
            if (request != null)
            {
                userKey = request.userKey;
                url = request.url;
            }
            if (!string.IsNullOrEmpty(userKey) && !string.IsNullOrEmpty(url))
            {
                string fullUrl = url + @"modules/api/contracts/add";
                string ContractName = "";
                string ContractType = "";
                string AssignedTeam = "";
                string AssignedMember = "";
                string Requester = "";
                string Description = "";
                string Priority = "";
                string ContractDate = "";
                string StartDate = "";
                string EndDate = "";
                string ReferenceNumber = "";
                string Renewal = "";
                string Currency = "";
                string Value = "";
                string Contributors = "";

                if (!string.IsNullOrEmpty(request.name) && !string.IsNullOrWhiteSpace(request.name))
                    ContractName = "name=" + request.name;
                if (!string.IsNullOrEmpty(request.contractType))
                    ContractType = "&type_id=" + request.contractType;
                if (!string.IsNullOrEmpty(request.requester))
                    Requester = "&requester_id=" + request.requester;
                if (!string.IsNullOrEmpty(request.description))
                    Description = "&description=" + request.description;
                if (!string.IsNullOrEmpty(request.priority))
                    Priority = "&priority=" + request.priority.ToLower();
                if (!string.IsNullOrEmpty(request.assignedteam))
                    AssignedTeam = "&assigned_team_id=" + request.assignedteam;
                if (!string.IsNullOrEmpty(request.assignedmember))
                    AssignedMember = "&assignee_id=" + request.assignedmember;
                if (!string.IsNullOrEmpty(request.contractDate))
                    ContractDate = "&contract_date=" + request.contractDate;
                if (!string.IsNullOrEmpty(request.startDate))
                    StartDate = "&start_date=" + request.startDate;
                if (!string.IsNullOrEmpty(request.endDate))
                    EndDate = "&end_date=" + request.endDate;
                if (!string.IsNullOrEmpty(request.referenceNumber))
                    ReferenceNumber = "&reference_number=" + request.referenceNumber;
                if (!string.IsNullOrEmpty(request.renewal))
                    Renewal = "&renewal_type=" + request.renewal;
                if (!string.IsNullOrEmpty(request.currency))
                    Currency = "&currency_id=" + request.currency;
                if (!string.IsNullOrEmpty(request.value))
                    Value = "&value=" + request.value;

                if (request.contributors != null)
                {
                    for (int i = 0; i < request.contributors.Length; i++)
                    {

                        if (!string.IsNullOrEmpty(request.contributors[i]))
                            Contributors += "&contributor_id[" + i + "]=" + request.contributors[i];
                    }
                }
                dataToPost = ContractName + ContractType + Requester + Description + Priority +
                     "&workflow_id=1&status=Active&status_id=1" + AssignedTeam + AssignedMember + ContractDate +
                      StartDate + EndDate + ReferenceNumber + Renewal + Currency + Value + Contributors + request.parties;

                jsonRespo = await Web.sendRequestToPostData(fullUrl, dataToPost, userKey);
                try
                {
                    string baseAttachmentsUri = request.outlookRestUrl;
                    if (!baseAttachmentsUri.EndsWith("/"))
                        baseAttachmentsUri += "/";
                    baseAttachmentsUri += "v2.0/me/messages/" + request.messageId + "/attachments/";

                    using (var client = new HttpClient())
                    {
                        UploadFile[] allFiles = new UploadFile[request.attachmentIds.Length];
                        for (int i = 0; i < request.attachmentIds.Length; i++)
                        {
                            var getAttachmentReq = new HttpRequestMessage(HttpMethod.Get, baseAttachmentsUri + request.attachmentIds[i]);
                            getAttachmentReq.Headers.Authorization = new AuthenticationHeaderValue("Bearer", request.outlookToken);
                            getAttachmentReq.Headers.UserAgent.Add(new ProductInfoHeaderValue("OTAOutlookAddin", "1.0"));
                            getAttachmentReq.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                            var result = await client.SendAsync(getAttachmentReq);
                            string json = await result.Content.ReadAsStringAsync();
                            OutlookAttachment attachment = JsonConvert.DeserializeObject<OutlookAttachment>(json);
                            if (attachment.Type.ToLower().Contains("itemattachment"))
                            {
                                var getAttachedItemJsonReq = new HttpRequestMessage(HttpMethod.Get, baseAttachmentsUri +
                                    request.attachmentIds[i] + "?$expand=Microsoft.OutlookServices.ItemAttachment/Item");
                                getAttachedItemJsonReq.Headers.Authorization = new AuthenticationHeaderValue("Bearer", request.outlookToken);
                                getAttachedItemJsonReq.Headers.UserAgent.Add(new ProductInfoHeaderValue("AttachmentsDemoOutlookAddin", "1.0"));
                                getAttachedItemJsonReq.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                                var getAttachedItemResult = await client.SendAsync(getAttachedItemJsonReq);
                                Stream jsonAttachedItem = await getAttachedItemResult.Content.ReadAsStreamAsync();
                            }
                            else
                            {
                                if (attachment.Size < (4 * 1024 * 1024))
                                {
                                    MemoryStream fileStream = new MemoryStream(Convert.FromBase64String(attachment.ContentBytes));
                                    allFiles[i] = new UploadFile(fileStream, attachment.Name, attachment.ContentType);
                                }
                                else
                                {
                                    return BadRequest("File is too large for simple upload.");
                                }
                            }
                        }
                        
                        errors data = JsonConvert.DeserializeObject<errors>(jsonRespo);
                        if (data.error.Equals(""))
                        {
                            string result = Web.serializeJson(jsonRespo);
                            Success res = JsonConvert.DeserializeObject<Success>(result);
                            string contractId = res.data.contract_id;
                            bool success = false;
                            //= await UploadFileToApp4legal(request.url + @"modules/api/documents/contract_upload_file", allFiles, "module_record_id=" + contractId, request.userKey);
                            if (!success)
                            {
                                return BadRequest(string.Format("Could not save {0} to Attachment ", "attachment.Name"));
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                }
                return Ok(jsonRespo);
            }
            return BadRequest("Unauthorized");

        }
        [HttpPost]
        public async Task<IActionResult> addMatterNote([FromBody] MatterNote request)
        {
            string userKey = string.Empty;
            string url = string.Empty;
            string jsonRespo = string.Empty;
            string customerPortal = string.Empty;
            string dataToPost = string.Empty;
            EmailProps mailProps = null;
            UploadFile[] allFiles = new UploadFile[request.attachmentIds.Length];
            if (request != null)
            {
                userKey = request.userKey;
                url = request.url;
                mailProps = JsonConvert.DeserializeObject<EmailProps>(request.mailProps);
            }
            if (!string.IsNullOrEmpty(userKey) && !string.IsNullOrEmpty(url) && mailProps != null)
            {
                if (request.customerPortal.ToLower() == "true")
                    customerPortal = "&isVisibleToCP=yes";
                else
                    customerPortal = "&isVisibleToCP=no";
                dataToPost = "comment=" + request.note;
                dataToPost += customerPortal;
                dataToPost += "&case_id=" + request.matter;
                var toEmails = mailProps.to;
                if (toEmails?.Length > 255)
                    toEmails = toEmails.Substring(0, 254);
                dataToPost += "&email_to=" + toEmails;
                dataToPost += "&email_from=" + mailProps.from;
                dataToPost += "&email_from_name =" + mailProps.fromName;
                dataToPost += "&email_date=" + reformatDate(mailProps.date);
                dataToPost += "&email_subject=" + mailProps.subject;
            }
            string baseAttachmentsUri = request.outlookRestUrl;
            if (!baseAttachmentsUri.EndsWith("/"))
                baseAttachmentsUri += "/";
            baseAttachmentsUri += "v2.0/me/messages/" + request.messageId + "/attachments/";
            try
            {
                using (var client = new HttpClient())
                {
                    for (int i = 0; i < request.attachmentIds.Length; i++)
                    {
                        var getAttachmentReq = new HttpRequestMessage(HttpMethod.Get, baseAttachmentsUri + request.attachmentIds[i]);
                        getAttachmentReq.Headers.Authorization = new AuthenticationHeaderValue("Bearer", request.outlookToken);
                        getAttachmentReq.Headers.UserAgent.Add(new ProductInfoHeaderValue("OTAOutlookAddin", "1.0"));
                        getAttachmentReq.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                        var result = await client.SendAsync(getAttachmentReq);
                        string json = await result.Content.ReadAsStringAsync();
                        OutlookAttachment attachment = JsonConvert.DeserializeObject<OutlookAttachment>(json);
                        if (attachment.Type.ToLower().Contains("itemattachment"))
                        {
                            var getAttachedItemJsonReq = new HttpRequestMessage(HttpMethod.Get, baseAttachmentsUri +
                                request.attachmentIds[i] + "?$expand=Microsoft.OutlookServices.ItemAttachment/Item");
                            getAttachedItemJsonReq.Headers.Authorization = new AuthenticationHeaderValue("Bearer", request.outlookToken);
                            getAttachedItemJsonReq.Headers.UserAgent.Add(new ProductInfoHeaderValue("AttachmentsDemoOutlookAddin", "1.0"));
                            getAttachedItemJsonReq.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                            var getAttachedItemResult = await client.SendAsync(getAttachedItemJsonReq);
                            Stream jsonAttachedItem = await getAttachedItemResult.Content.ReadAsStreamAsync();
                        }
                        else
                        {
                            if (attachment.Size < (4 * 1024 * 1024))
                            {
                                MemoryStream fileStream = new MemoryStream(Convert.FromBase64String(attachment.ContentBytes));
                                allFiles[i] = new UploadFile(fileStream, attachment.Name, attachment.ContentType);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
            }
            string caseId = request.matter;
            jsonRespo = await addMatterNoteWithAttach(request.url + @"modules/api/cases/email_note_add", allFiles, dataToPost, request.userKey);
            return Ok(jsonRespo);
        }
        [HttpPost]
        public async Task<IActionResult> AddIntellectualProperty([FromBody] IntellectualProperty request)
        {
            string userKey = string.Empty;
            string url = string.Empty;
            string jsonRespo = string.Empty;
            string cbPrivate = string.Empty;
            string dataToPost = string.Empty;
            string opponentname = string.Empty;
            if (request != null)
            {
                userKey = request.userKey;
                url = request.url;
            }
            if (!string.IsNullOrEmpty(userKey) && !string.IsNullOrEmpty(url))
            {
                string fullUrl = url + @"modules/api/intellectual_properties/add";
                dataToPost = "intellectual_property_right_id=" + request.ipRights;
                if (!string.IsNullOrEmpty(request.iPClass))
                    dataToPost = dataToPost + "&ip_class_id=" + request.iPClass;
                if (!string.IsNullOrEmpty(request.iPSubcategory))
                    dataToPost = dataToPost + "&ip_subcategory_id=" + request.iPSubcategory;
                if (!string.IsNullOrEmpty(request.iPName))
                    dataToPost = dataToPost + "&ip_name_id=" + request.iPName;
                if (!string.IsNullOrEmpty(request.subject))
                    dataToPost = dataToPost + "&subject=" + request.subject;
                if (!string.IsNullOrEmpty(request.description))
                    dataToPost = dataToPost + "&description=" + request.description;
                if (!string.IsNullOrEmpty(request.clientCompanyGroup))
                    dataToPost = dataToPost + "&clientType=" + request.clientCompanyGroup;
                if (!string.IsNullOrEmpty(request.clientName))
                    dataToPost = dataToPost + "&contact_company_id=" + request.clientName;
                if (!string.IsNullOrEmpty(request.country))
                    dataToPost = dataToPost + "&country_id=" + request.country;
                if (!string.IsNullOrEmpty(request.assignedteam))
                    dataToPost = dataToPost + "&provider_group_id=" + request.assignedteam;
                if (!string.IsNullOrEmpty(request.assignedmember))
                    dataToPost = dataToPost + "&user_id=" + request.assignedmember;
                if (!string.IsNullOrEmpty(request.filedon))
                    dataToPost = dataToPost + "&arrivalDate=" + request.filedon;
                if (!string.IsNullOrEmpty(request.registrationDate))
                    dataToPost = dataToPost + "&registrationDate=" + request.registrationDate;
                if (!string.IsNullOrEmpty(request.registrationRef))
                    dataToPost = dataToPost + "&registrationReference=" + request.registrationRef;
                if (!string.IsNullOrEmpty(request.agentCompanyGroup))
                    dataToPost = dataToPost + "&agentType=" + request.agentCompanyGroup;
                if (!string.IsNullOrEmpty(request.agent))
                    dataToPost = dataToPost + "&agentId=" + request.agent;
                if (!string.IsNullOrEmpty(request.filingNumber))
                    dataToPost = dataToPost + "&filingNumber=" + request.filingNumber;
                jsonRespo = await Web.sendRequestToPostData(fullUrl, dataToPost, userKey);
            }

            string baseAttachmentsUri = request.outlookRestUrl;
            if (!baseAttachmentsUri.EndsWith("/"))
                baseAttachmentsUri += "/";
            baseAttachmentsUri += "v2.0/me/messages/" + request.messageId + "/attachments/";
            try
            {
                using (var client = new HttpClient())
                {
                    UploadFile[] allFiles = new UploadFile[request.attachmentIds.Length];
                    for (int i = 0; i < request.attachmentIds.Length; i++)
                    {
                        var getAttachmentReq = new HttpRequestMessage(HttpMethod.Get, baseAttachmentsUri + request.attachmentIds[i]);
                        getAttachmentReq.Headers.Authorization = new AuthenticationHeaderValue("Bearer", request.outlookToken);
                        getAttachmentReq.Headers.UserAgent.Add(new ProductInfoHeaderValue("OTAOutlookAddin", "1.0"));
                        getAttachmentReq.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                        var result = await client.SendAsync(getAttachmentReq);
                        string json = await result.Content.ReadAsStringAsync();
                        OutlookAttachment attachment = JsonConvert.DeserializeObject<OutlookAttachment>(json);

                        if (attachment.Type.ToLower().Contains("itemattachment"))
                        {
                            var getAttachedItemJsonReq = new HttpRequestMessage(HttpMethod.Get, baseAttachmentsUri +
                                request.attachmentIds[i] + "?$expand=Microsoft.OutlookServices.ItemAttachment/Item");
                            getAttachedItemJsonReq.Headers.Authorization = new AuthenticationHeaderValue("Bearer", request.outlookToken);
                            getAttachedItemJsonReq.Headers.UserAgent.Add(new ProductInfoHeaderValue("AttachmentsDemoOutlookAddin", "1.0"));
                            getAttachedItemJsonReq.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                            var getAttachedItemResult = await client.SendAsync(getAttachedItemJsonReq);
                            Stream jsonAttachedItem = await getAttachedItemResult.Content.ReadAsStreamAsync();
                        }
                        else
                        {
                            if (attachment.Size < (4 * 1024 * 1024))
                            {
                                MemoryStream fileStream = new MemoryStream(Convert.FromBase64String(attachment.ContentBytes));
                                allFiles[i] = new UploadFile(fileStream, attachment.Name, attachment.ContentType);
                            }
                            else
                            {
                            }
                        }
                    }
                    
                    errors data = JsonConvert.DeserializeObject<errors>(jsonRespo);
                    if (data.error.Equals(""))
                    {
                        string result = Web.serializeJson(jsonRespo);
                        addedIP res = JsonConvert.DeserializeObject<addedIP>(result);
                        string caseId = string.Empty;
                        if (Char.IsLetter(res.data.id.FirstOrDefault()))
                            caseId = res.data.id.Remove(0, 1);
                        else
                            caseId = res.data.id;
                        bool success = false;
                        //await UploadFileToApp4legal(request.url + @"modules/api/cases/attachment_add", allFiles, "legal_case_id=" + caseId, request.userKey);
                    }
                }
            }
            catch (Exception ex)
            {
            }
            return Ok(jsonRespo);
        }
        [HttpPost]
        public async Task<IActionResult> AddNewHearing([FromBody] Hearing request)
        {
            string userKey = string.Empty;
            string url = string.Empty;
            string jsonRespo = string.Empty;
            string dataToPost = string.Empty;
            string matter = string.Empty;
            string types = string.Empty;
            string ddate = string.Empty;
            string ddateTime = string.Empty;
            string postponedUntilDate = string.Empty;
            string postponedUntilTime = string.Empty;
            string reasonsOfPostponement = string.Empty;
            string assigneeResults = string.Empty;
            string comments = string.Empty;
            string summary = string.Empty;
            string chkbxJudgment = string.Empty;
            string judgment = string.Empty;
            string stageStatus = string.Empty;
            string stageId = string.Empty;
            if (!string.IsNullOrEmpty(request.matter) && !string.IsNullOrWhiteSpace(request.matter))
                matter = "&legal_case_id=" + request.matter;
            if (!string.IsNullOrEmpty(request.stageId) && !string.IsNullOrWhiteSpace(request.stageId))
                stageId = "&stage=" + request.stageId;
            if (!string.IsNullOrEmpty(request.types) && !string.IsNullOrWhiteSpace(request.types))
                types = "&type=" + request.types;
            if (!string.IsNullOrEmpty(request.ddate) && !string.IsNullOrWhiteSpace(request.ddate))
                ddate = "&startDate=" + request.ddate;
            if (!string.IsNullOrEmpty(request.ddateTime) && !string.IsNullOrWhiteSpace(request.ddateTime))
                ddateTime = "&startTime=" + request.ddateTime;
            if (!string.IsNullOrEmpty(request.postponedUntilDate) && !string.IsNullOrWhiteSpace(request.postponedUntilDate))
                postponedUntilDate = "&postponedDate=" + request.postponedUntilDate;
            if (!string.IsNullOrEmpty(request.postponedUntilTime) && !string.IsNullOrWhiteSpace(request.postponedUntilTime))
                postponedUntilTime = "&postponedTime=" + request.postponedUntilTime;
            if (!string.IsNullOrEmpty(request.reasonsOfPostponement) && !string.IsNullOrWhiteSpace(request.reasonsOfPostponement))
                reasonsOfPostponement = "&reasons_of_postponement=" + request.reasonsOfPostponement;
            if (!string.IsNullOrEmpty(request.comments) && !string.IsNullOrWhiteSpace(request.comments))
                comments = "&judgment=" + request.comments;
            if (!string.IsNullOrEmpty(request.summary) && !string.IsNullOrWhiteSpace(request.summary))
                summary = "&summary=" + request.summary;
            chkbxJudgment = "&judged=" + request.chkbxJudgment;
            if (chkbxJudgment != null)
            { // if judged is null no need to send 
                if (!string.IsNullOrEmpty(request.judgment) && !string.IsNullOrWhiteSpace(request.judgment))
                    judgment = "&judgment=" + request.judgment;
                if (!string.IsNullOrEmpty(request.stageStatus) && !string.IsNullOrWhiteSpace(request.stageStatus))
                    stageStatus = "&stage_status=" + request.stageStatus;
                if (!string.IsNullOrEmpty(request.judgmentValue) && !string.IsNullOrWhiteSpace(request.judgmentValue))
                    stageStatus = "&judgmentValue=" + request.judgmentValue;
            }
            if (request != null)
            {
                userKey = request.userKey;
                url = request.url;
            }
            if (!string.IsNullOrEmpty(userKey) && !string.IsNullOrEmpty(url))
            {
                string fullUrl = url + @"modules/api/hearings/add";
                if (request.assigneeResults != null)
                {
                    if (request.assigneeResults.Length > 0)
                    {
                        for (int i = 0; i < request.assigneeResults.Length; i++)
                        {
                            assigneeResults += "&Hearing_Lawyers[" + i + "]=" + request.assigneeResults[i];
                        }
                    }
                }
                dataToPost = matter + stageId + types + ddate + ddateTime + postponedUntilDate + postponedUntilTime + reasonsOfPostponement +
                             assigneeResults + comments + summary + chkbxJudgment + judgment + stageStatus;
                jsonRespo = await Web.sendRequestToPostData(fullUrl, dataToPost, userKey);
                try
                {
                    string baseAttachmentsUri = request.outlookRestUrl;
                    if (!baseAttachmentsUri.EndsWith("/"))
                        baseAttachmentsUri += "/";
                    baseAttachmentsUri += "v2.0/me/messages/" + request.messageId + "/attachments/";

                    using (var client = new HttpClient())
                    {
                        UploadFile[] allFiles = new UploadFile[request.attachmentIds.Length];
                        for (int i = 0; i < request.attachmentIds.Length; i++)
                        {
                            var getAttachmentReq = new HttpRequestMessage(HttpMethod.Get, baseAttachmentsUri + request.attachmentIds[i]);
                            getAttachmentReq.Headers.Authorization = new AuthenticationHeaderValue("Bearer", request.outlookToken);
                            getAttachmentReq.Headers.UserAgent.Add(new ProductInfoHeaderValue("OTAOutlookAddin", "1.0"));
                            getAttachmentReq.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                            var result = await client.SendAsync(getAttachmentReq);
                            string json = await result.Content.ReadAsStringAsync();
                            OutlookAttachment attachment = JsonConvert.DeserializeObject<OutlookAttachment>(json);
                            if (attachment.Type.ToLower().Contains("itemattachment"))
                            {
                                var getAttachedItemJsonReq = new HttpRequestMessage(HttpMethod.Get, baseAttachmentsUri +
                                    request.attachmentIds[i] + "?$expand=Microsoft.OutlookServices.ItemAttachment/Item");
                                getAttachedItemJsonReq.Headers.Authorization = new AuthenticationHeaderValue("Bearer", request.outlookToken);
                                getAttachedItemJsonReq.Headers.UserAgent.Add(new ProductInfoHeaderValue("AttachmentsDemoOutlookAddin", "1.0"));
                                getAttachedItemJsonReq.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                                var getAttachedItemResult = await client.SendAsync(getAttachedItemJsonReq);
                                Stream jsonAttachedItem = await getAttachedItemResult.Content.ReadAsStreamAsync();
                            }
                            else
                            {
                                if (attachment.Size < (4 * 1024 * 1024))
                                {
                                    MemoryStream fileStream = new MemoryStream(Convert.FromBase64String(attachment.ContentBytes));
                                    allFiles[i] = new UploadFile(fileStream, attachment.Name, attachment.ContentType);
                                }
                                else
                                {
                                    return BadRequest("File is too large for simple upload.");
                                }
                            }
                        }
                        
                        errors data = JsonConvert.DeserializeObject<errors>(jsonRespo);
                        if (data.error.Equals(""))
                        {
                            string result = Web.serializeJson(jsonRespo);
                            Success res = JsonConvert.DeserializeObject<Success>(result);
                            string caseId = request.matter;
                            bool success = false;
                            //= await UploadFileToApp4legal(request.url + @"modules/api/cases/attachment_add", allFiles, "legal_case_id=" + caseId, request.userKey);
                            if (!success)
                            {
                                //return BadRequest(string.Format("Could not save {0} to Attachment ", "attachment.Name"));
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    string errorMsg = ex.Message.ToString();
                    string stacktraceMessage = ex.StackTrace.ToString();
                }
            }
            return Ok(jsonRespo);
        }
        [HttpPost]
        public async Task<IActionResult> AddNewTask([FromBody] NewTask request)
        {
            string userKey = string.Empty;
            string url = string.Empty;
            string jsonRespo = string.Empty;
            string cbPrivate = string.Empty;
            string dataToPost = string.Empty;
            if (request != null)
            {
                userKey = request.userKey;
                url = request.url;
            }
            if (!string.IsNullOrEmpty(userKey) && !string.IsNullOrEmpty(url))
            {
                string fullUrl = url + @"modules/api/tasks/add";
                string privateArray = string.Empty;
                if (request.cbprivate != null)
                {
                    if (request.cbprivate.ToLower() == "true")
                    {
                        if (request.sharedwith != null)
                        {
                            if (request.sharedwith.Length > 0)
                            {
                                cbPrivate = "&private=yes";
                                for (int i = 0; i < request.sharedwith.Length; i++)
                                {
                                    privateArray += "&Task_Users[" + i + "]=" + request.sharedwith[i];
                                }
                            }
                        }
                    }
                }
                string description = string.Empty;
                string stageId = string.Empty;
                if (!string.IsNullOrEmpty(request.stageId) && !string.IsNullOrWhiteSpace(request.stageId))
                    stageId = "&stage=" + request.stageId;
                if (!string.IsNullOrEmpty(request.description) && !string.IsNullOrWhiteSpace(request.description))
                    description = "description=" + request.description;
                string relatedMatter = string.Empty;
                if (!string.IsNullOrEmpty(request.relatedMatter) && !string.IsNullOrWhiteSpace(request.relatedMatter))
                    relatedMatter = "&legal_case_id=" + request.relatedMatter;
                string priority = string.Empty;
                if (!string.IsNullOrEmpty(request.priority) && !string.IsNullOrWhiteSpace(request.priority))
                    priority = "&priority=" + request.priority.ToLower();
                string reporter = string.Empty;
                if (!string.IsNullOrEmpty(request.reporter) && !string.IsNullOrWhiteSpace(request.reporter))
                    reporter = "&reporter=" + request.reporter;
                string assignee = string.Empty;
                if (!string.IsNullOrEmpty(request.assignee) && !string.IsNullOrWhiteSpace(request.assignee))
                    assignee = "&assigned_to=" + request.assignee;
                string dueDate = string.Empty;
                if (!string.IsNullOrEmpty(request.dueDate) && !string.IsNullOrWhiteSpace(request.dueDate))
                    dueDate = "&due_date=" + request.dueDate;
                string estimatedEffort = string.Empty;
                if (!string.IsNullOrEmpty(request.estEffort) && !string.IsNullOrWhiteSpace(request.estEffort))
                    estimatedEffort = "&estimated_effort=" + request.estEffort;
                string location = string.Empty;
                if (!string.IsNullOrEmpty(request.location) && !string.IsNullOrWhiteSpace(request.location))
                    location = "&task_location_id=" + request.location;
                string status = string.Empty;
                if (!string.IsNullOrEmpty(request.status) && !string.IsNullOrWhiteSpace(request.status))
                    status = "&task_status_id=" + request.status;
                string taskType = string.Empty;
                if (!string.IsNullOrEmpty(request.taskType) && !string.IsNullOrWhiteSpace(request.taskType))
                    taskType = "&task_type_id=" + request.taskType;
                dataToPost = description + relatedMatter + stageId + priority + reporter + assignee + dueDate + estimatedEffort + location + status + taskType + cbPrivate + privateArray;
                jsonRespo = await Web.sendRequestToPostData(fullUrl, dataToPost, userKey);
            }
            return Ok(jsonRespo);
        }

        [HttpPost]
        public async Task<IActionResult> AddNewContact([FromBody] OTAWebAppWeb.Contact request)
        {
            string userKey = string.Empty;
            string url = string.Empty;
            string jsonRespo = string.Empty;
            string cbPrivate = string.Empty;
            string dataToPost = string.Empty;
            string companyGroup = string.Empty;
            string isLawyer = string.Empty;
            string inHouseLawyer = string.Empty;
            string nationality = string.Empty;
            string addressOne = string.Empty;
            string addressTwo = string.Empty;
            string city = string.Empty;
            string state = string.Empty;
            string country = string.Empty;
            string zip = string.Empty;
            string comment = string.Empty;

            if (request != null)
            {
                userKey = request.userKey;
                url = request.url;
            }
            if (!string.IsNullOrEmpty(userKey) && !string.IsNullOrEmpty(url))
            {
                string fullUrl = url + @"modules/api/contacts/add/";

                string SharedWith = string.Empty;
                if (request.cbPrivate != null)
                {
                    if (request.cbPrivate.ToLower() == "true")
                    {
                        cbPrivate = "&private=yes";
                        if (request.sharedwith != null)
                        {
                            for (int i = 0; i < request.sharedwith.Length; i++)
                            {
                                SharedWith += "&Contact_Users[" + i + "]=" + request.sharedwith[i];
                            }
                        }
                    }
                    else
                    {
                        cbPrivate = "&private=no";
                    }
                }
                if (request.companyGroupResults != null)
                {
                    for (int i = 0; i < request.companyGroupResults.Length; i++)
                    {
                        companyGroup += "&companies_contacts[" + i + "]=" + request.companyGroupResults[i];
                    }
                }
                if (request.isLawyer.ToLower() == "true" || request.isLawyer.ToLower() == "on")
                    isLawyer = "&isLawyer=yes";
                else
                    isLawyer = "&isLawyer=no";
                if (request.inHouseLawyer.ToLower() == "true" || request.inHouseLawyer.ToLower() == "on")
                    inHouseLawyer = "&lawyerForCompany=yes";
                else
                    inHouseLawyer = "&lawyerForCompany=no";
                if (request.nationality != null)
                {
                    for (int i = 0; i < request.nationality.Length; i++)
                    {
                        nationality += "&Contact_Nationalities[" + i + "]=" + request.nationality[i];
                    }
                }

                string title = string.Empty;
                if (!string.IsNullOrEmpty(request.title) && !string.IsNullOrWhiteSpace(request.title))
                    title = "title_id=" + request.title;
                string gender = string.Empty;
                if (!string.IsNullOrEmpty(request.gender) && !string.IsNullOrWhiteSpace(request.gender))
                    gender = "&gender=" + request.gender;
                string category = string.Empty;
                if (!string.IsNullOrEmpty(request.category) && !string.IsNullOrWhiteSpace(request.category))
                    category = "&contact_category_id=" + request.category;

                //subCategory
                string subCategory = string.Empty;
                if (!string.IsNullOrEmpty(request.subCategory) && !string.IsNullOrWhiteSpace(request.subCategory))
                    subCategory = "&contact_sub_category_id=" + request.subCategory;

                string firstName = string.Empty;
                if (!string.IsNullOrEmpty(request.firstName) && !string.IsNullOrWhiteSpace(request.firstName))
                    firstName = "&firstName=" + request.firstName;
                string lastName = string.Empty;
                if (!string.IsNullOrEmpty(request.lastName) && !string.IsNullOrWhiteSpace(request.lastName))
                    lastName = "&lastName=" + request.lastName;
                string middleName = string.Empty;
                if (!string.IsNullOrEmpty(request.middleName) && !string.IsNullOrWhiteSpace(request.middleName))
                    middleName = "&father=" + request.middleName;
                string motherName = string.Empty;
                if (!string.IsNullOrEmpty(request.motherName) && !string.IsNullOrWhiteSpace(request.motherName))
                    motherName = "&mother=" + request.motherName;
                string referenceNumber = string.Empty;
                if (!string.IsNullOrEmpty(request.referenceNumber) && !string.IsNullOrWhiteSpace(request.referenceNumber))
                    referenceNumber = "&internalReference=" + request.referenceNumber;
                string foreignFirstName = string.Empty;
                if (!string.IsNullOrEmpty(request.foreignFirstName) && !string.IsNullOrWhiteSpace(request.foreignFirstName))
                    foreignFirstName = "&foreignFirstName=" + request.foreignFirstName;
                string foreignLastName = string.Empty;
                if (!string.IsNullOrEmpty(request.foreignLastName) && !string.IsNullOrWhiteSpace(request.foreignLastName))
                    foreignLastName = "&foreignLastName=" + request.foreignLastName;

                if (!string.IsNullOrEmpty(request.addressOne) && !string.IsNullOrWhiteSpace(request.addressOne))
                    addressOne = "&address1=" + request.addressOne;
                if (!string.IsNullOrEmpty(request.addressTwo))
                    addressTwo = "&address2=" + request.addressTwo;
                if (!string.IsNullOrEmpty(request.city))
                    city = "&city=" + request.city;
                if (!string.IsNullOrEmpty(request.state))
                    state = "&state=" + request.state;
                if (!string.IsNullOrEmpty(request.country))
                    country = "&country_id=" + request.country;
                if (!string.IsNullOrEmpty(request.zip))
                    zip = "&zip=" + request.zip;
                if (!string.IsNullOrEmpty(request.comments))
                    comment = "&comments=" + request.comments;
                dataToPost = title + gender + category + subCategory + firstName + lastName + isLawyer + inHouseLawyer + middleName + motherName + referenceNumber +
                      foreignFirstName + foreignLastName +
                        "&dateOfBirth=" + request.dateOfBirth + "&phone=" + request.phone + "&jobTitle=" + request.jobTitle + "&fax=" + request.fax +
                        "&email=" + request.email + "&mobile=" + request.mobile + "&website=" + request.website + companyGroup + SharedWith + addressOne + addressTwo + city + state + country + zip
                        + nationality + cbPrivate + comment;
                jsonRespo = await Web.sendRequestToPostData(fullUrl, dataToPost, userKey);
            }
            return Ok(jsonRespo);
        }
        [HttpPost]
        public async Task<IActionResult> AddNewCompany([FromBody] Company request)
        {
            string userKey = string.Empty;
            string url = string.Empty;
            string jsonRespo = string.Empty;
            string cbPrivate = string.Empty;
            string dataToPost = string.Empty;
            if (request != null)
            {
                userKey = request.userKey;
                url = request.url;
            }
            if (!string.IsNullOrEmpty(userKey) && !string.IsNullOrEmpty(url))
            {
                string fullUrl = url + @"modules/api/companies/add/";
                string SharedWith = string.Empty;
                if (request.cbPrivate != null)
                {
                    if (request.cbPrivate.ToLower() == "true")
                    {
                        if (request.sharedWith != null)
                        {
                            cbPrivate = "&private=yes";
                            for (int i = 0; i < request.sharedWith.Length; i++)
                            {
                                SharedWith += "&Company_Users[" + i + "]=" + request.sharedWith[i];
                            }
                        }
                    }
                    else
                    {
                        cbPrivate = "&private=no";
                    }
                }
                string name = string.Empty;
                if (!string.IsNullOrEmpty(request.name) && !string.IsNullOrWhiteSpace(request.name))
                    name = "name=" + request.name;
                string shortName = string.Empty;
                if (!string.IsNullOrEmpty(request.shortName) && !string.IsNullOrWhiteSpace(request.shortName))
                    shortName = "&shortName=" + request.shortName;
                string companyGroup = string.Empty;
                if (!string.IsNullOrEmpty(request.companyGroup) && !string.IsNullOrWhiteSpace(request.companyGroup))
                    companyGroup = "&company_id=" + request.companyGroup;
                string category = string.Empty;
                if (!string.IsNullOrEmpty(request.category) && !string.IsNullOrWhiteSpace(request.category))
                    category = "&company_category_id=" + request.category;
                string subCategory = string.Empty;
                if (!string.IsNullOrEmpty(request.subCategory) && !string.IsNullOrWhiteSpace(request.subCategory))
                    subCategory = "&company_sub_category_id=" + request.subCategory;
                string referenceNumber = string.Empty;
                if (!string.IsNullOrEmpty(request.referenceNumber) && !string.IsNullOrWhiteSpace(request.referenceNumber))
                    referenceNumber = "&internalReference=" + request.referenceNumber;
                string nationality = string.Empty;
                if (!string.IsNullOrEmpty(request.nationality) && !string.IsNullOrWhiteSpace(request.nationality))
                    nationality = "&nationality_id=" + request.nationality;
                string companyLegalType = string.Empty;
                if (!string.IsNullOrEmpty(request.companyLegalType) && !string.IsNullOrWhiteSpace(request.companyLegalType))
                    companyLegalType = "&company_legal_type_id=" + request.companyLegalType;
                string objectVal = string.Empty;
                if (!string.IsNullOrEmpty(request.objectVal) && !string.IsNullOrWhiteSpace(request.objectVal))
                    objectVal = "&object=" + request.objectVal;
                string capital = string.Empty;
                if (!string.IsNullOrEmpty(request.capital) && !string.IsNullOrWhiteSpace(request.capital))
                    capital = "&capital=" + request.capital;
                string currency = string.Empty;
                if (!string.IsNullOrEmpty(request.currency) && !string.IsNullOrWhiteSpace(request.currency))
                    currency = "&capitalCurrency=" + request.currency;
                string commercialRegReleaseDate = string.Empty;
                if (!string.IsNullOrEmpty(request.commercialRegReleaseDate) && !string.IsNullOrWhiteSpace(request.commercialRegReleaseDate))
                    commercialRegReleaseDate = "&registrationDate=" + request.commercialRegReleaseDate;
                string commercialRegNb = string.Empty;
                if (!string.IsNullOrEmpty(request.commercialRegNb) && !string.IsNullOrWhiteSpace(request.commercialRegNb))
                    commercialRegNb = "&registrationNb=" + request.commercialRegNb;
                string comments = string.Empty;
                if (!string.IsNullOrEmpty(request.comments) && !string.IsNullOrWhiteSpace(request.comments))
                    comments = "&comments=" + request.comments;
                dataToPost = name + "&category=Internal" + shortName + category + subCategory + "&status=Active" + companyGroup + referenceNumber + nationality + companyLegalType + objectVal + request.addresses + capital + currency +
                commercialRegReleaseDate + commercialRegNb + comments + SharedWith + cbPrivate;
                jsonRespo = await Web.sendRequestToPostData(fullUrl, dataToPost, userKey);
            }
            return Ok(jsonRespo);
        }
        [HttpPost]
        public async Task<IActionResult> postAnyData([FromBody] AnyDataClass request)
        {
            string userKey = string.Empty;
            string url = string.Empty;
            string jsonRespo = string.Empty;
            string dataToPost = string.Empty;

            if (request != null)
            {
                userKey = request.userKey;
                url = request.url;
            }
            string fullUrl = url;
            if (!string.IsNullOrEmpty(userKey) && !string.IsNullOrEmpty(url))
            {
                jsonRespo = await Web.sendRequestToPostData(fullUrl, dataToPost, userKey);
            }

            return Ok(jsonRespo);
        }
        [HttpPost]
        public async Task<IActionResult> getUserInfo([FromBody] UserInfo request)
        {
            string response = string.Empty;
            string dataToPost = string.Empty;
            response = await Web.sendRequestToPostData(request.url + @"modules/api/users/user_info", "lang=english", request.token);
            return Ok(response);
        }
        string reformatDate(string dateAsString)
        {
            string newDate = string.Empty;
            try
            {
                DateTime date = Convert.ToDateTime(dateAsString);
                newDate = date.ToString("yyyy-MM-dd HH:mm:ss");
            }
            catch (Exception ex)
            {
                return dateAsString;
            }
            return newDate;
        }
    }
}
