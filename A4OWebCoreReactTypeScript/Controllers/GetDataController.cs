﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using OTAWebAppWeb;

namespace A4OWebCoreReactWebTs.Controllers
{
    public class GetDataController : Controller
    {
        [HttpGet]
        public string GetDataAny(string url, string userKey)
        {
            string completeUrl = url;
            string response = string.Empty;
            response = Web.sendRequestToGETtData(completeUrl, userKey);
            return response;
        }
        [HttpGet]
        public async Task<ActionResult> GetDataAny(string url, string userKey, string dataToPost)
        {
            string completeUrl = url;
            string response = string.Empty;
            response = await Web.sendRequestToPostData(completeUrl, dataToPost, userKey);
            return Ok(response);
        }
        [HttpGet]
        public async Task<ActionResult> loadHearingData(string url, string userKey, string userId, string caseId)
        {
            string dataToPost = "user_id=" + userId + "&key=" + userKey + "&isOutlookAddIn=1&caseId=" + caseId;
            string completeUrl = url;
            string response = string.Empty;
            response = await Web.sendRequestToPostData(completeUrl, dataToPost, userKey);
            return Ok(response);
        }
        [HttpGet]
        public async Task<ActionResult> autoCompleteHearing(string url, string userKey, string userId)
        {
            string dataToPost = "user_id=" + userId + "&key=" + userKey + "&isOutlookAddIn=1"; ;
            string completeUrl = url;
            string response = string.Empty;
            response = await Web.sendRequestToPostData(completeUrl, dataToPost, userKey);
            return Ok(response);
        }
        [HttpGet]
        public async Task<ActionResult> checkCredentials(string url, string email, string password)
        {
            string response = string.Empty;
            string dataToPost = string.Empty;
            dataToPost = "email=" + email + "&password=" + password + "&lang=english" + "&userLogin=" + email;
            response = await Web.login(url + @"modules/api/users/login/", dataToPost);
            return Ok(response);
        }
        [HttpGet]
        public async Task<ActionResult> cloudLogin(string isCloud, string url, string email, string password, string loginMode)
        {
            string response = string.Empty;
            string dataToPost = string.Empty;
            string SelectedInstanceType = string.Empty;
            if (isCloud.ToLower() == "cloud")
            {
                dataToPost = "email=" + email + "&userLogin=" + password;
                response = await Web.login(url, dataToPost);
                if (response.Contains("error"))
                {
                    // errors data = js.Deserialize<errors>(response);
                }
                else
                {
                    Dictionary<string, string> instances = new Dictionary<string, string>();
                    string goToURL = string.Empty;
                    var dyn = JsonConvert.DeserializeObject<dynamic>(response);
                    var p = dyn["instances"];
                    var jObject = (JObject)p;
                    instances = JsonConvert.DeserializeObject<Dictionary<string, string>>(jObject.ToString());
                    goToURL = dyn["goToURL"];
                    if (instances.Count > 0)
                    {
                        if (instances.Count == 1)
                        {
                            string instanceId = instances.Keys.First();
                            string cloudInstanceUrl = goToURL + "/" + instanceId + "/";
                            if (loginMode != null)
                                if (loginMode.Equals("sso"))
                                {
                                    var instance = "{\"instance\":\"" + cloudInstanceUrl + "\"}";
                                    return Ok(instance);
                                }
                            string organizationName = instances.Values.First();
                            var responseDf = await checkCredentials(cloudInstanceUrl, email, password);
                            if (responseDf is OkObjectResult)
                            {
                                var result = responseDf as OkObjectResult;
                                string content = "";
                                JObject rss = JObject.Parse(content);
                                if (!string.IsNullOrEmpty(rss["error"].ToString()))
                                {
                                    return Ok(rss.ToString());
                                }
                                else
                                {
                                    JObject data = (JObject)rss["success"]["data"];
                                    data.Property("userId").AddAfterSelf(new JProperty("instanceUrl", cloudInstanceUrl));
                                    return Ok(rss.ToString());
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                dataToPost = "email=" + email + "&password=" + password + "&lang=english" + "&userLogin=" + email;
                response = await Web.login(url + @"modules/api/users/login/", dataToPost);
            }
            return Ok(response);
        }
    }
}
