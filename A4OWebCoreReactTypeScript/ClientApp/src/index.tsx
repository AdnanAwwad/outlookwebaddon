﻿/*
 * Copyright (c) Microsoft Corporation.
 * Licensed under the MIT license.
 */

/// <reference types="office-js" />
/// <reference types="node" />
/* global Office */  //Required for this to be found.  see: https://github.com/OfficeDev/office-js-docs-pr/issues/691
import 'react-app-polyfill/ie11';
import 'react-app-polyfill/stable';
import 'office-ui-fabric-react/dist/css/fabric.min.css';
import App from './components/App';
import { AppContainer } from 'react-hot-loader';
import { initializeIcons } from 'office-ui-fabric-react/lib/Icons';
import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import store from './redux/store';
import CustomThemeProvider from "./components/Theme/CustomThemeProvider";
import { Helpers } from './Helpers/Helpers';
import { FileObject } from 'material-ui-dropzone';


initializeIcons();


let isOfficeInitialized = false;

const title = 'App4Legal for Outlook';

const render = (Component, isUserLoggedIn) => {
    ReactDOM.render(
        <AppContainer>
            <Provider store={store}>
                <CustomThemeProvider>
                     <Component title={title} isOfficeInitialized={isOfficeInitialized} isUserLoggedIn={isUserLoggedIn} />
                </CustomThemeProvider>
            </Provider>
        </AppContainer>,
        document.getElementById('container')
    );
};

/* Render application after Office initializes */
Office.initialize = () => {
    var _settings;
    var _url;
    var _userKey;
    var userDataObj;
    isOfficeInitialized = true;
    userDataObj = Helpers.getTemplogginData();
    _settings = Office.context.roamingSettings;
    if (userDataObj == undefined || userDataObj == null) {
        _url = _settings.get("url");
        _userKey = _settings.get("userKey");
    } else {
        _url = userDataObj.url;
        _userKey = userDataObj.userKey;
    }
    if (_userKey && _userKey.length > 0 && _url && _url.length > 0) {
        render(App, true);
    }
    else {
        render(App, false);
    }
};

/* Initial render showing a progress bar */
render(App, false);

//if ((module as any).hot) {
//    (module as any).hot.accept('./components/App', () => {
//        const NextApp = require('./components/App').default;
//        render(NextApp, false);
//    });
//}
export interface ReduxState {
    settings: {
        language: string,
        direction: string,
        theme: string
    },
    alert: {
        open: boolean,
        severity: string,
        alertMessage: string
    },
    modal: {
        openModal: boolean
    },
    saving: {
        saving: boolean
    },
    loader: {
        runLoader: boolean
    },
    focus: {
        id: string
    },
    attachments: {
        files: IFileForm[],
        moreEmails: any[],
        includeEmail: boolean,
        includeEmailAttachments: boolean,
        hasEmailAttachments: boolean,
        fetchingAttachments: boolean
    }
}
interface IFileForm {
    fileObject: FileObject,
    isEmailAttachment: boolean,
    isInline: boolean,
}