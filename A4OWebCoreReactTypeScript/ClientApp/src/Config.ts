﻿export const config = {
    appId: '552d2989-096b-4848-b324-72d271d9602f',
    redirectUri: 'https://localhost:44355',
    scopes: [
        'user.read',
        'mailboxsettings.read',
        'calendars.readwrite',
        'Contacts.Read',
        'Mail.Read',
        'Mail.ReadWrite',
        'MailboxSettings.ReadWrite'
    ]
};