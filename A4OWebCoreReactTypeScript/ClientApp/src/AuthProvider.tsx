﻿import * as React from 'react';
import { PublicClientApplication, BrowserCacheLocation } from '@azure/msal-browser';
import * as Msal from 'msal';
import { config } from './Config';
import { getUserDetails } from './GraphService';
import createLog from 'localstorage-logger';

const log = createLog({
    logName: 'appLogs',
    maxLogSizeInBytes: 512 * 1024
});
export interface AuthComponentProps {
    error: any;
    isAuthenticated: boolean;
    user: any;
    login: Function;
    logout: Function;
    getAccessToken: Function;
    setError: Function;
    handleAuthorizationResponse: Function;
    initializing: boolean;
}
interface AuthProviderState {
    error: any;
    isAuthenticated: boolean;
    user: any;
    response: any;
    initializing: boolean;
}
let loginDialog: Office.Dialog;
export default function withAuthProvider<T extends React.Component<AuthComponentProps>>
    (WrappedComponent: new (props: AuthComponentProps, context?: any) => T): React.ComponentClass {
    return class extends React.Component<any, AuthProviderState> {
        private msalInstance: Msal.UserAgentApplication;
        private isRest: string;
        constructor(props: any) {
            super(props);
            this.state = {
                error: null,
                isAuthenticated: false,
                user: {},
                response: {},
                initializing: true
            };
            this.isRest = localStorage.getItem('isRest');
            this.handleResponse = this.handleResponse.bind(this);
            // Initialize the MSAL application object
            this.msalInstance = new Msal.UserAgentApplication({
                auth: {
                    clientId: config.appId,
                    redirectUri: config.redirectUri
                },
                cache: {
                    cacheLocation: BrowserCacheLocation.LocalStorage,
                    storeAuthStateInCookie: true
                }
            });
            this.msalInstance.handleRedirectCallback(response => {
                sessionStorage.setItem('isSigningIn', 'false');
                console.log(response);
            }, error => {
                    console.log(error);
            })
        }

        componentDidMount() {
            // If MSAL already has an account, the user
            // is already logged in
            Office.onReady(() => {
                try {
                    const mailType = Office?.context?.mailbox?.userProfile?.accountType;
                    if (this.isRest == null || mailType === 'gmail') {
                        if (mailType === 'gmail') {
                            localStorage.setItem('isRest', 'true');
                            this.isRest = 'true';
                            this.setState({
                                isAuthenticated: true,
                                error: null,
                                initializing: false
                            });
                        }
                        else {
                            localStorage.setItem('isRest', 'false');
                            this.isRest = 'false';
                            const accounts = this.msalInstance.getAccount();

                            if (accounts) {
                                // Enhance user object with data from Graph
                                this.getUserProfile();
                            }
                            else {
                                this.setState({ initializing: false });
                            }
                        }
                    }
                    else {
                        if (this.isRest === 'true') {
                            this.setState({
                                isAuthenticated: true,
                                error: null,
                                initializing: false
                            });
                        }
                        else {
                            const accounts = this.msalInstance.getAccount();
                            if (accounts) {
                                // Enhance user object with data from Graph
                                this.getUserProfile();
                            }
                            else {
                                this.setState({ initializing: false });
                            }
                        }
                    }
                }
                catch (err) {
                    console.log(err);
                    log.error('AuthProvider[componentDidMount]: ' + err);
                    localStorage.setItem('isRest', 'false');
                }
            });
        }

        render() {
            return <WrappedComponent
                error={this.state.error}
                isAuthenticated={this.state.isAuthenticated}
                user={this.state.user}
                login={() => this.login()}
                logout={() => this.logout()}
                getAccessToken={(scopes: string[]) => this.getAccessToken(scopes)}
                setError={(message: string, debug: string) => this.setErrorMessage(message, debug)}
                handleAuthorizationResponse={this.state.response}
                initializing={this.state.initializing}
                {...this.props} />;
        }
        handleResponse(resp) {
            if (resp !== null) {
                this.setState({ response: resp });
                sessionStorage.setItem('isSigningIn', 'false');
                localStorage.setItem('isRest', 'false');
                window.location.reload();
            } else {
            }
        }
        async login() {
            sessionStorage.setItem('isSigningIn', 'true');
            try {
                var self = this;
                const platform = Office?.context?.platform;
                switch (platform) {
                    case Office.PlatformType.iOS: case Office.PlatformType.Android: {
                        self.msalInstance.loginRedirect({
                            scopes: config.scopes,
                            prompt: 'select_account'
                        });
                    } break;
                    default: {
                        await self.msalInstance.loginPopup(
                            {
                                scopes: config.scopes,
                                prompt: "select_account"
                            }).then(self.handleResponse).catch(error => {
                                log.error('AuthProvider[login-loginPopup]: ' + JSON.stringify(error));
                                sessionStorage.setItem('isSigningIn', 'false');
                                self.setState({
                                    isAuthenticated: false,
                                    user: {},
                                    error: self.normalizeError('error')
                                });
                                window.location.reload();
                            });
                    } break;
                }
                
            }
            catch (err) {
                var self = this;
                log.error('AuthProvider[login-Outer]: ' + err);
            }
        }
        processLoginMessage = (arg) => {
            sessionStorage.setItem('isSigningIn', 'false');
            let messageFromDialog = JSON.parse(arg.message);
            if (messageFromDialog.status === 'success') {
                localStorage.setItem('isRest', 'false');
                // We now have a valid access token.
                loginDialog.close();                
            }
            else {
                // Something went wrong with authentication or the authorization of the web application.
                loginDialog.close();
            }
            window.location.reload();
        };

        processLoginDialogEvent = (arg) => {
            this.processDialogEvent(arg);
        };
        processDialogEvent = (arg: { error: number, type: string }) => {
            sessionStorage.setItem('isSigningIn', 'false');
            switch (arg.error) {
                case 12002:
                    log.warn('AuthProvider[ClosedPopUp]: The dialog box has been directed to a page that it cannot find or load, or the URL syntax is invalid.');
                    break;
                case 12003:
                    log.warn('AuthProvider[ClosedPopUp]:The dialog box has been directed to a URL with the HTTP protocol. HTTPS is required.');
                    break;
                case 12006:
                    // 12006 means that the user closed the dialog instead of waiting for it to close.
                    // It is not known if the user completed the login or logout, so assume the user is
                    // logged out and revert to the app's starting state. It does no harm for a user to
                    // press the login button again even if the user is logged in.
                    log.warn('AuthProvider[ClosedPopUp]: Closed by user');
                    window.location.reload();
                    break;
                default:
                    console.log('Unknown error in dialog box.');
                    break;
            }
        };
        logout() {
            //this.publicClientApplication.logoutPopup().then(response => {
            //    console.log(response);
            //}).catch(err => { console.error(err)});
            localStorage.clear();
            sessionStorage.clear();
        }

        async getAccessToken(scopes: string[]): Promise<string> {
            try {
                if (this.isRest === 'true') 
                    return null;
                const accounts = this.msalInstance
                    .getAccount();

                if (!accounts) throw new Error('login_required');
                // Get the access token silently
                // If the cache contains a non-expired token, this function
                // will just return the cached token. Otherwise, it will
                // make a request to the Azure OAuth endpoint to get a token
                var silentResult = await this.msalInstance
                    .acquireTokenSilent({
                        scopes: config.scopes
                    });

                return silentResult.accessToken;
            } catch (err) {
                // If a silent request fails, it may be because the user needs
                // to login or grant consent to one or more of the requested scopes
                if (err.name === "InteractionRequiredAuthError") {
                    var interactiveResult = await this.msalInstance
                        .acquireTokenPopup({
                            scopes: config.scopes
                        });

                    return interactiveResult.accessToken;
                } else {
                    throw err;
                }
            }
        }

        async getUserProfile() {
            try {
                var accessToken = await this.getAccessToken(config.scopes);
                if (accessToken) {
                    const isSigningIn = sessionStorage.getItem('isSigningIn');
                    if (isSigningIn === 'true')
                        sessionStorage.setItem('isSigningIn', 'false');
                    this.setState({
                        isAuthenticated: true,
                        error: null,
                        initializing: false
                    });
                }
            }
            catch (err) {
                this.setState({
                    isAuthenticated: false,
                    user: {},
                    error: this.normalizeError(err),
                    initializing: false
                });
            }
        }

        setErrorMessage(message: string, debug: string) {
            this.setState({
                error: { message: message, debug: debug }
            });
        }

        normalizeError(error: string | Error): any {
            var normalizedError = {};
            if (typeof (error) === 'string') {
                var errParts = error.split('|');
                normalizedError = errParts.length > 1 ?
                    { message: errParts[1], debug: errParts[0] } :
                    { message: error };
            } else {
                normalizedError = {
                    message: error.message,
                    debug: JSON.stringify(error)
                };
            }
            return normalizedError;
        }

        isInteractionRequired(error: Error): boolean {
            if (!error.message || error.message.length <= 0) {
                return false;
            }

            return (
                error.message.indexOf('consent_required') > -1 ||
                error.message.indexOf('interaction_required') > -1 ||
                error.message.indexOf('login_required') > -1 ||
                error.message.indexOf('no_account_in_silent_request') > -1
            );
        }
    }
}