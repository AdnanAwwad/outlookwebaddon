﻿
export class References {
    static signInCloudApi = "https://www.app4legal.com/app4legal-cc/public/app/signin/";
    static loginApi = "modules/api/users/login/";
    static refreshTokenApi = "modules/api/users/refresh_api_key";
    static userCheckApi = "modules/api/users/check_api";
    static clauseAddApi = "modules/api/clauses/add";
    static addCompanyApi = "modules/api/companies/add";
    static addCorporateMatterApi = "modules/api/cases/add_legal_matter";
    static addLitigationCaseApi = "modules/api/cases/add_litigation";
    static companiesAutoCompleteApi = "modules/api/companies/autocomplete";
    static addContactApi = "modules/api/contacts/add";
    static companyAutoCompleteApi = "modules/api/companies/list_companies";
    static clauseEditApi = "modules/api/clauses/edit/"
    static addContractApi = "modules/api/contracts/add";
    static loadHearingApi = "modules/api/hearings/load_data";
    static loadTaskApi = "modules/api/tasks/load_data/";
    static taskLocationAutoCompleteApi = "modules/api/task_locations/autocomplete";
    static uploadTaskAttachmentsApi = 'modules/api/tasks/upload_file';
    static moveStatusApi = "modules/api/cases/move_status";
    static addScreenTransitionApi = "modules/api/cases/add_screen_transition";
    static editMatterSingleFieldApi = "modules/api/cases/edit_single";
    static addTaskApi = "modules/api/tasks/add";
    static addMeetingApi = "modules/api/calendars/add/";
    static addIntellectualPropertyApi = "modules/api/intellectual_properties/add";
    static addHearingApi = "modules/api/hearings/add";
    static userInfoApi = "modules/api/users/user_info";
    static usersLookupApi = "modules/api/auth_users/autocomplete";
    static contactLookupApi = "modules/api/contacts/autocomplete";
    static clientsLookupApi = "modules/api/clients/autocomplete";
    static clauseLookupApi = "modules/api/clauses/lookup";
    static contractLookupApi = "modules/api/contracts/autocomplete";
    static casesLookupApi = "modules/api/cases/autocomplete";
    static addMatterApi = "modules/api/cases/add_legal_matter";
    static editMatterApi = "modules/api/cases/edit";
    static addEmailMatterNoteApi = 'modules/api/cases/email_note_add';
    static uploadAttachmentApi = "modules/api/documents/case_upload_file";
    static uploadMatterAttachmentsApi = "modules/api/cases/attachment_add";
    static loadDocumentApi = "modules/api/documents/docs_load_documents/";
    static uploadDocsApi = "modules/api/documents/docs_upload_file";
    static contractViewLink = "modules/contract/contracts/view/";
    static clauseViewLink = "modules/contract/clauses/";
    static uploadContractAttachmentsApi = "modules/api/documents/contract_upload_file";
    static licenseApi = "modules/outlook/license/checkAvailability";
    static checkApi = 'modules/api/users/check_api';
    static productVersion = "modules/api/users/productVersion";
};