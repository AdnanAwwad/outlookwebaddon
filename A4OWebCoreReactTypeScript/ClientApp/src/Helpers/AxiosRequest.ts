﻿/// <reference types="office-js" />

import axios, { AxiosResponse } from 'axios';
import { References } from './References';
import store from '../redux/store';
import createLog from 'localstorage-logger';
import { errorAlert } from '../redux/actions';
const log = createLog({
    logName: 'appLogs',
    maxLogSizeInBytes: 512 * 1024
});
export class AxiosRequest {
    static get = async (api, noHeaders = null) => {
        const reqHeader = new RequestHeaders();
        let url = reqHeader.url;
        if (!url) {
            return { status: null };
        }
        const header = reqHeader.headers(false, noHeaders);
        let fullURL = url + api;
        let response = await axios.get(fullURL, {
            headers: header
        });
        if (response.status === 200) {
            if (response.data?.error === "") {
            } else if (response.data?.error !== "") {
                if (response.data?.error?.code) {
                    if (response.data?.error?.code === "refresh_key_required") {
                        let res = await AxiosRequest.refreshToken();
                        if (res) {
                            return await AxiosRequest.get(api);
                        }
                    }
                    else {
                        if (response.data?.error?.code === "unauthorized") {
                            var path = window.location.pathname.replace('/', '');
                            var from = 'from=' + path;
                            window.location.href = 'login?' + from;
                        }
                    }
                }
            }
        }
        return response;
    };
    static post = async (api, dataToPost): Promise<AxiosResponse> => {
        const reqHeader = new RequestHeaders();
        let url = reqHeader.url;
        if (!url) {
            return null;
        }
        const header = reqHeader.headers();
        let fullURL = url + api;
        let response: AxiosResponse;
        try {
         response = await axios.post(fullURL, dataToPost, {
            headers: header
        });
        if (response.status === 200) {
            if (response.data?.error === "") {                
            } else if (response.data?.error != "") {
                if (response.data?.error?.code) {
                    if (response.data?.error?.code === "refresh_key_required") {
                        let res = await AxiosRequest.refreshToken();
                        if (res) {
                            return await AxiosRequest.post(api, dataToPost);
                        }
                    }
                    else {
                        if (response.data?.error?.code === "unauthorized") {
                            var path = window.location.pathname.replace('/', '');
                            var from = 'from=' + path;
                            window.location.href = 'login?' + from;
                        }
                    }
                }
            }
            }
        } catch (e) {
            log.error("Error post request - MS Outlook Addin:" + JSON.stringify(e.message));
            response = { ...response, status: null };
        }
        return response ;
    };
    static postToServer = async (url, dataToPost, token = null): Promise<AxiosResponse> => {
        const header = {
            'content-type': 'application/json',
            'Authorization': 'Bearer ' + token
        };
        let response;
        try {
            response = await axios.post(url, dataToPost, {
                headers: header
            });
            
        } catch (e) {
            log.error("Error postToServer request - MS Outlook Addin:" + JSON.stringify(e.message));
            response = { status: null };
        }
        return response;
    };
    static getFromServer = async (url, token = null): Promise<AxiosResponse> => {
        const header = {
            'content-type': 'application/json',
            'Authorization': 'Bearer ' + token
        };
        let response;
        try {
            response = await axios.get(url, {
                headers: header
            });

        } catch (e) {
            log.error("Error getFromServer request - MS Outlook Addin:" + JSON.stringify(e.message));
            response = { status: null };
        }
        return response;
    };
    static patchToServer = async (url, dataToPost, token = null): Promise<AxiosResponse> => {
        const header = {
            'Content-Type': 'application/json;charset=utf-8',
            'Authorization': 'Bearer ' + token
        };
        let response;
        try {
            response = await axios.patch(url, dataToPost, {
                headers: header
            });

        } catch (e) {
            log.error("Error patchToServer request - MS Outlook Addin:" + JSON.stringify(e.message));
            response = { status: null };
        }
        return response;
    };
    static upload = async (api, dataToPost) => {
        const reqHeader = new RequestHeaders();
        let url = reqHeader.url;
        if (!url) {
            return { status: null };
        }
        const header = reqHeader.uploadHeaders();
        let fullURL = url + api;
        let response = await axios.post(fullURL, dataToPost, {
            headers: header
        });
        if (response.status === 200) {
            if (response.data.error === "") {
            } else if (response.data.error != "") {
                if (response.data.error.code) {
                    if (response.data.error.code === "refresh_key_required") {
                        let res = await AxiosRequest.refreshToken();
                        if (res) {
                           return await AxiosRequest.upload(api, dataToPost);
                        }
                    }
                    else {
                        if (response.data.error.code === "unauthorized") {
                            var path = window.location.pathname.replace('/', '');
                            var from = 'from=' + path;
                            window.location.href = 'login?' + from;
                        }
                    }
                }
            }
        }
        return response;
    };
    static cloudSignIn = async (url, dataToPost) => {
        try {
            const reqHeader = new RequestHeaders();
            const header = reqHeader.headers(true);
            let response = await axios.post(url, dataToPost, {
                headers: header
            });
            return response;
        } catch (e) {
            store.dispatch(errorAlert(e.message));
        }
  
    };
    static refreshToken = async () => {
        let isTokenRefreshed = false;
        let api = References.refreshTokenApi;
        const response = await AxiosRequest.post(api, "");
        if (response.status === 200) {
            if (response.data.error === "") {
                if (response.data.success.data.key) {
                    AxiosRequest.updateNewToken(response.data.success.data.key);
                    isTokenRefreshed = true;
                }
            }
        }
        return isTokenRefreshed;
    };
    static updateNewToken = (key) => {
        localStorage.setItem('userKey', key);
    };
    static app4LegalUrl =() => {
    const reqHeader = new RequestHeaders();
    let url = reqHeader.url;
    return url;
    } 
};

export class RequestHeaders {
    url: string;
    userKey: string;
    constructor() {
        this.url = localStorage.getItem("url");
        this.userKey = localStorage.getItem("userKey");
        if (this.url == null || this.url ==  '' || this.userKey == null || this.userKey == '') {
            var settings = Office.context.roamingSettings;
            this.url = settings.get('url');
            this.userKey = settings.get('userKey');
        }
    };
    headers(noAuth = null, noHeaders = null) {
        return (noHeaders) ? null :
        ((noAuth) ?
            apiHeaders : { ...apiHeaders, "x-api-key": "" + this.userKey + "" })
    };
    uploadHeaders() {
        return ({ "x-api-key": "" + this.userKey + "" });
    };
};

const apiHeaders = {
    "Content-type": "application/x-www-form-urlencoded",
    "x-api-channel": "outlook"
};