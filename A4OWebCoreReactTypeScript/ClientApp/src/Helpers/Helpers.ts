﻿import { AxiosRequest } from "./AxiosRequest";
import { References } from './References';
import * as GraphService from '../GraphService';
import HijrahDate from 'hijrah-date';

import createLog from 'localstorage-logger';

const log = createLog({
    logName: 'appLogs',
    maxLogSizeInBytes: 512 * 1024
});
export class Helpers {
    static getProfileData = async () => {
        let responses = await AxiosRequest.post(References.userCheckApi, "");
        return responses;
    };
    static checkOutlookAddinLicense = async () => {
        return await AxiosRequest.get(References.licenseApi, true);
    };
    static contractViewLink = (contractId) => {
        let fullLink = AxiosRequest.app4LegalUrl() + References.contractViewLink + contractId;
        return fullLink;
    };
    static clausetViewLink = () => {
        let fullLink = AxiosRequest.app4LegalUrl() + References.clauseViewLink;
        return fullLink;
    };
    static logout = () => {
        localStorage.removeItem("url");
        localStorage.removeItem("username");
        localStorage.removeItem("userKey");
        localStorage.removeItem("userId");
        localStorage.removeItem("userImagebase64");
    };
    static getTemplogginData = () => {
        var userData;
        try {
            var url = localStorage.getItem('url');
            var username = localStorage.getItem('username');
            var userKey = localStorage.getItem('userKey');
            var userId = localStorage.getItem('userId');
            var userImagebase64 = localStorage.getItem('userImagebase64');
            userData = {
                url: url,
                username: username,
                userKey: userKey,
                userId: userId,
                userImage: userImagebase64
            };
        } catch (e) {
            console.log(e);
        }
        return userData;
    }
    static checkUrl(url) {
        var validURL = url;
        try {
            if (url.substring(url.length - 1) != '/') {
                validURL = url + "/";
            }
            if (!(url.startsWith("http://")) && !(url.startsWith("https://"))) {
                validURL = "http://" + validURL;
            }
        } catch (e) {
        }
        return validURL;
    }
    static getUrlPar = (sParam) => {
        try {
            var sPageURL = decodeURIComponent(window.location.search.substring(1)),
                sURLVariables = sPageURL.split('&'),
                sParameterName,
                i;
            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');
                if (sParameterName[0] === sParam) {
                    return sParameterName[1] === undefined ? '' : sParameterName[1];
                }
            }
        } catch (e) {
            return '';
        }
    }
    static handleErrorResponse = (error, from, data, callBack) => {
        if (error.code == "refresh_key_required") {
            var errorRefresh = AxiosRequest.refreshToken();
            if (errorRefresh) {
                callBack(data);                
                return;
            } else {//Error when refreshing Token
                window.location.href = "../login?from=" + from;
            }
        } else if (error.code == "unauthorized") {
            window.location.href = "../login?from=" + from;
            return;
        } else {
            return error;
        }
    }
    static ValidateEmail(inputText) {
        var mailformat = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
        if (inputText.match(mailformat)) {
            return true;
        }
        else {
            return false;
        }
    }
    static formatDate(date, isDateTime = false, isHijri = false) {
        if (date) {
            var d = new Date(date),
                month = '' + (d.getMonth() + 1),
                day = '' + d.getDate(),
                hr = d.getHours(),
                min = d.getMinutes(),
                sec = d.getSeconds(),
                year = d.getFullYear();
            var hour = String(hr);
            var minute = String(min);
            var second = String(sec);
            if (month.length < 2)
                month = '0' + month;
            if (day.length < 2)
                day = '0' + day;
            if (hr < 10) {
                hour = "0" + hr;
            }
            if (min < 10) {
                minute = "0" + min;
            }
            if (sec < 10) {
                second = "0" + sec;
            }
            if (isDateTime) {
                if (isHijri) {
                    const hijriDate = new HijrahDate(date);
                    return `${hijriDate.getFullYear()}-${String(hijriDate.getMonth() + 1)}-${hijriDate.getDate()} ${hijriDate.getHours()}:${hijriDate.getMinutes()}:${hijriDate.getSeconds()}`;
                }
                return `${year}-${month}-${day} ${hour}:${minute}:${second}`;
            }
            if (isHijri) {
                const hijriDate = new HijrahDate(date);
                return `${hijriDate.getFullYear()}-${String(hijriDate.getMonth() + 1)}-${hijriDate.getDate()}`;
            }
            return [year, month, day].join('-');
        }
        return null;
    }
    static formatTime(date) {
        var d = new Date(date),
            hr = d.getHours(),
            min = d.getMinutes();
        var minute = String(min);
        if (min < 10) {
            minute = "0" + min;
        }
        return [hr, minute].join(':');
    }
    static char_count(str, letter) {
        var letter_Count = 0;
        for (var position = 0; position < str.length; position++) {
            if (str.charAt(position) == letter) {
                letter_Count += 1;
            }
        }
        return letter_Count;
    }
    static cleaningData(data) {
        var cleanedData = data;
        Object.entries(data).map(([key, value]) => {
            if (typeof value == 'string' || typeof value == 'boolean' || typeof value == 'number')
                cleanedData[key] = encodeURIComponent(value);
            if (value != null) {
                if (typeof value === 'string' || Array.isArray(value)) {
                    if (value.length == 0)
                        delete cleanedData[key];
                }              
            }
            else
                delete cleanedData[key];
        });
        return cleanedData;
    }
    static getRandomColor() {
        var letters = '0123456789ABCDEF';
        var color = '#';
        for (var i = 0; i < 6; i++) {
            color += letters[Math.floor(Math.random() * 16)];
        }
        return color;
    }
    static buildEmailAddressString(address) {
        return address.displayName + " &lt;" + address.emailAddress + "&gt;";
    }
    static buildEmailAddressesString(addresses) {
        if (addresses && addresses.length > 0) {
            var returnString = "";
            for (var i = 0; i < addresses.length; i++) {
                if (i > 0) {
                    returnString = returnString + "<br/>";
                }
                returnString = returnString + Helpers.buildEmailAddressString(addresses[i]);
            }
            return returnString;
        }
        return "None";
    }
    static buildSpecialEmailAddressesString(addresses) {
        if (addresses && addresses.length > 0) {
            var returnString = "";
            for (var i = 0; i < addresses.length; i++) {
                returnString = returnString + addresses[i].emailAddress + ';';
            }
            return returnString;
        }
        return "None";
    }
    static jsUcfirst(string) {
        return string.charAt(0).toUpperCase() + string.slice(1);
    }
    static blendColors(color1, color2, percentage) {
        // check input
        color1 = color1 || '#000000';
        color2 = color2 || '#ffffff';
        percentage = percentage || 0.5;

        // 2: check to see if we need to convert 3 char hex to 6 char hex, else slice off hash
        //      the three character hex is just a representation of the 6 hex where each character is repeated
        //      ie: #060 => #006600 (green)
        if (color1.length == 4)
            color1 = color1[1] + color1[1] + color1[2] + color1[2] + color1[3] + color1[3];
        else
            color1 = color1.substring(1);
        if (color2.length == 4)
            color2 = color2[1] + color2[1] + color2[2] + color2[2] + color2[3] + color2[3];
        else
            color2 = color2.substring(1);


        // 3: we have valid input, convert colors to rgb
        color1 = [parseInt(color1[0] + color1[1], 16), parseInt(color1[2] + color1[3], 16), parseInt(color1[4] + color1[5], 16)];
        color2 = [parseInt(color2[0] + color2[1], 16), parseInt(color2[2] + color2[3], 16), parseInt(color2[4] + color2[5], 16)];


        // 4: blend
        var color3 = [
            (1 - percentage) * color1[0] + percentage * color2[0],
            (1 - percentage) * color1[1] + percentage * color2[1],
            (1 - percentage) * color1[2] + percentage * color2[2]
        ];

        // 5: convert to hex
        var hexColor3 = '#' + Helpers.int_to_hex(color3[0]) + Helpers.int_to_hex(color3[1]) + Helpers.int_to_hex(color3[2]);

        // return hex
        return hexColor3;
    }

    /*
        convert a Number to a two character hex string
        must round, or we will end up with more digits than expected (2)
        note: can also result in single digit, which will need to be padded with a 0 to the left
        @param: num         => the number to conver to hex
        @returns: string    => the hex representation of the provided number
    */
    static int_to_hex(num) {
        var hex = Math.round(num).toString(16);
        if (hex.length == 1)
            hex = '0' + hex;
        return hex;
    }
    static lowerFirstLetter(string: string) {
        return string.charAt(0).toLowerCase() + string.slice(1);
    }
};
export function handleUniversalChangeTextField(object, event, valid = { type: '', valid: false }) {
    var type = event.target.name;
    var value = event.target.value;
    var currentState = object.data;
    if (valid.type.length > 0 && !valid.valid)
        currentState[type] = '';
    else
        currentState[type] = value;
    var reqFields = object.state.requiredFields;
    if (reqFields[type] != null) {
        if (valid.type.length > 0)
            reqFields[type] = !valid.valid;
        else
            reqFields[type] = !value.length;
        object.setState({ requiredFields: reqFields });
    }
    object.data = currentState;
}
export function handleUniversalChangeAutoComplete(object, ref, value, isAttendees = false) {
    var type = ref.replace(/[^A-Z0-9]+/ig, '').replace(' ', '');
    var currentState = object.data;
    if (Array.isArray(value)) {
        var values = [];
        value.forEach(item => {
            if(isAttendees)
                values.push({ id: item.id, required: item.required });
            else
                values.push(item.id);
        });
        currentState[type] = values;
    }
    else
        currentState[type] = value;
    var reqFields = object.state.requiredFields;
    if (reqFields[type] != null) {
        reqFields[type] = !value?.id?.length;
        object.setState({ requiredFields: reqFields });
    }
    object.data = currentState;
}
export function handleUniversalChangeDateTimeField(object, type, newValue) {
    if (type.toLowerCase().includes('time')) {
        object.data[type] = Helpers.formatTime(newValue);
    }
    else {
        object.data[type] = Helpers.formatDate(newValue);
    }
}
export async function getAttachments(object) {
    var files = [];
    object.props.files.forEach((item) => {
        var file = item.fileObject;
        var d = {
            Data: file.data, File: { ContentType: file.file.type, Length: file.file.size, FileName: file.file.name }
        };
        files.push(JSON.stringify(d));
    });
    if (object.props.includeEmail && object.props.moreEmails?.length > 0) {
        for (var i = 0; i < object.props.moreEmails.length; i++) {
            var email = object.props.moreEmails[i];
            try {
                const token = await object.props.getAccessToken();
                if (token) {
                    var mimeContent = await GraphService.getMIMEMessageContent(token, email.id);

                    if (mimeContent) {
                        const mimeToBase64 = btoa(mimeContent);
                        const getEqualCount = Helpers.char_count(mimeToBase64, '=');
                        const fileLength = (3 * (mimeToBase64.length / 4)) - (getEqualCount);
                        const removedSpecialCharacters = email.subject.replace(/[/\\?%*:|"<>]/g, '-');
                        const fileName = removedSpecialCharacters.length > 255 ? removedSpecialCharacters.substring(0, 254) : removedSpecialCharacters;
                        var moreD = {
                            Data: 'Base64,' + btoa(mimeContent), File: { ContentType: 'application/octet-stream', Length: fileLength, FileName: fileName + '.eml', isEmail: email.isEmail }
                        };
                        files.push(JSON.stringify(moreD));
                    }
                }
                else {
                    const response = await promiseGetData(email);  
                    files.push(response);
                }
            }
            catch (err) {
                log.error('Helpers[getAttachments-Add More Emails]: ' + err);
            }
        }
    }
    return files;
}
async function promiseGetData(email) {
    return await getData(email); // await is actually optional here
    // you'd return a Promise either way.
}
function getData(email) {
    return new Promise(resolve => {
        Office.context.mailbox.getCallbackTokenAsync({ isRest: true }, async function (result) {
            if (result.status === Office.AsyncResultStatus.Succeeded) {
                const token = result.value;
                var mimeContent = await GraphService.getMIMEMessageContent(token, email.id || email.Id, true);

                if (mimeContent) {
                    const mimeToBase64 = btoa(mimeContent);
                    const getEqualCount = Helpers.char_count(mimeToBase64, '=');
                    const fileLength = (3 * (mimeToBase64.length / 4)) - (getEqualCount);
                    const removedSpecialCharacters = email.subject?.replace(/[/\\?%*:|"<>]/g, '-') || email.Subject?.replace(/[/\\?%*:|"<>]/g, '-');
                    const fileName = removedSpecialCharacters.length > 255 ? removedSpecialCharacters.substring(0, 254) : removedSpecialCharacters;
                    var moreD = {
                        Data: 'Base64,' + btoa(mimeContent), File: { ContentType: 'application/octet-stream', Length: fileLength, FileName: fileName + '.eml', isEmail: email.isEmail }
                    };
                    resolve(JSON.stringify(moreD));
                }
                resolve(null);
            }
        });
    });
}
export async function loadEmailAttachments(object){
    object.props.setFetchingAttachments(true);
    var mailAttachments = [];
    try {
        const token = await object.props.getAccessToken();
        if (token) {
            mailAttachments = await GraphService.getMessageAttachments(await object.props.getAccessToken(), object.messageID);
            if (mailAttachments) {
                mailAttachments.forEach(attachment => {
                    if (attachment['@odata.type'] === '#microsoft.graph.fileAttachment') {
                        var file = {};
                        var data = new File([`data:${attachment.contentType};base64,${attachment.contentBytes}`],
                            attachment.name,
                            { type: attachment.contentType });
                        file['data'] = `data:${attachment.contentType};base64,${attachment.contentBytes}`;
                        file['file'] = data;
                        object.attachments.push({ fileObject: file, isEmailAttachment: true, isInline: attachment.isInline });

                    }
                });
            }
            object.props.setFetchingAttachments(false);
            object.props.addFile(object.attachments);
            object.props.setHasEmailAttachments(Boolean(object.attachments?.length));
        }
        else {
            await Office.context.mailbox.getCallbackTokenAsync({ isRest: true }, async function (result) {
                if (result.status === Office.AsyncResultStatus.Succeeded) {
                    const token = result.value;
                    mailAttachments = await GraphService.getMessageAttachments(token, object.messageID, true);
                    if (mailAttachments) {
                        mailAttachments.forEach(attachment => {
                            var file = {};
                            var data = new File([`data:${attachment.ContentType};base64,${attachment.ContentBytes}`],
                                attachment.Name,
                                { type: attachment.ContentType });
                            file['data'] = `data:${attachment.ContentType};base64,${attachment.ContentBytes}`;
                            file['file'] = data;
                            object.attachments.push({ fileObject: file, isEmailAttachment: true, isInline: attachment.IsInline });
                        });
                    }
                    object.props.setFetchingAttachments(false);
                    object.props.addFile(object.attachments);
                    object.props.setHasEmailAttachments(Boolean(object.attachments?.length));
                }
            });
        }
    }
    catch (err) {
        object.props.setFetchingAttachments(false);
        log.error('Helpers[loadEmailAttachments]: ' + err);
    }

}
export function loadEmailProps(mailItem, bodyResult) {
    var dateTimeCreated = mailItem.dateTimeCreated?.toLocaleString();
    var cc = Helpers.buildEmailAddressesString(mailItem.cc);
    var from = Helpers.buildEmailAddressString(mailItem.from);
    var subject = mailItem.subject;
    var to = Helpers.buildEmailAddressesString(mailItem.to);
    var header = '';
    if (from != "")
        header += "<b>From:</b> " + from + "<br>";
    if (to != "")
        header += "<b>To:</b> " + to + "<br>";
    if (cc != "" && cc != "None")
        header += "<b>Cc:</b> " + cc + "<br>";;
    if (subject != "")
        header += "<b>Subject:</b> " + subject + "<br>";
    var timeStamp = '<b>Received on: </b>' + dateTimeCreated + '<br>';
    return {
        emailBody: { value: bodyResult.value, checked: true },
        emailHeader: { value: header, checked: true },
        emailTimeStamp: { value: timeStamp, checked: true },
        emailtTo: Helpers.buildSpecialEmailAddressesString(mailItem.to),
        emailFrom: mailItem.from.emailAddress,
        emailFromName: mailItem.from.displayName,
        emailSubject: mailItem.subject,
        emailDate: mailItem.dateTimeCreated
    };
}