﻿import * as React from "react";
import Button from "@material-ui/core/Button";
import FormControl from '@material-ui/core/FormControl';
import InputAdornment from '@material-ui/core/InputAdornment';
import { withStyles, makeStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import Grid from "@material-ui/core/Grid";
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import CustomizedDialogs from './InstanceList';
import PopupAlert from '../Common/Alert';
import { AxiosRequest } from '../../Helpers/AxiosRequest';
import { Helpers } from '../../Helpers/Helpers';
import { withRouter, RouteComponentProps } from 'react-router-dom';
import { connect } from 'react-redux';
import ProgressLoader from '../ProgressLoader';
import { ActionType } from "typesafe-actions";
import * as actions from '../../redux/actions';
import Authorization from '../../assets/authorization.png';
import logo from '../../assets/SetupHeader.png';
import mailapp from '../../assets/mailapp.svg';
import { References } from '../../Helpers/References';
import * as UIStrings from '../../UIStrings';
import IconButton from '@material-ui/core/IconButton';
import { Theme, createStyles, WithStyles, Typography, CircularProgress, createMuiTheme, ThemeProvider, OutlinedInput } from "@material-ui/core";
import { AppProps } from "../App";
import SwitchControl from "./SwitchControl";
import EmailIconSvg from './LoginIcons/envelope.svg';
import PassWordIconSvg from './LoginIcons/padlock.svg';
import ServerIconSvg from './LoginIcons/server.svg';
import WindowsIconSvg from './LoginIcons/windows.svg';
import LogoIconSvg from './LoginIcons/app4legallogo.png';
import NortonIconSvg from './LoginIcons/norton.svg';
import Divider from "./DividerLine";
import SSOLogin from "./SSOLogin";
import CallbackSSO from "./CallbackSSO";
import LoginRedirect from "./LoginRedirect";
import Login from "./Login";
import Blank from "./Blank";


const useStyles = (theme: Theme) => createStyles({
    paper: {
        margin: '0 auto',
        height: '100%',
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
    },
    avatar: {
        margin: 'auto'
    },
    form: {
        width: "100%", // Fix IE 11 issue.
        marginTop: theme.spacing(1),
        paddingRight: "5px"
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
    menu: {
        zIndex: 9999
    },
    margin: {
        margin: theme.spacing(2, 0, 2, 0),
    },
    fontStyle: {
        margin: '2px 4px 5px 5px',
        textAlign: 'center',
        color: 'rgb(140 138 138)'
    },
    footer: {
        marginTop: '46px',
        width: '100%',
    },
    field: {
        margin: 'auto',
        width: '100%'
    }
});
const loginADtheme = createMuiTheme({
    components: {
        MuiButton: {
            styleOverrides: {
                root: {
                    fontSize: "11px",
                    fontWeight: 'normal',
                    fontStretch: 'normal',
                    fontStyle: 'normal',
                    "&:hover": {
                        backgroundColor: '#106ebe !important',
                    },
                    height: "33px",
                    width: '170px',
                    borderRadius: '20px',
                    paddingLeft: "20px",
                    paddingRight: "20px",
                    textTransform: 'none'
                }
            }
        },
    },
    palette: {
        primary: {
            light: '#757ce8',
            main: 'rgb(0, 164, 239)',
            dark: '#002884',
            contrastText: '#fff',
        },
    },
});
const theme = createMuiTheme({
    palette: {
        primary: {
            light: '#757ce8',
            main: 'rgb(13, 83, 150)',
            dark: '#002884',
            contrastText: '#fff'
        },
    },
    components: {
        MuiButton: {
            styleOverrides: {
                root: {
                    height: "39px",
                    fontSize: "11px",
                    fontWeight: 'normal',
                    fontStretch: 'normal',
                    fontStyle: 'normal',
                    "&:hover": {
                        backgroundColor: 'rgb(11 67 120) !important'
                    },
                    borderRadius: '35px',
                    width: "193px"
                }
            }
        }
    }
});
const outLinedTheme = createMuiTheme({
    components: {
        MuiOutlinedInput: {
            styleOverrides: {
                input: {
                    padding: '15.5px 0 14px 0'
                }
            }
        }
    }
});
class SignIn extends React.Component<ISignInProps, ISignInInterface> {
    instanceDialog: React.RefObject<CustomizedDialogs>;
    url: string;
    email: string;
    password: string;
    dialog: Office.Dialog;
    constructor(props, context) {
        super(props, context);
        this.instanceDialog = React.createRef();
        this.state = {
            open: false,
            serverSelected: false,
            openDialog: false,
            closeStartScreen: false,
            isUserLogged: props.isUserLogged,
            instances: [],
            goToURL: null,
            defaultUserName: (props.isUserLogged ? localStorage.getItem('username') : null),
            urlError: false,
            emailError: false,
            passwordError: false,
            showPassword: false,
            isSigningIn: false,
            text: '',
        };
    };
    UIText = UIStrings.getLocaleStrings(this.props.language);
    componentDidMount() {
        this.props.stopLoader();
        const { isAuthenticated, isUserLoggedIn } = this.props;
        var text = '';
        if (!isAuthenticated)
            text = this.UIText.notAuthorized;
        if (isAuthenticated && !isUserLoggedIn)
            text = this.UIText.notLoggedIn;
        this.setState({ text: text });
    }
    handleButtonClick(e) {
        e.preventDefault();
        e.stopPropagation();
        this.setState({ isSigningIn: true });
        const { isAuthenticated, isUserLoggedIn } = this.props;
        if (!isAuthenticated) {
            this.props.login();
        }
    }
    handleButtonClickNonMicrosoft(e) {
        e.preventDefault();
        e.stopPropagation();
        localStorage.setItem('isRest', 'true');
        window.location.reload();
    }
    handleServerCloudSelectionEvent = (event) => {
        this.setState({ serverSelected: event.target.checked });
        this.resetErrors();
    };
    handleUrlChange = (event) => {
        this.url = event.target.value;
        if (this.url) {
            this.setState({ urlError: false });
        }
    };
    handleEmailChange = (event) => {
        this.email = event.target.value;
        if (this.email) {
            this.setState({ emailError: false });
        }
    };
    handlePasswordChange = (event) => {
        this.password = event.target.value;
        if (this.password) {
            this.setState({ passwordError: false });
        }
    };
    handleClose = (event) => {
        this.setState({ open: false });
    };
    handelSelectedInstance = (instanceId) => {
        if (instanceId) {
            let url = this.state.goToURL + "/" + instanceId + "/";
            this.loginRequest(url, this.email, this.password);
        }
        else {
            this.props.stopLoader();
            this.setState({ isSigningIn: false });
        }
    };
    saveCredentials = (url, username, response) => {
        try {
            const data = response?.success?.data;
            sessionStorage.setItem('isSigningIn', 'false');
            this.props.logger.info('Login: ID = ' + data.userId);
            this.props.logger.info('Login: UserName = ' + username);
            this.saveToLocalStorage('url', url);
            this.saveToLocalStorage('username', username);
            if (data != null) {
                this.saveToLocalStorage('userKey', data.key);
                this.saveToLocalStorage('userId', data.userId);
                this.saveToLocalStorage('userImagebase64', "data:image/" + data.profilePicture.type + ";base64," + data.profilePicture.content);
                var settings = Office.context.roamingSettings;
                settings.set('url', url);
                settings.set('username', username);
                settings.set('userKey', data.key);
                settings.set('userId', data.userId);
                settings.saveAsync(async (response) => {
                    if (response.status.toString() == 'succeeded') {
                        this.setState({ isUserLogged: true });
                        await this.checkLicenseAndNavigateToSettings();
                    }
                    else {
                        this.setState({ isUserLogged: false });
                    }
                });
            }

        } catch (e) {
            this.props.logger.error('Login: ' + e);
        }
    };
    saveToLocalStorage = (_key, _value) => {
        localStorage.setItem(_key, _value)
    };
    resetErrors = () => {
        this.setState({ urlError: false, emailError: false, passwordError: false });
    }
    HandleSSOLoginClick = (event) => {
        event.preventDefault();
        window.location.href = '/sso';
    };
    handleSubmit = async (event) => {
        event.preventDefault();
        event.stopPropagation();
        let username = event.target.email.value;
        let password = event.target.password.value;
        if (!username) {
            this.setState({ emailError: true });
        }
        if (!password) {
            this.setState({ passwordError: true });
        }
        if (!this.state.serverSelected) {
            if (!username || !password) {
                return;
            }
            this.props.startLoader();
            this.setState({ isSigningIn: true });
            let dataToPost = "email=" + username + "&userLogin=" + username + "&lang=english";
            let signInCloud = References.signInCloudApi;
            let respo = await AxiosRequest.cloudSignIn(signInCloud, dataToPost);
            if (respo?.status === 200) {
                let errorResponse = respo.data.error;
                if (!errorResponse) {
                    let instances = respo.data.instances;
                    if (instances) {
                        if (Object.keys(instances)?.length > 1) {
                            this.setState({ goToURL: respo.data.goToURL, instances: instances, openDialog: true });
                            this.instanceDialog.current.handleClickOpen();
                        }
                        else {
                            this.setState({ goToURL: respo.data.goToURL });
                            this.handelSelectedInstance(Object.keys(instances)[0]);
                        }
                    }
                } else {
                    this.props.errorAlert(errorResponse);
                }
            } else {
                //Handel Errors request
                this.props.errorAlert("error");
            }
            this.props.startLoader();
            this.setState({ isSigningIn: true });

        } else {
            let url = !this.state.serverSelected ? null : event.target.url.value;
            if (!url) {
                this.setState({ urlError: true });
                return;
            }
            this.props.startLoader();
            this.setState({ isSigningIn: true });
            //check last character
            let lastChar = url.slice(-1);
            url = (lastChar !== "/") ? (url + "/") : url;
            this.loginRequest(url, username, password);
        }
    };
    loginRequest = async (url, username, password) => {
        this.props.startLoader();
        this.setState({ isSigningIn: true });
        let loginApi = url + References.loginApi;
        let dataString = "email=" + username + "&password=" + password + "&lang=english&userLogin=" + username;
        let respo = await AxiosRequest.cloudSignIn(loginApi, dataString);
        if (respo.status === 200) {
            let errorResponse = respo.data.error;
            if (errorResponse === '') {
                this.saveCredentials(url, username, respo.data);
                this.instanceDialog.current.handleClose();
                this.setState({ openDialog: false });
            } else {
                this.props.errorAlert(errorResponse);
                this.props.stopLoader();
                this.setState({ isSigningIn: false });
            }
        } else {
            this.props.errorAlert(this.UIText.connectionError);
            this.props.stopLoader();
            this.setState({ isSigningIn: false });
        }
    };
    checkLicenseAndNavigateToSettings = async () => {
        let isOutlookAddinLicensed = { status: false, message: "" };
        try {
            let responses = await Helpers.checkOutlookAddinLicense();
            if (responses) {
                if (responses.length > 0) {
                    let checkAddonLicenseResponse = responses[1];
                    if (checkAddonLicenseResponse.status === 200) {
                        if (checkAddonLicenseResponse.data.error === "") {
                            let licenseStatus = checkAddonLicenseResponse.data.status; //true 
                            if (licenseStatus === 'valid') {
                                isOutlookAddinLicensed.status = true;
                            } else {
                                isOutlookAddinLicensed.status = false;
                                isOutlookAddinLicensed.message = checkAddonLicenseResponse.data.message;
                            }
                        } else {
                            isOutlookAddinLicensed.status = false;
                        }
                    } else {
                        isOutlookAddinLicensed.status = false;
                        isOutlookAddinLicensed.message = "Network Error";
                    }
                }
            }
        } catch (e) {
            this.props.logger.error('Login: ' + e);
        }
        this.props.logger.info('Login: logged-in');
        if (isOutlookAddinLicensed.status) {
            var from = Helpers.getUrlPar('from');
            if (from != null && from.length > 0)
                window.location.href = from;
            else {
                if (this.props.from != null && this.props.from != 'login')
                    window.location.href = this.props.from;
            }
        }
        else
            window.location.href = 'settings?isOutlookAddinLicensed=' + isOutlookAddinLicensed;
    };
    handleClickShowPassword = () => {
        this.setState({
            showPassword: !this.state.showPassword
        });
    };
    handleMouseDownPassword = (event) => {
        event.preventDefault();
    };
    render() {
        const { classes } = this.props;
        const UIText = this.UIText;
        const { urlError, emailError, passwordError } = this.state;
        const getLoginPage = () => {
            switch (this.props.from) {
                default: {
                    return (
                        <>
                            <PopupAlert />
                            <ProgressLoader />
                            {!this.props.isAuthenticated ?
                                <div className={classes.paper}>
                                    <Grid container style={{ width: '90%' }}>
                                        <Grid item xs={12} sm={12}>
                                            <Grid item container alignItems="center" textAlign='center'>
                                                <img src={logo} alt="company-logo" className={classes.avatar} />
                                            </Grid>
                                        </Grid>
                                        <Grid item xs={12} sm={12}>
                                            <Grid item container alignItems="center" textAlign='center'>
                                                <img src={Authorization} alt="microsoft-authorization" className={classes.avatar} />
                                            </Grid>
                                        </Grid>
                                        <Grid item xs={12} sm={12}>
                                            <Typography variant='h6' style={{ textAlign: 'center' }}>
                                                {this.state.text}
                                            </Typography>
                                        </Grid>
                                        {this.state.isSigningIn ? <CircularProgress style={{ margin: 'auto' }} /> :
                                            <Grid item xs={12} sm={12}>
                                                <Button style={{
                                                    marginTop: '10px',
                                                }}
                                                    fullWidth
                                                    variant="contained"
                                                    color="primary"
                                                    className={classes.submit}
                                                    onClick={this.handleButtonClick.bind(this)}
                                                >
                                                    {this.props.isAuthenticated ? "" : UIText.signinMicrosoft}
                                                </Button>
                                            </Grid>
                                        }
                                        <Grid item xs={12} sm={12} style={{ marginTop: '40px' }}>
                                            <Typography variant="body1" style={{ textAlign: 'center', padding: '15px' }} color="muted" gutterBottom>
                                                {UIText.welcomeText}
                                            </Typography>
                                        </Grid>
                                    </Grid>
                                </div>
                                :
                                <div className={classes.paper}>
                                    <Grid container alignItems='center' justifyItems='center'>
                                        <CustomizedDialogs ref={this.instanceDialog} handelSelectedInstance={this.handelSelectedInstance.bind(this)} instances={this.state.instances} open={this.state.openDialog} />
                                        <Grid item container alignItems='center' justifyItems='center' xs={12} sm={12}>
                                            <img src={LogoIconSvg} className={classes.avatar} alt="Company-Logo" />
                                        </Grid>
                                        <Grid item xs={12} sm={12} style={{ marginTop: '-60px' }}>
                                            <form
                                                onSubmit={this.handleSubmit.bind(this)}
                                                className={classes.form}
                                                noValidate
                                            >
                                                <Grid container alignItems="center" justifyItems='center' direction='column'>
                                                    <SwitchControl style={{ margin: 'auto' }} serverSelected={this.state.serverSelected} handleServerCloudSelectionEvent={this.handleServerCloudSelectionEvent} />
                                                    <Grid item style={{ marginLeft: '3px', width: '90%' }}>
                                                        <Grid item container xs={12} alignItems="center" justifyItems='center' direction='column' spacing={2}>
                                                            <Grid item style={{ width: '100%' }}>
                                                                {this.state.serverSelected === true ? (
                                                                    <FormControl fullWidth variant="outlined">
                                                                        <ThemeProvider theme={outLinedTheme}>
                                                                            <OutlinedInput
                                                                                margin="none"
                                                                                required
                                                                                error={urlError}
                                                                                onChange={this.handleUrlChange}
                                                                                fullWidth
                                                                                id="url"
                                                                                placeholder={UIText.App4LegalUrl}
                                                                                name="url"
                                                                                autoComplete="url"
                                                                                className={classes.field}
                                                                                autoFocus
                                                                                startAdornment={
                                                                                    <InputAdornment position="start">
                                                                                        <img src={ServerIconSvg} />
                                                                                    </InputAdornment>
                                                                                }
                                                                            />
                                                                        </ThemeProvider>
                                                                    </FormControl>
                                                                ) : <></>}
                                                            </Grid>
                                                            <Grid item style={{ width: '100%' }}>
                                                                <FormControl fullWidth variant="outlined">
                                                                    <ThemeProvider theme={outLinedTheme}>
                                                                        <OutlinedInput
                                                                            defaultValue={this.state.defaultUserName}
                                                                            margin="none"
                                                                            required
                                                                            fullWidth
                                                                            error={emailError}
                                                                            onChange={this.handleEmailChange}
                                                                            id="email"
                                                                            placeholder={UIText.emailAddress}
                                                                            name="email"
                                                                            autoComplete="email"
                                                                            startAdornment={
                                                                                <InputAdornment position="start">
                                                                                    <img src={EmailIconSvg} />
                                                                                </InputAdornment>
                                                                            }
                                                                        />
                                                                    </ThemeProvider>
                                                                </FormControl>
                                                            </Grid>

                                                            <Grid item style={{ width: '100%' }}>
                                                                <FormControl fullWidth variant="outlined">
                                                                    <ThemeProvider theme={outLinedTheme}>
                                                                        <OutlinedInput
                                                                            placeholder={UIText.password}
                                                                            id="input-passsword"
                                                                            type={this.state.showPassword ? 'text' : 'password'}
                                                                            error={passwordError}
                                                                            name="password"
                                                                            onChange={this.handlePasswordChange}
                                                                            startAdornment={
                                                                                <InputAdornment position="start">
                                                                                    <img src={PassWordIconSvg} />
                                                                                </InputAdornment>
                                                                            }
                                                                        />
                                                                    </ThemeProvider>
                                                                </FormControl>
                                                                {this.state.serverSelected === false ? (
                                                                    <Grid item className={classes.field} style={{ marginBottom: '10px' }}>
                                                                        <Typography variant="body1" className={classes.fontStyle} style={{ float: 'right' }} gutterBottom>
                                                                            <a style={{ textDecoration: 'none', color: '#176ae6', fontSize: '10px', fontWeight: 'normal', fontStretch: 'normal', fontStyle: 'normal' }} target="_blank" href="https://www.app4legal.com/app4legal-cc/public/app/forgotPassword">{UIText.forgotPassword}</a>
                                                                        </Typography>
                                                                    </Grid>
                                                                ) : null
                                                                }
                                                            </Grid>
                                                        </Grid>
                                                        <Grid item container alignItems='center' justifyItems='center' xs={12} sm={12} style={{ marginTop: this.state.serverSelected ? '20px' : '0px' }}>
                                                            <ThemeProvider theme={theme}>
                                                                <Button variant="contained" type="submit" color="primary" style={{ margin: 'auto' }}>{UIText.login}</Button>
                                                            </ThemeProvider>
                                                        </Grid>
                                                        <Grid item container alignItems='center' justifyItems='center' xs={12} sm={12}>
                                                            <Grid item container justifyItems='center'>
                                                                <Divider style={{ width: '193px', margin: '21px auto' }}>{UIText.or}</Divider>
                                                            </Grid>
                                                        </Grid>
                                                        <Grid item container alignItems='center' justifyItems='center' xs={12} sm={12}>
                                                            <ThemeProvider theme={loginADtheme}>
                                                                <Button variant="contained" color="primary" onClick={this.HandleSSOLoginClick} style={{ margin: 'auto' }} startIcon={<img src={WindowsIconSvg} />}>{UIText.loginsso}</Button>
                                                            </ThemeProvider>
                                                        </Grid>
                                                    </Grid>
                                                </Grid>
                                            </form>
                                        </Grid>
                                    </Grid>
                                    <div className={classes.footer}>
                                        <Grid container justifyItems='center' alignItems='center'>
                                            <Grid item xs={12} sm={12} alignItems="center" >
                                                <Typography variant="body1" component="div" className={classes.fontStyle} gutterBottom>{UIText.learnMoreAbout}<a style={{ color: '#176ae6' }} target="_blank" href="/termsconditions">{UIText.security}</a> & <a style={{ color: '#176ae6' }} target="_blank" href="/privacypolicy">{UIText.privacyPolicy}</a>
                                                </Typography>
                                                <Typography variant="body1" component="div" className={classes.fontStyle} gutterBottom><a style={{ color: '#176ae6' }} target="_blank" href="https://www.app4legal.com/app4legal-cc/public/app/signup">{UIText.firstTimeSignUp}</a>
                                                </Typography>
                                            </Grid>
                                            <Grid item xs={12} sm={12} container alignItems='center' justifyItems='center'>
                                                <img src={NortonIconSvg} style={{ width: "60px", objectFit: "contain", margin: 'auto' }} />
                                            </Grid>
                                        </Grid>
                                    </div>
                                </div>
                            }
                        </>
                    );
                }
                case 'sso': {
                    return (<>
                        <SSOLogin from={this.props.from} {...this.props} />
                    </>);
                }
                case 'callbacksso': {
                    return (<>
                        <CallbackSSO {...this.props} />
                    </>);
                }
                case 'loginredirect': {
                    return (<>
                        <LoginRedirect />
                    </>)
                };
                case 'blank': {
                    return (<>
                        <Blank />
                    </>);
                }
            }
        }
        return (
            <React.Fragment>
                {getLoginPage()}
            </React.Fragment>);
    }
};
const mapStateToProps = (state) => {
    return {
        language: state.settings.language,
        severity: state.alert.severity,
        alertMessage: state.alert.alertMessage,
        open: state.alert.open,
    }
};
type Actions = ActionType<typeof actions>;
const mapDispatchToProps = (dispatch: any) => {
    return {
        successAlert: (message: string) => dispatch(actions.successAlert(message)),
        errorAlert: (message: string) => dispatch(actions.errorAlert(message)),
        stopLoader: () => dispatch(actions.stopLoader()),
        startLoader: () => dispatch(actions.startLoader()),
    }
};
interface ISignInInterface {
    text: string;
    open: boolean;
    openDialog: boolean;
    closeStartScreen: boolean;
    isSigningIn: boolean;
    serverSelected: boolean;
    isUserLogged: boolean;
    instances: [];
    goToURL: null;
    defaultUserName: string;
    urlError: boolean;
    emailError: boolean;
    passwordError: boolean;
    showPassword: boolean
}
type ReduxProps = ReturnType<typeof mapStateToProps> & ReturnType<typeof mapDispatchToProps>;
interface ISignInProps extends ReduxProps, RouteComponentProps<any>, WithStyles<typeof useStyles>, AppProps {
    from: string;
}
const AppContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(SignIn);
export default withStyles(useStyles, { withTheme: true })(withRouter(AppContainer));