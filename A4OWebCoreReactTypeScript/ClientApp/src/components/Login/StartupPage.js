﻿/* global Office,UIStrings */
import React from "react";
import Button from "@material-ui/core/Button";
import app4legalLogo from '../../assets/SetupHeader.png';
import mailapp from '../../assets/mailapp.svg'
import { withStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import Grid from "@material-ui/core/Grid";
import Box from '@material-ui/core/Box';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import Typography from '@material-ui/core/Typography';
import * as UIStrings from '../../UIStrings';
import { Image } from "office-ui-fabric-react";


const useStyles = (theme) => ({
    paper: {
        marginTop: theme.spacing(2),
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
    },
    avatar: {
        margin: theme.spacing(1)
    },
    form: {
        width: "100%", // Fix IE 11 issue.
        marginTop: theme.spacing(1),
        paddingRight: "5px"
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
    menu: {
        zIndex: "9999",
    },
});

class StartupPage extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.anchorRef = React.createRef();
        this.urlRef = React.createRef();
        this.instanceDialog = React.createRef();
        this.emailRef = React.createRef();
        this.passwordRef = React.createRef();
        this.state = {
            open: false,
            openDialog: false,
            isUserLogged: props.isUserLogged,
            userName: ''
        };
    };
    UIText = UIStrings.getLocaleStrings(this.props.language);
    HandleLoginOpen = (event) => {
        event.preventDefault();
        window.location.href = "/login?from=settings";
    };
    componentDidMount() {
        Office.initialize = () => {
            var accountData = Office.context.mailbox.userProfile;
            var userName = accountData.displayName;
            this.setState({ userName: userName });
        };
    }
    render() {
        const { classes } = this.props;
        const UIText = this.UIText;
        return (
            <React.Fragment>
                <Container component="main" maxWidth="xs">
                    <div className={classes.paper}>
                        <Grid container alignItems="center">
                            <Grid item xs={12} align="center">
                                <img src={app4legalLogo} alt="App4Legal" className={classes.avatar} />
                            </Grid>
                        </Grid>

                        <Grid container alignItems="center">
                            <Grid item xs={12} align="center">
                                <img src={mailapp} alt='App4Legal for Outlook Logo' title='App4Legal for Outlook' />
                            </Grid>
                        </Grid>

                        <form
                            className={classes.form}
                            noValidate
                        >
                            <Grid container direction="column" alignItems="center">
                                <Grid item xs={12} sm={12}>
                                    <Typography variant="h6" color="muted" gutterBottom>
                                        {UIText.welcomTitle}
                                    </Typography>
                                </Grid>
                            </Grid>
                            <Grid container alignItems="center">
                                <Grid item xs={12} sm={12}>
                                    <Typography variant="body2" color="muted" gutterBottom>
                                        {UIText.welcomeText}
                                    </Typography>
                                </Grid>
                                <Grid item xs={12} sm={12} align="center">
                                    <Button style={{ backgroundColor: "#0D5396", marginTop: '40px' }}
                                        type="submit"
                                        variant="contained"
                                        color="primary"
                                        className={classes.submit}
                                        onClick={this.HandleLoginOpen}
                                    >
                                        {UIText.btnStart}
                                    </Button>
                                </Grid>
                            </Grid>
                        </form>

                    </div>
                </Container>
            </React.Fragment>);
    }
};
const mapStateToProps = state => ({
    language: state.settings.language
});

const AppContainer = connect(
    mapStateToProps
)(StartupPage);
export default withStyles(useStyles, { withTheme: true })(withRouter(AppContainer));