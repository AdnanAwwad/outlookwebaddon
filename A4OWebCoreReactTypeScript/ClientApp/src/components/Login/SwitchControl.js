﻿/* global UIStrings */
import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import FormGroup from '@material-ui/core/FormGroup';
import Box from '@material-ui/core/Box';
import Switch from '@material-ui/core/Switch';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { useSelector } from "react-redux";
import * as UIStrings from '../../UIStrings';
 
const CloudServerSwitch = withStyles({
    switchBase: {
        color: '#155495',
        '&$checked': {
            color: '#155495',
        },
        '&$checked + $track': {
            backgroundColor: '#155495',
        },
    },
    checked: {},
    track: {},
})(Switch);

function SwitchControl(props) {

    const languageSelected = useSelector(state => state.settings.language);
    const UIText = UIStrings.getLocaleStrings(languageSelected);

    const [state, setState] = React.useState({
        isServer: false,
    });
    const handleChange = (event) => {
        setState({ ...state, [event.target.name]: event.target.checked });
    };

    const boldStyle = {
        fontSize: '12px',
        fontWeight: 'bold',
        fontStretch: 'normal',
        fontStyle: 'normal',
        lineHeight: '1.25',
        letterSpacing: '0.6px',
        textAlign: 'left',
        color: '#000000',
    }
    const lightStyle = {
        fontSize: '12px',
        fontWeight: 'normal',
        opacity: '0.53',
        fontStretch: 'normal',
        fontStyle: 'normal',
        lineHeight: '1.27',
        letterSpacing: '0.55px',
        textAlign: 'left',
        color: '#000000',
    }
    const cloudSelected = (isServer) => (isServer ? {
        fontFamily: props.serverSelected ? `'SourceSansPro-Bold'` : `'SourceSansPro-Light'`
    } : {
            fontFamily: props.serverSelected ? `'SourceSansPro-Light'` : `'SourceSansPro-Bold'`
        });
    return (
        <Box mt={1} mb={1} style={{marginLeft: '10px'}}>
            <FormGroup>
                <Typography component="div" mt={5}>
                    <Grid component="label" container alignItems="center" spacing={1}>
                        <Grid item style={props.serverSelected ? lightStyle : boldStyle}>{UIText.cloud}</Grid>
                        <Grid item>
                            <CloudServerSwitch checked={props.serverSelected} onChange={props.handleServerCloudSelectionEvent} name="isServer" />
                        </Grid>
                        <Grid item style={props.serverSelected ? boldStyle : lightStyle}>{UIText.server}</Grid>
                    </Grid>
                </Typography>
            </FormGroup>
        </Box>
    );
}
export default SwitchControl;
