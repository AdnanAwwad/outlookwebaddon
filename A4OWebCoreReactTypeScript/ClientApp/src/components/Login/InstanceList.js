﻿import React from "react";
import { withStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import Dialog from '@material-ui/core/Dialog';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import MuiDialogContent from '@material-ui/core/DialogContent';
import MuiDialogActions from '@material-ui/core/DialogActions';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';

const styles = (theme) => ({
    root: {
        margin: 0,
        padding: theme.spacing(2),
    },
    closeButton: {
        position: 'absolute',
        right: theme.spacing(1),
        top: theme.spacing(1),
        color: theme.palette.grey[500],
    },
});

const DialogTitle = withStyles(styles)((props) => {
    const { children, classes, onClose, ...other } = props;
    return (
        <MuiDialogTitle disableTypography className={classes.root} {...other}>
            <Typography variant="h6">{children}</Typography>
            {onClose ? (
                <IconButton aria-label="close" className={classes.closeButton} onClick={onClose}>
                    <CloseIcon />
                </IconButton>
            ) : null}
        </MuiDialogTitle>
    );
});

const DialogContent = withStyles((theme) => ({
    root: {
        padding: theme.spacing(2),
    },
}))(MuiDialogContent);

const DialogActions = withStyles((theme) => ({
    root: {
        margin: 0,
        padding: theme.spacing(1),
    },
}))(MuiDialogActions);

class CustomizedDialogs extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
            open: props.open
        };
    }
    handleClickOpen = () => {
        this.setState({ open: true });
    };
    handleClose = () => {
        this.setState({ open: false });
        this.props.handelSelectedInstance(null);
    };
    render() {
        return (
            <div>
                <Dialog fullWidth={true}
                    maxWidth={"sm"}
                    onClose={this.handleClose}
                    aria-labelledby="max-width-dialog-title"
                    open={this.state.open}>
                    <DialogTitle id="customized-dialog-title" onClose={this.handleClose}>
                        Choose Instance
                    </DialogTitle>
                    <DialogContent dividers>
                        <SimpleList handelSelectedInstance={(instanceId) => {
                            this.setState({ open: false });
                            this.props.handelSelectedInstance(instanceId);
                        }} instances={this.props.instances} />
                    </DialogContent>
                    <DialogActions>
                        <Button autoFocus onClick={this.handleClose} color="primary">
                            Close
          </Button>
                    </DialogActions>
                </Dialog>
            </div>
        );
    }
}
export default CustomizedDialogs;

const useItemStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
        maxWidth: 360,
        backgroundColor: theme.palette.background.paper,
    },
}));

const SimpleList = (props) => {
    const classes = useItemStyles();

    return (
        <div className={classes.root}>
            <List component="nav" aria-label="secondary mailbox folders">
                {
                    (props.instances != null) ? Object.keys(props.instances).map(function (key) {
                        return <ListItem key={key} button onClick={() => { props.handelSelectedInstance(key) }}> <ListItemText primary={props.instances[key]} /> </ListItem>
                    }) : null
                }
            </List>
        </div>
    );
}