﻿/// <reference types="office-js" />
/* global Office,UIStrings */
import * as React from "react";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import logo from './LoginIcons/app4legallogo.png'
import { withStyles, WithStyles, Theme, createStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import Grid from "@material-ui/core/Grid";
import CustomizedDialogs from './InstanceList';
import PopupAlert from '../Common/Alert';
import { AxiosRequest } from '../../Helpers/AxiosRequest';
import { References } from '../../Helpers/References';
import { Helpers } from '../../Helpers/Helpers';
import { withRouter, RouteComponentProps } from 'react-router-dom';
import { connect } from 'react-redux';
import * as UIStrings from '../../UIStrings';
import { AppProps } from "../App";
import * as actions from '../../redux/actions';
import NortonIconSvg from './LoginIcons/norton.svg';
import { Typography } from "@material-ui/core";

const useStyles = (theme: Theme) => createStyles({
    paper: {
        marginTop: theme.spacing(2),
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
    },
    avatar: {
        margin: theme.spacing(1)
    },
    form: {
        width: "100%", // Fix IE 11 issue.
        marginTop: theme.spacing(1),
        padding: "0 10px",
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
    menu: {
        zIndex: 9999,
    },
    fontStyle: {
        margin: '2px 4px 5px 5px',
        textAlign: 'center',
        color: 'rgb(140 138 138)'
    },
    footer: {
        marginTop: '45px',
        width: '100%'
    }
});
var dialog: Office.Dialog;
class SSOLogin extends React.Component<ISignInProps, ISignInState> {
    instanceDialog: React.RefObject<CustomizedDialogs>;
    goToUrl: string;
    instances: any;
    constructor(props, context) {
        super(props, context);
        this.instanceDialog = React.createRef();
        this.state = {
            emailError: false,
            isSigningIn: false,
            openDialog: false
        };
        this.goToUrl = '';
        this.processMessage = this.processMessage.bind(this);
    };
    UIText = UIStrings.getLocaleStrings(this.props.language);
    handleEmailChange = (event) => {
        let urlValue = event.target.value;
        if (urlValue) {
            this.setState({ emailError: false });
        }
    };
    componentDidMount() {
        this.props.stopLoader();
    }
    resetErrors = () => {
        this.setState({ emailError: false });
    }
    handleSubmit = async (event) => {
        event.preventDefault();
        let username = event.target.email.value;
        if (!username) {
            this.setState({ emailError: true });
        }
        if (!username) {
            return;
        }
        this.props.startLoader();
        this.setState({ isSigningIn: true });
        let dataToPost = "email=" + username + "&userLogin=" + username + "&lang=english";
        let signInCloud = References.signInCloudApi;
        let respo = await AxiosRequest.cloudSignIn(signInCloud, dataToPost);
        if (respo.status === 200) {
            let errorResponse = respo.data.error;
            if (!errorResponse || errorResponse.legnth == 0) {
                let instances = respo.data.instances;
                if (instances) {
                    this.goToUrl = respo.data.goToURL;
                    this.instances = instances;
                    this.setState({ openDialog: true });
                    this.instanceDialog.current.handleClickOpen();
                }
            } else {
                this.props.errorAlert(errorResponse);
            }
        } else {
            //Handel Errors request
            this.props.errorAlert("error");
        }
    };
    handelSelectedInstance = (instanceId) => {
        if (this.goToUrl.length > 0 && instanceId.length > 0) {
            let instance = this.goToUrl + "/" + instanceId + "/";
            const settings = Office.context.roamingSettings;
            settings.set('url', instance);
            settings.saveAsync(async (response) => {
                if (response.status == Office.AsyncResultStatus.Succeeded) {
                    var redirectURL = window.location.origin + '/callbacksso';
                    var encodedURLBtoA = btoa(redirectURL);
                    var url = instance + 'users/login_sso_external?redirect_url=' + encodedURLBtoA;
                    var encodedURL = btoa(url);
                    var ssoPage = window.location.origin + '/loginredirect?redirect_url=' + encodedURL;
                    Office.context.ui.displayDialogAsync(ssoPage, {
                        height: 60, width: 40
                    },
                        ((asyncResult) => {
                            if (asyncResult.status === Office.AsyncResultStatus.Failed) {
                                console.log(asyncResult.error.code + ": " + asyncResult.error.message);
                            } else {
                                dialog = asyncResult.value;
                                dialog.addEventHandler(Office.EventType.DialogMessageReceived, this.processMessage);
                                dialog.addEventHandler(Office.EventType.DialogEventReceived, this.eventHandler);
                            }                            
                        })
                    );
                }
                else {
                    this.props.errorAlert(this.UIText.somethingWentWrong);
                }
            });
        }
    };
    processMessage(arg) {
        try {
            dialog.close();
        }
        catch (err) {
            console.log(err);
        }
        try {
            var message = arg.message;
            var messageParsed = JSON.parse(message);
            var type = messageParsed.messageType;
            if (type == 'LoginSuccessReceived') {
                var ssoresponse = messageParsed.message;
                var decodedResponse = atob(ssoresponse);
                var response = JSON.parse(decodedResponse);
                var success = response.success;
                var data = success.data;
                var token = data.token;
                var email = data.email;
                var userName = data.username;
                if (token != null && email != null && userName != null) {
                    const settings = Office.context.roamingSettings;
                    settings.set('email', email);
                    settings.set('username', userName);
                    settings.set('userKey', token);
                    settings.saveAsync(async (asyncResult) => {
                        var status = asyncResult.status;
                        if (status == Office.AsyncResultStatus.Succeeded) {
                            const url = settings.get('url');
                            var validURL = Helpers.checkUrl(url);
                            localStorage.setItem('userKey', token);
                            localStorage.setItem('url', validURL);
                            localStorage.setItem('username', userName);
                            const dataToPost = 'lang=english';
                            const response = await AxiosRequest.post(References.userInfoApi, dataToPost);
                            if (response.status === 200) {
                                const data = response.data?.success?.data;
                                if (data) {
                                    localStorage.setItem('userImagebase64', "data:image/" + data.profilePicture.type + ";base64," + data.profilePicture.content);
                                }
                                let isOutlookAddinLicensed = { status: false, message: "" };
                                try {
                                    let responses = await Helpers.checkOutlookAddinLicense();
                                    if (responses) {
                                        if (responses.length > 0) {
                                            let checkAddonLicenseResponse = responses[1];
                                            if (checkAddonLicenseResponse.status === 200) {
                                                if (checkAddonLicenseResponse.data.error === "") {
                                                    let licenseStatus = checkAddonLicenseResponse.data.status; //true 
                                                    if (licenseStatus === 'valid') {
                                                        isOutlookAddinLicensed.status = true;
                                                    } else {
                                                        isOutlookAddinLicensed.status = false;
                                                        isOutlookAddinLicensed.message = checkAddonLicenseResponse.data.message;
                                                    }
                                                } else {
                                                    isOutlookAddinLicensed.status = false;
                                                }
                                            } else {
                                                isOutlookAddinLicensed.status = false;
                                                isOutlookAddinLicensed.message = "Network Error";
                                            }
                                        }
                                    }
                                } catch (e) {
                                    console.log(e);
                                }
                                if (isOutlookAddinLicensed.status) {
                                    if (this.props.from != null && this.props.from != 'login')
                                        window.location.href = this.props.from;
                                    else {
                                        window.location.href = 'settings?isOutlookAddinLicensed=' + isOutlookAddinLicensed;
                                    }
                                }
                                else {
                                    window.location.href = 'settings?isOutlookAddinLicensed=' + isOutlookAddinLicensed;
                                }
                            }
                        }
                        else {
                            this.props.errorAlert(this.UIText.somethingWentWrong);
                        }
                    });
                }
            }
        }
        catch (err) {
            this.props.errorAlert(this.UIText.somethingWentWrong);
        }
        if (arg.type == "dialogMessageReceived") {
            if (arg.message == "closedialog") {
                dialog.close();
                //await this.checkLicenseAndNavigateToSettings();
                this.props.stopLoader();
                return;
            }
        }
    };
    eventHandler(arg) {
        switch (arg.error) {
            case 12002:
                //showNotification("Cannot load URL, no such page or bad URL syntax.");
                break;
            case 12003:
                // showNotification("HTTPS is required.");
                break;
            case 12006:
                console.log(arg);
                this.props.stopLoader();
                break;
            default:
                this.props.stopLoader();
                break;
        }
        dialog.close();
    };
    render() {
        const { classes } = this.props;
        const { emailError } = this.state;
        const UIText = this.UIText;
        return (
            <React.Fragment>
                <PopupAlert />
                <CustomizedDialogs ref={this.instanceDialog} handelSelectedInstance={this.handelSelectedInstance.bind(this)} instances={this.instances} open={this.state.openDialog} />
                <div className={classes.paper}>
                    <Grid container alignItems="center" justifyItems='center' direction='column'>
                        <Grid item xs={12} alignItems="center">
                            <img src={logo} alt="Company-Logo" className={classes.avatar} />
                        </Grid>
                        <form
                            onSubmit={this.handleSubmit.bind(this)}
                            className={classes.form}
                            noValidate
                        >
                            <Grid container alignItems="center">
                                <Grid item xs={12} sm={12}>
                                    <TextField
                                        margin="normal"
                                        required
                                        fullWidth
                                        error={emailError}
                                        onChange={this.handleEmailChange}
                                        id="email"
                                        label={UIText.emailAddress}
                                        name="email"
                                        autoComplete="email"
                                        autoFocus
                                    />
                                </Grid>
                                <Grid item xs={12} sm={12}>
                                    <Button style={{ backgroundColor: "#0D5396" }}
                                        type="submit"
                                        fullWidth
                                        variant="contained"
                                        color="primary"
                                        className={classes.submit}
                                    >
                                        {UIText.login}
                                    </Button>
                                </Grid>
                            </Grid>
                        </form>
                    </Grid>
                    <div className={classes.footer}>
                        <Grid container justifyItems='center' alignItems='center' direction='column'>
                            <Grid item xs={12} sm={12} alignItems="center" >
                                <Typography variant="body1" component="div" className={classes.fontStyle} gutterBottom>{UIText.learnMoreAbout}<a style={{ color: '#176ae6' }} target="_blank" href="/termsconditions">{UIText.security}</a> & <a style={{ color: '#176ae6' }} target="_blank" href="/privacypolicy">{UIText.privacyPolicy}</a>
                                </Typography>
                                <Typography variant="body1" component="div" className={classes.fontStyle} gutterBottom><a style={{ color: '#176ae6' }} target="_blank" href="https://www.app4legal.com/app4legal-cc/public/app/signup">{UIText.firstTimeSignUp}</a>
                                </Typography>
                            </Grid>
                            <Grid item xs={12} sm={12} alignItems="center">
                                <img src={NortonIconSvg} style={{ width: "60px", objectFit: "contain" }} />
                            </Grid>
                        </Grid>
                    </div>
                </div>
            </React.Fragment>);
    }
};
const mapStateToProps = (state) => {
    return {
        language: state.settings.language,
        severity: state.alert.severity,
        alertMessage: state.alert.alertMessage,
        open: state.alert.open,
    }
};
const mapDispatchToProps = (dispatch: any) => {
    return {
        successAlert: (message: string) => dispatch(actions.successAlert(message)),
        errorAlert: (message: string) => dispatch(actions.errorAlert(message)),
        stopLoader: () => dispatch(actions.stopLoader()),
        startLoader: () => dispatch(actions.startLoader()),
    }
};
type ReduxProps = ReturnType<typeof mapStateToProps> & ReturnType<typeof mapDispatchToProps>;
interface ISignInProps extends ReduxProps, RouteComponentProps<any>, WithStyles<typeof useStyles>, AppProps {
    from: string;
}
interface ISignInState {
    isSigningIn: boolean;
    emailError: boolean;
    openDialog: boolean;
}
const AppContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(SSOLogin);
export default withStyles(useStyles, { withTheme: true })(withRouter(AppContainer));