﻿/// <reference types="office-js" />
/* global Office,UIStrings */
import * as React from "react";
import CheckCircleOutline from '@material-ui/icons/CheckCircleOutline';
import * as Colors from '@material-ui/core/colors';
import { withStyles, WithStyles, Theme, createStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import Typography from '@material-ui/core/Typography';
import Grid from "@material-ui/core/Grid";
import PopupAlert from '../Common/Alert';
import { AxiosRequest } from '../../Helpers/AxiosRequest';
import { References } from '../../Helpers/References';
import { Helpers } from '../../Helpers/Helpers';
import { withRouter, RouteComponentProps } from 'react-router-dom';
import { connect } from 'react-redux';
import * as actions from '../../redux/actions';
import * as UIStrings from '../../UIStrings';
import { AppProps } from "../App";
import ErrorIcon from '@material-ui/icons/Error';
const useStyles = (theme: Theme) => createStyles({
    paper: {
        marginTop: theme.spacing(2),
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
    },
    avatar: {
        margin: theme.spacing(1)
    },
    form: {
        width: "100%", // Fix IE 11 issue.
        marginTop: theme.spacing(1),
        paddingRight: "5px"
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
    menu: {
        zIndex: 9999,
    },
});
class CallBackSSO extends React.Component<ISignInProps, ISignInState> {
    constructor(props, context) {
        super(props, context);
        this.state = { status: { success: false, message: '' } };
    };
    UIText = UIStrings.getLocaleStrings(this.props.language);
    componentDidMount() {
        this.fetchData();   
    }
    fetchData() {
        var ssoresponse = Helpers.getUrlPar('sso_response');
        if (ssoresponse != null) {
            var decodedResponse = atob(ssoresponse);
            var response = JSON.parse(decodedResponse);
            try {
                if (response.status == 200) {
                    this.setState({ status: { success: true, message: response.success.msg } })
                    var message = { messageType: 'LoginSuccessReceived', message: ssoresponse };
                    setTimeout(function () {
                        try {
                            Office.context.ui.messageParent(JSON.stringify(message));
                        }
                        catch (err) {
                            console.log(err);
                        }
                    }, 2000);
                }
                else {
                    console.log(response);
                    this.setState({ status: { success: false, message: this.UIText.authenticationFailed } })
                }
            }
            catch (err) {
                console.log(decodedResponse);
                this.setState({ status: { success: false, message: this.UIText.authenticationFailed } })
            }
        }
    }
    render() {
        const { classes } = this.props;
        return (
            <React.Fragment>
                <PopupAlert />
                    <div className={classes.paper}>
                        <Grid container alignItems="center" justifyItems='center' spacing={3} direction='column'>
                            <Grid item xs={12}>
                                <Typography variant="h4"  gutterBottom>
                                    {this.state.status.message}
                               </Typography>
                            </Grid>
                            <Grid item xs={12}>
                                {this.state.status.success ?
                                    <CheckCircleOutline style={{ color: Colors.green[500], fontSize: 90 }} />
                                    :
                                    <ErrorIcon style={{ color: Colors.red[500], fontSize: 90 }}/>
                                    }
                            </Grid>
                        </Grid>
                    </div>
            </React.Fragment>);
    }
};
const mapStateToProps = (state) => {
    return {
        language: state.settings.language,
        severity: state.alert.severity,
        alertMessage: state.alert.alertMessage,
        open: state.alert.open,
    }
};
const mapDispatchToProps = (dispatch: any) => {
    return {
        successAlert: (message: string) => dispatch(actions.successAlert(message)),
        errorAlert: (message: string) => dispatch(actions.errorAlert(message)),
        stopLoader: () => dispatch(actions.stopLoader()),
        startLoader: () => dispatch(actions.startLoader()),
    }
};
type ReduxProps = ReturnType<typeof mapStateToProps> & ReturnType<typeof mapDispatchToProps>;
interface ISignInProps extends ReduxProps, RouteComponentProps<any>, WithStyles<typeof useStyles>, AppProps {
    from: string;
}
interface ISignInState {
    status: IStatusPros;
}
interface IStatusPros {
    success: boolean;
    message: any;
}
const AppContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(CallBackSSO);
export default withStyles(useStyles, { withTheme: true })(withRouter(AppContainer));