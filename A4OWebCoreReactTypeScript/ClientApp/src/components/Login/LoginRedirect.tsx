﻿import * as React from "react";
import { Helpers } from '../../Helpers/Helpers';

export default class LoginRedirect extends React.Component {
    constructor(props) {
        super(props);
    }
    componentDidMount() {
        const redirectUrl = Helpers.getUrlPar('redirect_url');
        var decodedURL = atob(redirectUrl);
        if (decodedURL != null) {
            window.location.href = decodedURL;
        }
        else {
            document.body.innerHTML = 'Something went wrong! Please try again later.';
        }      
    }
    render() {
        return (<div style={{ margin: '0 auto' }}>Redirecting...</div>);
    }
}