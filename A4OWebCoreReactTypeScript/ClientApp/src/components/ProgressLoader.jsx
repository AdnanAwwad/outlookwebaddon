﻿import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import CircularProgress from '@material-ui/core/CircularProgress';
import Backdrop from '@material-ui/core/Backdrop';
import { useDispatch, useSelector } from "react-redux";
import { stopLoader } from '../redux/actions';

const useStyles = makeStyles((theme) => ({
    backdrop: {
        zIndex: theme.zIndex.drawer + 1,
    },
}));

export default function ProgressLoader(props) {
    const dispatch = useDispatch()
    const runLoader = useSelector(state => state.loader.runLoader);
    const classes = useStyles();
    const handleClose = () => {
        dispatch(stopLoader());
    };
    return (
        <React.Fragment>
            <Backdrop className={classes.backdrop} open={runLoader} onClick={handleClose}>
                <CircularProgress color="primary" />
            </Backdrop>
        </React.Fragment>
    );
}
