﻿import * as React from "react";
import TextField from "@material-ui/core/TextField";
import { withStyles, makeStyles } from "@material-ui/core/styles";
import { connect } from "react-redux";
import Container from "@material-ui/core/Container";
import Grid from "@material-ui/core/Grid";
import PopupAlert from '../Common/Alert';
import { AxiosRequest, RequestHeaders } from '../../Helpers/AxiosRequest';
import { References } from '../../Helpers/References';
import * as actions from '../../redux/actions';
import * as UIStrings from '../../UIStrings';
import { Typography, createStyles, AppBar, Toolbar, Autocomplete, Theme, WithStyles, Paper, Divider, Accordion, AccordionSummary, AccordionDetails, IconButton, Switch, FormControl, InputLabel, Input, FormControlLabel, Checkbox } from "@material-ui/core";
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import InfoIcon from '@material-ui/icons/Info';
import SearchBox from '../Common/SearchBox';
import BottomBar from '../Common/BottomBar';
import Modal from '../Common/Modal';
import { Helpers } from "../../Helpers/Helpers";
import ContactsIcon from '@material-ui/icons/Contacts';
import ContactPhoneIcon from '@material-ui/icons/ContactPhone';
import { Array } from "core-js";
import FormSkeleton from "../Common/Skeleton/FormSkeleton";
import { AppProps } from "../App";
import { UniversalTextField, UniversalAutoComplete, SwitchTextField } from "../Common/TextFields";

const useStyles = (theme: Theme) => createStyles({
    paper: {
        marginTop: theme?.spacing(2),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center'
    },
    avatar: {
        margin: theme?.spacing(1),
        backgroundColor: 'blue',
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme?.spacing(3),
        paddingRight: "5px",
        marginBottom: theme?.spacing(8)
    },
    submit: {
        color: 'red'
    },
    textarea: {
        resize: "both",
        minHeight: "200px"
    },
    formControl: {
        margin: theme?.spacing(1),
        minWidth: 120,
    },
    selectEmpty: {
        marginTop: theme?.spacing(2),
    },
    root: {
        display: 'flex',
        flexDirection: 'column',
        flexGrow: 1,
        marginTop: '10px'
    },
    main: {
        marginBottom: theme?.spacing(8),
    },
    footer: {
        position: 'fixed',
        bottom: "0",
        marginTop: 'auto',
        width: "100%",
        backgroundColor: '#fff',
    },
    margin: {
        margin: theme?.spacing(1),
    },
    textField: {
        width: '25ch',
    },
    appBar: {
        position: 'fixed',
        height: '55px',
        paddingBottom: '10px',
        bottom: 0,
    },
    addAddress: {
        margin: theme?.spacing(1),
    },
    icon: {
        width: '20px',
        height: '20px'
    }
});
class Contact extends React.Component<IContactProps, IContactState> {
    categories: any[];
    subCategories: any[];
    titles: any[];
    genders: any[];
    countries: any[];
    companyGroup: any[];
    constructor(props: IContactProps, context: any) {
        super(props, context);
        this.state = {
            sharedWith: false,
            requiredFields: {
                firstName: false,
                lastName: false,
                category: false
            },
            id: '',
            data: {}
        };
        this.categories = [];
        this.subCategories = [];
        this.titles = [];
        this.genders = [];
        this.countries = [];
        this.companyGroup = [];
        this.handleChangeTextField = this.handleChangeTextField.bind(this);
        this.handleChangeAutoComplete = this.handleChangeAutoComplete.bind(this);
        this.handleSelectImportContact = this.handleSelectImportContact.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }
    componentDidMount() {
        this.props.startLoader();
        this.fillData();
    }
    UIText = UIStrings.getLocaleStrings(this.props.language);
    async fillData() {
        try {
            let response = await AxiosRequest.get(References.addContactApi);
            if (response.data.error === "") {
                this.fillCustomData('titles', response.data.success.data.formData.titles);
                this.fillCustomData('genders', response.data.success.data.formData.genders);
                this.fillCustomData('categories', response.data.success.data.formData.categories);
                this.fillCustomData('subCategories', response.data.success.data.formData.sub_categories);
                this.fillCustomData('countries', response.data.success.data.formData.countries);
            }
            else {
                this.props.errorAlert(JSON.stringify(response.data.error));
            }
            this.props.stopLoader();
        } catch (e) {
            this.props.logger.error('Contact: ' + e);            
            this.props.errorAlert(this.UIText.errorOccured);
            this.props.stopLoader();
        }
    }
    fillCustomData(type: any, items: any) {
        if (!items || items.length == 0) {
            return;
        }
        var dataToFill = [];
        if (items) {
            Object.entries(items).map(([key, value]) => dataToFill.push({ value: value, id: key }));
        }
        this[type] = dataToFill;
    }
    async onSubmit(form) {
        form.preventDefault();
        form.stopPropagation();
        var firstNameField = this.state.data['givenName'];
        var lastNameField = this.state.data['surname'];
        var categoryField = this.state.data['category']?.value;
        var requiredFields = {
            firstName: !firstNameField?.length,
            lastName: !lastNameField?.length,
            category: !categoryField?.length
        };
        this.setState({
            requiredFields: requiredFields
        });
        for (const [key, value] of Object.entries(requiredFields)) {
            if (value) {
                document.getElementById(key)?.scrollIntoView();
                return;
            }
        }
        this.props.startSaving();

        var title = this.state.data['title'];
        var firstName = this.state.data['givenName'];
        var lastName = this.state.data['surname'];
        var middleName = this.state.data['middleName'];
        var category = this.state.data['category'];
        var subCategory = this.state.data['subcategory'];
        var companyGroup = this.state.data['companygroup'];
        var jobTitle = this.state.data['jobTitle'];
        var email = this.state.data['emailAddresses'];
        var phone = this.state.data['phone'];
        var mobile = this.state.data['mobilePhone'];
        var isLawyer = this.state.data['isLawyer'];
        var inHouseLawyer = this.state.data['inHouseLawyer'];
        var website = this.state.data['website'];
        var fax = this.state.data['fax'];
        var address1 = this.state.data['address1'];
        var address2 = this.state.data['address2'];
        var city = this.state.data['city'];
        var state = this.state.data['state'];
        var country = this.state.data['country'];
        var zip = this.state.data['zip'];
        var cbPrivate = this.state.sharedWith;
        var sharedWith = this.state.data['sharedwith'];
        var gender = this.state.data['gender'];
        var motherName = this.state.data['motherName'];
        var referenceNumber = this.state.data['referenceNumber'];
        var nationality = this.state.data['nationality'];
        var foreignFirstName = this.state.data['foreignFirstName'];
        var foreignLastName = this.state.data['foreignLastName'];
        var date = this.state.data['birthday'];
        var comments = this.state.data['comments'];

        const reqHeader = new RequestHeaders();
        var newContact = {
            url: reqHeader.url,
            userKey: reqHeader.userKey,
            title: title?.id,
            firstName: firstName,
            lastName: lastName,
            middleName: middleName,
            category: category?.id,
            subCategory: subCategory?.id,
            companyGroupResults: companyGroup,
            jobTitle: jobTitle,
            email: email,
            phone: phone,
            mobile: mobile,
            isLawyer: isLawyer != null ? isLawyer : false,
            inHouseLawyer: inHouseLawyer != null ? inHouseLawyer : false,
            website: website,
            fax: fax,
            addressOne: address1,
            addressTwo: address2,
            city: city,
            state: state,
            country: country?.id,
            zip: zip,
            cbPrivate: cbPrivate,
            sharedwith: sharedWith,
            gender: gender?.id,
            motherName: motherName,
            referenceNumber: referenceNumber,
            nationality: nationality,
            foreignFirstName: foreignFirstName,
            foreignLastName: foreignLastName,
            dateOfBirth: date,
            comments: comments
        };

        newContact = Helpers.cleaningData(newContact);

        var dataToPost = 'private=' + (newContact.cbPrivate ? 'yes' : 'no');
        if (newContact.title != null)
            dataToPost += "&title_id=" + newContact.title;
        if (newContact.gender != null)
            dataToPost += "&gender=" + newContact.gender;
        if (newContact.category)
            dataToPost += "&contact_category_id=" + newContact.category;
        if (newContact.subCategory != null)
            dataToPost += "&contact_sub_category_id=" + newContact.subCategory;
        if (newContact.firstName != null)
            dataToPost += "&firstName=" + newContact.firstName;
        if (newContact.lastName != null)
            dataToPost += "&lastName=" + newContact.lastName;
        if (newContact.middleName != null)
            dataToPost += "&father=" + newContact.middleName;
        if (newContact.motherName != null)
            dataToPost += "&mother=" + newContact.motherName;
        if (newContact.referenceNumber != null)
            dataToPost += "&internalReference=" + newContact.referenceNumber;
        if (newContact.foreignFirstName != null)
            dataToPost += "&foreignFirstName=" + newContact.foreignFirstName;
        if (newContact.foreignLastName != null)
            dataToPost += "&foreignLastName=" + newContact.foreignLastName;
        if (newContact.addressOne != null)
            dataToPost += "&address1=" + newContact.addressOne;
        if (newContact.addressTwo != null)
            dataToPost += "&address2=" + newContact.addressTwo;
        if (newContact.city != null)
            dataToPost += "&city=" + newContact.city;
        if (newContact.state != null)
            dataToPost += "&state=" + newContact.state;
        if (newContact.country != null)
            dataToPost += "&country_id=" + newContact.country;
        if (newContact.zip != null)
            dataToPost += "&zip=" + newContact.zip;
        if (newContact.comments != null)
            dataToPost += "&comments=" + newContact.comments;
        if (newContact.dateOfBirth != null)
            dataToPost += "&dateOfBirth=" + newContact.dateOfBirth;
        if (newContact.phone != null)
            dataToPost += "&phone=" + newContact.phone;
        if (newContact.mobile != null)
            dataToPost += "&mobile=" + newContact.mobile;
        if (newContact.email != null)
            dataToPost += "&email=" + newContact.email;
        if (newContact.jobTitle != null)
            dataToPost += "&jobTitle=" + newContact.jobTitle;
        if (newContact.fax != null)
            dataToPost += "&fax=" + newContact.fax;
        if (newContact.website != null)
            dataToPost += "&website=" + newContact.website;
        if (newContact.inHouseLawyer != null)
            dataToPost += "&lawyerForCompany=" + (newContact.inHouseLawyer ? 'yes' : 'no');
        if (newContact.isLawyer != null)
            dataToPost += "&isLawyer=" + (newContact.isLawyer ? 'yes' : 'no');

        if (newContact.cbPrivate) {
            var privateArray = '';
            if (newContact.sharedwith != null) {
                for (var i = 0; i < newContact.sharedwith.length; i++) {
                    privateArray += `&Contact_Users[${i}]=` + newContact.sharedwith[i];
                }
            }
            if (privateArray.length > 0)
                dataToPost += privateArray;
        }

        if (newContact.companyGroupResults != null && newContact.companyGroupResults.length > 0) {
            newContact.companyGroupResults.forEach((item, index) => {
                dataToPost += `&companies_contacts[${index}]=` + item;
            });
        }
        if (newContact.nationality != null && newContact.nationality.length > 0) {
            newContact.nationality.forEach((item, index) => {
                dataToPost += `&Contact_Nationalities[${index}]=` + item;
            });
        }

        var response = await AxiosRequest.post(References.addContactApi, dataToPost);
        try {
            var resp = response.data;
            var error = resp?.error;
            if (error != "") {
                var respError = Helpers.handleErrorResponse(error, 'contact', form, this.onSubmit);
                if (respError != null && respError != '') {
                    this.props.errorAlert(JSON.stringify(respError));
                }
            }
            else {
                var contactId = resp?.success?.data?.contact_id;
                this.setState({ id: contactId });
                this.props.openModal();
            }
        }
        catch (err) {
            this.props.logger.error('Contact: ' + err);
            this.props.errorAlert(this.UIText.errorOccured);
        }
        this.props.stopSaving();
    }

    handleChangeAutoComplete(ref: string, value) {
        var type = ref.replace(/[^A-Z0-9]+/ig, '').toLowerCase();
        var currentState = this.state.data;
        if (Array.isArray(value)) {
            var values = [];
            value.forEach(item => {
                values.push(item.id);
            });
            currentState[type] = values;
        }
        else
            currentState[type] = value;
        var reqFields = this.state.requiredFields;
        if (reqFields[type] != null) {
            reqFields[type] = !value?.id?.length;
            this.setState({ requiredFields: reqFields });
        }
        this.setState({ data: currentState });
    }
    handleSelectImportContact(ref: string, value) {
        if (value) {
            var emailAddresses = [];
            emailAddresses = value.emailAddresses;
            if (emailAddresses) {
                var parsedMails = [];
                emailAddresses.forEach(email => {
                    if (email?.address) {
                        parsedMails.push(email.address);
                    }
                });
                var emails = parsedMails.join(';');
                value.emailAddresses = emails;
            }
            var businessAddress = value.businessAddress;
            if (businessAddress) {
                value.address2 = businessAddress.street || '';
                value.city = businessAddress.city || '';
                value.state = businessAddress.state || '';
                value.zip = businessAddress.postalCode || '';
            }
            var homeAddress = value.homeAddress;
            if (homeAddress) {
                var address1 = '';
                address1 = Object.values(homeAddress).join(' - ');
                value.address1 = address1;
            }
            var birthday = value.birthday;
            if (birthday) {
                var date = Helpers.formatDate(birthday)
                value.birthday = date;
            }
            var businessPhones = value.businessPhones;
            var homePhones = value.homePhones;
            var phones = '';
            if (businessPhones)
                phones += businessPhones.join(' - ');
            if (homePhones) {
                if (phones.length > 0)
                    phones += ' - ';
                phones += homePhones.join(' - ');
            }
            value.phone = phones;
        }
        this.setState({ data: { ...this.state.data, ...value } });
    }
    handleChangeTextField(event, valid = { type: '', valid: false }) {
        var type = event.target.name;
        var value = event.target.value;
        var currentState = this.state.data;
        if (valid.type.length > 0 && !valid.valid)
            currentState[type] = '';
        else
            currentState[type] = value;
        var reqFields = this.state.requiredFields;
        if (reqFields[type] != null && valid.type.length) {
            reqFields[type] = !valid.valid;
            this.setState({ requiredFields: reqFields });
        }
        this.setState({ data: currentState });
    }
    handleCheckBoxChange(event) {
        var name = event.target.name;
        var checked = event.target.checked;
        this.setState({ data: { ...this.state.data, [name]: checked } });
    }
    handleSharedWithSwitch(event) {
        this.setState({ sharedWith: event.target.checked });
    }
    render() {
        const { classes } = this.props;
        const UIText = this.UIText;
        const group1 = [{ type: 'jobTitle', autoComplete: 'off' }, { type: 'emailAddresses', autoComplete: 'email', hint: UIText.emailinfo }, { type: 'phone', autoComplete: 'tel' }, { type: 'mobilePhone', autoComplete: 'tel' }];
        const group2 = [{ type: 'website', autoComplete: 'url' }, { type: 'fax', autoComplete: 'tel' }, { type: 'address1', autoComplete: 'address-level1' }, { type: 'address2', autoComplete: 'address-level2' }, { type: 'city', autoComplete: 'street-address' }, { type: 'state', autoComplete: 'country' }];
        return (<React.Fragment>
            <PopupAlert />
            <Modal type='contact' id={this.state.id} />
            {this.props.runLoader ?
                <FormSkeleton input='form' /> :
                <div className={classes.root}>
                    <div className={classes.paper}>
                        <div style={{ marginTop: '10px', width: '100%' }}>
                            <SearchBox
                                token={this.props.getAccessToken()}
                                icon
                                apiType='outlookContact'
                                type='outlookContact'
                                title={UIText.importFromOutlook}
                                handleChangeAutoComplete={this.handleSelectImportContact}
                            />
                        </div>
                        <form id="addContact" className={classes.form} noValidate onSubmit={this.onSubmit.bind(this)}>
                            <Accordion defaultExpanded={true}>
                                <AccordionSummary
                                    expandIcon={<ExpandMoreIcon />}
                                    aria-controls="panel1a-content"
                                    id="panel1a-header"
                                >
                                    <Grid container alignItems='center'>
                                        <Grid item xs={2}>
                                            <img src={require('../../assets/contactIcon.svg')} className={classes.icon} />
                                        </Grid>
                                        <Grid item xs>
                                            <Typography variant='h6' gutterBottom>{UIText.generalInformation}</Typography>
                                        </Grid>
                                    </Grid>
                                </AccordionSummary>
                                <AccordionDetails>
                                    <Grid container spacing={2} >
                                        <Grid item xs={12} sm={12}>
                                            <UniversalAutoComplete
                                                type='title'
                                                label={UIText.title}
                                                options={this.titles}
                                                onChange={this.handleChangeAutoComplete}
                                            />
                                        </Grid>
                                        <Grid item xs={12} sm={12}>
                                            <UniversalTextField
                                                type='givenName'
                                                label={UIText.firstName}
                                                autoComplete='given-name'
                                                onChange={this.handleChangeTextField}
                                                required
                                                error={this.state.requiredFields.firstName}
                                                value={this.state.data?.givenName}
                                                focus
                                                controlled
                                            />
                                            <UniversalTextField
                                                type='surname'
                                                label={UIText.lastName}
                                                autoComplete='family-name'
                                                onChange={this.handleChangeTextField}
                                                required
                                                error={this.state.requiredFields.lastName}
                                                value={this.state.data?.surname}
                                                controlled
                                            />
                                            <UniversalTextField
                                                type='middleName'
                                                label={UIText.middleName}
                                                autoComplete='additional-name'
                                                onChange={this.handleChangeTextField}
                                                value={this.state.data?.middleName}
                                                controlled
                                            />
                                        </Grid>
                                        <Grid item xs={12} sm={12}>
                                            <UniversalAutoComplete
                                                type='category'
                                                label={UIText.category}
                                                options={this.categories}
                                                onChange={this.handleChangeAutoComplete}
                                                error={this.state.requiredFields.category}
                                                required
                                            />
                                        </Grid>
                                        <Grid item xs={12} sm={12}>
                                            <UniversalAutoComplete
                                                type='subcategory'
                                                label={UIText.subCategory}
                                                options={this.subCategories}
                                                onChange={this.handleChangeAutoComplete}
                                            />
                                        </Grid>
                                        <Grid item xs={12} sm={12}>
                                            <SearchBox
                                                url={References.companiesAutoCompleteApi}
                                                multiple
                                                apiType='company'
                                                type='companyGroup'
                                                title={UIText.companyGroup}
                                                handleChangeAutoComplete={this.handleChangeAutoComplete}
                                            />
                                        </Grid>
                                    </Grid>
                                    <Grid item xs={12} sm={12}>
                                        {
                                            group1.map((item, index) => {
                                                return (
                                                    <UniversalTextField
                                                        key={index}
                                                        type={item.type}
                                                        label={UIText[item.type]}
                                                        autoComplete={item.autoComplete}
                                                        onChange={this.handleChangeTextField}
                                                        value={this.state.data[item.type]}
                                                        helperText={item.hint}
                                                        controlled
                                                    />
                                                )
                                            })
                                        }
                                    </Grid>
                                </AccordionDetails>
                            </Accordion>
                            <Accordion>
                                <AccordionSummary
                                    expandIcon={<ExpandMoreIcon />}
                                    aria-controls="panel3a-content"
                                    id="panel3a-header"
                                >
                                    <Grid container alignItems='center'>
                                        <Grid item xs={2}>
                                            <img src={require('../../assets/moreFieldsIcon.svg')} className={classes.icon} />
                                        </Grid>
                                        <Grid item xs>
                                            <Typography variant='h6' gutterBottom>{UIText.moreFields}</Typography>
                                        </Grid>
                                    </Grid>
                                </AccordionSummary>
                                <AccordionDetails>
                                    <Grid container spacing={2} >
                                        <Grid item xs={12} sm={12}>
                                            <FormControlLabel
                                                control={
                                                    <Checkbox
                                                        onChange={this.handleCheckBoxChange.bind(this)}
                                                        name="isLawyer"
                                                        color="primary"
                                                    />
                                                }
                                                label={UIText.isLawyer}
                                            />
                                            <FormControlLabel
                                                control={
                                                    <Checkbox
                                                        onChange={this.handleCheckBoxChange.bind(this)}
                                                        name="inHouseLawyer"
                                                        color="primary"
                                                    />
                                                }
                                                label={UIText.inHouseLawyer}
                                            />
                                        </Grid>
                                        <Grid item xs={12} sm={12}>
                                            {
                                                group2.map((item, index) => {
                                                    return (
                                                        <UniversalTextField
                                                            key={index}
                                                            type={item.type}
                                                            label={UIText[item.type]}
                                                            autoComplete={item.autoComplete}
                                                            onChange={this.handleChangeTextField}
                                                            value={this.state.data[item.type]}
                                                            controlled
                                                        />
                                                    )
                                                })
                                            }
                                        </Grid>
                                        <Grid item xs={12} sm={12}>
                                            <UniversalAutoComplete
                                                type='country'
                                                label={UIText.country}
                                                options={this.countries}
                                                onChange={this.handleChangeAutoComplete}
                                            />
                                        </Grid>
                                        <Grid item xs={12} sm={12}>
                                            <UniversalTextField
                                                type='zip'
                                                label={UIText.zip}
                                                autoComplete='postal-code'
                                                onChange={this.handleChangeTextField}
                                                value={this.state.data?.zip}
                                                controlled
                                            />
                                        </Grid>
                                        <Grid item xs={12} sm={12}>
                                            <Typography>{this.UIText.sharedWith}</Typography>
                                            <SwitchTextField
                                                type='sharedWith'
                                                firstLabel={UIText.everyone}
                                                secondLabel={UIText.private}
                                                onSwitchChange={this.handleSharedWithSwitch.bind(this)}
                                                onChange={this.handleChangeAutoComplete}
                                                selected={this.state.sharedWith}
                                                searchType='user'
                                                searchUrl={References.usersLookupApi}
                                                label={UIText.sharedWith}
                                                multiple
                                            />
                                        </Grid>
                                        <Grid item xs={12} sm={12}>
                                            <UniversalAutoComplete
                                                type='gender'
                                                label={UIText.gender}
                                                options={this.genders}
                                                onChange={this.handleChangeAutoComplete}
                                            />
                                        </Grid>
                                        <Grid item xs={12} sm={12}>
                                            <UniversalTextField
                                                type='motherName'
                                                label={UIText.motherName}
                                                autoComplete='name'
                                                onChange={this.handleChangeTextField}
                                                value={this.state.data?.motherName}
                                                controlled
                                            />
                                            <UniversalTextField
                                                type='referenceNumber'
                                                label={UIText.referenceNum}
                                                helperText={UIText.referenceNumTip}
                                                onChange={this.handleChangeTextField}
                                                value={this.state.data?.referenceNumber}
                                                controlled
                                            />
                                        </Grid>
                                        <Grid item xs={12} sm={12}>
                                            <UniversalAutoComplete
                                                type='nationality'
                                                label={UIText.nationality}
                                                options={this.countries}
                                                multiple
                                                onChange={this.handleChangeAutoComplete}
                                            />
                                        </Grid>
                                        <Grid item xs={12} sm={12}>
                                            <UniversalTextField
                                                type='foreignFirstName'
                                                label={UIText.foreignFirstName}
                                                onChange={this.handleChangeTextField}
                                                value={this.state.data?.foreignFirstName}
                                                controlled
                                            />
                                            <UniversalTextField
                                                type='foreignLastName'
                                                label={UIText.foreignLastName}
                                                onChange={this.handleChangeTextField}
                                                value={this.state.data?.foreignLastName}
                                                controlled
                                            />
                                            <TextField
                                                margin="normal"
                                                onChange={this.handleChangeTextField}
                                                type='date'
                                                fullWidth
                                                id="birthday"
                                                label={UIText.dateOfBirth}
                                                name="birthday"
                                                value={this.state.data?.birthday || ''}
                                                InputLabelProps={{
                                                    shrink: true,
                                                }}
                                            />
                                            <UniversalTextField
                                                type='comments'
                                                label={UIText.comments}
                                                onChange={this.handleChangeTextField}
                                                multiline
                                                rows={6}
                                                value={this.state.data?.comments}
                                                controlled
                                            />
                                        </Grid>
                                    </Grid>
                                </AccordionDetails>
                            </Accordion>
                        </form>
                    </div>
                    <BottomBar onSubmit={this.onSubmit} />
                </div >
            }
        </React.Fragment>
        );
    }
}
const AntSwitch = withStyles((theme: Theme) =>
    createStyles({
        root: {
            width: 28,
            height: 16,
            padding: 0,
            display: 'flex',
        },
        switchBase: {
            padding: 2,
            color: theme.palette.grey[500],
            '&$checked': {
                transform: 'translateX(12px)',
                color: theme.palette.common.white,
                '& + $track': {
                    opacity: 1,
                    backgroundColor: theme.palette.primary.main,
                    borderColor: theme.palette.primary.main,
                },
            },
        },
        thumb: {
            width: 12,
            height: 12,
            boxShadow: 'none',
        },
        track: {
            border: `1px solid ${theme.palette.grey[500]}`,
            borderRadius: 16 / 2,
            opacity: 1,
            backgroundColor: theme.palette.common.white,
        },
        checked: {},
    }),
)(Switch);
const mapStateToProps = state => ({
    language: state.settings.language,
    runLoader: state.loader.runLoader
});
const mapDispatchToProps = (dispatch: any) => {
    return {
        successAlert: (message: string) => dispatch(actions.successAlert(message)),
        errorAlert: (message: string) => dispatch(actions.errorAlert(message)),
        stopLoader: () => dispatch(actions.stopLoader()),
        startLoader: () => dispatch(actions.startLoader()),
        startSaving: () => dispatch(actions.startSaving()),
        stopSaving: () => dispatch(actions.stopSaving()),
        openModal: () => dispatch(actions.openModal()),
        focusError: (id: string) => dispatch(actions.focusError(id))
    }
};
type ContacReduxProps = ReturnType<typeof mapStateToProps> & ReturnType<typeof mapDispatchToProps>;
interface IContactState {
    sharedWith,
    requiredFields,
    id: string,
    data: any
}
interface IContactProps extends ContacReduxProps, WithStyles<typeof useStyles>, AppProps {
}
interface IContactOutlookData {
    surname: string;
    givenName: string;
    middleName: string;
    emailAddresses: Array<any>;
    profession: string;
    homePhones: Array<string>;
    businessPhones: Array<string>;
    homeAddress: {};
    businessAddress: {};
    mobilePhone: string;
    birthday: Date;
    jobTitle: string;
}
const AppContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(Contact);
export default withStyles(useStyles, { withTheme: true })(AppContainer);