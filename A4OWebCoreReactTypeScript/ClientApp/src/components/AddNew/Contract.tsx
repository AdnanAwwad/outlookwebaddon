﻿import * as React from "react";
import { withStyles, makeStyles } from "@material-ui/core/styles";
import { connect, useSelector } from "react-redux";
import Grid from "@material-ui/core/Grid";
import PopupAlert from '../Common/Alert';
import { AxiosRequest, RequestHeaders } from '../../Helpers/AxiosRequest';
import { References } from '../../Helpers/References';
import * as actions from '../../redux/actions';
import * as UIStrings from '../../UIStrings';
import { Typography, createStyles, AppBar, Toolbar, Autocomplete, Theme, WithStyles, Paper, Divider, Accordion, AccordionSummary, AccordionDetails, IconButton, Switch, FormControl, InputLabel, Input, Container, Button, Tooltip } from "@material-ui/core";
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import InfoIcon from '@material-ui/icons/Info';
import SearchBox from '../Common/SearchBox';
import BottomBar from '../Common/BottomBar';
import Modal from '../Common/Modal';
import { Helpers, handleUniversalChangeTextField, handleUniversalChangeDateTimeField, handleUniversalChangeAutoComplete, getAttachments, loadEmailAttachments, loadEmailProps } from "../../Helpers/Helpers";
import TextFieldWithValidation from '../CustomInputs/TextFieldWithValidation';
import FormSkeleton from "../Common/Skeleton/FormSkeleton";
import FormatAlignJustifyIcon from '@material-ui/icons/FormatAlignJustify';
import AddIcon from '@material-ui/icons/Add';
import { AppProps } from "../App";
import { AutoCompleteWithImage, UniversalAutoComplete, AutoCompleteWithTwoStage, UniversalTextField, DateTextField, SwitchTextField } from "../Common/TextFields";
import AttachFileIcon from '@material-ui/icons/AttachFile';
import AttachmentsArea from "../Common/Attachments";
import { ReduxState } from "../..";
import DeleteIcon from '@material-ui/icons/Delete';
import { IEmailProps } from "./MatterNote";
import { PriorityButton } from "../Common/SplitButton";
import { addMessageProperty } from "../../GraphService";
import { v4 as uuidv4 } from 'uuid';
import { ActivityProps } from "../Activities/Activities";
import uniqid from "uniqid";


const useStyles = (theme: Theme) => createStyles({
    paper: {
        marginTop: theme?.spacing(2),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center'
    },
    avatar: {
        margin: theme?.spacing(1),
        backgroundColor: 'blue',
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme?.spacing(3),
        paddingRight: "5px",
        marginBottom: theme?.spacing(8)
    },
    submit: {
        color: 'red'
    },
    textarea: {
        resize: "both",
        minHeight: "200px"
    },
    formControl: {
        margin: theme?.spacing(1),
        minWidth: 120,
    },
    selectEmpty: {
        marginTop: theme?.spacing(2),
    },
    root: {
        display: 'flex',
        flexDirection: 'column',
        flexGrow: 1
    },
    main: {
        marginBottom: theme?.spacing(8),
    },
    footer: {
        position: 'fixed',
        bottom: "0",
        marginTop: 'auto',
        width: "100%",
        backgroundColor: '#fff',
    },
    margin: {
        margin: theme?.spacing(1),
    },
    textField: {
        width: '25ch',
    },
    appBar: {
        position: 'fixed',
        height: '55px',
        paddingBottom: '10px',
        bottom: 0,
    },
    addOpponent: {
        margin: theme?.spacing(1),
    },
    icon: {
        width: '20px',
        height: '20px'
    }
});
class Contract extends React.Component<IFormProps, IFormState> {

    attachments: any[];
    messageID: string;
    assignedTeams: any[];
    contractTypes: any[];
    clientTypes: any[];
    currencies: any[];
    contractRenewals: any[];
    categories: any[];
    priorities: any[];
    countries: any[];
    applicableLaws: any[];
    data: any;
    emailInfo: IEmailProps;
    constructor(props: IFormProps, context: any) {
        super(props, context);
        const partyId1 = uniqid();
        const partyId2 = uniqid();
        this.state = {
            assignees: [],
            parties: [{ id: partyId1 }, { id: partyId2 }],
            errorType: false,
            requiredFields: {
                name: false,
                contractType: false,
                requestedBy: false
            },
            id: '',
        };
        this.attachments = [];
        this.messageID = '';
        this.assignedTeams = [];
        this.contractTypes = [];
        this.clientTypes = [];
        this.currencies = [];
        this.contractRenewals = [];
        this.categories = [];
        this.priorities = [];
        this.countries = [];
        this.applicableLaws = [];
        this.handlePartyData = this.handlePartyData.bind(this);
        this.handleDeleteParty = this.handleDeleteParty.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
        this.data = { parties: [{ id: partyId1 }, { id: partyId2 }] };
    }
    componentDidMount() {
        this.props.startLoader();
        this.getMailItem();
        this.fillData();
        window.addEventListener('includeEmailAttachment', this.handleChangeEmailAttachment.bind(this));

    }
    componentWillUnmount() {
        window.removeEventListener('includeEmailAttachment', this.handleChangeEmailAttachment);
    }
    handleChangeEmailAttachment(event) {
        if (event) {
            var includeEmailAttachments = event.detail;
            if (includeEmailAttachments) {
                this.props.addFile(this.attachments);
            }
            else {
                var files = this.props.files.filter(file => file.isEmailAttachment == false);
                this.props.setAttachments(files);
            }
        }
    }
    addAttachments(files: any[]) {
        if (files && files.length > 0) {
            this.attachments = this.attachments.concat(files);
        }
    }
    UIText = UIStrings.getLocaleStrings(this.props.language);
    getMailItem() {
        try {
            var item = Office.context.mailbox.item;
            var self = this;
            item.body.getAsync(Office.CoercionType.Text, function (asyncResult) {
                if (asyncResult.status !== Office.AsyncResultStatus.Succeeded) {
                }
                else {
                    var body = asyncResult.value.trim();
                    var subject = item.subject;
                    self.messageID = item.itemId;
                    if (body || subject) {
                        if (!Helpers.getUrlPar('isNew')) {
                            self.data = {
                                ...self.data,
                                name: subject,
                                description: body
                            }
                        }
                    }
                    self.emailInfo = loadEmailProps(item, asyncResult);
                    loadEmailAttachments(self);
                }
            });
        } catch (e) {
            this.props.logger.error('Contract: ' + e);            
        }
    }
    async fillData() {
        try {
            let response = await AxiosRequest.get(References.addContractApi);            
            if (response.data.error === "") {
                this.fillCustomData('contractTypes', response.data.success.data.types);
                this.fillCustomData('currencies', response.data.success.data.currencies);
                this.fillCustomData('contractRenewals', response.data.success.data.renewals);
                this.fillCustomData('priorities', response.data.success.data.priorities);
                this.fillCustomData('countries', response.data.success.data.countries);
                this.fillCustomData('categories', response.data.success.data.categories);
                this.fillCustomData('countries', response.data.success.data.countries);
                this.fillCustomData('applicableLaws', response.data.success.data.applicable_laws);
                response = await AxiosRequest.get(References.addCorporateMatterApi);
                if (response.data.error === "") {
                    this.fillCustomData('clientTypes', response.data.success.data.clientTypes);
                    this.fillCustomData('assignedTeams', response.data.success.data.provider_groups);
                }
            }
            else {
                this.props.errorAlert(JSON.stringify(response.data.error));
            }
            this.props.stopLoader();
        } catch (e) {
            this.props.logger.error('Contract: ' + e);
            this.props.stopLoader();
            this.props.errorAlert(this.UIText.errorOccured);
        }
    }
    fillCustomData(type: any, items: any) {
        if (!items || items.length == 0) {
            return;
        }
        var dataToFill = [];
        if (items) {
            if (type === 'priorities') {
                Object.entries(items).map(([key, value]) => dataToFill.push({ value: value, id: key, image: value + '.png' }));
            }
            else {
                Object.entries(items).map(([key, value]) => dataToFill.push({ value: value, id: key }));
            }
        }
        this[type] = dataToFill;
    }
    async onSubmit(form) {
        form.preventDefault();
        form.stopPropagation();
        var nameField = this.data.name;
        var contractTypeField = this.data.contractType?.id;
        var requesterField = this.data.requestedBy?.id;
        var requiredFields = {
            name: !nameField?.length,
            contractType: !contractTypeField?.length,
            requestedBy: !requesterField?.length
        };
        this.setState({
            requiredFields: requiredFields
        });
        for (const [key, value] of Object.entries(requiredFields)) {
            if (value) {
                document.getElementById(key)?.scrollIntoView();
                return;
            }
        }
        this.props.startSaving();
        var name = this.data.name;
        var description = this.data.description;
        var contractType = this.data.contractType?.id;
        var country = this.data.country?.id;
        var applicableLaw = this.data.applicableLaw?.id;
        var value = this.data.value;
        var currency = this.data.currency?.id;
        var assignedTeam = this.data.assignedTeam != null ? this.data.assignedTeam.value?.id : this.assignedTeams[0].value?.id;
        var assignee = this.data.assignee?.id;
        var requester = this.data.requestedBy?.id;
        var authorizedSignatory = this.data.authorizedSignatory?.id;
        var priority = this.data.priority != null ? this.data.priority.id : 'medium';
        var contractDate = this.data.contractDate != null ? this.data.contractDate : Helpers.formatDate(new Date());
        var startDate = this.data.startDate;
        var endDate = this.data.endDate;
        var renewal = this.data.renewal?.id;
        var referenceNumber = this.data.referenceNumber;
        var contributors = this.data.contributors;
        
        var contract = {
            name: name,
            description: description,
            applicableLaw: applicableLaw,
            country: country,
            value: value,
            currency: currency,
            contractType: contractType,
            assignedTeam: assignedTeam,
            assignedMember: assignee,
            contractDate: contractDate,
            startDate: startDate,
            endDate: endDate,
            renewal: renewal,
            priority: priority,
            referenceNumber: referenceNumber,
            contributors: contributors,
            requester: requester,
            authorizedSignatory: authorizedSignatory
        };
        //Cleaning the data
        contract = Helpers.cleaningData(contract);

        var dataToPost = 'workflow_id=1&status=Active&status_id=1';
        if (contract.name != null)
            dataToPost += "&name=" + contract.name;
        if (contract.contractType != null)
            dataToPost += "&type_id=" + contract.contractType;
        if (contract.description)
            dataToPost += "&description=" + encodeURI(contract.description);
        if (contract.assignedTeam != null)
            dataToPost += "&assigned_team_id=" + contract.assignedTeam;
        if (contract.country != null)
            dataToPost += '&country_id=' + contract.country;
        if (contract.applicableLaw != null)
            dataToPost += '&app_law_id=' + contract.applicableLaw;
        if (contract.assignedMember != null)
            dataToPost += "&assignee_id=" + contract.assignedMember;
        if (contract.requester != null)
            dataToPost += "&requester_id=" + contract.requester;
        if (contract.contractDate != null)
            dataToPost += "&contract_date=" + contract.contractDate;
        if (contract.startDate != null)
            dataToPost += "&start_date=" + contract.startDate;
        if (contract.endDate != null)
            dataToPost += "&end_date=" + contract.endDate;
        if (contract.priority != null)
            dataToPost += "&priority=" + contract.priority.toLowerCase();
        if (contract.authorizedSignatory != null)
            dataToPost += "&authorized_signatory=" + contract.authorizedSignatory;
        if (contract.referenceNumber != null)
            dataToPost += "&reference_number=" + contract.referenceNumber;
        if (contract.renewal != null)
            dataToPost += "&renewal_type=" + contract.renewal;
        if (contract.currency != null)
            dataToPost += "&currency_id=" + contract.currency;
        if (contract.value != null)
            dataToPost += "&value=" + contract.value;
        
        if (contract.contributors != null && contract.contributors.length > 0) {
            contract.contributors.forEach((contributor, index) => {
                dataToPost += `&contributor_id[${index}]=` + contributor;
            });
        }
        if (this.data.parties != null) {
            var parties = this.data.parties;
            if (parties?.length > 0) {
                parties.forEach((party, index) => {
                    var partyToAdd = '';
                    var type = party?.partyType?.id;
                    var name = party?.partyName?.id;
                    var category = party?.partyCategory?.id;
                    if (type != null && type.length > 0 && name != null && name.length > 0) {
                        partyToAdd += `&party_member_type[${index}]=` + type;
                        partyToAdd += `&party_member_id[${index}]=` + name;
                        if (category != null && category.length > 0) {
                            partyToAdd += `&party_category[${index}]=` + category;
                        }
                    }
                    dataToPost += partyToAdd;
                });
            }
        }
        var response = await AxiosRequest.post(References.addContractApi, dataToPost);
        try {
            var resp = response.data;
            var error = resp?.error;
            if (error != "") {
                var respError = Helpers.handleErrorResponse(error, 'contract', form, this.onSubmit);
                if (respError != null && respError != '') {
                    this.props.errorAlert(JSON.stringify(respError));
                }
            }
            else {
                var caseId = resp.success?.data?.contract_id;
                if (caseId) {
                    try {
                        const reqHeader = new RequestHeaders();
                        var formData = new FormData();
                        const files = await getAttachments(this);
                        if (files.length > 0) {
                            files.forEach(file => {
                                formData.append('Files', file);
                            });
                            formData.append('Url', reqHeader.url + References.uploadContractAttachmentsApi);
                            formData.append('DataToPost', 'module_record_id=' + caseId);
                            formData.append('UserKey', reqHeader.userKey);
                            var uploadResponse = await AxiosRequest.postToServer('PostData/UploadFileToApp4legal', formData);
                            if (uploadResponse.status != 200) {
                                this.props.errorAlert(this.UIText.errorUploadingFiles);
                            }
                            const resp = uploadResponse.data;
                            if (resp?.error === '') {
                            }
                            else {
                                this.props.errorAlert(JSON.stringify(resp?.error));
                            }
                        }
                    }
                    catch (err) {
                        this.props.logger.error('Contract: ' + err);
                        this.props.errorAlert(this.UIText.errorOccured);
                    }
                    this.setState({ id: caseId });
                    var propertyData: ActivityProps = {
                        guid: uuidv4(),
                        type: 'contract',
                        id: caseId,
                        subject: contract.name,
                        date: Helpers.formatDate(new Date(), true),
                        priority: contract.priority
                    }
                    const token = await this.props.getAccessToken();
                    await addMessageProperty(token, this.messageID, propertyData);
                    this.props.openModal();
                }
            }
        }
        catch (err) {
            this.props.logger.error('Contract: ' + err);
            this.props.errorAlert(this.UIText.errorOccured);
        }
        this.props.stopSaving();
    }
    handleDeleteParty(id)
    {
        this.data.parties = this.data.parties.filter(party => party.id !== id );        
        this.setState({ parties: this.state.parties.filter(party => party.id !== id) });
    }
    handlePartyData(index, type, value) {
        if (this.data.parties == null) {
            this.data.parties = [];
        }
        var currentData = this.data.parties[index];
        this.data.parties[index] = { ...currentData, [type]: value };
    }
    render() {
        const { classes } = this.props;
        const UIText = this.UIText;
        return (<React.Fragment>
            <PopupAlert />
            <Modal type='contract' id={this.state.id} />
            {this.props.runLoader ?
                <FormSkeleton input='form' /> :
                <div className={classes.root}>
                    <div className={classes.paper}>
                        <form id='contract' className={classes.form} noValidate onSubmit={this.onSubmit.bind(this)}>
                            <Accordion defaultExpanded={true}>
                                <AccordionSummary
                                    expandIcon={<ExpandMoreIcon />}
                                    aria-controls="panel1a-content"
                                    id="panel1a-header"
                                >
                                    <Grid container alignItems='center'>
                                        <Grid item xs={2}>
                                            <img alt='contract-logo' src={require('../../assets/contractIcon.svg')} className={classes.icon} />
                                        </Grid>
                                        <Grid item xs>
                                            <Typography variant='h6' gutterBottom>{UIText.generalInformation} </Typography>
                                        </Grid>
                                    </Grid>
                                </AccordionSummary>
                                <AccordionDetails>
                                    <Grid container spacing={2} >
                                        <Grid item xs={12}>
                                            <UniversalAutoComplete
                                                type='contractType'
                                                label={UIText.type}
                                                options={this.contractTypes}
                                                onChange={(type, value) => handleUniversalChangeAutoComplete(this, type, value)}
                                                error={this.state.requiredFields.contractType}
                                                required
                                            />
                                        </Grid>
                                        <Grid item xs={12}>
                                            <UniversalTextField
                                                type='name'
                                                error={this.state.requiredFields.name}
                                                required
                                                focus
                                                label={UIText.name}
                                                onChange={(event, value) => handleUniversalChangeTextField(this, event, value)}
                                                defaultValue={this.data.name}                                                
                                            />
                                            <UniversalTextField
                                                type='description'
                                                label={UIText.description}
                                                onChange={(event, value) => handleUniversalChangeTextField(this, event, value)}
                                                multiline
                                                rows={10}
                                                defaultValue={this.data.description}
                                            />
                                        </Grid>
                                        <Grid item container alignItems='center' justifyItems='space-between' xs={12}>
                                                <UniversalTextField
                                                    type='value'
                                                    label={UIText.contractValue}
                                                    onChange={(event, value) => handleUniversalChangeTextField(this, event, value)}
                                                />
                                                <UniversalAutoComplete
                                                    type='currency'
                                                    label={UIText.currency}
                                                    options={this.currencies}
                                                    onChange={(type, value) => handleUniversalChangeAutoComplete(this, type, value)}
                                                />
                                        </Grid>
                                        <Grid item xs={12}>
                                            <SearchBox
                                                url={References.contactLookupApi}
                                                apiType='contact'
                                                required
                                                error={this.state.requiredFields.requestedBy}
                                                hint={UIText.startTyping}
                                                type='requestedBy'
                                                title={UIText.requester}
                                                handleChangeAutoComplete={(type, value) => {
                                                    handleUniversalChangeAutoComplete(this, type, value);
                                                }}
                                            />
                                        </Grid>
                                        <Grid item xs={12}>
                                            <UniversalAutoComplete
                                                type='country'
                                                label={this.UIText.country}
                                                options={this.countries}
                                                onChange={(type, value) => handleUniversalChangeAutoComplete(this, type, value)}
                                                />
                                        </Grid>
                                        <Grid item xs={12}>
                                            <UniversalAutoComplete
                                                type='applicableLaw'
                                                label={this.UIText.applicableLaw}
                                                options={this.applicableLaws}
                                                onChange={(type, value) => handleUniversalChangeAutoComplete(this, type, value)}
                                            />
                                        </Grid>
                                        <Grid item xs={12}>
                                            {this.state.parties.map((item, index) => {
                                                return (<Party
                                                    key={item.id}
                                                    id={item.id}
                                                    index={index}
                                                    clientTypes={this.clientTypes}
                                                    categories={this.categories}
                                                    onChange={this.handlePartyData}
                                                    onDelete={this.handleDeleteParty}
                                                    partiesCount={this.state.parties.length}
                                                />);
                                            })}
                                            <Grid item container xs={12} justifyItems='center' style={{ marginTop: '10px' }}>
                                                <Button
                                                    type="button"
                                                    variant="contained"
                                                    color="primary"
                                                    style={{ margin: 'auto' }}
                                                    startIcon={<AddIcon />}
                                                    onClick={(event) => {
                                                        event.preventDefault();
                                                        event.stopPropagation();
                                                        const id = uniqid();
                                                        this.data.parties?.push({ id: id});
                                                        this.setState({ parties: [...this.state.parties, { id: id }] });
                                                    }}
                                                >
                                                    {UIText.addMore}
                                                </Button>
                                            </Grid>
                                        </Grid>
                                        <Grid item xs={12}>
                                            <DateTextField
                                                type='contractDate'
                                                label={UIText.contractDate}
                                                onChange={(type, value) => handleUniversalChangeDateTimeField(this, type, value)}
                                                defaultValue={Helpers.formatDate(new Date())}
                                            />
                                        </Grid>
                                        <Grid item xs={12}>
                                            <DateTextField
                                                type='startDate'
                                                label={UIText.startDate}
                                                defaultValue={null}
                                                onChange={(type, value) => handleUniversalChangeDateTimeField(this, type, value)}
                                            />
                                        </Grid>
                                        <Grid item xs={12}>
                                            <DateTextField
                                                type='endDate'
                                                label={UIText.endDate}
                                                defaultValue={null}
                                                onChange={(type, value) => handleUniversalChangeDateTimeField(this, type, value)}
                                            />                                            
                                        </Grid>
                                        <Grid item xs={12}>
                                            <UniversalAutoComplete
                                                options={this.contractRenewals}
                                                type='renewal'
                                                label={UIText.renewal}
                                                onChange={(type, value) => handleUniversalChangeAutoComplete(this, type, value)} />
                                        </Grid>
                                    </Grid>
                                </AccordionDetails>
                            </Accordion>
                            <Accordion>
                                <AccordionSummary
                                    expandIcon={<ExpandMoreIcon />}
                                    aria-controls="panel2a-content"
                                    id="panel2a-header"
                                >
                                    <Grid container alignItems='center'>
                                        <Grid item xs={2}>
                                            <img alt='more-fields-logo' src={require('../../assets/moreFieldsIcon.svg')} className={classes.icon} />
                                        </Grid>
                                        <Grid item xs>
                                            <Typography variant='h6' gutterBottom>{UIText.moreFields}</Typography>
                                        </Grid>
                                    </Grid>
                                </AccordionSummary>
                                <AccordionDetails>
                                    <Grid container  spacing={2}>
                                        <Grid item xs={12}>
                                            <UniversalTextField
                                                type='referenceNumber'
                                                label={UIText.referenceNum}
                                                onChange={(event, value) => handleUniversalChangeTextField(this, event, value)}
                                            />
                                        </Grid>
                                        <AutoCompleteWithTwoStage
                                            type='assign'
                                            parentOptions={this.assignedTeams}
                                            parentType='assignedTeam'
                                            parentLabel={UIText.assignedTeam}
                                            childType='assignee'
                                            childLabel={UIText.assignee}
                                            onChange={(type, value) => handleUniversalChangeAutoComplete(this, type, value)}
                                        />
                                        <Grid item xs={12}>
                                            <SearchBox
                                                url={References.usersLookupApi}
                                                apiType='user'
                                                hint={UIText.startTyping}
                                                type='contributors'
                                                title={UIText.contributors}
                                                multiple
                                                handleChangeAutoComplete={(type, value) => {
                                                    handleUniversalChangeAutoComplete(this, type, value);
                                                }}                                            />
                                        </Grid>
                                        <Grid item xs={12}>
                                            <SearchBox
                                                url={References.usersLookupApi}
                                                apiType='user'
                                                hint={UIText.startTyping}
                                                type='authorizedSignatory'
                                                title={UIText.authorizedSignatory}
                                                handleChangeAutoComplete={(type, value) => {
                                                    handleUniversalChangeAutoComplete(this, type, value);
                                                }}
                                            />
                                        </Grid>
                                        <Grid item container justifyItems='space-between' alignItems='center'>
                                            <Grid item xs>
                                                <Typography>{UIText.priority}</Typography>
                                            </Grid>
                                            <Grid item xs>
                                                <PriorityButton
                                                    options={this.priorities}
                                                    handleItemClicked={(option) => { handleUniversalChangeAutoComplete(this, 'priority', option) }} />
                                            </Grid>
                                        </Grid>
                                    </Grid>
                                </AccordionDetails>
                            </Accordion>
                            <Accordion>
                                <AccordionSummary
                                    expandIcon={<ExpandMoreIcon />}
                                    aria-controls="panel3a-content"
                                    id="panel3a-header"
                                >
                                    <Grid container alignItems='center'>
                                        <Grid item xs={2}>
                                            <img alt='attachments-logo' src={require('../../assets/attachmentsIcon.svg')} className={classes.icon} />
                                        </Grid>
                                        <Grid item xs>
                                            <Typography variant='h6' gutterBottom>{UIText.attachments}</Typography>
                                        </Grid>
                                    </Grid>
                                </AccordionSummary>
                                <AccordionDetails>
                                    <AttachmentsArea token={this.props.getAccessToken()} onAddAttachments={this.addAttachments.bind(this)} />
                                </AccordionDetails>
                            </Accordion>
                        </form>
                    </div>
                    <BottomBar onSubmit={this.onSubmit} />
                </div >
            }
        </React.Fragment>
        );
    }
}
function Party(props) {
    const { language } = useSelector((state: ReduxState) => state.settings);
    const UIText = UIStrings.getLocaleStrings(language);
    const [clientType, setClientType] = React.useState('company');
    const handlePartyData = (type, value) => {
        if (type == 'partyType') {
            setClientType(value?.id);
        }
        props.onChange(props.index, type, value);
    }
    return (<div>
        <Grid container direction='row' alignItems='center'>
            <Grid item xs>
                <Typography>{UIText.party + ` (${props.index + 1})`}</Typography>
            </Grid>
            {props.partiesCount > 1 ?
                <Grid item xs={2}>
                    <Tooltip title={UIText.remove} placement='top'>
                        <IconButton color='secondary' aria-label="delete" onClick={(e) => {
                            e.preventDefault();
                            e.stopPropagation();
                            props.onDelete(props.id);
                        }}>
                            <DeleteIcon fontSize="small" />
                        </IconButton>
                    </Tooltip>
                </Grid> : <></>}
        </Grid>
        <Grid container item spacing={1} style={{ marginTop: '10px' }}>
            <Grid item xs={12}>
                <UniversalAutoComplete
                    type='partyType'
                    label={UIText.type}
                    options={props.clientTypes}
                    onChange={handlePartyData}
                />
            </Grid>
            <Grid item xs={12}>
                <SearchBox
                    url={References.clientsLookupApi}
                    apiType='client'
                    extraData={'&clientType=' + clientType}
                    hint={UIText.startTyping}
                    type='partyName'
                    title={UIText.name}
                    handleChangeAutoComplete={handlePartyData}
                />
            </Grid>
            <Grid item xs={12}>
                <UniversalAutoComplete
                    type='partyCategory'
                    label={UIText.category + ` (${props.index + 1})`}
                    options={props.categories}
                    onChange={handlePartyData}
                />
            </Grid>
        </Grid>
    </div>);
}
const mapStateToProps = (state: ReduxState) => ({
    language: state.settings.language,
    runLoader: state.loader.runLoader,
    includeEmail: state.attachments.includeEmail,
    files: state.attachments.files,
    moreEmails: state.attachments.moreEmails,
    includeEmailAttachments: state.attachments.includeEmailAttachments
});
const mapDispatchToProps = (dispatch) => {
    return {
        successAlert: (message: string) => dispatch(actions.successAlert(message)),
        errorAlert: (message: string) => dispatch(actions.errorAlert(message)),
        stopLoader: () => dispatch(actions.stopLoader()),
        startLoader: () => dispatch(actions.startLoader()),
        startSaving: () => dispatch(actions.startSaving()),
        stopSaving: () => dispatch(actions.stopSaving()),
        openModal: () => dispatch(actions.openModal()),
        focusError: (id: string) => dispatch(actions.focusError(id)),
        setAttachments: (files) => dispatch(actions.setAttachments(files)),
        addFile: (file) => dispatch(actions.addFile(file)),
        deleteFile: (index) => dispatch(actions.deleteFile(index)),
        setHasEmailAttachments: (found: boolean) => dispatch(actions.setEmailHasAttachments(found)),
        setFetchingAttachments: (status: boolean) => dispatch(actions.setFetchingAttachments(status))
    }
};
type FormReduxProps = ReturnType<typeof mapStateToProps> & ReturnType<typeof mapDispatchToProps>;
interface IFormState {
    assignees: [],
    parties: any[],
    errorType,
    requiredFields,
    id: string
}
interface IFormProps extends FormReduxProps, WithStyles<typeof useStyles>, AppProps {

}
const AppContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(Contract);
export default withStyles(useStyles, { withTheme: true })(AppContainer);