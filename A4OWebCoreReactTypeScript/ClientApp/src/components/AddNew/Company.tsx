﻿import * as React from "react";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import { withStyles, makeStyles } from "@material-ui/core/styles";
import { useDispatch, useSelector, connect } from "react-redux";
import Container from "@material-ui/core/Container";
import Grid from "@material-ui/core/Grid";
import PopupAlert from '../Common/Alert';
import { AxiosRequest, RequestHeaders } from '../../Helpers/AxiosRequest';
import { References } from '../../Helpers/References';
import { withRouter } from 'react-router-dom';
import * as actions from '../../redux/actions';
import ProgressLoader from '../ProgressLoader';
import * as UIStrings from '../../UIStrings';
import axios from "axios";
import { Typography, createStyles, AppBar, Toolbar, Autocomplete, Theme, WithStyles, Paper, Divider, Accordion, AccordionSummary, AccordionDetails, IconButton, Switch, FormControl, InputLabel, Input, Tooltip } from "@material-ui/core";
import { Dispatch } from "redux";
import { ActionType } from 'typesafe-actions';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import BusinessIcon from '@material-ui/icons/Business';
import HomeIcon from '@material-ui/icons/Home';
import DeleteIcon from '@material-ui/icons/Delete';
import AddressComponent from './Address';
import AddIcon from '@material-ui/icons/Add';
import InfoIcon from '@material-ui/icons/Info';
import SearchBox from '../Common/SearchBox';
import BottomBar from '../Common/BottomBar';
import Modal from '../Common/Modal';
import { Helpers, handleUniversalChangeTextField, handleUniversalChangeAutoComplete, handleUniversalChangeDateTimeField  } from "../../Helpers/Helpers";
import TextFieldWithValidation from '../CustomInputs/TextFieldWithValidation';
import FormSkeleton from "../Common/Skeleton/FormSkeleton";
import { UniversalTextField, UniversalAutoComplete, SwitchTextField, DateTextField } from "../Common/TextFields";
import { AppProps } from "../App";
import uniqid from "uniqid";
import { ReduxState } from "../..";

const useStyles = (theme: Theme) => createStyles({    
    paper: {
        marginTop: theme?.spacing(2),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center'        
    },    
    avatar: {
        margin: theme?.spacing(1),
        backgroundColor: 'blue',
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme?.spacing(3),
        paddingRight: "5px",
        marginBottom: theme?.spacing(8)
    },
    submit: {
        color: 'red'
    },
    textarea: {
        resize: "both",
        minHeight: "200px"
    },
    formControl: {
        margin: theme?.spacing(1),
        minWidth: 120,
    },
    selectEmpty: {
        marginTop: theme?.spacing(2),
    },
    root: {
        display: 'flex',
        flexDirection: 'column',
        flexGrow: 1
    },
    main: {
        marginBottom: theme?.spacing(8),
    },
    footer: {
        position: 'fixed',
        bottom: "0",
        marginTop: 'auto',
        width: "100%",
        backgroundColor: '#fff',
    },
    margin: {
        margin: theme?.spacing(1),
    },
    textField: {
        width: '25ch',
    },
    appBar: {
        position: 'fixed',
        height: '55px',
        paddingBottom: '10px',
        bottom: 0,
    },
    addAddress: {
        margin: theme?.spacing(1),
    },
    icon: {
        width: '20px',
        height: '20px'
    }
});
class Company extends React.Component<ICompanyProps, ICompanyState> {
    data: any;
    categories: any[];
    subCategories: any[];
    companyTypes: any[];
    companyGroup: any[];
    currencies: any[];
    countries: any[];
    constructor(props: ICompanyProps, context: any) {
        super(props, context);
        const add1Id = uniqid();
        this.state = {
            errorType: false,
            addresses: [{ id: add1Id}],
            sharedWith: false,
            requiredFields: {
                name: false,
                shortName: false,
                category: false
            },
            id: ''
        };
        this.data = { addresses: [{ id: add1Id}]};
        this.categories = [];
        this.subCategories = [];
        this.companyTypes = [];
        this.companyGroup = [];
        this.currencies = [];
        this.countries = [];
        this.handleDeleteAddress = this.handleDeleteAddress.bind(this);
        this.handleAddressData = this.handleAddressData.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }
    componentDidMount() {
        this.props.startLoader();        
        this.fillData();
    }
    UIText = UIStrings.getLocaleStrings(this.props.language);
    async fillData() {
        try {
            let response = await AxiosRequest.get(References.addCompanyApi);
            if (response.data.error === "") {
                this.fillCustomData('categories', response.data.success.data.company_categories);
                this.fillCustomData('subCategories', response.data.success.data.company_sub_categories);
                this.fillCustomData('companyTypes', response.data.success.data.Company_Legal_Types);
                this.fillCustomData('companyGroup', response.data.success.data.Companies);
                this.fillCustomData('countries', response.data.success.data.Countries);
                this.fillCustomData('currencies', response.data.success.data.Currencies);
            }
            else {
                this.props.errorAlert(JSON.stringify(response.data.error));
            }
            this.props.stopLoader();
        } catch (e) {
            this.props.logger.error('Company: ' + e);            
            this.props.stopLoader();
            this.props.errorAlert(this.UIText.errorOccured);
        }
    }
    fillCustomData(type: any, items: any) {
        if (!items || items.length == 0) {
            return;
        }
        var dataToFill = [];
        if (items) {
            Object.entries(items).map(([key, value]) => dataToFill.push({ value: value, id: key }));
        }
        this[type] = dataToFill;
    }
    async onSubmit(form) {   
        form.preventDefault();
        form.stopPropagation();
        var nameField = this.data.name;
        var shortNameField = this.data.shortName;
        var categoryField = this.data.category?.value;
        var requiredFields = {
            name: !nameField?.length,
            shortName: !shortNameField?.length,
            category: !categoryField?.length
        };
        this.setState({
            requiredFields: requiredFields
        });
        for (const [key, value] of Object.entries(requiredFields)) {
            if (value) {
                document.getElementById(key)?.scrollIntoView();
                return;
            }
        }
        this.props.startSaving();
        var name = this.data.name;
        var shortName = this.data.shortName;
        var category = this.data.category;
        var selectSubCategory = this.data.subCategory;
        var companyLegalType = this.data.companyType;
        var companyGroup = this.data.companyGroup;
        var nationality = this.data.nationality;
        var capital = this.data.capital;
        var currency = this.data.currency;
        var referenceNumber = this.data.referenceNumber;
        var foreignName = this.data.foreignName;
        var object = this.data.object;
        var commercialRegReleaseDate = this.data.commercialRelease;
        var commercialRegNb = this.data.commercialNumber;
        var cbPrivate = this.state.sharedWith;
        var sharedWith = this.data.sharedWith;
        var comments = this.data.comments;
        var addresses = this.getAllAdresses();
        if (addresses == null) {
            this.props.stopSaving();
            return;
        }

        const reqHeader = new RequestHeaders();

        var newCompany = {
            url: reqHeader.url,
            userKey: reqHeader.userKey,
            name: name,
            shortName: shortName,
            category: category?.id,
            subCategory: selectSubCategory?.id,
            companyLegalType: companyLegalType?.id,
            companyGroup: companyGroup?.id,
            nationality: nationality?.id,
            addresses: addresses,
            capital: capital,
            currency: currency?.value,
            referenceNumber: referenceNumber,
            foreignName: foreignName,
            objectVal: object,
            commercialRegReleaseDate: commercialRegReleaseDate,
            commercialRegNb: commercialRegNb,
            cbPrivate: cbPrivate,
            sharedWith: sharedWith,
            comments: comments
        }
        newCompany = Helpers.cleaningData(newCompany);

        var dataToPost = 'category=Internal&status=Active&private=' + (newCompany.cbPrivate ? 'yes' : 'no');
        if (newCompany.name != null)
            dataToPost += "&name=" + newCompany.name;
        if (newCompany.shortName != null)
            dataToPost += "&shortName=" + newCompany.shortName;
        if (newCompany.companyGroup)
            dataToPost += "&company_id=" + newCompany.companyGroup;
        if (newCompany.category != null)
            dataToPost += "&company_category_id=" + newCompany.category;
        if (newCompany.subCategory != null)
            dataToPost += "&company_sub_category_id=" + newCompany.subCategory;
        if (newCompany.referenceNumber != null)
            dataToPost += "&internalReference=" + newCompany.referenceNumber;
        if (newCompany.foreignName != null)
            dataToPost += '&foreignName=' + newCompany.foreignName;
        if (newCompany.nationality != null)
            dataToPost += "&nationality_id=" + newCompany.nationality;
        if (newCompany.companyLegalType != null)
            dataToPost += "&company_legal_type_id=" + newCompany.companyLegalType;
        if (newCompany.objectVal != null)
            dataToPost += "&object=" + newCompany.objectVal;
        if (newCompany.capital != null)
            dataToPost += "&capital=" + newCompany.capital;
        if (newCompany.currency != null)
            dataToPost += "&capitalCurrency=" + newCompany.currency;
        if (newCompany.commercialRegReleaseDate != null)
            dataToPost += "&registrationDate=" + newCompany.commercialRegReleaseDate;
        if (newCompany.commercialRegNb != null)
            dataToPost += "&registrationNb=" + newCompany.commercialRegNb;
        if (newCompany.comments != null)
            dataToPost += "&comments=" + newCompany.comments;
        if (newCompany.cbPrivate) {
            var privateArray = '';
            if (newCompany.sharedWith != null) {
                for (var i = 0; i < newCompany.sharedWith.length; i++) {
                    privateArray += `&Company_Users[${i}]=` + newCompany.sharedWith[i];
                }
            }
            if (privateArray.length > 0)
                dataToPost += privateArray;
        }
        if (newCompany.addresses != null)
            dataToPost += newCompany.addresses;

        var response = await AxiosRequest.post(References.addCompanyApi, dataToPost);
        try {
            var resp = response.data;
            var error = resp?.error;
            if (error != "") {
                var respError = Helpers.handleErrorResponse(error, 'company', form, this.onSubmit);
                if (respError != null && respError != '') {
                    this.props.errorAlert(JSON.stringify(respError));
                }
            }
            else {
                var companyId = resp?.success?.data?.company_id;
                this.setState({ id: companyId });
                this.props.openModal();
            }
        }
        catch (err) {
            this.props.logger.error('Company: ' + err);            
            this.props.errorAlert(this.UIText.errorOccured);
        }
        this.props.stopSaving();
    }
    getAllAdresses() {
        var addresses = this.data.addresses;
        var postData = '';
        var isValid = true;
        addresses?.forEach((item, index) => {
            var address = item?.address;            
            var city = item?.city;
            var state = item?.state;
            var country = item?.country?.id;
            var zip = item?.zip;
            var website = item?.website;
            var phone = item?.phone;
            var fax = item?.fax;
            var mobile = item?.mobile;
            var email = item?.email;
            if (address != null && address != "")
                postData += "&address_details[address][" + index + "]=" + address;
            if (city != null && city != "")
                postData += "&address_details[city][" + index + "]=" + city;
            if (state != null && state != "")
                postData += "&address_details[state][" + index + "]=" + state;
            if (country != null && country != "")
                postData += "&address_details[country][" + index + "]=" + country;
            if (zip != null && zip != "")
                postData += "&address_details[zip][" + index + "]=" + zip;
            if (website != null && website != "")
                postData += "&address_details[website][" + index + "]=" + website;
            if (phone != null && phone != "")
                postData += "&address_details[phone][" + index + "]=" + phone;
            if (fax != null && fax != "")
                postData += "&address_details[fax][" + index + "]=" + fax;
            if (mobile != null && mobile != "")
                postData += "&address_details[mobile][" + index + "]=" + mobile;
            if (email != null && email != "") {
                var isValidMail = Helpers.ValidateEmail(email);
                if (isValidMail)
                    postData += "&address_details[email][" + index + "]=" + email;
                else {
                    this.props.focusError('email' + index);
                    document.getElementById('email' + index)?.scrollIntoView();
                    isValid = false;
                }
            }
        });
        if (isValid)
            return postData;
        else
            return null;
    }
    handleDeleteAddress = (id) => {
        this.data.addresses = this.data.addresses.filter(address => address.id !== id);
        this.setState({ addresses: this.state.addresses.filter(address => address.id !== id) });
    }
    handleAddressData(index, addressData) {  
        if (this.data.addresses == null) {
            this.data.addresses = []
        }
        this.data.addresses[index] = { ...this.data.addresses[index], ...addressData };
    }
    handleSharedWithSwitch(event) {
        this.setState({ sharedWith: event.target.checked });
    }
    render() {
        const { classes } = this.props;        
        const UIText = this.UIText;
        return (<React.Fragment>
            <PopupAlert />
            <Modal type='company' id={this.state.id} />                
            {this.props.runLoader ?
                <FormSkeleton input='form' /> :
                <div className={classes.root}>
                    <div className={classes.paper}>
                        <form id="addCompany" className={classes.form} noValidate onSubmit={this.onSubmit.bind(this)}>
                                <Accordion defaultExpanded={true}>
                                    <AccordionSummary
                                        expandIcon={<ExpandMoreIcon />}
                                        aria-controls="panel1a-content"
                                        id="panel1a-header"
                                >
                                    <Grid container alignItems='center'>
                                        <Grid item xs={2}>
                                            <img src={require('../../assets/companyIcon.svg')} className={classes.icon} />
                                        </Grid>
                                        <Grid item xs>
                                            <Typography variant='h6' gutterBottom> {UIText.generalInformation} </Typography>
                                        </Grid>
                                    </Grid>
                                    </AccordionSummary>
                                    <AccordionDetails>
                                        <Grid container spacing={2}>
                                            <Grid item xs={12} sm={12} >
                                                <TextFieldWithValidation
                                                    handleChangeTextField={(event, value) => handleUniversalChangeTextField(this, event, value)}
                                                    type='name'
                                                    required
                                                    focus
                                                    error={this.state.requiredFields.name}
                                                    apiType='company'
                                                    label={UIText.name}
                                                />
                                                <TextFieldWithValidation
                                                    handleChangeTextField={(event, value) => handleUniversalChangeTextField(this, event, value)}
                                                    type='shortName'
                                                    required
                                                    error={this.state.requiredFields.shortName}
                                                    apiType='company'
                                                    label={UIText.shortName}
                                                />
                                            </Grid>
                                        <Grid item xs={12} sm={12} >
                                            <UniversalAutoComplete
                                                type='category'
                                                label={UIText.category}
                                                options={this.categories}
                                                onChange={(type, value) => handleUniversalChangeAutoComplete(this, type, value)}
                                                error={this.state.requiredFields.category}
                                                required
                                            />
                                            </Grid>
                                        <Grid item xs={12} sm={12} >
                                            <UniversalAutoComplete
                                                type='subCategory'
                                                label={UIText.subCategory}
                                                options={this.subCategories}
                                                onChange={(type, value) => handleUniversalChangeAutoComplete(this, type, value)}
                                            />
                                            </Grid>
                                        <Grid item xs={12} sm={12} >
                                            <UniversalAutoComplete
                                                type='companyType'
                                                label={UIText.companyType}
                                                options={this.companyTypes}
                                                onChange={(type, value) => handleUniversalChangeAutoComplete(this, type, value)}
                                            />
                                        </Grid>
                                        <Grid item xs={12} sm={12} >
                                            <UniversalAutoComplete
                                                type='companyGroup'
                                                label={UIText.companyGroup}
                                                options={this.companyGroup}
                                                onChange={(type, value) => handleUniversalChangeAutoComplete(this, type, value)}
                                            />
                                             
                                            </Grid>
                                        <Grid item xs={12} sm={12} >
                                            <UniversalAutoComplete
                                                type='nationality'
                                                label={UIText.nationality}
                                                options={this.countries}
                                                onChange={(type, value) => handleUniversalChangeAutoComplete(this, type, value)}
                                            />
                                        </Grid>
                                        </Grid>
                                    </AccordionDetails>
                                </Accordion>
                                <Accordion>
                                    <AccordionSummary
                                        expandIcon={<ExpandMoreIcon />}
                                        aria-controls="panel2a-content"
                                        id="panel2a-header"
                                >
                                    <Grid container alignItems='center'>
                                        <Grid item xs={2}>
                                            <img src={require('../../assets/addressIcon.svg')} className={classes.icon} />
                                        </Grid>
                                        <Grid item xs>
                                            <Typography variant='h6' gutterBottom>{UIText.address}</Typography>
                                        </Grid>
                                    </Grid>
                                    </AccordionSummary>
                                    <AccordionDetails>
                                    <AddressComponent logger={this.props.logger} index={0} dataToParent={this.handleAddressData} />
                                        {this.state.addresses.map((address, index) => {
                                            if (index != 0)
                                                return (<Wrappedaddress key={address.id} logger={this.props.logger} index={index} id={address.id} onChangeData={this.handleAddressData} onDelete={this.handleDeleteAddress}/>);
                                        })}
                                        <Grid item container xs justifyItems='center' style={{ marginTop: '10px' }}>
                                                <Button
                                                    type="button"
                                                    variant="contained"
                                                    color="primary"
                                                    style={{ margin: 'auto' }}
                                                    startIcon={<AddIcon />}
                                                    onClick={(event) => {
                                                        event.preventDefault();
                                                        event.stopPropagation();
                                                        const id = uniqid();
                                                        this.data.addresses?.push({ id: id });
                                                        this.setState({ addresses: [...this.state.addresses, { id: id }] });
                                                    }}
                                                >
                                                    {UIText.addAddress}
                                            </Button>
                                        </Grid>
                                    </AccordionDetails>
                                </Accordion>
                                <Accordion>
                                    <AccordionSummary
                                        expandIcon={<ExpandMoreIcon />}
                                        aria-controls="panel3a-content"
                                        id="panel3a-header"
                                >
                                    <Grid container alignItems='center'>
                                        <Grid item xs={2}>
                                            <img src={require('../../assets/moreFieldsIcon.svg')} className={classes.icon} />
                                        </Grid>
                                        <Grid item xs>
                                            <Typography variant='h6' gutterBottom>{UIText.moreFields}</Typography>
                                        </Grid>
                                    </Grid>
                                    </AccordionSummary>
                                    <AccordionDetails>
                                        <Grid container spacing={1}>
                                        <Grid item xs={12} sm={12}>
                                            <UniversalTextField
                                                type='capital'
                                                label={UIText.capital}
                                                onChange={(event, value) => handleUniversalChangeTextField(this, event, value)}
                                            />
                                        </Grid>
                                        <Grid item xs={12} sm={12}>
                                            <UniversalAutoComplete
                                                type='currency'
                                                label={UIText.currency}
                                                options={this.currencies}
                                                onChange={(type, value) => handleUniversalChangeAutoComplete(this, type, value)}
                                            />
                                        </Grid>
                                        <Grid item container xs={12}>
                                            <Grid item xs={12} sm={12}>
                                                <UniversalTextField
                                                    type='foreignName'
                                                    label={UIText.foreignName}
                                                    onChange={(event, value) => handleUniversalChangeTextField(this, event, value)}
                                                />
                                                <UniversalTextField
                                                    type='referenceNumber'
                                                    label={UIText.referenceNum}
                                                    helperText={UIText.referenceNumTip}
                                                    onChange={(event, value) => handleUniversalChangeTextField(this, event, value)}
                                                />
                                                <UniversalTextField
                                                    type='object'
                                                    label={UIText.object}
                                                    onChange={(event, value) => handleUniversalChangeTextField(this, event, value)}
                                                    multiline
                                                    rows={4}
                                                />
                                                <DateTextField
                                                    type='commercialRelease'
                                                    label={UIText.commercialRelease}
                                                    defaultValue={null}
                                                    onChange={(type, value) => handleUniversalChangeDateTimeField(this, type, value)}
                                                />
                                                <UniversalTextField
                                                    type='commercialNumber'
                                                    label={UIText.commercialNumber}
                                                    onChange={(event, value) => handleUniversalChangeTextField(this, event, value)}
                                                />
                                                <SwitchTextField
                                                    type='sharedWith'
                                                    firstLabel={UIText.everyone}
                                                    secondLabel={UIText.private}
                                                    onSwitchChange={this.handleSharedWithSwitch.bind(this)}
                                                    onChange={(type, value) => handleUniversalChangeAutoComplete(this, type, value)}
                                                    selected={this.state.sharedWith}
                                                    searchType='user'
                                                    searchUrl={References.usersLookupApi}
                                                    label={UIText.sharedWith}
                                                    multiple
                                                />
                                                <UniversalTextField
                                                    type='comments'
                                                    label={UIText.comments}
                                                    onChange={(event, value) => handleUniversalChangeTextField(this, event, value)}
                                                    multiline
                                                    rows={4}
                                                />
                                            </Grid>
                                        </Grid>
                                        </Grid>
                                    </AccordionDetails>
                                </Accordion>
                        </form>
                    </div>
                    <BottomBar onSubmit={ this.onSubmit} />
                </div >
            }
            </React.Fragment>
        );
    }
}
function Wrappedaddress(props) {
    const { language } = useSelector((state: ReduxState) => state.settings);
    const UIText = UIStrings.getLocaleStrings(language);
    return (<Accordion>
        <AccordionSummary
            expandIcon={<ExpandMoreIcon />}>
            <Grid container alignItems='center' direction='row'>
                <Grid item xs container direction='row' alignItems='flex-start' spacing={1}>
                    <Grid item style={{ marginTop: '3px' }}>
                        <img src={require('../../assets/addressIcon.svg')} style={{
                            width: '20px',
                            height: '20px'
                        }}/>
                    </Grid>
                    <Grid item>
                        <Typography variant='subtitle1' gutterBottom >
                            {UIText.address + ' ' + props.index}
                        </Typography>
                    </Grid>
                </Grid>
                <Grid item xs={2}>
                    <Tooltip title={UIText.remove} placement='top'>
                        <IconButton color='secondary' aria-label="delete" onClick={(e) => {
                            e.preventDefault();
                            e.stopPropagation();
                            props.onDelete(props.id);
                        }}>
                            <DeleteIcon fontSize="medium" />
                        </IconButton>
                    </Tooltip>
                </Grid>
            </Grid>
        </AccordionSummary>
        <AccordionDetails>
            <AddressComponent logger={props.logger} index={props.index} dataToParent={props.onChangeData} />
        </AccordionDetails>
    </Accordion>);
};
const mapStateToProps = state => ({
    language: state.settings.language,
    runLoader: state.loader.runLoader
});
const mapDispatchToProps = (dispatch) => {
    return {
        successAlert: (message: string) => dispatch(actions.successAlert(message)),
        errorAlert: (message: string) => dispatch(actions.errorAlert(message)),
        stopLoader: () => dispatch(actions.stopLoader()),
        startLoader: () => dispatch(actions.startLoader()),
        startSaving: () => dispatch(actions.startSaving()),
        stopSaving: () => dispatch(actions.stopSaving()),
        openModal: () => dispatch(actions.openModal()),
        focusError: (id: string) => dispatch(actions.focusError(id))
    }
};
type CompanyReduxProps = ReturnType<typeof mapStateToProps> & ReturnType<typeof mapDispatchToProps>;
interface ICompanyState {
    errorType,
    addresses: any[],
    sharedWith,
    requiredFields,
    id: string
}
interface ICompanyProps extends CompanyReduxProps, WithStyles<typeof useStyles>, AppProps{
    
}
const AppContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(Company);
export default withStyles(useStyles, { withTheme: true })(AppContainer);