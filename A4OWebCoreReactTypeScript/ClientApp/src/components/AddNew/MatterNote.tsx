﻿import * as React from "react";
import { withStyles, makeStyles } from "@material-ui/core/styles";
import { connect, useSelector, useDispatch } from "react-redux";
import Grid from "@material-ui/core/Grid";
import PopupAlert from '../Common/Alert';
import { AxiosRequest, RequestHeaders } from '../../Helpers/AxiosRequest';
import { References } from '../../Helpers/References';
import * as actions from '../../redux/actions';
import * as UIStrings from '../../UIStrings';
import { Typography, createStyles, AppBar, Toolbar, Autocomplete, Theme, WithStyles, Paper, Divider, Accordion, AccordionSummary, AccordionDetails, IconButton, Switch, FormControl, InputLabel, Input, Container, Button, Tooltip, FormControlLabel, Checkbox, FormGroup, ToggleButtonGroup, ToggleButton, ButtonGroup, CircularProgress, Modal } from "@material-ui/core";
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import InfoIcon from '@material-ui/icons/Info';
import SearchBox from '../Common/SearchBox';
import BottomBar from '../Common/BottomBar';
import { Helpers, handleUniversalChangeTextField, handleUniversalChangeDateTimeField, handleUniversalChangeAutoComplete, getAttachments, loadEmailAttachments, loadEmailProps } from "../../Helpers/Helpers";
import FormSkeleton from "../Common/Skeleton/FormSkeleton";
import FormatAlignJustifyIcon from '@material-ui/icons/FormatAlignJustify';
import { AppProps } from "../App";
import { UniversalAutoComplete, AutoCompleteWithTwoStage, UniversalTextField, DateTextField, SwitchTextField, TimeTextField, ContactCompanyComponent, UniversalRichTextEditor, AutoCompleteWithImage } from "../Common/TextFields";
import AttachFileIcon from '@material-ui/icons/AttachFile';
import AttachmentsArea from "../Common/Attachments";
import { ReduxState } from "../..";
import { getMessageAttachments, addMessageProperty } from "../../GraphService";
import DeleteIcon from '@material-ui/icons/Delete';
import * as GraphService from '../../GraphService';
import { TMUIRichTextEditorRef } from "mui-rte";
import { EditorState, convertToRaw, convertFromRaw, convertFromHTML, ContentState } from "draft-js";
import { stateToHTML } from "draft-js-export-html";
import FeaturedPlayListIcon from '@material-ui/icons/FeaturedPlayList';
import ShortTextIcon from '@material-ui/icons/ShortText';
import AvTimerIcon from '@material-ui/icons/AvTimer';
import ViewHeadlineIcon from '@material-ui/icons/ViewHeadline';
import UniversalSplitButton from "../Common/SplitButton";
import { v4 as uuidv4 } from 'uuid';
import { ActivityProps } from "../Activities/Activities";
import ImageIcon from '@material-ui/icons/Image';
import AddIcon from '@material-ui/icons/Add';
import * as PopModal from '../Common/Modal';
import { Array } from "core-js";
import { object } from "prop-types";
const useStyles = (theme: Theme) => createStyles({
    paper: {
        marginTop: theme?.spacing(2),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center'
    },
    avatar: {
        margin: theme?.spacing(1),
        backgroundColor: 'blue',
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme?.spacing(3),
        marginBottom: theme?.spacing(8)
    },
    submit: {
        color: 'red'
    },
    textarea: {
        resize: "both",
        minHeight: "200px"
    },
    formControl: {
        margin: theme?.spacing(1),
        minWidth: 120,
    },
    selectEmpty: {
        marginTop: theme?.spacing(2),
    },
    root: {
        display: 'flex',
        flexGrow: 1,
        flexDirection: 'column'
    },
    main: {
        marginBottom: theme?.spacing(8),
    },
    footer: {
        position: 'fixed',
        bottom: "0",
        marginTop: 'auto',
        width: "100%",
        backgroundColor: '#fff',
    },
    margin: {
        margin: theme?.spacing(1),
    },
    textField: {
        width: '25ch',
    },
    appBar: {
        position: 'fixed',
        height: '55px',
        paddingBottom: '10px',
        bottom: 0,
    },
    addOpponent: {
        margin: theme?.spacing(1),
    },
    emailPref: {
        border: '1px solid lightGrey',
        borderRadius: '5px',
    },
    label: {
        border: '1px solid white',
        padding: '10px',
        borderRadius: '5px',
        backgroundColor: '#4cb0fc'
    },
    icon: {
        width: '20px',
        height: '20px'
    }
});
interface IEmailItemProps {
    value: string,
    checked: boolean
}
export interface IEmailProps {
    emailBody: IEmailItemProps,
    emailHeader: IEmailItemProps,
    emailTimeStamp: IEmailItemProps,
    emailtTo: string,
    emailFrom: string,
    emailFromName: string,
    emailSubject: string,
    emailDate: Date
}

var statusData = {};

class MatterNote extends React.Component<IFormProps, IFormState> {

    attachments: any[];
    messageID: string;
    data: any;
    emailInfo: IEmailProps;
    editorState: EditorState;
    transitionId: string;
    fieldsData: FieldProps[];
    enableHeaderTimeStamp: boolean;
    constructor(props: IFormProps, context: any) {
        super(props, context);
        this.state = {
            emailInfoValues: ["emailInlineImages", "emailBody"],
            matterSelected: false,
            caseValues: {
                id: null,
                status: null,
                priority: null,
                closedDate: new Date()
            },
            noteContent: '',
            caseStatuses: [],
            matter: null,
            errorType: false,
            changingStatus: false,
            requiredFields: {
                matter: false,
            },
            id: '',
            openModal: false
        };
        this.attachments = [];
        this.messageID = '';
        this.data = {};
        this.transitionId = '';
        this.fieldsData = [];
        this.enableHeaderTimeStamp = false;
        this.handlePreferencesChange = this.handlePreferencesChange.bind(this);
        this.handleCaseChanged = this.handleCaseChanged.bind(this);
        this.handleStatusChanged = this.handleStatusChanged.bind(this);
        this.handleAddScreenTransition = this.handleAddScreenTransition.bind(this);
        this.onSubmit = this.onSubmit.bind(this);

        try {
            const preferencesSettings = Office.context.roamingSettings.get('preferences');
            if (preferencesSettings) {
                Object.keys(preferencesSettings).forEach(key => {
                    if (key === 'enableHeaderTimeStamp') {
                        const value = preferencesSettings[key];
                        if (value === 'true') {
                            this.enableHeaderTimeStamp = true;                            
                        }
                    }
                });
            }
        }
        catch (err) {

        }
    }
    componentDidMount() {
        this.props.startLoader();
        this.getMailItem();
        window.addEventListener('includeEmailAttachment', this.handleChangeEmailAttachment.bind(this));
        statusData = {};
        if (this.enableHeaderTimeStamp)
            this.setState({ emailInfoValues: [...this.state.emailInfoValues, 'emailHeader', 'emailTimeStamp'] })
    }
    componentWillUnmount() {
        window.removeEventListener('includeEmailAttachment', this.handleChangeEmailAttachment);
    }
    handleChangeEmailAttachment(event) {
        if (event) {
            var includeEmailAttachments = event.detail;
            if (includeEmailAttachments) {
                this.props.addFile(this.attachments);
            }
            else {
                var files = this.props.files.filter(file => file.isEmailAttachment == false);
                this.props.setAttachments(files);
            }
        }
    }
    addAttachments(files: any[]) {
        if (files && files.length > 0) {
            this.attachments = this.attachments.concat(files);
        }
    }
    UIText = UIStrings.getLocaleStrings(this.props.language);
    getMailItem() {
        try {
            var item = Office.context.mailbox.item;
            var self = this;
            item.body.getAsync(Office.CoercionType.Text, function (asyncResult) {
                if (asyncResult.status !== Office.AsyncResultStatus.Succeeded) {
                }
                else {
                    self.emailInfo = loadEmailProps(item, asyncResult);
                    if (!Helpers.getUrlPar('isNew')) {
                        var initialHtml: string = '';
                        if (self.enableHeaderTimeStamp)
                            initialHtml = self.emailInfo.emailHeader.value + self.emailInfo.emailTimeStamp.value; 
                        initialHtml += self.emailInfo.emailBody.value;
                        const contentHTML = convertFromHTML(initialHtml);
                        const state = ContentState.createFromBlockArray(contentHTML.contentBlocks, contentHTML.entityMap)
                        const content = JSON.stringify(convertToRaw(state))
                        self.setState({ noteContent: content });
                    }
                    self.messageID = item.itemId;
                    self.props.stopLoader();
                    loadEmailAttachments(self);
                }
            });
        } catch (e) {
            this.props.logger.error('Matter Note: ' + e);
        }
    }
    async onSubmit(form) {
        form.preventDefault();
        form.stopPropagation();
        var description = stateToHTML(this.editorState?.getCurrentContent());
        var requiredFields = {
            matter: !this.data.matter?.id?.length,
        };
        this.setState({
            requiredFields: requiredFields
        });
        for (const [key, value] of Object.entries(requiredFields)) {
            if (value) {
                document.getElementById(key)?.scrollIntoView();
                return;
            }
        }
        this.props.startSaving();
        var matter = this.data.matter?.id;
        var note = description;
        note = note.replace(new RegExp("<(br|BR)>", "gm"), "\n");
        note = note.replace(new RegExp("\\&lt;", "gm"), "");
        note = note.replace(new RegExp("\\&gt;", "gm"), "");
        note = note.replace(new RegExp("\\gt;", "gm"), "");
        note = note.replace(new RegExp("\\lt;", "gm"), "");
        note = note.replace(new RegExp("\\&amp;", "gm"), "and");
        note = note.replace(new RegExp("<(br|BR)>", "gm"), "and");
        note = note.replace(new RegExp("\\=", "gm"), "equal");
        note = note.replace(/<img .*?>/g, '');
        note = note.replace(/&nbsp;/gi, '');
        note = encodeURI(note);
        var dataToPost = 'isVisibleToCP=no';
        if (matter != null)
            dataToPost += "&case_id=" + matter;
        if (note != null)
            dataToPost += "&comment=" + note + '\n\n\n' + this.UIText.addedByCompany;
        if (this.emailInfo != null) {
            dataToPost += "&email_to=" + this.emailInfo.emailtTo;
            dataToPost += "&email_from=" + (this.emailInfo.emailFrom.length > 255 ? this.emailInfo.emailFrom.substring(0, 254) : this.emailInfo.emailFrom);
            dataToPost += "&email_from_name=" + this.emailInfo.emailFromName;
            dataToPost += "&email_date=" + Helpers.formatDate(this.emailInfo.emailDate, true);
            dataToPost += "&email_subject=" + this.emailInfo.emailSubject;
        }

        try {

            const reqHeader = new RequestHeaders();
            var formData = new FormData();
            const files = await getAttachments(this);
            files.forEach(file => {
                formData.append('Files', file);
            });
            formData.append('Url', reqHeader.url + References.addEmailMatterNoteApi);
            formData.append('DataToPost', dataToPost);
            formData.append('UserKey', reqHeader.userKey);
            var uploadResponse = await AxiosRequest.postToServer('PostData/UploadFileToApp4legal', formData);
            if (uploadResponse.status != 200) {
                this.props.errorAlert(this.UIText.errorUploadingFiles);
            }
            const resp = uploadResponse.data;
            if (resp?.success?.msg && resp?.success?.msg !== '') {
                this.setState({ id: matter });
                var propertyData: ActivityProps = {
                    guid: uuidv4(),
                    type: 'matterNote',
                    id: matter,
                    subject: this.data.matter?.subject,
                    date: Helpers.formatDate(new Date(), true),
                    priority: this.state.caseValues.priority
                }
                const token = await this.props.getAccessToken();
                await addMessageProperty(token, this.messageID, propertyData);
                this.props.openModal();
                if (resp?.error !== '') {
                    this.props.errorAlert(JSON.stringify(resp?.error));
                }
            }
            else {
                this.props.errorAlert(JSON.stringify(resp?.error));
            }
        }
        catch (err) {
            this.props.logger.error('Matter Note: ' + err);
            this.props.errorAlert(this.UIText.errorOccured);
        }
        this.props.stopSaving();
    }
    async handleCaseChanged(caseId) {
        if (caseId) {
            var response = await AxiosRequest.get(References.editMatterApi + '/' + caseId);
            try {
                var resp = response.data;
                var error = resp?.error;
                if (error != "") {
                    var respError = Helpers.handleErrorResponse(error, 'matterNote', caseId, this.handleCaseChanged);
                    if (respError != null && respError != '') {
                        this.props.errorAlert(JSON.stringify(respError));
                    }
                }
                else {
                    var caseStatuses = resp.success?.data?.formData.case_statuses;
                    var caseStatusId = resp.success?.data?.caseValues?.case_status_id;
                    var matterStatus = resp.success?.data?.caseValues?.status;
                    var closedOnDate = resp.success?.data?.caseValues?.closedOn;
                    var matterPriority = resp.success?.data?.caseValues?.priority;
                    var id = resp.success?.data?.caseValues?.id;
                    this.setState({
                        caseStatuses: caseStatuses,
                        caseValues: {
                            id: id,
                            closedDate: closedOnDate,
                            status: {
                                name: matterStatus,
                                id: caseStatusId
                            },
                            priority: matterPriority
                        }
                    });
                }
            }
            catch (err) {
                this.props.logger.error('Matter Note: ' + err);
            }
        }
    }
    handlePreferencesChange(event: object, value: string[]) {
        this.setState({ emailInfoValues: value });
        var header = '';
        var time = '';
        var body = ''
        value.forEach(v => {
            switch (v) {
                case 'emailHeader': header = this.emailInfo[v]?.value; break;
                case 'emailTimeStamp': time = this.emailInfo[v]?.value; break;
                case 'emailBody': body = this.emailInfo[v]?.value; break;
                default: break;
            }
        });
        if (value.includes('emailInlineImages')) {
            this.props.addFile(this.attachments.filter(file => file.isInline));
        }
        else {
            var files = this.props.files.filter(file => file.isInline == false);
            this.props.setAttachments(files);
        }
        const contentHTML = convertFromHTML(header + time + body);
        const state = ContentState.createFromBlockArray(contentHTML.contentBlocks, contentHTML.entityMap)
        const content = JSON.stringify(convertToRaw(state))
        this.setState({ noteContent: content });
    }
    async handleStatusChanged(statusId) {
        this.setState({ changingStatus: true });
        const response = await AxiosRequest.get(References.moveStatusApi + `/${this.state.caseValues.id}/${statusId}`)
            .catch(err => {
                this.props.logger.error('Matter Note: ' + err);
                this.props.errorAlert(this.UIText.somethingWentWrong)
            });
        try {
            const success = response.data?.success;
            const error = response.data?.error;
            if (error) {
                this.props.errorAlert(error);
            }
            else {
                if (success && success != '') {
                    this.transitionId = success.transition_id;
                    const successData = success.data;
                    if (successData) {
                        if (!Array.isArray(successData)) {
                            var fields: any[] = [];
                            this.fieldsData = [];
                            statusData = {};
                            fields = successData.fields;
                            if (fields.length > 0) {
                                fields.forEach(field => {
                                    this.fieldsData.push(
                                        {
                                            id: field.id,
                                            type: field.field_name,
                                            label: field.field_label,
                                            fieldType: field.field_type,
                                            hiddenValue: field.field_hidden_value,
                                            hiddenName: field.field_hidden_name,
                                            hiddenRecord: field.field_hidden_record,
                                            typeData: field.type_data,
                                            value: field.field_value,
                                            required: field.required,
                                            options: field.field_options,
                                            records: field.records
                                        });
                                });
                                this.setState({ openModal: true });
                            }
                        }
                        else {
                            this.props.successAlert(this.UIText.recordSaved);
                        }
                    }
                }
            }
            this.handleCaseChanged(this.state.caseValues.id);
        }
        catch (err) {
            this.props.logger.error('Matter Note: ' + err);
        }
        this.setState({ changingStatus: false });
    }
    async handleAddScreenTransition(data) {
        if (data) {
            data = { ...data, transition_id: this.transitionId, case_id: this.state.caseValues.id };
            var dataToPost = '';
            Object.entries(data).map(([key, value]) => dataToPost += `&${key}=${value}`);
            dataToPost = dataToPost.substring(1, dataToPost.length);
            const response = await AxiosRequest.post(References.addScreenTransitionApi, dataToPost);
            const error = response.data?.error;
            if (error != '') {
                if (typeof error == 'object') {
                    this.props.errorAlert(JSON.stringify(error));
                }
                else {
                    this.props.errorAlert(this.UIText.somethingWentWrong);
                }
            }
            else {
                this.props.successAlert(this.UIText.recordSaved);
                this.handleCaseChanged(this.state.caseValues.id);
                this.setState({ openModal: false });
            }
        }
    }
    render() {
        const { classes } = this.props;
        const UIText = this.UIText;
        return (<React.Fragment>
            <PopupAlert />
            <PopModal.default type='matterNote' id={this.state.id} />
            <ScreenModal caseId={this.state.caseValues.id} open={this.state.openModal} closeModal={() => { this.setState({ openModal: false }) }} onClose={null} fields={this.fieldsData} saveData={this.handleAddScreenTransition} />
            {this.props.runLoader ?
                <FormSkeleton input='form' /> :
                <div className={classes.root}>
                    <div className={classes.paper}>
                        <form id='matterNote' className={classes.form} noValidate onSubmit={this.onSubmit.bind(this)}>
                            <Accordion defaultExpanded={true}>
                                <AccordionSummary
                                    expandIcon={<ExpandMoreIcon />}
                                    aria-controls="panel1a-content"
                                    id="panel1a-header"
                                >
                                    <Grid container alignItems='center'>
                                        <Grid item xs={2}>
                                            <img src={require('../../assets/matterNoteIcon.svg')} className={classes.icon} />
                                        </Grid>
                                        <Grid item xs>
                                            <Typography variant='h6' gutterBottom> {UIText.noteDetails} </Typography>
                                        </Grid>
                                    </Grid>
                                </AccordionSummary>
                                <AccordionDetails>
                                    <Grid container spacing={2} >
                                        <Grid item xs={12}>
                                            <SearchBox
                                                url={References.casesLookupApi}
                                                required
                                                extraData='&category=Litigation_Matter'
                                                error={this.state.requiredFields.matter}
                                                apiType='matter'
                                                hint={UIText.startTypingMatter}
                                                type='matter'
                                                title={UIText.matter}
                                                handleChangeAutoComplete={(type, value) => {
                                                    this.setState({ matterSelected: value != null });
                                                    var caseId = value?.id;
                                                    this.handleCaseChanged(caseId);
                                                    handleUniversalChangeAutoComplete(this, type, value);
                                                }}
                                            />
                                        </Grid>
                                        <Grid item xs={12}>
                                            <Typography style={{ color: this.state.requiredFields.description ? 'red' : 'inherit' }}>{UIText.note}</Typography>
                                            <Grid item container xs alignItems='center' justifyItems='center' className={classes.emailPref}>
                                                <Grid item xs={12}>
                                                    <ToggleButtonGroup
                                                        value={this.state.emailInfoValues}
                                                        onChange={this.handlePreferencesChange}
                                                        style={{ width: '100%' }}
                                                        aria-label="email-preferences"
                                                        size='large'>
                                                        <ToggleButton value="emailBody" aria-label='body' style={{ width: '25%' }}>
                                                            <Tooltip title={UIText.addEmailBody} placement='top'>
                                                                <Grid container direction='column' justifyItems='center' alignItems='center'>
                                                                    <ViewHeadlineIcon />
                                                                    <Typography variant='caption'>
                                                                        {UIText.body}
                                                                    </Typography>
                                                                </Grid>
                                                            </Tooltip>
                                                        </ToggleButton>
                                                        <ToggleButton value="emailInlineImages" aria-label='images' style={{ width: '25%' }}>
                                                            <Tooltip title={UIText.addEmailInlineImages} placement='top'>
                                                                <Grid container direction='column' justifyItems='center' alignItems='center'>
                                                                    <ImageIcon />
                                                                    <Typography variant='caption'>
                                                                        {UIText.images}
                                                                    </Typography>
                                                                </Grid>
                                                            </Tooltip>
                                                        </ToggleButton>
                                                        <ToggleButton value="emailHeader" aria-label='header' style={{ width: '25%' }}>
                                                            <Tooltip title={UIText.addEmailHeader} placement='top'>
                                                                <Grid container direction='column' justifyItems='center' alignItems='center'>
                                                                    <ShortTextIcon />
                                                                    <Typography variant='caption'>
                                                                        {UIText.header}
                                                                    </Typography>
                                                                </Grid>
                                                            </Tooltip>
                                                        </ToggleButton>
                                                        <ToggleButton value="emailTimeStamp" aria-label='timeStamp' style={{ width: '26%' }}>
                                                            <Tooltip title={UIText.addEmailTimeStamp} placement='top'>
                                                                <Grid container direction='column' justifyItems='center' alignItems='center'>
                                                                    <AvTimerIcon />
                                                                    <Typography variant='caption'>
                                                                        {UIText.timeStamp}
                                                                    </Typography>
                                                                </Grid>
                                                            </Tooltip>
                                                        </ToggleButton>
                                                    </ToggleButtonGroup>
                                                </Grid>
                                            </Grid>
                                            <UniversalRichTextEditor
                                                type='contacts'
                                                onChange={(state: EditorState) => {
                                                    this.editorState = state;
                                                }}
                                                defaultValue={this.state.noteContent}
                                                token={this.props.getAccessToken}
                                            />
                                        </Grid>
                                    </Grid>
                                </AccordionDetails>
                            </Accordion>
                            {this.state.matterSelected ?
                                <Accordion>
                                    <AccordionSummary
                                        expandIcon={<ExpandMoreIcon />}
                                        aria-controls="panel2a-content"
                                        id="panel2a-header"
                                    >
                                        <Grid container alignItems='center'>
                                            <Grid item xs={2}>
                                                <img src={require('../../assets/corporateMatterIcon.svg')} className={classes.icon} />
                                            </Grid>
                                            <Grid item xs>
                                                <Typography variant='h6' gutterBottom>{UIText.matterDetails}</Typography>
                                            </Grid>
                                        </Grid>
                                    </AccordionSummary>
                                    <AccordionDetails>
                                        <Grid container spacing={2}>
                                            <Grid item xs container alignItems='center' justifyItems='space-between'>
                                                <Grid item xs={6}>
                                                    <Typography>{UIText.matterStatus}</Typography>
                                                </Grid>
                                                <Grid item container justifyItems='flex-end' xs={6}>
                                                    {this.state.changingStatus ?
                                                        <CircularProgress /> :
                                                        <UniversalSplitButton type='caseStatus' currentValue={this.state.caseValues.status} options={this.state.caseStatuses} handleItemClicked={this.handleStatusChanged} />
                                                    }
                                                </Grid>
                                            </Grid>
                                            <Grid item xs={12}>
                                                <DateTextField
                                                    type='closedDate'
                                                    value={this.state.caseValues.closedDate}
                                                    label={UIText.closedOn}
                                                    onChange={async (type, value) => {
                                                        const date = Helpers.formatDate(value);
                                                        const dataToPost = `id=${this.state.caseValues.id}&closedOn=${date}`;
                                                        const response = await AxiosRequest.post(References.editMatterSingleFieldApi, dataToPost);
                                                        if (response.data?.error != '') {
                                                            this.props.errorAlert(response.data.error);
                                                        }
                                                        else {
                                                            this.props.successAlert(UIText.recordSaved);
                                                            this.setState({ caseValues: { ...this.state.caseValues, closedDate: value } });
                                                        }
                                                    }}
                                                />
                                            </Grid>
                                        </Grid>
                                    </AccordionDetails>
                                </Accordion> : <></>}
                            <Accordion>
                                <AccordionSummary
                                    expandIcon={<ExpandMoreIcon />}
                                    aria-controls="panel3a-content"
                                    id="panel3a-header"
                                >
                                    <Grid container alignItems='center'>
                                        <Grid item xs={2}>
                                            <img src={require('../../assets/attachmentsIcon.svg')} className={classes.icon} />
                                        </Grid>
                                        <Grid item xs>
                                            <Typography variant='h6' gutterBottom>{UIText.attachments}</Typography>
                                        </Grid>
                                    </Grid>
                                </AccordionSummary>
                                <AccordionDetails>
                                    <AttachmentsArea token={this.props.getAccessToken()} onAddAttachments={this.addAttachments.bind(this)} />
                                </AccordionDetails>
                            </Accordion>
                        </form>
                    </div>
                    <BottomBar onSubmit={this.onSubmit} loading={this.state.changingStatus} />
                </div >
            }
        </React.Fragment>
        );
    }
}
const mapStateToProps = (state: ReduxState) => ({
    language: state.settings.language,
    runLoader: state.loader.runLoader,
    includeEmail: state.attachments.includeEmail,
    files: state.attachments.files,
    moreEmails: state.attachments.moreEmails,
    includeEmailAttachments: state.attachments.includeEmailAttachments
});
const mapDispatchToProps = (dispatch) => {
    return {
        successAlert: (message: string) => dispatch(actions.successAlert(message)),
        errorAlert: (message: string) => dispatch(actions.errorAlert(message)),
        stopLoader: () => dispatch(actions.stopLoader()),
        startLoader: () => dispatch(actions.startLoader()),
        startSaving: () => dispatch(actions.startSaving()),
        stopSaving: () => dispatch(actions.stopSaving()),
        openModal: () => dispatch(actions.openModal()),
        focusError: (id: string) => dispatch(actions.focusError(id)),
        setAttachments: (files) => dispatch(actions.setAttachments(files)),
        addFile: (file) => dispatch(actions.addFile(file)),
        deleteFile: (index) => dispatch(actions.deleteFile(index)),
        setHasEmailAttachments: (found: boolean) => dispatch(actions.setEmailHasAttachments(found)),
        setFetchingAttachments: (status: boolean) => dispatch(actions.setFetchingAttachments(status))
    }
};
type FormReduxProps = ReturnType<typeof mapStateToProps> & ReturnType<typeof mapDispatchToProps>;
interface IFormState {
    emailInfoValues: string[],
    caseValues: any;
    matterSelected: boolean,
    noteContent: string,
    caseStatuses: [],
    matter: any,
    changingStatus: boolean;
    errorType,
    requiredFields,
    id: string,
    openModal: boolean;
}
interface IFormProps extends FormReduxProps, WithStyles<typeof useStyles>, AppProps {

}
const AppContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(MatterNote);
export default withStyles(useStyles, { withTheme: true })(AppContainer);

const modalStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            overflow: 'auto',
            height: '100%'
        },
        form: {
            paddingTop: '5px',
            paddingBottom: '65px'
        },
        bottomBar: {
            position: 'fixed',
            height: '65px',
            bottom: '0px',
            width: '100%',
        }
    }),
);
export function ScreenModal(props: ModalProps) {
    const language = useSelector((state: ReduxState) => state.settings.language);
    const UIText = UIStrings.getLocaleStrings(language);
    const [lookPerType, setLookPerType] = React.useState({});
    const [saving, setSaving] = React.useState(false);
    const [requiredFields, setRequiredFields] = React.useState({});
    const classes = modalStyles();
    const handleChangeTextField = (event: React.ChangeEvent<HTMLInputElement>) => {
        const type = event.target.name;
        const value = event.target.value;
        statusData[type] = value;
    };
    const handleChangeDateField = (type, value) => {
        statusData[type] = Helpers.formatDate(value);
    };
    const handleChangeAutoComplete = async (type: string, value) => {

        if (type.includes('provider_group_id')) {
            if (value != null) {
                var dataToPost = 'provider_group_id=' + value?.id;
                var response = await AxiosRequest.post(References.usersLookupApi, dataToPost);
                if (response?.data?.error === "") {
                    var users = response.data.success.data;
                    var data = users.map(user => { return ({ value: user.firstName + ' ' + user.lastName, id: user.id }) });
                    setLookPerType({ ...lookPerType, user_id: data });
                }
            }
        }
        if (Array.isArray(value)) {
            value.forEach((v, index) => {
                statusData[type.replace('[]', `[${index}]`)] = v?.id;
            })
        }
        else {
            value = value?.id;
            statusData[type] = value;
        }
    };
    const handleMultipleLookUp = (index: number, type: string, value: any) => {
        statusData[type] = value?.id;
    };
    const handleSaveData = async () => {
        var initRequiredFields = {};
        props.fields.forEach(field => {
            const id = field.type;
            const element = document.getElementById(id);
            if (element) {
                const required = element['required'];
                const value = element['value'];
                if (required)
                    initRequiredFields = { ...initRequiredFields, [id]: !(value != null && value?.length > 0) };
            }
        });
        var missingFields = false;
        Object.entries(initRequiredFields).map(([key, value]) => {
            if (value) {
                missingFields = true;
                document.getElementById(key)?.scrollIntoView();
                return;
            }
        });
        setRequiredFields(initRequiredFields);
        if (!missingFields) {
            await props.saveData(statusData);
        }
        setSaving(false);
    }
    const getLookUpUrl = (type) => {
        switch (type) {
            case 'contacts': case 'contact': return References.contactLookupApi;
            case 'company': case 'companies': return References.companiesAutoCompleteApi;
            default: return null;
        }
    }
    const setFieldTypeScreen = (field: FieldProps) => {
        switch (field.fieldType) {
            case 'short_text': return <UniversalTextField
                required={field.required?.length > 0}
                error={requiredFields[field.type]}
                label={field.label}
                type={field.type}
                defaultValue={field.value}
                onChange={handleChangeTextField} />;
            case 'long_text': return <UniversalTextField
                required={field.required?.length > 0}
                error={requiredFields[field.type]}
                label={field.label}
                type={field.type}
                defaultValue={field.value}
                multiline
                rows={4}
                onChange={handleChangeTextField} />;
            case 'date': return <DateTextField
                type={field.type}
                defaultValue={field.value}
                label={field.label}
                required={field.required?.length > 0}
                error={requiredFields[field.type]}
                onChange={(type, value) => {
                    handleChangeDateField(type, value);
                }}
            />;
            case 'list': {
                var dataToFill = [];
                if (field.options) {
                    if (field.id === 'priority') {
                        Object.entries(field.options).map(([key, value]) => dataToFill.push({ value: value, id: key, image: value + '.png' }));
                    }
                    else {
                        Object.entries(field.options).map(([key, value]) => dataToFill.push({ value: value, id: key }));
                    }
                }
                var selectedValue = null;
                if (field.value && field.value.length > 0) {
                    selectedValue = dataToFill.filter(item => item?.id === field.value)[0];
                }
                if (field.id === 'user_id' && lookPerType['user_id'] != null) {
                    dataToFill = lookPerType['user_id'];
                }
                return <UniversalAutoComplete
                    type={field.type}
                    label={field.label}
                    options={field.id === 'user_id' ? (lookPerType['user_id'] || dataToFill) : dataToFill}
                    controlled={selectedValue != null}
                    defaultValue={selectedValue}
                    required={field.required?.length > 0}
                    error={requiredFields[field.type]}
                    onChange={handleChangeAutoComplete}
                />;
            }
            case 'single_lookup': {
                var selectedValue;
                const value = field.value;
                if (value) {
                    if (typeof value == 'string') {
                        selectedValue = { id: field.hiddenValue, name: field.value };
                    }
                    else {

                        selectedValue = [];
                        Object.entries(value).map(([key, val]) => selectedValue.push({ name: val, id: key }));
                    }
                }
                return <SearchBox
                    url={getLookUpUrl(field.typeData)}
                    required={field.required?.length > 0}
                    error={requiredFields[field.type]}
                    apiType={field.typeData}
                    hint={UIText.startTyping}
                    defaultValue={selectedValue}
                    type={field.hiddenName}
                    title={field.label}
                    handleChangeAutoComplete={handleChangeAutoComplete}
                />
            }
            case 'lookup_per_type': {
                var dataToFill = [];
                if (field.options) {
                    Object.entries(field.options).map(([key, value]) => dataToFill.push({ value: value, id: key }));
                }
                var typeValue = null;
                var nameValue = null;
                if (field.value && field.value.length > 0) {
                    typeValue = field.value[0];
                    nameValue = field.value[1];
                }
                if (nameValue) {
                    nameValue = {
                        id: field.hiddenValue,
                        name: nameValue
                    };
                }
                var selectedValue = null;
                if (typeValue) {
                    selectedValue = dataToFill.filter(item => item?.id === typeValue)[0];
                }
                return <>
                    <Grid item xs={12}>
                        <UniversalAutoComplete
                            type={field.type}
                            label={field.label}
                            defaultValue={selectedValue}
                            controlled={selectedValue != null}
                            required={field.required?.length > 0}
                            error={requiredFields[field.type]}
                            options={dataToFill}
                            onChange={(type, value) => {
                                setLookPerType({ ...lookPerType, [field.id]: value });
                                handleChangeAutoComplete(type, value);
                            }}
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <SearchBox
                            url={getLookUpUrl(lookPerType[`${field.id}`] ? lookPerType[`${field.id}`].id : typeValue)}
                            required={field.required?.length > 0}
                            error={requiredFields[field.hiddenName]}
                            apiType={lookPerType[`${field.id}`]?.id ? lookPerType[`${field.id}`]?.id : typeValue}
                            hint={UIText.startTyping}
                            defaultValue={nameValue}
                            type={field.hiddenName}
                            title={field.label}
                            handleChangeAutoComplete={handleChangeAutoComplete}
                        />
                    </Grid>
                </>
            }
            case 'multiple_lookup_per_type': {
                var dataToFill = [];
                if (field.options) {
                    Object.entries(field.options).map(([key, value]) => dataToFill.push({ value: value, id: key }));
                }
                return <Grid item xs={12}>
                    {lookPerType[`${field.id}`]?.map((record, index) => {
                        return (<Opponent
                            key={index}
                            index={index}
                            typeOptions={dataToFill}
                            label={field.label}
                            typeDefaultValue={record.type}
                            valueDefaultValue={record.value}
                            typeType={field.type[1].replace('[]', `[${index}]`)}
                            valueType={field.type[0].replace('[]', `[${index}]`)}
                            onChange={handleMultipleLookUp}
                            opponentCount={lookPerType[`${field.id}`]?.length}
                            onDelete={(index) => {
                                var records = lookPerType[`${field.id}`];
                                records.splice(index, 1);
                                //this.data.opponents?.splice(index, 1);
                                setLookPerType({ ...lookPerType, [field.id]: records });
                            }}
                        />
                        );
                    })}
                    <Grid item container xs={12} justifyItems='center' style={{ marginTop: '10px' }}>
                        <Button
                            type="button"
                            variant="contained"
                            color="primary"
                            style={{ margin: 'auto' }}
                            startIcon={<AddIcon />}
                            onClick={(event) => {
                                event.preventDefault();
                                event.stopPropagation();
                                var records: any[] = lookPerType[`${field.id}`];
                                records.push({ type: dataToFill[0], value: null });
                                setLookPerType({ ...lookPerType, [field.id]: records })
                            }}
                        >
                            {UIText.addMore}
                        </Button>
                    </Grid>
                </Grid>
            }
            case 'lookup': {
                var selectedValue;
                const value = field.value;
                if (typeof value == 'string') {
                    selectedValue = { id: field.hiddenValue, name: field.value };
                }
                else {
                    selectedValue = [];
                    Object.entries(value).map(([key, val]) => selectedValue.push({ name: val, id: key }));
                }
                return <SearchBox
                    url={getLookUpUrl(field.typeData)}
                    required={field.required?.length > 0}
                    error={requiredFields[field.type]}
                    apiType={field.typeData === 'contacts' ? 'client' : field.typeData}
                    hint={UIText.startTyping}
                    defaultValue={selectedValue}
                    multiple={!(typeof value == 'string')}
                    type={field.type}
                    title={field.label}
                    handleChangeAutoComplete={handleChangeAutoComplete}
                />
            }
        }
    }
    React.useEffect(() => {
        function fetchFields() {
            const fields = props.fields;
            fields.forEach(field => {
                if (field.fieldType === 'lookup_per_type') {
                    const values: any[] = field.value;
                    statusData = { ...statusData, [field.type]: values[0], [field.hiddenName]: values[1] };
                }
                else {
                    if (field.fieldType === 'multiple_lookup_per_type') {
                        const types: any[] = field.type;
                        const rcrds = field.records;
                        for (let i = 0; i < rcrds.length; i++) {
                            for (let j = 0; j < types.length; j++) {
                                const r = rcrds[i];
                                if (r) {
                                    statusData = { ...statusData, [types[j].replace('[]', `[${i}]`)]: r[j] };
                                }
                            }
                        }
                        var records = [];
                        if (field.records?.length > 0) {
                            var dataToFill = [];
                            if (field.options) {
                                Object.entries(field.options).map(([key, value]) => dataToFill.push({ value: value, id: key }));
                            }
                            if (field.records.length > 0) {
                                field.records.forEach((record: any[], index) => {
                                    try {
                                        const typeValue = record[1];
                                        if (typeValue) {
                                            const selectedType = dataToFill.filter(item => item.id === typeValue)[0];
                                            const selectedValue = { id: record[0], name: record[2] };
                                            records.push({ type: selectedType, value: selectedValue });
                                        }
                                    }
                                    catch (err) {
                                        this.props.logger.error('Matter Note: ' + err);
                                    }
                                });
                            }
                            setLookPerType({ ...lookPerType, [field.id]: records });
                        }
                    }
                    else {
                        if (field.fieldType === 'lookup') {
                            if (field.value) {
                                try {
                                    Object.entries(field.value).map(([key, value], index) => {
                                        statusData = { ...statusData, [field.type.replace('[]', `[${index}]`)]: key };
                                    });
                                    statusData = {
                                        ...statusData,
                                        [field.hiddenName]: field.id,
                                        [field.hiddenRecord]: props.caseId
                                    };
                                }
                                catch (err) {
                                    this.props.logger.error('Matter Note: ' + err);
                                    statusData = { ...statusData, [field.type]: field.value };
                                }
                            }
                        }
                        else {
                            if (field.fieldType === 'single_lookup') {
                                if (field.value) {
                                    statusData = {
                                        ...statusData,
                                        [field.hiddenName]: field.hiddenValue,
                                        [field.type]: field.typeData
                                    };
                                }
                            }
                            else
                                statusData = { ...statusData, [field.type]: field.value };
                        }
                    }
                }
            });
        }
        fetchFields();
    }, [props]);
    return (
        <Modal
            open={props.open}
            onClose={props.onClose}
            aria-labelledby="simple-modal-title"
            aria-describedby="simple-modal-description"
            BackdropComponent={null}
        >
            <div className={classes.root}>
                <Grid container>
                    <Grid item xs={12} className={classes.form}>
                        <Paper>
                            <Grid container spacing={2}>
                                {props.fields.map((field, index) => {
                                    return (
                                        <Grid item xs={12} key={index}>
                                            {setFieldTypeScreen(field)}
                                        </Grid>);
                                })}
                            </Grid>
                        </Paper>
                    </Grid>
                    <Grid item xs={12}>
                        <div className={classes.bottomBar} >
                            <Paper style={{ height: '100%' }}>
                                <Toolbar style={{ width: '100%', paddingTop: '10px' }}>
                                    <Grid alignItems='center' justifyItems='center' style={{ width: '100%' }}>
                                        <Grid container alignItems='center' style={{ width: '100%' }}>
                                            <Grid item xs={6}>
                                                <Button
                                                    fullWidth
                                                    variant="contained"
                                                    color="primary"
                                                    disabled={saving}
                                                    onClick={(event) => {
                                                        event.preventDefault();
                                                        event.stopPropagation();
                                                        setSaving(true);
                                                        handleSaveData();
                                                    }}
                                                >
                                                    {UIText.save}
                                                </Button>
                                            </Grid>
                                            <Grid item xs={6}>
                                                <Button
                                                    fullWidth
                                                    variant="contained"
                                                    color="secondary"
                                                    disabled={saving}

                                                    onClick={(event) => {
                                                        event.preventDefault();
                                                        event.stopPropagation();
                                                        props.closeModal();
                                                    }}
                                                >
                                                    {UIText.cancel}
                                                </Button>
                                            </Grid>
                                        </Grid>
                                    </Grid>
                                </Toolbar>
                            </Paper>
                        </div>
                    </Grid>
                </Grid>
            </div>
        </Modal>);
}
function Opponent(props) {
    const { language } = useSelector((state: ReduxState) => state.settings);
    const UIText = UIStrings.getLocaleStrings(language);
    const [clientType, setClientType] = React.useState(props.typeDefaultValue?.id || 'company');
    const handleOpponentData = (type, value) => {
        props.onChange(props.index, type, value);
    }
    return (<div>
        <Grid container direction='row' alignItems='center'>
            <Grid item xs>
                <Typography>{props.label + ` (${props.index + 1})`}</Typography>
            </Grid>
            {props.index > 0 || props.opponentCount > 1 ?
                <Grid item xs={2}>
                    <Tooltip title={UIText.remove} placement='top'>
                        <IconButton color='secondary' aria-label="delete" onClick={(e) => {
                            e.preventDefault();
                            e.stopPropagation();
                            props.onDelete(props.index);
                        }}>
                            <DeleteIcon fontSize="small" />
                        </IconButton>
                    </Tooltip>
                </Grid> : <></>}
        </Grid>
        <Grid container item spacing={1} direction='column' style={{ marginTop: '10px' }}>
            <Grid item xs>
                <UniversalAutoComplete
                    type={props.typeType}
                    label={props.label}
                    options={props.typeOptions}
                    defaultValue={props.typeDefaultValue}
                    controlled={props.typeDefaultValue}
                    onChange={(type, value) => {
                        setClientType(value?.id);
                        handleOpponentData(type, value);
                    }}
                />
            </Grid>
            <Grid item xs>
                <SearchBox
                    url={References.clientsLookupApi}
                    apiType='client'
                    extraData={'&clientType=' + clientType}
                    defaultValue={props.valueDefaultValue}
                    hint={UIText.startTyping}
                    type={props.valueType}
                    title={UIText.clientName}
                    handleChangeAutoComplete={handleOpponentData}
                />
            </Grid>
        </Grid>
    </div>);
}
interface ModalProps {
    caseId: string;
    open: boolean;
    closeModal;
    onClose;
    fields: FieldProps[];
    saveData;
}
interface FieldProps {
    id: string;
    type: any;
    label: string;
    fieldType: string;
    value: any;
    hiddenValue?: any;
    hiddenName?: string;
    hiddenRecord?: string;
    required: string;
    options: any[];
    typeData?: string;
    records?: any[];
}