﻿import * as React from "react";
import { withStyles, makeStyles } from "@material-ui/core/styles";
import { connect, useSelector } from "react-redux";
import Grid from "@material-ui/core/Grid";
import PopupAlert from '../Common/Alert';
import { AxiosRequest, RequestHeaders } from '../../Helpers/AxiosRequest';
import { References } from '../../Helpers/References';
import * as actions from '../../redux/actions';
import * as UIStrings from '../../UIStrings';
import { Typography, createStyles, AppBar, Toolbar, Autocomplete, Theme, WithStyles, Paper, Divider, Accordion, AccordionSummary, AccordionDetails, IconButton, Switch, FormControl, InputLabel, Input, Container, Button, Tooltip, FormControlLabel, Checkbox } from "@material-ui/core";
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import SearchBox from '../Common/SearchBox';
import BottomBar from '../Common/BottomBar';
import Modal from '../Common/Modal';
import { Helpers, handleUniversalChangeTextField, handleUniversalChangeDateTimeField, handleUniversalChangeAutoComplete, loadEmailProps, loadEmailAttachments, getAttachments } from "../../Helpers/Helpers";
import FormSkeleton from "../Common/Skeleton/FormSkeleton";
import FormatAlignJustifyIcon from '@material-ui/icons/FormatAlignJustify';
import { AppProps } from "../App";
import { UniversalAutoComplete, AutoCompleteWithTwoStage, UniversalTextField, DateTextField, SwitchTextField, TimeTextField } from "../Common/TextFields";
import AttachFileIcon from '@material-ui/icons/AttachFile';
import AttachmentsArea from "../Common/Attachments";
import { ReduxState } from "../..";
import { getMessageAttachments } from "../../GraphService";
import DeleteIcon from '@material-ui/icons/Delete';
import { IEmailProps } from "./MatterNote";
import moment from "moment";
const useStyles = (theme: Theme) => createStyles({
    paper: {
        marginTop: theme?.spacing(2),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center'
    },
    avatar: {
        margin: theme?.spacing(1),
        backgroundColor: 'blue',
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme?.spacing(3),
        paddingRight: "5px",
        marginBottom: theme?.spacing(8)
    },
    submit: {
        color: 'red'
    },
    textarea: {
        resize: "both",
        minHeight: "200px"
    },
    formControl: {
        margin: theme?.spacing(1),
        minWidth: 120,
    },
    selectEmpty: {
        marginTop: theme?.spacing(2),
    },
    root: {
        display: 'flex',
        flexDirection: 'column',
        flexGrow: 1
    },
    main: {
        marginBottom: theme?.spacing(8),
    },
    footer: {
        position: 'fixed',
        bottom: "0",
        marginTop: 'auto',
        width: "100%",
        backgroundColor: '#fff',
    },
    margin: {
        margin: theme?.spacing(1),
    },
    textField: {
        width: '25ch',
    },
    appBar: {
        position: 'fixed',
        height: '55px',
        paddingBottom: '10px',
        bottom: 0,
    },
    addOpponent: {
        margin: theme?.spacing(1),
    },
    icon: {
        width: '20px',
        height: '20px'
    }
});
class Hearing extends React.Component<IFormProps, IFormState> {

    attachments: any[];
    messageID: string;
    hearingTypes: any[];
    stageStatuses: any[];
    isHijriEnabled: boolean;
    valueCurrency: string;
    data: any;
    emailInfo: IEmailProps;
    toMe: any;
    constructor(props: IFormProps, context: any) {
        super(props, context);
        this.state = {
            errorType: false,
            isJudged: false,
            requiredFields: {
                matterName: false,
            },
            id: null,
            date: new Date(),
            assignees: []
        };
        this.attachments = [];
        this.messageID = '';
        this.hearingTypes = [];
        this.stageStatuses = [];
        this.data = {};
        this.toMe = null;
        this.isHijriEnabled = false;
        this.handleControlledField = this.handleControlledField.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }
    componentDidMount() {
        this.props.startLoader();
        this.getMailItem();
        this.fillData();
        window.addEventListener('includeEmailAttachment', this.handleChangeEmailAttachment.bind(this));
    }
    componentWillUnmount() {
        window.removeEventListener('includeEmailAttachment', this.handleChangeEmailAttachment);
    }
    handleChangeEmailAttachment(event) {
        if (event) {
            var includeEmailAttachments = event.detail;
            if (includeEmailAttachments) {
                this.props.addFile(this.attachments);
            }
            else {
                var files = this.props.files.filter(file => file.isEmailAttachment == false);
                this.props.setAttachments(files);
            }
        }
    }
    addAttachments(files: any[]) {
        if (files && files.length > 0) {
            this.attachments = this.attachments.concat(files);
        }
    }
    UIText = UIStrings.getLocaleStrings(this.props.language);
    getMailItem() {
        try {
            var item = Office.context.mailbox.item;
            var self = this;
            item.body.getAsync(Office.CoercionType.Text, function (asyncResult) {
                if (asyncResult.status !== Office.AsyncResultStatus.Succeeded) {
                }
                else {
                    self.messageID = item.itemId;
                    self.emailInfo = loadEmailProps(item, asyncResult);
                    const start = moment(new Date());
                    const remainder = 30 - (start.minute() % 30);
                    const dateTime = moment(start).add(remainder, "minutes");
                    self.data = {
                        ...self.data,
                        hearingDate: new Date(),
                        hearingTime: Helpers.formatTime(dateTime)
                    }
                    loadEmailAttachments(self);
                }
            });
        } catch (e) {
            this.props.logger.error('Hearing: ' + e);
        }
    }
    async fillData() {
        try {
            let response = await AxiosRequest.get(References.loadHearingApi);
            if (response.data.error === "") {
                this.fillCustomData('hearingTypes', response.data.success.data.types);
                this.fillCustomData('stageStatuses', response.data.success.data.stage_statuses);
                this.isHijriEnabled = response.data.success.data.hijri_calendar_enabled;
                this.valueCurrency = response.data.success.data.value_currency;
            }
            else {
                this.props.errorAlert(JSON.stringify(response.data.error));
            }
            this.props.stopLoader();
        } catch (e) {
            this.props.logger.error('Hearing: ' + e);
            this.props.stopLoader();
            this.props.errorAlert(this.UIText.errorOccured);
        }
    }
    fillCustomData(type: any, items: any) {
        if (!items || items.length == 0) {
            return;
        }
        var dataToFill = [];
        if (items) {
            if (type === 'priorities') {
                Object.entries(items).map(([key, value]) => dataToFill.push({ value: value, id: key, image: value + '.png' }));
            }
            else {
                Object.entries(items).map(([key, value]) => dataToFill.push({ value: value, id: key }));
            }
        }
        this[type] = dataToFill;
    }
    async onSubmit(form) {
        form.preventDefault();
        form.stopPropagation();                       
        var requiredFields = {
            matterName: !this.data.matterName?.id?.length                        
        };
        this.setState({
            requiredFields: requiredFields
        });
        for (const [key, value] of Object.entries(requiredFields)) {
            if (value) {
                document.getElementById(key)?.scrollIntoView();
                return;
            }
        }
        this.props.startSaving();
        var matterId = this.data.matterName?.id;
        var type = this.data.type?.id;
        var date = Helpers.formatDate(this.data.hearingDate, false, this.isHijriEnabled);
        var time = this.data.hearingTime;
        var timeSpent = this.data.timeSpent || '1:00';
        var assignees = this.data.assignees;
        var postDate = this.data.postponedDate;
        var postTime = this.data.postponedTime;
        var reasonsPost = this.data.reasonsOfPostponed;
        var comment = this.data.comments;
        var summary = this.data.summary;
        var isJudged = this.state.isJudged;
        var judgment = this.data.judgment;
        var stageStatus = this.data.stageStatus?.id;
        var judgmentValue = this.data.judgmentValue;

        var hearing = {
            matterId: matterId,
            type: type,
            date: date,
            time: time,
            postponedUntilDate: postDate,
            postponedUntilTime: postTime,
            reasonsOfPostponement: reasonsPost,
            assignees: assignees,
            timeSpent: timeSpent,
            comment: comment,
            summary: summary,
            isJudged: isJudged,
            judgment: judgment,
            stageStatus: stageStatus,
            judgmentValue: judgmentValue,
        };
        //Cleaning the data
        hearing = Helpers.cleaningData(hearing);
        var dataToPost = '';
        if (hearing.matterId != null)
            dataToPost += "&legal_case_id=" + hearing.matterId;
        if (hearing.type != null)
            dataToPost += "&type=" + hearing.type;
        if (hearing.date != null)
            dataToPost += "&startDate=" + hearing.date;
        if (hearing.time != null)
            dataToPost += "&startTime=" + hearing.time;
        if (hearing.postponedUntilDate != null)
            dataToPost += "&postponedDate=" + hearing.postponedUntilDate;
        if (hearing.postponedUntilTime != null)
            dataToPost += "&postponedTime=" + hearing.postponedUntilTime;
        if (hearing.reasonsOfPostponement != null)
            dataToPost += "&reasons_of_postponement=" + hearing.reasonsOfPostponement;
        if (hearing.comment != null)
            dataToPost += "&comments=" + hearing.comment;
        if (hearing.summary != null)
            dataToPost += "&summary=" + hearing.summary;
        if (hearing.assignees?.length > 0) {
            hearing.assignees.forEach((assignee, index) => {
                dataToPost += `&Hearing_Lawyers[${index}]=${assignee}`;
            });
        }
        if (hearing.timeSpent != null && this.state.date <= new Date()) {
            dataToPost += '&timeSpent=' + hearing.timeSpent;
        }
        if (hearing.isJudged != null)
            dataToPost += "&judged=" + (hearing.isJudged ? 'yes' : null);
        if (hearing.isJudged) {
            if (hearing.judgment != null)
                dataToPost += "&judgment=" + hearing.judgment;
            if (hearing.stageStatus != null)
                dataToPost += "&stage_status=" + hearing.stageStatus;
            if (hearing.judgmentValue != null)
                dataToPost += "&judgmentValue=" + hearing.judgmentValue;
        }

        var response = await AxiosRequest.post(References.addHearingApi, dataToPost);
        try {
            var resp = response.data;
            var error = resp?.error;
            if (error != "") {
                var respError = Helpers.handleErrorResponse(error, 'hearing', form, this.onSubmit);
                if (respError != null && respError != '') {
                    this.props.errorAlert(JSON.stringify(respError));
                }
            }
            else {
                var caseId = resp.success?.data?.hearing_id;
                if (caseId) {
                    try {
                        const reqHeader = new RequestHeaders();
                        var formData = new FormData();
                        const files = await getAttachments(this);
                        if (files.length > 0) {
                            files.forEach(file => {
                                formData.append('Files', file);
                            });
                            formData.append('Url', reqHeader.url + References.uploadMatterAttachmentsApi);
                            formData.append('DataToPost', 'legal_case_id=' + hearing.matterId);
                            formData.append('UserKey', reqHeader.userKey);
                            var uploadResponse = await AxiosRequest.postToServer('PostData/UploadFileToApp4legal', formData);
                            if (uploadResponse.status != 200) {
                                this.props.errorAlert(this.UIText.errorUploadingFiles);
                            }
                            const resp = uploadResponse.data;
                            if (resp?.error === '') {
                            }
                            else {
                                this.props.errorAlert(JSON.stringify(resp?.error));
                            }
                        }
                    }
                    catch (err) {
                        this.props.logger.error('Hearing: ' + err);
                        this.props.errorAlert(this.UIText.errorOccured);
                    }
                    this.setState({ id: { hearingId: caseId, matterId: hearing.matterId} });
                    this.props.openModal();
                }
            }
        }
        catch (err) {
            this.props.logger.error('Hearing: ' + err);
            this.props.errorAlert(this.UIText.errorOccured);
        }
        this.props.stopSaving();
    }
    handleControlledField(event, valid = { type: '', valid: false }) {
        var type = event.target.name;
        var value = event.target.value;
        var reqFields = this.state.requiredFields;
        if (reqFields[type] != null) {
            if (valid.type.length > 0)
                reqFields[type] = !valid.valid;
            else
                reqFields[type] = !value.length;
            this.setState({ requiredFields: reqFields });
        }
        this.setState({ ...this.state, [type]: value });
    }
    render() {
        const { classes } = this.props;
        const UIText = this.UIText;
        return (<React.Fragment>
            <PopupAlert />
            <Modal type='hearing' id={this.state.id} />
            {this.props.runLoader ?
                <FormSkeleton input='form' /> :
                <div className={classes.root}>
                    <div className={classes.paper}>
                        <form id='hearing' className={classes.form} noValidate onSubmit={this.onSubmit.bind(this)}>
                            <Accordion defaultExpanded={true}>
                                <AccordionSummary
                                    expandIcon={<ExpandMoreIcon />}
                                    aria-controls="panel1a-content"
                                    id="panel1a-header"
                                >
                                    <Grid container alignItems='center'>
                                        <Grid item xs={2}>
                                            <img src={require('../../assets/hearingIcon.svg')} className={classes.icon} />
                                        </Grid>
                                        <Grid item xs>
                                            <Typography variant='h6' gutterBottom> {UIText.generalInformation} </Typography>
                                        </Grid>
                                    </Grid>
                                </AccordionSummary>
                                <AccordionDetails>
                                    <Grid container spacing={2} >
                                        <Grid item xs={12}>
                                            <SearchBox
                                                url={References.casesLookupApi}
                                                apiType='matter'
                                                required
                                                error={this.state.requiredFields.matterName}
                                                hint={UIText.startTypingMatter}
                                                type='matterName'
                                                extraData='&category=litigation'
                                                title={UIText.matter}
                                                handleChangeAutoComplete={(type, value) => {
                                                    handleUniversalChangeAutoComplete(this, type, value);
                                                }}
                                            />
                                        </Grid>
                                        <Grid item xs={12}>
                                            <UniversalAutoComplete
                                                type='type'
                                                label={UIText.type}
                                                options={this.hearingTypes}
                                                onChange={(type, value) => handleUniversalChangeAutoComplete(this, type, value)}
                                            />
                                        </Grid>
                                        <Grid item container xs={12} >
                                            <Grid item xs={12}>
                                                <DateTextField
                                                    type='hearingDate'
                                                    hijri={this.isHijriEnabled}
                                                    label={UIText.date}
                                                    onChange={(type, value) => {
                                                        this.setState({ date: value });
                                                        handleUniversalChangeDateTimeField(this, type, value)
                                                    }}
                                                />
                                            </Grid>
                                            <Grid item xs={12} style={{ marginTop: '5px' }}>
                                                <TimeTextField
                                                    type='hearingTime'
                                                    defaultValue={() => {
                                                        const start = moment(new Date());
                                                        const remainder = 30 - (start.minute() % 30);
                                                        const dateTime = moment(start).add(remainder, "minutes");
                                                        return dateTime;
                                                    }}
                                                    label={UIText.time}                                                    
                                                    onChange={(type, value) => handleUniversalChangeDateTimeField(this, type, value)}
                                                />
                                            </Grid>
                                        </Grid>
                                        {
                                            //<Grid item container xs={12} >
                                        //    <Grid item xs={12}>
                                        //        <DateTextField
                                        //            type='postponedDate'
                                        //            label={UIText.postponedDate}
                                        //            disabled
                                        //            defaultValue={null}
                                        //            onChange={(type, value) => handleUniversalChangeDateTimeField(this, type, value)}
                                        //        />
                                        //    </Grid>
                                        //    <Grid item xs={12} style={{ marginTop: '5px' }}>
                                        //        <TimeTextField
                                        //            type='postponedTime'
                                        //            label={UIText.postponedTime}
                                        //            disabled
                                        //            defaultValue={null}
                                        //            onChange={(type, value) => handleUniversalChangeDateTimeField(this, type, value)}
                                        //        />
                                        //    </Grid>
                                        //    <Grid item xs={12}>
                                        //        <UniversalTextField
                                        //            type='reasonsOfPostponed'
                                        //            label={UIText.reasonOfPostponment}
                                        //            onChange={(event, value) => handleUniversalChangeTextField(this, event, value)}
                                        //            multiline
                                        //            rows={4}
                                        //        />
                                        //    </Grid>
                                            //</Grid>
                                        }
                                        <Grid item xs={12} >
                                            <SearchBox
                                                url={References.usersLookupApi}
                                                apiType='user'
                                                hint={UIText.startTyping}
                                                type='assignees'
                                                defaultValue={this.state.assignees}                                                
                                                title={UIText.assignees}
                                                multiple
                                                handleChangeAutoComplete={(type, value) => {
                                                    this.setState({ assignees: value });
                                                    handleUniversalChangeAutoComplete(this, type, value);
                                                }}
                                            />
                                            <Button onClick={async (event) => {
                                                event.preventDefault();
                                                event.stopPropagation();
                                                if (this.toMe == null) {
                                                    const response = await AxiosRequest.get(References.checkApi);
                                                    const data = response?.data?.success?.data;
                                                    this.toMe = {
                                                        id: data.user_id,
                                                        name: data.profileName
                                                    }
                                                }
                                                this.setState({ assignees: [this.toMe] });
                                                handleUniversalChangeAutoComplete(this, 'assignees', [this.toMe]);
                                            }}>{UIText.assignToMe}</Button>
                                        </Grid>
                                        {this.state.date > new Date() ? <></> :
                                            <Grid item xs={12} >
                                                <UniversalTextField
                                                    type='timeSpent'
                                                    label={UIText.timeSpent}
                                                    onChange={(event, value) => handleUniversalChangeTextField(this, event, value)}
                                                    defaultValue='1:00'
                                                    helperText={UIText.timeSpentTip}
                                                />
                                            </Grid>
                                        }
                                        <Grid item xs={12} >
                                            <UniversalTextField
                                                type='comments'
                                                label={UIText.comments}
                                                onChange={(event, value) => handleUniversalChangeTextField(this, event, value)}
                                                multiline
                                                rows={4}
                                            />
                                        </Grid>
                                        <Grid item xs={12} >
                                            <UniversalTextField
                                                type='summary'
                                                label={UIText.summary}
                                                onChange={(event, value) => handleUniversalChangeTextField(this, event, value)}
                                                multiline
                                                rows={10}
                                            />
                                        </Grid>
                                        <Grid item xs={12} >
                                            <FormControlLabel
                                                control={
                                                    <Checkbox
                                                        onChange={(event) => {
                                                            var checked = event.target.checked;
                                                            this.setState({ isJudged: checked });
                                                        }}
                                                        color="primary"
                                                    />
                                                }
                                                label={UIText.judged}
                                            />
                                        </Grid>
                                        {this.state.isJudged ?
                                            <Grid item container >
                                                <Grid item xs={12} >
                                                    <UniversalTextField
                                                        type='judgment'
                                                        label={UIText.judgment}
                                                        onChange={(event, value) => handleUniversalChangeTextField(this, event, value)}
                                                        multiline
                                                        rows={5}
                                                    />
                                                </Grid>
                                                <Grid item xs={12}>
                                                    <UniversalAutoComplete
                                                        type='stageStatus'
                                                        label={UIText.stageStatus}
                                                        options={this.stageStatuses}
                                                        onChange={(type, value) => { console.log(value); handleUniversalChangeAutoComplete(this, type, value) }}
                                                    />
                                                </Grid>
                                                <Grid item xs={12} >
                                                    <UniversalTextField
                                                        type='judgmentValue'
                                                        label={UIText.judgmentValue + ` (${this.valueCurrency})`}
                                                        onChange={(event, value) => handleUniversalChangeTextField(this, event, value)}
                                                    />
                                                </Grid>
                                            </Grid>:
                                            <></>}
                                    </Grid>
                                </AccordionDetails>
                            </Accordion>
                            <Accordion>
                                <AccordionSummary
                                    expandIcon={<ExpandMoreIcon />}
                                    aria-controls="panel3a-content"
                                    id="panel3a-header"
                                >
                                    <Grid container alignItems='center'>
                                        <Grid item xs={2}>
                                            <img src={require('../../assets/attachmentsIcon.svg')} className={classes.icon} />
                                        </Grid>
                                        <Grid item xs>
                                            <Typography variant='h6' gutterBottom>{UIText.attachments}</Typography>
                                        </Grid>
                                    </Grid>
                                </AccordionSummary>
                                <AccordionDetails>
                                    <AttachmentsArea token={this.props.getAccessToken()} onAddAttachments={this.addAttachments.bind(this)} />
                                </AccordionDetails>
                            </Accordion>
                        </form>
                    </div>
                    <BottomBar onSubmit={this.onSubmit} />
                </div >
            }
        </React.Fragment>
        );
    }
}
const mapStateToProps = (state: ReduxState) => ({
    language: state.settings.language,
    runLoader: state.loader.runLoader,
    includeEmail: state.attachments.includeEmail,
    files: state.attachments.files,
    moreEmails: state.attachments.moreEmails,
    includeEmailAttachments: state.attachments.includeEmailAttachments
});
const mapDispatchToProps = (dispatch) => {
    return {
        successAlert: (message: string) => dispatch(actions.successAlert(message)),
        errorAlert: (message: string) => dispatch(actions.errorAlert(message)),
        stopLoader: () => dispatch(actions.stopLoader()),
        startLoader: () => dispatch(actions.startLoader()),
        startSaving: () => dispatch(actions.startSaving()),
        stopSaving: () => dispatch(actions.stopSaving()),
        openModal: () => dispatch(actions.openModal()),
        focusError: (id: string) => dispatch(actions.focusError(id)),
        setAttachments: (files) => dispatch(actions.setAttachments(files)),
        addFile: (file) => dispatch(actions.addFile(file)),
        deleteFile: (index) => dispatch(actions.deleteFile(index)),
        setHasEmailAttachments: (found: boolean) => dispatch(actions.setEmailHasAttachments(found)),
        setFetchingAttachments: (status: boolean) => dispatch(actions.setFetchingAttachments(status))
    }
};
type FormReduxProps = ReturnType<typeof mapStateToProps> & ReturnType<typeof mapDispatchToProps>;
interface IFormState {
    isJudged: boolean,
    date: Date,
    errorType,
    requiredFields,
    id: any,
    assignees: any[];
}
interface IFormProps extends FormReduxProps, WithStyles<typeof useStyles>, AppProps {

}
const AppContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(Hearing);
export default withStyles(useStyles, { withTheme: true })(AppContainer);