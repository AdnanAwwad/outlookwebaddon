﻿import * as React from "react";
import TextField from "@material-ui/core/TextField";
import { useSelector } from "react-redux";
import Grid from "@material-ui/core/Grid";
import * as UIStrings from '../../UIStrings';
import { Typography, Autocomplete } from "@material-ui/core";
import { AxiosRequest } from '../../Helpers/AxiosRequest';
import { References } from '../../Helpers/References';
import { ReduxState } from "../..";
import TextFieldWithValidation from "../CustomInputs/TextFieldWithValidation";
import { UniversalTextField, UniversalAutoComplete } from "../Common/TextFields";

export default function AddressComponent(props) {
    const languageSelected = useSelector((state: ReduxState) => state.settings.language);
    const [state, setState] = React.useState(null);
    const UIText = UIStrings.getLocaleStrings(languageSelected);
    const [countries, setCountries] = React.useState<any>([]);
    const handleChangeAutoComplete = (ref, value) => {
        setState({
            ...state,
            [ref]: value
        });
        var newState = state;
        if (newState == null) {
            newState = {
                [ref]: value
            };
        }
        else {
            newState[ref] = value;
        }
        props.dataToParent(props.index, newState);
    };
    const handleChangeTextField = (event, valid = {}) => {
        var type = event.target.name;
        var value = event.target.value;
        var newState = state;
        if (newState == null) {
            newState = {
                [type]: value
            };
        }
        else {
            newState[type] = value;
        }
        props.dataToParent(props.index, newState);
        setState({
            ...state,
            [type]: value
        });
    }
    React.useEffect(() => {
        async function fetchAPI() {
            try {
                let response = await AxiosRequest.get(References.addCompanyApi);
                if (response.data.error === "") {
                    var items = response.data.success.data.Countries;
                    var data = [];
                    if (items) {
                        Object.entries(items).map(([key, value]) => data.push({ value: value, id: key }));
                    }
                    setCountries(data);
                }
            } catch (e) {
                props.logger.error('Address: ' + e);
            }
        };
         fetchAPI();
    }, []);
    return (<Grid container spacing={2}>
        <Grid item xs={12} sm={12} >
            <UniversalTextField
                type='address'
                label={UIText.address}
                autoComplete='address-level1'
                onChange={handleChangeTextField}
            />
            <UniversalTextField
                type='city'
                label={UIText.city}
                autoComplete='street-address'
                onChange={handleChangeTextField}
            />
            <UniversalTextField
                type='state'
                label={UIText.state}
                autoComplete='country'
                onChange={handleChangeTextField}
            />
        </Grid>
        <Grid item xs={12} sm={12} >
            <UniversalAutoComplete
                type='country'
                label={UIText.country}
                options={countries}
                onChange={handleChangeAutoComplete}
            />
        </Grid>
        <Grid item xs={12} sm={12}>
            <UniversalTextField
                type='zip'
                label={UIText.zip}
                autoComplete='postal-code'
                onChange={handleChangeTextField}
            />
            <UniversalTextField
                type='website'
                label={UIText.website}
                autoComplete='url'
                onChange={handleChangeTextField}
            />
            <UniversalTextField
                type='phone'
                label={UIText.phone}
                autoComplete='tel'
                onChange={handleChangeTextField}
            />
            <UniversalTextField
                type='fax'
                label={UIText.fax}
                autoComplete='tel'
                onChange={handleChangeTextField}
            />
            <UniversalTextField
                type='mobile'
                label={UIText.mobile}
                autoComplete='tel'
                onChange={handleChangeTextField}
            />
            <TextFieldWithValidation
                handleChangeTextField={handleChangeTextField}
                type='email'
                label={UIText.email}
                id={"email" + props.index}
                autoComplete="email"
                apiType='email'
                validationType='email'
            />
        </Grid>
    </Grid>);
};