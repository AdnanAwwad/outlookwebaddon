﻿import * as React from "react";
import { withStyles, makeStyles } from "@material-ui/core/styles";
import { connect, useSelector } from "react-redux";
import Grid from "@material-ui/core/Grid";
import PopupAlert from '../Common/Alert';
import { AxiosRequest, RequestHeaders } from '../../Helpers/AxiosRequest';
import { References } from '../../Helpers/References';
import * as actions from '../../redux/actions';
import * as UIStrings from '../../UIStrings';
import { Typography, createStyles, AppBar, Toolbar, Autocomplete, Theme, WithStyles, Paper, Divider, Accordion, AccordionSummary, AccordionDetails, IconButton, Switch, FormControl, InputLabel, Input, Container, Button, Tooltip, FormControlLabel, Checkbox } from "@material-ui/core";
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import InfoIcon from '@material-ui/icons/Info';
import SearchBox, { AttendeesSearchBox } from '../Common/SearchBox';
import BottomBar from '../Common/BottomBar';
import Modal from '../Common/Modal';
import { Helpers, handleUniversalChangeTextField, handleUniversalChangeDateTimeField, handleUniversalChangeAutoComplete } from "../../Helpers/Helpers";
import FormSkeleton from "../Common/Skeleton/FormSkeleton";
import FormatAlignJustifyIcon from '@material-ui/icons/FormatAlignJustify';
import { AppProps } from "../App";
import { UniversalAutoComplete, AutoCompleteWithTwoStage, UniversalTextField, DateTextField, SwitchTextField, TimeTextField, AutoCompleteWithImage } from "../Common/TextFields";
import AttachFileIcon from '@material-ui/icons/AttachFile';
import AttachmentsArea from "../Common/Attachments";
import { ReduxState } from "../..";
import { getMessageAttachments } from "../../GraphService";
import DeleteIcon from '@material-ui/icons/Delete';
import { PriorityButton } from "../Common/SplitButton";
import moment from "moment";
const useStyles = (theme: Theme) => createStyles({
    paper: {
        marginTop: theme?.spacing(2),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center'
    },
    avatar: {
        margin: theme?.spacing(1),
        backgroundColor: 'blue',
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme?.spacing(3),
        paddingRight: "5px",
        marginBottom: theme?.spacing(8)
    },
    submit: {
        color: 'red'
    },
    textarea: {
        resize: "both",
        minHeight: "200px"
    },
    formControl: {
        margin: theme?.spacing(1),
        minWidth: 120,
    },
    selectEmpty: {
        marginTop: theme?.spacing(2),
    },
    root: {
        display: 'flex',
        flexDirection: 'column',
        flexGrow: 1
    },
    main: {
        marginBottom: theme?.spacing(8),
    },
    footer: {
        position: 'fixed',
        bottom: "0",
        marginTop: 'auto',
        width: "100%",
        backgroundColor: '#fff',
    },
    margin: {
        margin: theme?.spacing(1),
    },
    textField: {
        width: '25ch',
    },
    appBar: {
        position: 'fixed',
        height: '55px',
        paddingBottom: '10px',
        bottom: 0,
    },
    addOpponent: {
        margin: theme?.spacing(1),
    },
    icon: {
        width: '20px',
        height: '20px'
    }
});
class Meeting extends React.Component<IFormProps, IFormState> {

    attachments: any[];
    messageID: string;
    meetingTypes: any[];
    priorities: any[];
    data: any;
    constructor(props: IFormProps, context: any) {
        super(props, context);
        this.state = {
            errorType: false,
            isJudged: false,
            requiredFields: {
                title: false,
                fromDate: false,
                toDate: false
            }
        };
        this.attachments = [];
        this.messageID = '';
        this.meetingTypes = [];
        this.priorities = [];
        this.data = {};
        this.onSubmit = this.onSubmit.bind(this);
    }
    componentDidMount() {
        this.props.startLoader();
        this.fillData();
    }

    UIText = UIStrings.getLocaleStrings(this.props.language);
    getMailItem() {
        try {
            var item = Office.context.mailbox.item;
            var self = this;
            item.body.getAsync(Office.CoercionType.Text, function (asyncResult) {
                if (asyncResult.status !== Office.AsyncResultStatus.Succeeded) {
                }
                else {
                    self.messageID = item.itemId;
                }
            });
        } catch (e) {
            this.props.logger.error('Meeting: ' + e);
        }
    }
    async fillData() {
        try {
            let response = await AxiosRequest.get(References.addMeetingApi);
            if (response.data.error === "") {
                this.fillCustomData('meetingTypes', response.data.success.data.formData.meeting_types);
                this.fillCustomData('priorities', response.data.success.data.formData.priorities);
                var today = new Date();
                today.setHours(today.getHours() + 1);
                var start = moment(today);
                var remainder = 30 - (start.minute() % 30);
                var dateTime = moment(start).add(remainder, "minutes");
                this.data.toTime = Helpers.formatTime(dateTime);
                start = moment(new Date());
                remainder = 30 - (start.minute() % 30);
                dateTime = moment(start).add(remainder, "minutes");
                this.data.fromTime = Helpers.formatTime(dateTime);
                this.data.fromDate = Helpers.formatDate(new Date());
                this.data.toDate = Helpers.formatDate(new Date());
            }
            else {
                this.props.errorAlert(JSON.stringify(response.data.error));
            }
            this.props.stopLoader();
        } catch (e) {
            this.props.logger.error('Meeting: ' + e);
            this.props.stopLoader();
            this.props.errorAlert(this.UIText.errorOccured);
        }
    }
    fillCustomData(type: any, items: any) {
        if (!items || items.length == 0) {
            return;
        }
        var dataToFill = [];
        if (items) {
            if (type === 'priorities') {
                Object.entries(items).map(([key, value]) => dataToFill.push({ value: value, id: key, image: value + '.png' }));
            }
            else {
                Object.entries(items).map(([key, value]) => dataToFill.push({ value: value, id: key }));
            }
        }
        this[type] = dataToFill;
    }
    async onSubmit(form) {
        form.preventDefault();
        form.stopPropagation();
        var requiredFields = {
            title: !this.data.title?.length,
            fromDate: !this.data.fromDate?.length,
            fromTime: !this.data.fromTime?.length,
            toDate: !this.data.toDate?.length,
            toTime: !this.data.toTime?.length,
        };
        this.setState({
            requiredFields: requiredFields
        });
        for (const [key, value] of Object.entries(requiredFields)) {
            if (value) {
                document.getElementById(key)?.scrollIntoView();
                return;
            }
        }
        this.props.startSaving();
        var title = this.data.title;
        var fromDate = this.data.fromDate;
        var fromTime = this.data.fromTime;
        var toDate = this.data.toDate
        var toTime = this.data.toTime;
        var description = this.data.description;
        var relatedMatter = this.data.relatedMatter?.id;
        var setAsPrivate = this.data.setAsPrivate;
        var attendees = this.data.attendees;
        var meetingType = this.data.meetingType?.value?.id;
        var priority = this.data.priority != null ? this.data.priority?.id : 'medium';
        var location = this.data.location?.id;
        var meeting = {
            title: title,
            fromDate: fromDate,
            fromTime: fromTime,
            toDate: toDate,
            toTime: toTime,
            description: description,
            relatedMatter: relatedMatter,
            privateVal: setAsPrivate,
            attendees: attendees,
            location: location,
            priority: priority,
            meetingType: meetingType
        };
        //Cleaning the data
        meeting = Helpers.cleaningData(meeting);
        var dataToPost = '';
        if (meeting.title != null)
            dataToPost += "&title=" + meeting.title;
        if (meeting.fromDate != null)
            dataToPost += "&start_date=" + meeting.fromDate;
        if (meeting.fromTime)
            dataToPost += "&start_time=" + meeting.fromTime;
        if (meeting.toDate != null)
            dataToPost += "&end_date=" + meeting.toDate;
        if (meeting.toTime != null)
            dataToPost += "&end_time=" + meeting.toTime;
        if (meeting.priority != null)
            dataToPost += "&priority=" + meeting.priority;
        if (meeting.relatedMatter != null)
            dataToPost += "&legal_case_id=" + meeting.relatedMatter;
        if (meeting.description != null)
            dataToPost += "&description=" + meeting.description;
        if (meeting.location != null)
            dataToPost += "&task_location_id=" + meeting.location;
        if (meeting.meetingType != null)
            dataToPost += "&event_type_id=" + meeting.meetingType;
        var attendeesArray = '';
        if (meeting.attendees != null) {
            for (var i = 0; i < meeting.attendees.length; i++) {
                attendeesArray += `&attendees[${i}]=` + meeting.attendees[i].id;
                const required = meeting.attendees[i].required;
                attendeesArray += `&mandatory[${meeting.attendees[i].id}]=` + (required != null ? (required ? 1 : 0) : 1);
            }
        }
        if (attendeesArray.length > 0)
            dataToPost += attendeesArray;
        if (meeting.privateVal != null)
            dataToPost += "&private =" + (meeting.privateVal ? 'yes' : 'no');

        var response = await AxiosRequest.post(References.addMeetingApi, dataToPost);
        try {
            var resp = response.data;
            var error = resp?.error;
            if (error != "") {
                var respError = Helpers.handleErrorResponse(error, 'meeting', form, this.onSubmit);
                if (respError != null && respError != '') {
                    this.props.errorAlert(JSON.stringify(respError));
                }
            }
            else {
                    this.props.successAlert(this.UIText.meetingCreated);
                }
        }
        catch (err) {
            this.props.logger.error('Meeting: ' + err);
            this.props.errorAlert(this.UIText.errorOccured);
        }
        this.props.stopSaving();
    }
    handleCheckBoxChange(event) {
        var name = event.target.name;
        var checked = event.target.checked;
        this.data[name] = checked;
    }
    render() {
        const { classes } = this.props;
        const UIText = this.UIText;
        return (<React.Fragment>
            <PopupAlert />
            <Modal type='meeting'/>
            {this.props.runLoader ?
                <FormSkeleton input='form' /> :
                <div className={classes.root}>
                    <div className={classes.paper}>
                        <form id='meeting' className={classes.form} noValidate onSubmit={this.onSubmit.bind(this)}>
                            <Accordion defaultExpanded={true}>
                                <AccordionSummary
                                    expandIcon={<ExpandMoreIcon />}
                                    aria-controls="panel1a-content"
                                    id="panel1a-header"
                                >
                                    <Grid container alignItems='center'>
                                        <Grid item xs={2}>
                                            <img src={require('../../assets/meetingIcon.svg')} className={classes.icon} />
                                        </Grid>
                                        <Grid item xs>
                                            <Typography variant='h6' gutterBottom> {UIText.generalInformation} </Typography>
                                        </Grid>
                                    </Grid>
                                </AccordionSummary>
                                <AccordionDetails>
                                    <Grid container spacing={2} >
                                        <Grid item xs={12}>
                                            <UniversalTextField
                                                type='title'
                                                required
                                                error={this.state.requiredFields.title}
                                                label={UIText.title}
                                                onChange={(event, value) => handleUniversalChangeTextField(this, event, value)}
                                            />
                                        </Grid>
                                        <Grid item container xs={12} >
                                            <Grid item xs={12}>
                                                <DateTextField
                                                    type='fromDate'
                                                    required
                                                    error={this.state.requiredFields.fromDate}
                                                    label={UIText.fromDate}
                                                    onChange={(type, value) => handleUniversalChangeDateTimeField(this, type, value)}
                                                />
                                            </Grid>
                                            <Grid item xs={12} style={{marginTop: '5px'}}>
                                                <TimeTextField
                                                    type='fromTime'
                                                    required
                                                    error={this.state.requiredFields.fromTime}
                                                    label={UIText.fromTime}
                                                    defaultValue={() => {
                                                        const start = moment(new Date());
                                                        const remainder = 30 - (start.minute() % 30);
                                                        const dateTime = moment(start).add(remainder, "minutes");
                                                        return dateTime;
                                                    }}
                                                    onChange={(type, value) => handleUniversalChangeDateTimeField(this, type, value)}
                                                />
                                            </Grid>
                                        </Grid>
                                        <Grid item container xs={12} >
                                            <Grid item xs={12}>
                                                <DateTextField
                                                    type='toDate'
                                                    required
                                                    error={this.state.requiredFields.toDate}
                                                    label={UIText.toDate}
                                                    onChange={(type, value) => handleUniversalChangeDateTimeField(this, type, value)}
                                                />
                                            </Grid>
                                            <Grid item xs={12} style={{ marginTop: '5px' }}>
                                                <TimeTextField
                                                    type='toTime'
                                                    required
                                                    error={this.state.requiredFields.toTime}
                                                    label={UIText.toTime}
                                                    defaultValue={() => {
                                                        var today = new Date();
                                                        today.setHours(today.getHours() + 1);
                                                        const start = moment(today);
                                                        const remainder = 30 - (start.minute() % 30);
                                                        const dateTime = moment(start).add(remainder, "minutes");
                                                        return dateTime;
                                                    }}
                                                    onChange={(type, value) => handleUniversalChangeDateTimeField(this, type, value)}
                                                />
                                            </Grid>
                                        </Grid>
                                        <Grid item xs={12}>
                                            <SearchBox
                                                url={References.casesLookupApi}
                                                apiType='matter'
                                                hint={UIText.startTypingMatter}
                                                type='relatedMatter'
                                                title={UIText.matter}
                                                handleChangeAutoComplete={(type, value) => {
                                                    handleUniversalChangeAutoComplete(this, type, value);
                                                }}
                                            />
                                        </Grid>
                                        <Grid item xs={12}>
                                            <UniversalTextField
                                                type='description'
                                                label={UIText.description}
                                                onChange={(event, value) => handleUniversalChangeTextField(this, event, value)}
                                                multiline
                                                rows={4}
                                            />
                                        </Grid>
                                        <Grid item xs={12}>
                                            <FormControlLabel
                                                control={
                                                    <Checkbox
                                                        onChange={(event) => {
                                                            var checked = event.target.checked;
                                                            this.data['setAsPrivate'] = checked;
                                                        }}
                                                        color="primary"
                                                    />
                                                }
                                                label={UIText.setAsPrivate}
                                            />
                                        </Grid>
                                        <Grid item xs={12} >
                                            <AttendeesSearchBox
                                                url={References.usersLookupApi}
                                                apiType='user'
                                                hint={UIText.startTyping}
                                                multiple
                                                type='attendees'
                                                title={UIText.attendees}
                                                handleChangeAutoComplete={(type, value) => {
                                                    handleUniversalChangeAutoComplete(this, type, value, true);
                                                }}
                                            />
                                        </Grid>
                                        <Grid item xs={12}>
                                            <SearchBox
                                                url={References.taskLocationAutoCompleteApi}
                                                apiType='taskLocation'
                                                hint={UIText.startTyping}
                                                type='location'
                                                title={UIText.location}
                                                handleChangeAutoComplete={(type, value) => {
                                                    handleUniversalChangeAutoComplete(this, type, value);
                                                }} />
                                        </Grid>
                                        <Grid item container justifyItems='space-between' alignItems='center'>
                                            <Grid item xs>
                                                <Typography>{UIText.priority}</Typography>
                                            </Grid>
                                            <Grid item xs>
                                                <PriorityButton
                                                    options={this.priorities}
                                                    handleItemClicked={(option) => { handleUniversalChangeAutoComplete(this, 'priority', option) }} />
                                            </Grid>
                                        </Grid>
                                        <Grid item xs={12} >
                                            <UniversalAutoComplete
                                                type='meetingType'
                                                label={UIText.meetingType}
                                                options={this.meetingTypes}
                                                onChange={(type, value) => handleUniversalChangeAutoComplete(this, type, value)}
                                            />
                                        </Grid>                                                                                                                                          
                                    </Grid>
                                </AccordionDetails>
                            </Accordion>
                        </form>
                    </div>
                    <BottomBar onSubmit={this.onSubmit} />
                </div >
            }
        </React.Fragment>
        );
    }
}
const mapStateToProps = (state: ReduxState) => ({
    language: state.settings.language,
    runLoader: state.loader.runLoader,
    includeEmail: state.attachments.includeEmail,
    files: state.attachments.files
});
const mapDispatchToProps = (dispatch) => {
    return {
        successAlert: (message: string) => dispatch(actions.successAlert(message)),
        errorAlert: (message: string) => dispatch(actions.errorAlert(message)),
        stopLoader: () => dispatch(actions.stopLoader()),
        startLoader: () => dispatch(actions.startLoader()),
        startSaving: () => dispatch(actions.startSaving()),
        stopSaving: () => dispatch(actions.stopSaving()),
        openModal: () => dispatch(actions.openModal()),
        focusError: (id: string) => dispatch(actions.focusError(id)),
        setAttachments: (files) => dispatch(actions.setAttachments(files)),
        addFile: (file) => dispatch(actions.addFile(file)),
        deleteFile: (index) => dispatch(actions.deleteFile(index)),
        setHasEmailAttachments: (found: boolean) => dispatch(actions.setEmailHasAttachments(found)),
        setFetchingAttachments: (status: boolean) => dispatch(actions.setFetchingAttachments(status))
    }
};
type FormReduxProps = ReturnType<typeof mapStateToProps> & ReturnType<typeof mapDispatchToProps>;
interface IFormState {
    isJudged: boolean,
    errorType,
    requiredFields
}
interface IFormProps extends FormReduxProps, WithStyles<typeof useStyles>, AppProps {

}
const AppContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(Meeting);
export default withStyles(useStyles, { withTheme: true })(AppContainer);