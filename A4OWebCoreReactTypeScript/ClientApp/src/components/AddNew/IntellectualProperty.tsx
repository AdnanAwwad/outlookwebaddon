﻿import * as React from "react";
import { withStyles } from "@material-ui/core/styles";
import { connect, useSelector } from "react-redux";
import Grid from "@material-ui/core/Grid";
import PopupAlert from '../Common/Alert';
import { AxiosRequest, RequestHeaders } from '../../Helpers/AxiosRequest';
import { References } from '../../Helpers/References';
import * as actions from '../../redux/actions';
import * as UIStrings from '../../UIStrings';
import { Typography, createStyles, Theme, WithStyles, Accordion, AccordionSummary, AccordionDetails, IconButton, Tooltip, TextField } from "@material-ui/core";
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import SearchBox from '../Common/SearchBox';
import BottomBar from '../Common/BottomBar';
import Modal from '../Common/Modal';
import { Helpers, handleUniversalChangeTextField, handleUniversalChangeDateTimeField, handleUniversalChangeAutoComplete, loadEmailProps, loadEmailAttachments, getAttachments } from "../../Helpers/Helpers";
import FormSkeleton from "../Common/Skeleton/FormSkeleton";
import { AppProps } from "../App";
import { UniversalAutoComplete, AutoCompleteWithTwoStage, DateTextField, ContactCompanyComponent, UniversalTextField } from "../Common/TextFields";
import AttachmentsArea from "../Common/Attachments";
import { ReduxState } from "../..";
import DeleteIcon from '@material-ui/icons/Delete';
import { IEmailProps } from "./MatterNote";
import { addMessageProperty } from "../../GraphService";
import { v4 as uuidv4 } from 'uuid';
import { ActivityProps } from "../Activities/Activities";

const useStyles = (theme: Theme) => createStyles({
    paper: {
        marginTop: theme?.spacing(2),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center'
    },
    avatar: {
        margin: theme?.spacing(1),
        backgroundColor: 'blue',
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme?.spacing(3),
        paddingRight: "5px",
        marginBottom: theme?.spacing(8)
    },
    submit: {
        color: 'red'
    },
    textarea: {
        resize: "both",
        minHeight: "200px"
    },
    formControl: {
        margin: theme?.spacing(1),
        minWidth: 120,
    },
    selectEmpty: {
        marginTop: theme?.spacing(2),
    },
    root: {
        display: 'flex',
        flexDirection: 'column',
        flexGrow: 1
    },
    main: {
        marginBottom: theme?.spacing(8),
    },
    footer: {
        position: 'fixed',
        bottom: "0",
        marginTop: 'auto',
        width: "100%",
        backgroundColor: '#fff',
    },
    margin: {
        margin: theme?.spacing(1),
    },
    textField: {
        width: '25ch',
    },
    appBar: {
        position: 'fixed',
        height: '55px',
        paddingBottom: '10px',
        bottom: 0,
    },
    addOpponent: {
        margin: theme?.spacing(1),
    },
    icon: {
        width: '20px',
        height: '20px'
    }
});
class IntellectualProperty extends React.Component<IFormProps, IFormState> {

    attachments: any[];
    messageID: string;
    propertyRights: any[];
    propertyClasses: any[];
    propertySubCategories: any[];
    propertyNames: any[];
    clientTypes: any[];
    agentTypes: any[];
    assignedTeams: any[];
    countries: any[];
    data: any;
    subject: string;
    description: string;
    emailInfo: IEmailProps;
    constructor(props: IFormProps, context: any) {
        super(props, context);
        this.state = {
            errorType: false,
            requiredFields: {
                propertyRight: false,
                propertyClass: false,
                propertyName: false,
                subject: false
            },
            id: ''
        };
        this.attachments = [];
        this.messageID = '';
        this.propertyRights = [];
        this.propertyClasses = [];
        this.propertySubCategories = [];
        this.propertyNames = [];
        this.clientTypes = [];
        this.agentTypes = [];
        this.assignedTeams = [];
        this.countries = [];
        this.data = {};
        this.onSubmit = this.onSubmit.bind(this);
    }
    componentDidMount() {
        this.props.startLoader();
        this.getMailItem();
        this.fillData();
        window.addEventListener('includeEmailAttachment', this.handleChangeEmailAttachment.bind(this));
    }
    componentWillUnmount() {
        window.removeEventListener('includeEmailAttachment', this.handleChangeEmailAttachment);
    }
    handleChangeEmailAttachment(event) {
        if (event) {
            var includeEmailAttachments = event.detail;
            if (includeEmailAttachments) {
                this.props.addFile(this.attachments);
            }
            else {
                var files = this.props.files.filter(file => file.isEmailAttachment == false);
                this.props.setAttachments(files);
            }
        }
    }
    addAttachments(files: any[]) {
        if (files && files.length > 0) {
            this.attachments = this.attachments.concat(files);
        }
    }
    UIText = UIStrings.getLocaleStrings(this.props.language);
    getMailItem() {
        try {
            var item = Office.context.mailbox.item;
            var self = this;
            item.body.getAsync(Office.CoercionType.Text, function (asyncResult) {
                if (asyncResult.status !== Office.AsyncResultStatus.Succeeded) {
                }
                else {
                    self.messageID = item.itemId;
                    var body = asyncResult.value.trim();
                    var subject = item.subject;
                    if (body || subject) {
                        if (!Helpers.getUrlPar('isNew')) {
                            self.subject = subject;
                            self.description = body;
                            self.data = {
                                subject: subject,
                                description: body,
                            };
                        }
                    }
                    self.emailInfo = loadEmailProps(item, asyncResult);
                    loadEmailAttachments(self);
                }
            });
        } catch (e) {
            this.props.logger.error('IP: ' + e);
        }
    }
    async fillData() {
        try {
            let response = await AxiosRequest.get(References.addIntellectualPropertyApi); 
            if (response.data.error === "") {
                this.fillCustomData('propertyRights', response.data.success.data.IPR);
                this.fillCustomData('propertyClasses', response.data.success.data.ip_classes);
                this.fillCustomData('propertySubCategories', response.data.success.data.ip_subcategories);
                this.fillCustomData('propertyNames', response.data.success.data.ip_names);
                this.fillCustomData('clientTypes', response.data.success.data.clientTypes);
                this.fillCustomData('agentTypes', response.data.success.data.agentTypes);
                this.fillCustomData('assignedTeams', response.data.success.data.Provider_Groups);
                this.fillCustomData('countries', response.data.success.data.countries);
            }
            else {
                this.props.errorAlert(JSON.stringify(response.data.error));
            }
            this.props.stopLoader();
        } catch (e) {
            this.props.logger.error('IP: ' + e);
            this.props.stopLoader();
            this.props.errorAlert(this.UIText.errorOccured);
        }
    }
    fillCustomData(type: any, items: any) {
        if (!items || items.length == 0) {
            return;
        }
        var dataToFill = [];
        if (items) {
            if (type === 'priorities') {
                Object.entries(items).map(([key, value]) => dataToFill.push({ value: value, id: key, image: value + '.png' }));
            }
            else {
                Object.entries(items).map(([key, value]) => dataToFill.push({ value: value, id: key }));
            }
        }
        this[type] = dataToFill;
    }
    async onSubmit(form) {
        form.preventDefault();
        form.stopPropagation();
        var requiredFields = {
            propertyRight: !this.data.propertyRight?.value?.id?.length,
            propertyClass: !this.data.propertyClass?.value?.id?.length,
            propertyName: !this.data.propertyName?.value?.id?.length,
            subject: !this.data.subject?.length
        };
        this.setState({
            requiredFields: requiredFields
        });
        for (const [key, value] of Object.entries(requiredFields)) {
            if (value) {
                document.getElementById(key)?.scrollIntoView();
                return;
            }
        }
        this.props.startSaving();
        var ipRight = this.data.propertyRight?.value?.id;
        var iPClass = this.data.propertyClass?.value?.id;
        var iPSubcategory = this.data.propertySubCategory?.value?.id;
        var iPName = this.data.propertyName?.value?.id;
        var subject = this.data.subject;
        var description = this.data.description;
        var clientCompanyGroup = this.data.clientOwnerType != null ? this.data.clientOwnerType?.id : this.clientTypes[0]?.id;
        var clientName = this.data.clientOwnerName?.id;
        var country = this.data.country?.id;
        var assignedTeam = this.data.assignedTeam != null ? this.data.assignedTeam.id : this.assignedTeams[0]?.value?.id;
        var assignedMember = this.data.assignee?.id;
        var filedOn = this.data.filedOn;
        var filingNumber = this.data.filingNumber;
        var registrationDate = this.data.registrationDate;
        var registrationRef = this.data.registrationRef;
        var agentCompanyGroup = this.data.agentType?.id;
        var agent = this.data.agentName?.id;

        var intellectualProperty = {
            ipRight: ipRight,
            iPClass: iPClass,
            iPSubcategory: iPSubcategory,
            iPName: iPName,
            subject: subject,
            description: description,
            clientCompanyGroup: clientCompanyGroup,
            clientName: clientName,
            country: country,
            assignedteam: assignedTeam,
            assignedmember: assignedMember,
            filedon: filedOn,
            filingNumber: filingNumber,
            registrationDate: registrationDate,
            registrationRef: registrationRef,
            agentCompanyGroup: agentCompanyGroup,
            agent: agent
        };
        //Cleaning the data
        intellectualProperty = Helpers.cleaningData(intellectualProperty);

        var dataToPost = '';
        if (intellectualProperty.ipRight != null)
            dataToPost += "&intellectual_property_right_id=" + intellectualProperty.ipRight;
        if (intellectualProperty.iPClass != null)
            dataToPost += "&ip_class_id=" + intellectualProperty.iPClass;
        if (intellectualProperty.iPSubcategory)
            dataToPost += "&ip_subcategory_id=" + intellectualProperty.iPSubcategory;
        if (intellectualProperty.iPName != null)
            dataToPost += "&ip_name_id=" + intellectualProperty.iPName;
        if (intellectualProperty.subject != null)
            dataToPost += "&subject=" + intellectualProperty.subject;
        if (intellectualProperty.description != null)
            dataToPost += "&description=" + intellectualProperty.description;
        if (intellectualProperty.clientCompanyGroup != null)
            dataToPost += "&clientType=" + intellectualProperty.clientCompanyGroup;
        if (intellectualProperty.clientName != null)
            dataToPost += "&contact_company_id=" + intellectualProperty.clientName;
        if (intellectualProperty.country != null)
            dataToPost += "&country_id=" + intellectualProperty.country;
        if (intellectualProperty.assignedteam != null)
            dataToPost += "&provider_group_id=" + intellectualProperty.assignedteam;
        if (intellectualProperty.assignedmember != null)
            dataToPost += "&user_id=" + intellectualProperty.assignedmember;
        if (intellectualProperty.filedon != null)
            dataToPost += "&arrivalDate=" + intellectualProperty.filedon;
        if (intellectualProperty.registrationDate != null)
            dataToPost += "&registrationDate=" + intellectualProperty.registrationDate;
        if (intellectualProperty.registrationRef != null)
            dataToPost += "&registrationReference=" + intellectualProperty.registrationRef;
        if (intellectualProperty.agentCompanyGroup != null)
            dataToPost += "&agentType=" + intellectualProperty.agentCompanyGroup;
        if (intellectualProperty.agent != null)
            dataToPost += "&agentId=" + intellectualProperty.agent;
        if (intellectualProperty.filingNumber != null)
            dataToPost += "&filingNumber=" + intellectualProperty.filingNumber;
        var response = await AxiosRequest.post(References.addIntellectualPropertyApi, dataToPost);
        try {
            var resp = response.data;
            var error = resp?.error;
            if (error != "") {
                var respError = Helpers.handleErrorResponse(error, 'intellectualProperty', form, this.onSubmit);
                if (respError != null && respError != '') {
                    this.props.errorAlert(JSON.stringify(respError));
                }
            }
            else {
                var caseId = resp.success?.data?.id;
                if (caseId) {
                    try {
                        const reqHeader = new RequestHeaders();
                        var formData = new FormData();
                        const files = await getAttachments(this);
                        if (files.length > 0) {
                            files.forEach(file => {
                                formData.append('Files', file);
                            });
                            formData.append('Url', reqHeader.url + References.uploadMatterAttachmentsApi);
                            formData.append('DataToPost', 'legal_case_id=' + caseId);
                            formData.append('UserKey', reqHeader.userKey);
                            var uploadResponse = await AxiosRequest.postToServer('PostData/UploadFileToApp4legal', formData);
                            if (uploadResponse.status != 200) {
                                this.props.errorAlert(this.UIText.errorUploadingFiles);
                            }
                            const resp = uploadResponse.data;
                            if (resp?.error === '') {
                            }
                            else {
                                this.props.errorAlert(JSON.stringify(resp?.error));
                            }
                        }
                    }
                    catch (err) {
                        this.props.logger.error('IP: ' + err);
                        this.props.errorAlert(this.UIText.errorOccured);
                    }
                    this.setState({ id: caseId });
                    var propertyData: ActivityProps = {
                        guid: uuidv4(),
                        type: 'intellectualProperty',
                        id: caseId,
                        subject: intellectualProperty.subject,
                        date: Helpers.formatDate(new Date(), true),
                        priority: 'medium'
                    }
                    const token = await this.props.getAccessToken();
                    await addMessageProperty(token, this.messageID, propertyData);
                    this.props.openModal();
                }
            }
        }
        catch (err) {
            this.props.logger.error('IP: ' + err);
            this.props.errorAlert(this.UIText.errorOccured);
        }
        this.props.stopSaving();
    }
    handleControlledField(event, valid = { type: '', valid: false }) {
        var type = event.target.name;
        var value = event.target.value;
        var reqFields = this.state.requiredFields;
        if (reqFields[type] != null) {
            if (valid.type.length > 0)
                reqFields[type] = !valid.valid;
            else
                reqFields[type] = !value.length;
            this.setState({ requiredFields: reqFields });
        }
        this.setState({ ...this.state, [type]: value });
    }
    render() {
        const { classes } = this.props;
        const UIText = this.UIText;
        return (<React.Fragment>
            <PopupAlert />
            <Modal type='intellectualProperty' id={this.state.id} />
            {this.props.runLoader ?
                <FormSkeleton input='form' /> :
                <div className={classes.root}>
                    <div className={classes.paper}>
                        <form id='intellectualProperty' className={classes.form} noValidate onSubmit={this.onSubmit.bind(this)}>
                            <Accordion defaultExpanded={true}>
                                <AccordionSummary
                                    expandIcon={<ExpandMoreIcon />}
                                    aria-controls="panel1a-content"
                                    id="panel1a-header"
                                >
                                    <Grid container alignItems='center'>
                                        <Grid item xs={2}>
                                            <img src={require('../../assets/ipIcon.svg')} className={classes.icon} />
                                        </Grid>
                                        <Grid item xs>
                                            <Typography variant='h6' gutterBottom> {UIText.generalInformation} </Typography>
                                        </Grid>
                                    </Grid>
                                </AccordionSummary>
                                <AccordionDetails>
                                    <Grid container spacing={2} >
                                        <Grid item xs={12}>
                                            <UniversalAutoComplete
                                                type='propertyRight'
                                                required
                                                error={this.state.requiredFields.propertyRight}
                                                label={UIText.propertyRight}
                                                options={this.propertyRights}
                                                onChange={(type, value) => handleUniversalChangeAutoComplete(this, type, value)}
                                            />
                                        </Grid>
                                        <Grid item xs={12}>
                                            <UniversalAutoComplete
                                                type='propertyClass'
                                                required
                                                error={this.state.requiredFields.propertyClass}
                                                label={UIText.propertyClass}
                                                options={this.propertyClasses}
                                                onChange={(type, value) => handleUniversalChangeAutoComplete(this, type, value)}
                                            />
                                        </Grid>
                                        <Grid item xs={12}>
                                            <UniversalAutoComplete
                                                type='propertySubCategory'
                                                label={UIText.propertySubCategory}
                                                options={this.propertySubCategories}
                                                onChange={(type, value) => handleUniversalChangeAutoComplete(this, type, value)}
                                            />
                                        </Grid>
                                        <Grid item xs={12}>
                                            <UniversalAutoComplete
                                                type='propertyName'
                                                required
                                                error={this.state.requiredFields.propertyName}
                                                label={UIText.propertyName}
                                                options={this.propertyNames}
                                                onChange={(type, value) => handleUniversalChangeAutoComplete(this, type, value)}
                                            />
                                        </Grid>
                                        <Grid item xs={12}>
                                            <UniversalTextField
                                                type='subject'
                                                required
                                                defaultValue={this.subject}
                                                error={this.state.requiredFields.subject}
                                                label={UIText.subject}
                                                onChange={(event, value) => handleUniversalChangeTextField(this, event, value)}
                                            />
                                            <UniversalTextField
                                                type='description'
                                                label={UIText.description}
                                                defaultValue={this.description}
                                                onChange={(event, value) => handleUniversalChangeTextField(this, event, value)}
                                                multiline
                                                rows={6}
                                            />
                                        </Grid>
                                        <ContactCompanyComponent
                                            type='ownerType'
                                            parentType='clientOwnerType'
                                            parentLabel={UIText.clientOwnerType}
                                            options={this.clientTypes}
                                            childType='clientOwnerName'
                                            childLabel={UIText.clientOwnerName}
                                            helperText={UIText.startTyping}
                                            onChange={(type, value) => handleUniversalChangeAutoComplete(this, type, value)}
                                        />
                                        <Grid item xs={12}>
                                            <UniversalAutoComplete
                                                type='country'
                                                label={UIText.country}
                                                options={this.countries}
                                                onChange={(type, value) => handleUniversalChangeAutoComplete(this, type, value)}
                                            />
                                        </Grid>
                                        <AutoCompleteWithTwoStage
                                            type='assign'
                                            parentOptions={this.assignedTeams}
                                            parentType='assignedTeam'
                                            parentLabel={UIText.assignedTeam}
                                            childType='assignee'
                                            childLabel={UIText.assignee}
                                            onChange={(type, value) => handleUniversalChangeAutoComplete(this, type, value)}
                                        />
                                        <Grid item xs={12}>
                                            <DateTextField
                                                type='filedOn'
                                                label={UIText.filedOn}
                                                defaultValue={null}
                                                onChange={(type, value) => handleUniversalChangeDateTimeField(this, type, value)}
                                            />
                                            <UniversalTextField
                                                type='filingNumber'
                                                label={UIText.filingNumber}
                                                onChange={(event, value) => handleUniversalChangeTextField(this, event, value)}
                                            />
                                            <Grid item xs={12} style={{marginTop: '5px'}}>
                                                <DateTextField
                                                    type='registrationDate'
                                                    label={UIText.registrationDate}
                                                    defaultValue={null}
                                                    onChange={(type, value) => handleUniversalChangeDateTimeField(this, type, value)}
                                                    />
                                            </Grid>
                                            <UniversalTextField
                                                type='registrationRef'
                                                label={UIText.registrationRef}
                                                onChange={(event, value) => handleUniversalChangeTextField(this, event, value)}
                                                />
                                        </Grid>
                                        <ContactCompanyComponent
                                            type='agentType'
                                            parentType='agentType'
                                            parentLabel={UIText.agentType}
                                            options={this.clientTypes}
                                            childType='agentName'
                                            childLabel={UIText.agentName}
                                            helperText={UIText.startTyping}
                                            onChange={(type, value) => handleUniversalChangeAutoComplete(this, type, value)}
                                        />
                                    </Grid>
                                </AccordionDetails>
                            </Accordion>
                            <Accordion>
                                <AccordionSummary
                                    expandIcon={<ExpandMoreIcon />}
                                    aria-controls="panel3a-content"
                                    id="panel3a-header"
                                >
                                    <Grid container alignItems='center'>
                                        <Grid item xs={2}>
                                            <img src={require('../../assets/attachmentsIcon.svg')} className={classes.icon} />
                                        </Grid>
                                        <Grid item xs>
                                            <Typography variant='h6' gutterBottom>{UIText.attachments}</Typography>
                                        </Grid>
                                    </Grid>
                                </AccordionSummary>
                                <AccordionDetails>
                                    <AttachmentsArea token={this.props.getAccessToken()} onAddAttachments={this.addAttachments.bind(this)} />
                                </AccordionDetails>
                            </Accordion>
                        </form>
                    </div>
                    <BottomBar onSubmit={this.onSubmit} />
                </div >
            }
        </React.Fragment>
        );
    }
}
const mapStateToProps = (state: ReduxState) => ({
    language: state.settings.language,
    runLoader: state.loader.runLoader,
    includeEmail: state.attachments.includeEmail,
    files: state.attachments.files,
    moreEmails: state.attachments.moreEmails,
    includeEmailAttachments: state.attachments.includeEmailAttachments
});
const mapDispatchToProps = (dispatch) => {
    return {
        successAlert: (message: string) => dispatch(actions.successAlert(message)),
        errorAlert: (message: string) => dispatch(actions.errorAlert(message)),
        stopLoader: () => dispatch(actions.stopLoader()),
        startLoader: () => dispatch(actions.startLoader()),
        startSaving: () => dispatch(actions.startSaving()),
        stopSaving: () => dispatch(actions.stopSaving()),
        openModal: () => dispatch(actions.openModal()),
        focusError: (id: string) => dispatch(actions.focusError(id)),
        setAttachments: (files) => dispatch(actions.setAttachments(files)),
        addFile: (file) => dispatch(actions.addFile(file)),
        deleteFile: (index) => dispatch(actions.deleteFile(index)),
        setHasEmailAttachments: (found: boolean) => dispatch(actions.setEmailHasAttachments(found)),
        setFetchingAttachments: (status: boolean) => dispatch(actions.setFetchingAttachments(status))
    }
};
type FormReduxProps = ReturnType<typeof mapStateToProps> & ReturnType<typeof mapDispatchToProps>;
interface IFormState {
    errorType,
    requiredFields,
    id: string
}
interface IFormProps extends FormReduxProps, WithStyles<typeof useStyles>, AppProps {

}
const AppContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(IntellectualProperty);
export default withStyles(useStyles, { withTheme: true })(AppContainer);