﻿import * as React from "react";
import { withStyles } from "@material-ui/core/styles";
import { connect, useSelector } from "react-redux";
import Grid from "@material-ui/core/Grid";
import PopupAlert from '../Common/Alert';
import { AxiosRequest, RequestHeaders } from '../../Helpers/AxiosRequest';
import { References } from '../../Helpers/References';
import * as actions from '../../redux/actions';
import * as UIStrings from '../../UIStrings';
import { Typography, createStyles, Theme, WithStyles, Accordion, AccordionSummary, AccordionDetails, IconButton, Button, Tooltip } from "@material-ui/core";
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import SearchBox from '../Common/SearchBox';
import BottomBar from '../Common/BottomBar';
import Modal from '../Common/Modal';
import { Helpers, handleUniversalChangeTextField, handleUniversalChangeAutoComplete, handleUniversalChangeDateTimeField, loadEmailProps, loadEmailAttachments, getAttachments } from "../../Helpers/Helpers";
import FormSkeleton from "../Common/Skeleton/FormSkeleton";
import AddIcon from '@material-ui/icons/Add';
import { AppProps } from "../App";
import { UniversalAutoComplete, AutoCompleteWithTwoStage, UniversalTextField, DateTextField, SwitchTextField } from "../Common/TextFields";
import AttachmentsArea from "../Common/Attachments";
import { ReduxState } from "../..";
import DeleteIcon from '@material-ui/icons/Delete';
import { IEmailProps } from "./MatterNote";
import { PriorityButton } from "../Common/SplitButton";
import { v4 as uuidv4 } from 'uuid';
import { ActivityProps } from "../Activities/Activities";
import { addMessageProperty } from "../../GraphService";
import uniqid from "uniqid";

const useStyles = (theme: Theme) => createStyles({
    paper: {
        marginTop: theme?.spacing(2),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center'
    },
    avatar: {
        margin: theme?.spacing(1),
        backgroundColor: 'blue',
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme?.spacing(3),
        paddingRight: "5px",
        marginBottom: theme?.spacing(8)
    },
    submit: {
        color: 'red'
    },
    textarea: {
        resize: "both",
        minHeight: "200px"
    },
    formControl: {
        margin: theme?.spacing(1),
        minWidth: 120,
    },
    selectEmpty: {
        marginTop: theme?.spacing(2),
    },
    root: {
        display: 'flex',
        flexDirection: 'column',
        flexGrow: 1
    },
    main: {
        marginBottom: theme?.spacing(8),
    },
    footer: {
        position: 'fixed',
        bottom: "0",
        marginTop: 'auto',
        width: "100%",
        backgroundColor: '#fff',
    },
    margin: {
        margin: theme?.spacing(1),
    },
    textField: {
        width: '25ch',
    },
    appBar: {
        position: 'fixed',
        height: '55px',
        paddingBottom: '10px',
        bottom: 0,
    },
    addOpponent: {
        margin: theme?.spacing(1),
    },
    icon: {
        width: '20px',
        height: '20px'
    }
});
class Matter extends React.Component<IFormProps, IFormState> {

    attachments: any;
    messageID: string;
    matterType: string;
    practiceAreas: any[];
    clientTypes: any[];
    assignedTeams: any[];
    priorities: any[];
    matterStages: any[];
    clientPositions: any[];
    data: any;
    emailInfo: IEmailProps;
    defaultCurrency: string;
    defaultCaseType: any;
    defaultAssignedTeam: any;
    defaultAssignee: any;
    visibleAssignedTeam: boolean;
    visibleAssignee: boolean; 
    teamUsers: [];
    constructor(props: IFormProps, context: any) {
        super(props, context);
        var opponentId = uniqid();
        this.state = {
            errorType: false,
            sharedWith: false,
            requiredFields: {
                name: false,
                practiceArea: false,
                arrivalDate: false
            },
            id: '',
            isClientCompany: true,
            opponents: [{ id: opponentId }]
        };
        this.attachments = [];
        this.assignedTeams = [];
        this.practiceAreas = [];
        this.clientTypes = [];
        this.matterStages = [];
        this.clientPositions = [];
        this.priorities = [];
        this.data = { opponents: [{ id: opponentId}]};
        this.defaultCurrency = '';
        this.handleOpponentData = this.handleOpponentData.bind(this);
        this.handleDeleteOpponent = this.handleDeleteOpponent.bind(this);
        this.matterType = Helpers.getUrlPar('matterType');
        this.onSubmit = this.onSubmit.bind(this);
    }
    componentDidMount() {
        this.props.startLoader();
        this.getMailItem();
        this.fillData();
        window.addEventListener('includeEmailAttachment', this.handleChangeEmailAttachment.bind(this));        
    }
    componentWillUnmount() {
        window.removeEventListener('includeEmailAttachment', this.handleChangeEmailAttachment);
    }
    handleChangeEmailAttachment(event) {
        if (event) {
            var includeEmailAttachments = event.detail;
            if (includeEmailAttachments) {
                this.props.addFile(this.attachments);
            }
            else {
                var files = this.props.files.filter(file => file.isEmailAttachment == false);
                this.props.setAttachments(files);
            }
        }
    }
    addAttachments(files: any[]) {
        if (files && files.length > 0) {
            this.attachments = this.attachments.concat(files);
        }
    }
    UIText = UIStrings.getLocaleStrings(this.props.language);
    getMailItem() {
        try {
            var item = Office.context.mailbox.item;
            var self = this;
            item.body.getAsync(Office.CoercionType.Text, function (asyncResult) {
                if (asyncResult.status !== Office.AsyncResultStatus.Succeeded) {
                }
                else {
                    var body = asyncResult.value.trim();
                    var subject = item.subject;
                    self.messageID = item.itemId;
                    if (body || subject) {
                        if (!Helpers.getUrlPar('isNew')) {
                            self.data = {
                                ...self.data,
                                name: subject,
                                description: body
                            }
                        }
                    }
                    self.emailInfo = loadEmailProps(item, asyncResult);
                    loadEmailAttachments(self);
                }
            });
        } catch (e) {
            this.props.logger.error(this.matterType + ': ' + e);
        }
    }
    async fillData() {
        try {
            let response = await AxiosRequest.get(this.matterType === 'corporateMatter' ? References.addCorporateMatterApi : References.addLitigationCaseApi);
            if (response.data.error === "") {
                this.fillCustomData('practiceAreas', response.data.success.data.case_types);
                this.fillCustomData('assignedTeams', response.data.success.data.provider_groups);
                this.fillCustomData('matterStages', response.data.success.data.caseStages);
                this.fillCustomData('priorities', response.data.success.data.priorities);
                this.fillCustomData('clientTypes', response.data.success.data.clientTypes);
                this.fillCustomData('clientPositions', response.data.success.data.clientPositions);
                try {
                    const defaultValues = response.data.success.data.default_values;
                    const assignments = response.data.success.data.assignments;
                    const defaultCaseTypeId = defaultValues.case_type_id;
                    const defaultCrncy = defaultValues.caseValueCurrency;
                    if (defaultCaseTypeId != null && defaultCaseTypeId.length > 0) {
                        this.defaultCaseType = this.practiceAreas.filter(type => type.value?.id === defaultCaseTypeId)[0];
                        this.data = { ...this.data, practiceArea: this.defaultCaseType };
                    }
                    if (defaultCrncy) {
                        this.defaultCurrency = defaultCrncy;
                    }
                    this.visibleAssignee = assignments?.visible_assignee === '1' ? false : true;
                    this.visibleAssignedTeam = assignments?.visible_assigned_team === '1' ? false : true;
                    var assignedTeam;
                    var assignee;
                    var teamUsers;
                    if (defaultValues.provider_group_id && defaultValues.provider_group_id.length > 0)
                        assignedTeam = defaultValues.provider_group_id;
                    if (assignments.assigned_team && assignments.assigned_team.length > 0)
                        assignedTeam = assignments.assigned_team;
                    if (defaultValues.provider_group_users && defaultValues.provider_group_users.length > 0)
                        teamUsers = defaultValues.provider_group_users;
                    if (assignments.user_relation && assignments.user_relation.length > 0)
                        assignee = assignments.user_relation;
                    if (assignedTeam) {
                        this.defaultAssignedTeam = this.assignedTeams.filter(team => team.value?.id === assignedTeam)[0];
                        this.data = { ...this.data, assignedTeam: this.defaultAssignedTeam };
                    }
                    this.teamUsers = teamUsers;
                    if (teamUsers?.length > 0 && assignee?.length > 0) {
                        this.defaultAssignee = teamUsers.filter(user => user.id === assignee)[0];
                        if (this.defaultAssignee) {
                            this.data = { ...this.data, assignee: this.defaultAssignee };
                        }
                    }
                }
                catch (err) {
                    this.props.logger.error(this.matterType + ': ' + err);
                }
            }
            else {
                this.props.errorAlert(JSON.stringify(response.data.error));
            }
            this.props.stopLoader();
        } catch (e) {
            this.props.logger.error(this.matterType + ': ' + e);
            this.props.stopLoader();
            this.props.errorAlert(this.UIText.errorOccured);
        }
    }
    fillCustomData(type: any, items: any) {
        if (!items || items.length == 0) {
            return;
        }
        var dataToFill = [];
        if (items) {
            if (type === 'priorities') {
                Object.entries(items).map(([key, value]) => dataToFill.push({ value: value, id: key, image: value + '.png' }));
            }
            else {
                Object.entries(items).map(([key, value]) => dataToFill.push({ value: value, id: key }));
            }
        }
        this[type] = dataToFill;
    }
    async onSubmit(form) {
        try {
            form.preventDefault();
            form.stopPropagation();
            var nameField = this.data.name;
            var practiceAreaField = this.data.practiceArea?.id;
            var arrivalDateField = this.data.arrivalDate != null ? this.data.arrivalDate : Helpers.formatDate(new Date());
            var requiredFields = {
                name: !nameField?.length,
                practiceArea: !practiceAreaField?.length,
                arrivalDate: !arrivalDateField?.length
            };
            this.setState({
                requiredFields: requiredFields
            });
            for (const [key, value] of Object.entries(requiredFields)) {
                if (value) {
                    document.getElementById(key)?.scrollIntoView();
                    return;
                }
            }
            this.props.startSaving();
            var name = this.data.name;
            var description = this.data.description;
            var practiceArea = this.data.practiceArea?.value?.id;
            var clientType = this.data.clientType;
            var clientName = this.data.clientName;
            var assignedTeam = this.data.assignedTeam != null ? this.data.assignedTeam?.value?.id : this.assignedTeams[0].value?.id;
            var assignee = this.data.assignee;
            var requester = this.data.requestedBy;
            var priority = this.data.priority != null ? this.data.priority?.id : 'medium';
            var arrivalDate = this.data.arrivalDate != null ? this.data.arrivalDate : Helpers.formatDate(new Date());
            var filedOn = this.data.filedOn;
            var dueDate = this.data.dueDate;
            var matterRef = this.data.matterRef ? encodeURIComponent(this.data.matterRef) : null;
            var matterStage = this.data.matterStage?.value?.id;
            var matterValue = this.data.matterValue;
            var estimatedEffort = this.data.estimatedEffort;
            var cbPrivate = this.state.sharedWith;
            var sharedWith = this.data.sharedWith;
            const reqHeader = new RequestHeaders();
            var legalMatter = {
                matterType: this.matterType,
                attachmentIds: null,
                accessToken: await this.props.getAccessToken(),
                messageId: this.messageID,
                isEmailIncluded: String(this.props.includeEmail),
                url: reqHeader.url,
                userKey: reqHeader.userKey,
                subject: name,
                casedescription: description,
                casetype: practiceArea,
                contactcompany: clientType?.id,
                clientname: clientName?.id,
                assignedteam: assignedTeam,
                assignedmember: assignee?.id,
                arrivaldate: arrivalDate,
                filedon: filedOn,
                duedate: dueDate,
                fileref: matterRef,
                casepriority: priority,
                caseStageSelect: matterStage,
                requestedby: requester?.id,
                casevalue: matterValue,
                estimatedeffort: estimatedEffort,
                cbprivate: cbPrivate,
                sharedwith: sharedWith,
            };
            //Cleaning the data
            legalMatter = Helpers.cleaningData(legalMatter);

            var dataToPost = 'lawSuit=no&externalizeLawyers=no&archived=no&private=' + (legalMatter.cbprivate ? 'yes' : 'no');
            if (legalMatter.subject != null)
                dataToPost += "&subject=" + legalMatter.subject;
            if (legalMatter.casetype != null)
                dataToPost += "&case_type_id=" + legalMatter.casetype;
            if (legalMatter.casedescription)
                dataToPost += "&description=" + encodeURI(legalMatter.casedescription);
            if (legalMatter.assignedteam != null)
                dataToPost += "&provider_group_id=" + legalMatter.assignedteam;
            if (legalMatter.assignedmember != null)
                dataToPost += "&assignedTo=" + legalMatter.assignedmember;
            if (legalMatter.fileref != null)
                dataToPost += "&internalReference=" + legalMatter.fileref;
            if (legalMatter.requestedby != null)
                dataToPost += "&requestedBy=" + legalMatter.requestedby;
            if (legalMatter.contactcompany != null)
                dataToPost += "&clientType=" + legalMatter.contactcompany;
            if (legalMatter.clientname != null)
                dataToPost += "&contact_company_id=" + legalMatter.clientname;
            if (legalMatter.caseStageSelect != null)
                dataToPost += "&legal_case_stage_id=" + legalMatter.caseStageSelect;
            if (legalMatter.casepriority != null)
                dataToPost += "&priority=" + legalMatter.casepriority.toLowerCase();
            if (legalMatter.arrivaldate != null)
                dataToPost += "&caseArrivalDate=" + legalMatter.arrivaldate;
            if (legalMatter.filedon != null)
                dataToPost += "&arrivalDate=" + legalMatter.filedon;
            if (legalMatter.duedate != null)
                dataToPost += "&dueDate=" + legalMatter.duedate;
            if (legalMatter.casevalue != null)
                dataToPost += "&caseValue=" + legalMatter.casevalue;
            if (legalMatter.estimatedeffort != null)
                dataToPost += "&estimatedEffort=" + legalMatter.estimatedeffort;
            if (legalMatter.cbprivate) {
                var privateArray = '';
                if (legalMatter.sharedwith != null) {
                    for (var i = 0; i < legalMatter.sharedwith.length; i++) {
                        privateArray += `&Legal_Case_Watchers_Users[${i}]=` + legalMatter.sharedwith[i];
                    }
                }
                if (privateArray.length > 0)
                    dataToPost += privateArray;
            }
            if (this.matterType == 'litigationCase') {
                var clientPosition = this.data.clientPosition?.value?.id;
                if (clientPosition != null && clientPosition.length > 0)
                    dataToPost += '&legal_case_client_position_id=' + clientPosition;
                var opponents = this.data.opponents;
                if (opponents?.length > 0) {
                    opponents.forEach((opponent, index) => {
                        var opponentToAdd = '';
                        var type = opponent?.opponentType?.id;
                        var name = opponent?.opponentName?.id;
                        var position = opponent?.opponentPosition?.value?.id;
                        if (type != null && type.length > 0 && name != null && name.length > 0) {
                            opponentToAdd += `&opponent_member_type[${index}]=` + type;
                            opponentToAdd += `&opponent_member_id[${index}]=` + name;
                            if (position != null && position.length > 0) {
                                opponentToAdd += `&opponent_position[${index}]=` + position;
                            }
                        }
                        dataToPost += opponentToAdd;
                    });
                }
            }
            var response = await AxiosRequest.post(this.matterType === 'corporateMatter' ? References.addMatterApi : References.addLitigationCaseApi, dataToPost);
            try {
                var resp = response.data;
                var error = resp?.error;
                if (error != "") {
                    var respError = Helpers.handleErrorResponse(error, 'matter', form, this.onSubmit);
                    if (respError != null && respError != '') {
                        this.props.errorAlert(JSON.stringify(respError));
                    }
                }
                else {
                    var caseId = resp.success?.data?.case_id;
                    if (caseId) {
                        try {
                            const reqHeader = new RequestHeaders();
                            var formData = new FormData();
                            const files = await getAttachments(this);
                            if (files.length > 0) {
                                files.forEach(file => {
                                    formData.append('Files', file);
                                });
                                formData.append('Url', reqHeader.url + References.uploadMatterAttachmentsApi);
                                formData.append('DataToPost', 'legal_case_id=' + caseId.replace('M', ''));
                                formData.append('UserKey', reqHeader.userKey);
                                var uploadResponse = await AxiosRequest.postToServer('PostData/UploadFileToApp4legal', formData);
                                if (uploadResponse.status != 200) {
                                    this.props.errorAlert(this.UIText.errorUploadingFiles);
                                }
                                const resp = uploadResponse.data;
                                if (resp?.error === '') {

                                }
                                else {
                                    this.props.errorAlert(JSON.stringify(resp?.error));
                                }
                            }
                        }
                        catch (err) {
                            this.props.logger.error(this.matterType + ': ' + err);
                            this.props.errorAlert(this.UIText.errorOccured);
                        }
                        this.setState({ id: caseId });
                        var propertyData: ActivityProps = {
                            guid: uuidv4(),
                            type: this.matterType,
                            id: caseId,
                            subject: legalMatter.subject,
                            date: Helpers.formatDate(new Date(), true),
                            priority: legalMatter.casepriority
                        }
                        const token = await this.props.getAccessToken();
                        await addMessageProperty(token, this.messageID, propertyData);
                        this.props.openModal();
                    }
                }
            }
            catch (err) {
                this.props.errorAlert(this.UIText.errorOccured);
            }
            this.props.stopSaving();
        }
        catch (err) {
            this.props.errorAlert(err);
        }
    }
    handleSharedWithSwitch(event) {
        this.setState({ sharedWith: event.target.checked });
    }
    handleDeleteOpponent(id) {
        this.data.opponents = this.data.opponents.filter(opponent => opponent.id !== id);
        this.setState({ opponents: this.state.opponents.filter(opponent => opponent.id !== id) });
    }
    handleOpponentData(index, type, value) {
        if (this.data.opponents == null) {
            this.data.opponents = []
        }
        var currentData = this.data.opponents[index];
        this.data.opponents[index] = { ...currentData, [type]: value };
    }
    render() {
        const { classes } = this.props;
        const UIText = this.UIText;
        return (<React.Fragment>
            <PopupAlert />
            <Modal type={this.matterType} id={this.state.id} />
            {this.props.runLoader ?
                <FormSkeleton input='form' /> :
                <div className={classes.root}>
                    <div className={classes.paper}>
                        <form id={this.matterType} className={classes.form} noValidate onSubmit={this.onSubmit.bind(this)}>
                            <Accordion defaultExpanded={true}>
                                <AccordionSummary
                                    expandIcon={<ExpandMoreIcon />}
                                    aria-controls="panel1a-content"
                                    id="panel1a-header"
                                >
                                    <Grid container alignItems='center'>
                                        <Grid item xs={2}>
                                            <img src={require('../../assets/' + this.matterType + 'Icon.svg')} className={classes.icon} />
                                        </Grid>
                                        <Grid item xs>
                                            <Typography variant='h6' gutterBottom> {UIText.generalInformation} </Typography>
                                        </Grid>
                                    </Grid>
                                </AccordionSummary>
                                <AccordionDetails>
                                    <Grid container spacing={2}>
                                        <Grid item xs={12}>
                                            <UniversalTextField
                                                type='name'
                                                error={this.state.requiredFields.name}
                                                required
                                                focus
                                                defaultValue={this.data.name}
                                                label={UIText.name}
                                                onChange={(event, value) => handleUniversalChangeTextField(this, event, value)}
                                            />
                                            <UniversalTextField
                                                type='description'
                                                label={UIText.description}
                                                defaultValue={this.data.description}
                                                multiline
                                                rows={10}
                                                onChange={(event, value) => handleUniversalChangeTextField(this, event, value)}
                                            />
                                        </Grid>
                                        <Grid item xs={12}>
                                            <UniversalAutoComplete
                                                type='practiceArea'
                                                label={UIText.practiceArea}
                                                options={this.practiceAreas}
                                                defaultValue={this.defaultCaseType}
                                                onChange={(type, value) => handleUniversalChangeAutoComplete(this, type, value)}
                                                error={this.state.requiredFields.practiceArea}
                                                required
                                            />
                                        </Grid>
                                        <Grid item xs={12}>
                                            <UniversalAutoComplete
                                                type='clientType'
                                                label={UIText.clientType}
                                                options={this.clientTypes}
                                                onChange={(type, value) => {
                                                    this.setState({ isClientCompany: value?.id === 'company' })
                                                    handleUniversalChangeAutoComplete(this, type, value)
                                                }}
                                            />
                                        </Grid>
                                        <Grid item xs={12}>
                                            <SearchBox
                                                url={References.clientsLookupApi}
                                                apiType='client'
                                                extraData={'&clientType=' + (this.state.isClientCompany ? 'company' : 'contact')}
                                                hint={UIText.startTyping}
                                                type='clientName'
                                                title={UIText.clientName}
                                                handleChangeAutoComplete={(type, value) => {
                                                    handleUniversalChangeAutoComplete(this, type, value);
                                                }}
                                            />
                                        </Grid>
                                        {this.matterType === 'litigationCase' ?
                                            <Grid item xs>
                                                <UniversalAutoComplete
                                                    type='clientPosition'
                                                    label={UIText.clientPosition}
                                                    options={this.clientPositions}
                                                    onChange={(type, value) => handleUniversalChangeAutoComplete(this, type, value)}
                                                />
                                            </Grid> : <></>}
                                        <Grid item xs={12}>
                                            <SearchBox
                                                url={References.contactLookupApi}
                                                apiType='contact'
                                                type='requestedBy'
                                                hint={UIText.startTyping}
                                                title={UIText.requestedBy}
                                                handleChangeAutoComplete={(type, value) => {
                                                    handleUniversalChangeAutoComplete(this, type, value);
                                                }}
                                            />
                                        </Grid>
                                        {
                                            this.matterType === 'litigationCase' ?
                                                <Grid item xs={12}>
                                                    {this.state.opponents.map((item, index) => {
                                                        return (<Opponent
                                                            key={item.id}
                                                            id={item.id}
                                                            index={index}
                                                            clientTypes={this.clientTypes}
                                                            clientPositions={this.clientPositions}
                                                            onChange={this.handleOpponentData}
                                                            onDelete={this.handleDeleteOpponent.bind(this)}
                                                            opponentCount={this.state.opponents.length}
                                                        />);
                                                    })}
                                                    <Grid item container xs={12} justifyItems='center' style={{ marginTop: '10px' }}>
                                                        <Button
                                                            type="button"
                                                            variant="contained"
                                                            color="primary"
                                                            style={{ margin: 'auto' }}
                                                            startIcon={<AddIcon />}
                                                            onClick={(event) => {
                                                                event.preventDefault();
                                                                event.stopPropagation();
                                                                const id = uniqid();
                                                                this.data.opponents?.push({ id: id });
                                                                this.setState({ opponents: [...this.state.opponents, { id: id }] });
                                                            }}
                                                        >
                                                            {UIText.addMore}
                                                        </Button>
                                                    </Grid>
                                                </Grid>
                                                :
                                                <></>
                                        }
                                        <AutoCompleteWithTwoStage
                                            type='assign'
                                            parentOptions={this.assignedTeams}
                                            parentType='assignedTeam'
                                            parentLabel={UIText.assignedTeam}
                                            parentVisible={this.visibleAssignedTeam}
                                            parentDefault={this.defaultAssignedTeam}
                                            childVisible={this.visibleAssignee}
                                            childType='assignee'
                                            childLabel={UIText.assignee}
                                            childOptions={this.teamUsers}
                                            childDefault={this.defaultAssignee}
                                            onChange={(type, value) => handleUniversalChangeAutoComplete(this, type, value)}
                                        />
                                        <Grid item xs={12}>
                                            <Typography>{this.UIText.sharedWith}</Typography>
                                            <SwitchTextField
                                                type='sharedWith'
                                                firstLabel={UIText.everyone}
                                                secondLabel={UIText.private}
                                                onSwitchChange={this.handleSharedWithSwitch.bind(this)}
                                                onChange={(type, value) => handleUniversalChangeAutoComplete(this, type, value)}
                                                selected={this.state.sharedWith}
                                                searchType='user'
                                                searchUrl={References.usersLookupApi}
                                                label={UIText.sharedWith}
                                                multiple
                                            />
                                        </Grid>
                                        <Grid item xs={12}>
                                            <DateTextField
                                                type='arrivalDate'
                                                label={UIText.arrivaleDate}
                                                required
                                                error={this.state.requiredFields.arrivalDate}
                                                onChange={(type, value) => {
                                                    var reqFields = this.state.requiredFields;
                                                    if (reqFields[type] != null) {
                                                        var date = Helpers.formatDate(value);
                                                        reqFields[type] = !date.length;
                                                        this.setState({ requiredFields: reqFields });
                                                    }
                                                    handleUniversalChangeDateTimeField(this, type, value);
                                                }}
                                            />
                                        </Grid>
                                        <Grid item xs={12}>
                                            <DateTextField
                                                type='filedOn'
                                                label={UIText.filedOn}
                                                defaultValue={null}
                                                onChange={(type, value) => handleUniversalChangeDateTimeField(this, type, value)}
                                            />
                                        </Grid>
                                        <Grid item xs={12}>
                                            <DateTextField
                                                type='dueDate'
                                                label={UIText.dueDate}
                                                defaultValue={null}
                                                onChange={(type, value) => handleUniversalChangeDateTimeField(this, type, value)}
                                            />
                                        </Grid>
                                        <Grid item xs={12}>
                                            <UniversalTextField
                                                type='matterRef'
                                                label={UIText.matterRef}
                                                helperText={UIText.matterRefDesc}
                                                onChange={(event, value) => handleUniversalChangeTextField(this, event, value)}
                                            />
                                        </Grid>
                                        <Grid item container justifyItems='space-between' alignItems='center'>
                                            <Grid item xs>
                                                <Typography>{UIText.priority}</Typography>
                                            </Grid>
                                            <Grid item xs>
                                                <PriorityButton
                                                    options={this.priorities}
                                                    handleItemClicked={(option) => { handleUniversalChangeAutoComplete(this, 'priority', option) }} />
                                            </Grid>
                                        </Grid>
                                    </Grid>
                                </AccordionDetails>
                            </Accordion>
                            <Accordion>
                                <AccordionSummary
                                    expandIcon={<ExpandMoreIcon />}
                                    aria-controls="panel2a-content"
                                    id="panel2a-header"
                                >
                                    <Grid container alignItems='center'>
                                        <Grid item xs={2}>
                                            <img src={require('../../assets/moreFieldsIcon.svg')} className={classes.icon} />
                                        </Grid>
                                        <Grid item xs>
                                            <Typography variant='h6' gutterBottom>{UIText.moreFields}</Typography>
                                        </Grid>
                                    </Grid>
                                </AccordionSummary>
                                <AccordionDetails>
                                    <Grid container spacing={2}>
                                        <Grid item xs={12}>
                                            <UniversalAutoComplete
                                                type='matterStage'
                                                label={UIText.caseStage}
                                                options={this.matterStages}
                                                onChange={(type, value) => handleUniversalChangeAutoComplete(this, type, value)}
                                            />
                                            <UniversalTextField
                                                type='matterValue'
                                                label={UIText.matterValue}
                                                onChange={(event, value) => handleUniversalChangeTextField(this, event, value)}
                                                helperText={this.defaultCurrency || 'USD'}
                                            />
                                            <UniversalTextField
                                                type='estimatedEffort'
                                                label={UIText.estimatedEffort}
                                                onChange={(event, value) => handleUniversalChangeTextField(this, event, value)}
                                                helperText={UIText.estimatedEffortPlcHldr + ': ' + UIText.estimatedEffortEx}
                                            />
                                        </Grid>
                                    </Grid>
                                </AccordionDetails>
                            </Accordion>
                            <Accordion>
                                <AccordionSummary
                                    expandIcon={<ExpandMoreIcon />}
                                    aria-controls="panel3a-content"
                                    id="panel3a-header"
                                >
                                    <Grid container alignItems='center'>
                                        <Grid item xs={2}>
                                            <img src={require('../../assets/attachmentsIcon.svg')} className={classes.icon} />
                                        </Grid>
                                        <Grid item xs>
                                            <Typography variant='h6' gutterBottom>{UIText.attachments}</Typography>
                                        </Grid>
                                    </Grid>
                                </AccordionSummary>
                                <AccordionDetails>
                                    <AttachmentsArea token={this.props.getAccessToken()} onAddAttachments={this.addAttachments.bind(this)} />
                                </AccordionDetails>
                            </Accordion>
                        </form>
                    </div>
                    <BottomBar onSubmit={this.onSubmit} />
                </div >
            }
        </React.Fragment>
        );
    }
}
export function Opponent(props) {
    const { language } = useSelector((state: ReduxState) => state.settings);
    const UIText = UIStrings.getLocaleStrings(language);
    const [clientType, setClientType] = React.useState('company');
    const handleOpponentData = (type, value) => {
        if (type == 'opponentType') {
            setClientType(value?.id);
        }
        props.onChange(props.index, type, value);
    }
    return (<div>
        <Grid container direction='row' alignItems='center'>
            <Grid item xs>
                <Typography>{UIText.opponent + ` (${props.index + 1})`}</Typography>
            </Grid>
            {props.index > 0 || props.opponentCount > 1 ?
                <Grid item xs={2}>
                    <Tooltip title={UIText.remove} placement='top'>
                        <IconButton color='secondary' aria-label="delete" onClick={(e) => {
                            e.preventDefault();
                            e.stopPropagation();
                            props.onDelete(props.id);
                        }}>
                            <DeleteIcon fontSize="small" />
                        </IconButton>
                    </Tooltip>
                </Grid> : <></>}
        </Grid>
        <Grid container item spacing={1} direction='column' style={{ marginTop: '10px' }}>
            <Grid item xs>
                <UniversalAutoComplete
                    type='opponentType'
                    label={UIText.opponentType}
                    options={props.clientTypes}
                    onChange={handleOpponentData}
                />
            </Grid>
            <Grid item xs>
                <SearchBox
                    url={References.clientsLookupApi}
                    apiType='client'
                    extraData={'&clientType=' + clientType}
                    hint={UIText.startTyping}
                    type='opponentName'
                    title={UIText.clientName}
                    handleChangeAutoComplete={handleOpponentData}
                />
            </Grid>
            <Grid item xs>
                <UniversalAutoComplete
                    type='opponentPosition'
                    label={UIText.opponentPosition}
                    options={props.clientPositions}
                    onChange={handleOpponentData}
                />
            </Grid>
        </Grid>
    </div>);
}
const mapStateToProps = (state: ReduxState) => ({
    language: state.settings.language,
    runLoader: state.loader.runLoader,
    includeEmail: state.attachments.includeEmail,
    files: state.attachments.files,
    moreEmails: state.attachments.moreEmails,
    includeEmailAttachments: state.attachments.includeEmailAttachments
});
const mapDispatchToProps = (dispatch) => {
    return {
        successAlert: (message: string) => dispatch(actions.successAlert(message)),
        errorAlert: (message: string) => dispatch(actions.errorAlert(message)),
        stopLoader: () => dispatch(actions.stopLoader()),
        startLoader: () => dispatch(actions.startLoader()),
        startSaving: () => dispatch(actions.startSaving()),
        stopSaving: () => dispatch(actions.stopSaving()),
        openModal: () => dispatch(actions.openModal()),
        focusError: (id: string) => dispatch(actions.focusError(id)),
        setAttachments: (files) => dispatch(actions.setAttachments(files)),
        addFile: (file) => dispatch(actions.addFile(file)),
        deleteFile: (index) => dispatch(actions.deleteFile(index)),
        setHasEmailAttachments: (found: boolean) => dispatch(actions.setEmailHasAttachments(found)),
        setFetchingAttachments: (status: boolean) => dispatch(actions.setFetchingAttachments(status))
    }
};
type FormReduxProps = ReturnType<typeof mapStateToProps> & ReturnType<typeof mapDispatchToProps>;
interface IFormState {
    opponents: any[],
    isClientCompany: boolean,
    errorType,
    sharedWith,
    requiredFields,
    id: string
}
interface IFormProps extends FormReduxProps, WithStyles<typeof useStyles>, AppProps {

}
const AppContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(Matter);
export default withStyles(useStyles, { withTheme: true })(AppContainer);