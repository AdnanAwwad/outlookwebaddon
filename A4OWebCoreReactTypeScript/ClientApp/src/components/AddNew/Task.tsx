﻿import * as React from "react";
import { withStyles, makeStyles } from "@material-ui/core/styles";
import { connect, useSelector } from "react-redux";
import Grid from "@material-ui/core/Grid";
import PopupAlert from '../Common/Alert';
import { AxiosRequest, RequestHeaders } from '../../Helpers/AxiosRequest';
import { References } from '../../Helpers/References';
import * as actions from '../../redux/actions';
import * as UIStrings from '../../UIStrings';
import { Typography, createStyles, AppBar, Toolbar, Autocomplete, Theme, WithStyles, Paper, Divider, Accordion, AccordionSummary, AccordionDetails, IconButton, Switch, FormControl, InputLabel, Input, Container, Button, Tooltip, FormControlLabel, Checkbox } from "@material-ui/core";
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import InfoIcon from '@material-ui/icons/Info';
import SearchBox from '../Common/SearchBox';
import BottomBar from '../Common/BottomBar';
import Modal from '../Common/Modal';
import { Helpers, handleUniversalChangeTextField, handleUniversalChangeDateTimeField, handleUniversalChangeAutoComplete, loadEmailAttachments, loadEmailProps, getAttachments } from "../../Helpers/Helpers";
import FormSkeleton from "../Common/Skeleton/FormSkeleton";
import FormatAlignJustifyIcon from '@material-ui/icons/FormatAlignJustify';
import { AppProps } from "../App";
import { UniversalAutoComplete, AutoCompleteWithTwoStage, UniversalTextField, DateTextField, SwitchTextField, TimeTextField, ContactCompanyComponent, UniversalRichTextEditor, AutoCompleteWithImage } from "../Common/TextFields";
import AttachFileIcon from '@material-ui/icons/AttachFile';
import AttachmentsArea from "../Common/Attachments";
import { ReduxState } from "../..";
import { getMessageAttachments } from "../../GraphService";
import DeleteIcon from '@material-ui/icons/Delete';
import * as GraphService from '../../GraphService';
import { TMUIRichTextEditorRef } from "mui-rte";
import { EditorState, convertToRaw, convertFromRaw } from "draft-js";
import { stateToHTML } from "draft-js-export-html";
import { IEmailProps } from "./MatterNote";
import { PriorityButton } from "../Common/SplitButton";
import CloseIcon from '@material-ui/icons/Close';
const useStyles = (theme: Theme) => createStyles({
    paper: {
        marginTop: theme?.spacing(2),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center'
    },
    avatar: {
        margin: theme?.spacing(1),
        backgroundColor: 'blue',
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme?.spacing(3),
        paddingRight: "5px",
        marginBottom: theme?.spacing(8)
    },
    submit: {
        color: 'red'
    },
    textarea: {
        resize: "both",
        minHeight: "200px"
    },
    formControl: {
        margin: theme?.spacing(1),
        minWidth: 120,
    },
    selectEmpty: {
        marginTop: theme?.spacing(2),
    },
    root: {
        display: 'flex',
        flexDirection: 'column',
        flexGrow: 1
    },
    main: {
        marginBottom: theme?.spacing(8),
    },
    footer: {
        position: 'fixed',
        bottom: "0",
        marginTop: 'auto',
        width: "100%",
        backgroundColor: '#fff',
    },
    margin: {
        margin: theme?.spacing(1),
    },
    textField: {
        width: '25ch',
    },
    appBar: {
        position: 'fixed',
        height: '55px',
        paddingBottom: '10px',
        bottom: 0,
    },
    addOpponent: {
        margin: theme?.spacing(1),
    },
    icon: {
        width: '20px',
        height: '20px'
    }
});
class Task extends React.Component<IFormProps, IFormState> {

    attachments: any[];
    messageID: string;
    taskTypes: any[];
    timeTypes: any[];
    notifyTypes: any[];
    priorities: any[];
    users: any[];
    toMe: any;
    data: any;
    emailInfo: IEmailProps;
    editorState: EditorState;
    constructor(props: IFormProps, context: any) {
        super(props, context);
        this.state = {
            assignee: {},
            assignToMe: false,
            sharedWith: false,
            notifyMe: false,
            errorType: false,
            requiredFields: {
                taskType: false,
                description: false,
                assignee: false,
                requestedBy: false,
                dueDate: false                
            },
            id: ''
        };
        this.attachments = [];
        this.messageID = '';
        this.taskTypes = [];
        this.timeTypes = [];
        this.notifyTypes = [];
        this.priorities = [];
        this.users = [];
        this.toMe = {};
        this.data = {};
        this.onSubmit = this.onSubmit.bind(this);
        this.handleNotifyBefore = this.handleNotifyBefore.bind(this);
    }
    componentDidMount() {
        this.props.startLoader();
        this.getMailItem();
        this.fillData();
        window.addEventListener('includeEmailAttachment', this.handleChangeEmailAttachment.bind(this));
    }
    componentWillUnmount() {
        window.removeEventListener('includeEmailAttachment', this.handleChangeEmailAttachment);
    }
    handleChangeEmailAttachment(event) {
        if (event) {
            var includeEmailAttachments = event.detail;
            if (includeEmailAttachments) {
                this.props.addFile(this.attachments);
            }
            else {
                var files = this.props.files.filter(file => file.isEmailAttachment == false);
                this.props.setAttachments(files);
            }
        }
    }
    addAttachments(files: any[]) {
        if (files && files.length > 0) {
            this.attachments = this.attachments.concat(files);
        }
    }
    UIText = UIStrings.getLocaleStrings(this.props.language);
    getMailItem() {
        try {
            var item = Office.context.mailbox.item;
            var self = this;
            item.body.getAsync(Office.CoercionType.Text, function (asyncResult) {
                if (asyncResult.status !== Office.AsyncResultStatus.Succeeded) {
                }
                else {
                    self.messageID = item.itemId;
                    self.emailInfo = loadEmailProps(item, asyncResult);
                    loadEmailAttachments(self);
                }
            });
        } catch (e) {
            this.props.logger.error('Task: ' + e);
        }
    }
    async fillData() {
        try {
            let response = await AxiosRequest.get(References.loadTaskApi);
            if (response.data.error === "") {
                this.fillCustomData('taskTypes', response.data.success.data.taskTypes);
                this.fillCustomData('timeTypes', response.data.success.data.notify_me_before_time_types);
                this.fillCustomData('notifyTypes', response.data.success.data.notify_me_before_types);
                this.fillCustomData('priorities', response.data.success.data.taskPriorities);
                this.toMe = {
                        id: response.data.success.data.toMeId,
                        name: response.data.success.data.toMeFullName
                };
                this.data.requestedBy = this.toMe;
                this.data.dueDate = Helpers.formatDate(new Date());
            }
            else {
                this.props.errorAlert(JSON.stringify(response.data.error));
            }
            response = await AxiosRequest.get(References.usersLookupApi);
            if (response.data.error === "") {
                this.fillCustomData('users', response.data.success.data);
            }
            this.props.stopLoader();
        } catch (e) {
            this.props.logger.error('Task: ' + e);
            this.props.stopLoader();
            this.props.errorAlert(this.UIText.errorOccured);
        }
    }
    fillCustomData(type: any, items: any) {
        if (!items || items.length == 0) {
            return;
        }
        var dataToFill = [];
        if (items) {
            if (type === 'priorities') {
                Object.entries(items).map(([key, value]) => dataToFill.push({ value: value, id: key, image: value + '.png' }));
            }
            else {
                Object.entries(items).map(([key, value]) => dataToFill.push({ value: value, id: key }));
            }
        }
        this[type] = dataToFill;
    }
    async onSubmit(form) {
        form.preventDefault();
        form.stopPropagation();
        var description = stateToHTML(this.editorState?.getCurrentContent());
        var isDescriptionFilled = description.replace(/(<([^>]+)>)/gi, "");
        var requiredFields = {
            taskType: !this.data.taskType?.id?.length,
            description: !isDescriptionFilled.length,
            assignee: !this.data.assignee?.value?.id?.length,
            requestedBy: !this.data.requestedBy?.id?.length,
            dueDate: !this.data.dueDate?.length
        };
        this.setState({
            requiredFields: requiredFields
        });
        for (const [key, value] of Object.entries(requiredFields)) {
            if (value) {
                document.getElementById(key)?.scrollIntoView();
                return;
            }
        }
        this.props.startSaving();
        var taskType = this.data.taskType?.value?.id;
        var description = encodeURIComponent(description);
        var relatedMatter = this.data.relatedMatter?.id;
        var relatedContract = this.data.relatedContract?.id;
        var assignee = this.data.assignee?.value?.id;
        var requestedBy = this.data.requestedBy?.id;
        var priority = this.data.priority != null ? this.data.priority?.id?.toLowerCase() : 'medium';
        var dueDate = this.data.dueDate != null ? this.data.dueDate : Helpers.formatDate(new Date());
        var estimatedEffort = this.data.estimatedEffort;
        var contributors = this.data.contributors;
        var location = this.data.location?.id;
        var cbPrivate = this.state.sharedWith;
        var sharedWith = this.data.sharedWith;
        var notifyTime = this.data.notifyTime;
        var notifyTimeType = this.data.notifyTimeType;
        var notifyType = this.data.notifyType;
        var task = {
            taskType: taskType,
            description: description,
            relatedMatter: relatedMatter,
            relatedContract: relatedContract,
            assignee: assignee,
            requestedBy: requestedBy,
            priority: priority,
            dueDate: dueDate,
            estimatedEffort: estimatedEffort,
            contributors: contributors,
            location: location,
            cbprivate: cbPrivate,
            sharedWith: sharedWith
        };
        //Cleaning the data
        task = Helpers.cleaningData(task);

        var dataToPost = 'private=' + (task.cbprivate ? 'yes' : 'no');
        if (task.relatedMatter != null)
            dataToPost += "&legal_case_id=" + task.relatedMatter;
        if (task.relatedContract != null)
            dataToPost += "&contract_id=" + task.relatedContract;
        if (task.description != null)
            dataToPost += "&description=" + task.description;
        if (task.priority != null)
            dataToPost += "&priority=" + task.priority;
        if (task.requestedBy)
            dataToPost += "&reporter=" + task.requestedBy;
        if (task.assignee != null)
            dataToPost += "&assigned_to=" + task.assignee;
        if (task.dueDate != null)
            dataToPost += "&due_date=" + task.dueDate;
        if (task.estimatedEffort != null)
            dataToPost += "&estimated_effort=" + task.estimatedEffort;
        if (task.location != null)
            dataToPost += "&task_location_id=" + task.location;
        if (task.taskType != null)
            dataToPost += "&task_type_id=" + task.taskType;
        if (task.cbprivate) {
            var privateArray = '';
            if (task.sharedWith != null) {
                for (var i = 0; i < task.sharedWith.length; i++) {
                    privateArray += `&Task_Users[${i}]=` + task.sharedWith[i];
                }
            }
            if (privateArray.length > 0)
                dataToPost += privateArray;
        }
        var contributorsArray = '';
        if (task.contributors != null) {
            for (var i = 0; i < task.contributors.length; i++) {
                contributorsArray += `&contributors[${i}]=` + task.contributors[i];
            }
        }
        if (contributorsArray.length > 0)
            dataToPost += contributorsArray;
        if (this.state.notifyMe) {
            try {
                const timeNumber = Number(notifyTime);
                if (notifyTime != null && Number.isInteger(timeNumber)) {
                    if (notifyType != null && notifyTimeType != null) {
                        dataToPost += '&notify_me_before[time]=' + notifyTime;
                        dataToPost += '&notify_me_before[time_type]=' + notifyTimeType;
                        dataToPost += '&notify_me_before[type]=' + notifyType;
                    }
                }
            }
            catch (err) {
                console.log('Add Task - Time is Not a number');
                this.props.logger.error('Task: ' + err);
            }
        }
        var response = await AxiosRequest.post(References.addTaskApi, dataToPost);
        try {
            var resp = response.data;
            var error = resp?.error;
            if (error != "") {
                var respError = Helpers.handleErrorResponse(error, 'task', form, this.onSubmit);
                if (respError != null && respError != '') {
                    this.props.errorAlert(JSON.stringify(respError));
                }
            }
            else {
                var caseId = resp.success?.data?.task_id;
                if (caseId) {
                    try {
                        const reqHeader = new RequestHeaders();
                        var formData = new FormData();
                        const files = await getAttachments(this);
                        if (files.length > 0) {
                            files.forEach(file => {
                                formData.append('Files', file);
                            });
                            formData.append('Url', reqHeader.url + References.uploadTaskAttachmentsApi);
                            formData.append('DataToPost', 'task_id=' + caseId);
                            formData.append('UserKey', reqHeader.userKey);
                            var uploadResponse = await AxiosRequest.postToServer('PostData/UploadFileToApp4legal', formData);
                            if (uploadResponse.status != 200) {
                                this.props.errorAlert(this.UIText.errorUploadingFiles);
                            }
                            const resp = uploadResponse.data;
                            if (resp?.error === '') {

                            }
                            else {
                                this.props.errorAlert(JSON.stringify(resp?.error));
                            }
                        }
                    }
                    catch (err) {
                        this.props.logger.error('Task: ' + err);
                        this.props.errorAlert(this.UIText.errorOccured);
                    }
                    this.setState({ id: caseId });
                    this.props.openModal();
                }
            }
        }
        catch (err) {
            this.props.errorAlert(this.UIText.errorOccured);
        }
        this.props.stopSaving();
    }
    handleNotifyBefore(type, value) {
        if (value) {
            this.data = { ...this.data, [type]: value?.id };
        }
        else {
            const xType = type.target.name;
            const xValue = type.target.value;
            this.data = { ...this.data, [xType]: xValue };
        }
    }
    render() {
        const { classes } = this.props;
        const UIText = this.UIText;
        return (<React.Fragment>
            <PopupAlert />
            <Modal type='task' id={this.state.id} />
            {this.props.runLoader ?
                <FormSkeleton input='form' /> :
                <div className={classes.root}>
                    <div className={classes.paper}>
                        <form id='task' className={classes.form} noValidate onSubmit={this.onSubmit.bind(this)}>
                            <Accordion defaultExpanded={true}>
                                <AccordionSummary
                                    expandIcon={<ExpandMoreIcon />}
                                    aria-controls="panel1a-content"
                                    id="panel1a-header"
                                >
                                    <Grid container alignItems='center'>
                                        <Grid item xs={2}>
                                            <img src={require('../../assets/taskIcon.svg')} className={classes.icon} />
                                        </Grid>
                                        <Grid item xs>
                                            <Typography variant='h6' gutterBottom> {UIText.generalInformation} </Typography>
                                        </Grid>
                                    </Grid>
                                </AccordionSummary>
                                <AccordionDetails>
                                    <Grid container spacing={2} >
                                        <Grid item xs={12}>
                                            <UniversalAutoComplete
                                                type='taskType'
                                                required
                                                error={this.state.requiredFields.taskType}
                                                label={UIText.taskType}
                                                options={this.taskTypes}
                                                onChange={(type, value) => handleUniversalChangeAutoComplete(this, type, value)}
                                            />
                                        </Grid>
                                        <Grid item xs={12}>
                                            <Typography style={{ color: this.state.requiredFields.description ? 'red' : 'inherit' }}>{UIText.description + ' *'}</Typography>
                                            <UniversalRichTextEditor
                                                error={this.state.requiredFields.description}
                                                type='contacts'
                                                onChange={(state: EditorState) => {
                                                    var description = stateToHTML(state?.getCurrentContent());
                                                    this.editorState = state;
                                                }}
                                                token={this.props.getAccessToken}                                                
                                            />
                                        </Grid>
                                        <Grid item xs={12}>
                                            <SearchBox
                                                url={References.casesLookupApi}
                                                apiType='matter'
                                                hint={UIText.startTypingMatter}
                                                type='relatedMatter'
                                                title={UIText.relatedMatter}
                                                handleChangeAutoComplete={(type, value) => {
                                                    handleUniversalChangeAutoComplete(this, type, value);
                                                }}
                                            />
                                        </Grid>
                                        <Grid item xs={12}>
                                            <SearchBox
                                                url={References.contractLookupApi}
                                                apiType='contract'
                                                hint={UIText.startTyping}
                                                type='relatedContract'
                                                title={UIText.relatedContract}
                                                handleChangeAutoComplete={(type, value) => {
                                                    handleUniversalChangeAutoComplete(this, type, value);
                                                }}
                                            />
                                        </Grid>
                                        <Grid item container xs={12} >
                                            <Grid item xs={12}>
                                                <UniversalAutoComplete
                                                    type='assignee'
                                                    required
                                                    error={this.state.requiredFields.assignee}
                                                    label={UIText.assignedTo}
                                                    options={this.users}
                                                    controlled
                                                    value={this.state.assignToMe ? this.toMe : null}
                                                    onChange={(type, value) => {
                                                        this.setState({ assignToMe: false });
                                                        handleUniversalChangeAutoComplete(this, type, value)
                                                    }}
                                                />
                                            </Grid>
                                            <Grid item xs={12}>
                                                <Button onClick={(event) => {
                                                    event.preventDefault();
                                                    event.stopPropagation();
                                                    this.setState({
                                                        assignToMe: true,
                                                        requiredFields:
                                                            { ...this.state.requiredFields, assignee: false }
                                                    });
                                                    this.data['assignee'] = { id: 4859, value: this.toMe };
                                                }}>{UIText.assignToMe}</Button>
                                            </Grid>
                                        </Grid>
                                        <Grid item xs={12}>
                                            <SearchBox
                                                url={References.usersLookupApi}
                                                apiType='user'
                                                hint={UIText.startTyping}
                                                required
                                                error={this.state.requiredFields.requestedBy}
                                                type='requestedBy'                                                
                                                defaultValue={this.toMe}
                                                title={UIText.requestedBy}
                                                handleChangeAutoComplete={(type, value) => {
                                                    handleUniversalChangeAutoComplete(this, type, value);
                                                }}
                                            />
                                        </Grid>
                                        <Grid item xs={12}>
                                            <DateTextField
                                                type='dueDate'
                                                required
                                                error={this.state.requiredFields.dueDate}
                                                label={UIText.dueDate}
                                                onChange={(type, value) => handleUniversalChangeDateTimeField(this, type, value)}
                                            />
                                        </Grid>
                                        <Grid item container justifyItems='space-between' alignItems='center'>
                                            <Grid item xs>
                                                <Typography>{UIText.priority}</Typography>
                                            </Grid>
                                            <Grid item xs>
                                                <PriorityButton
                                                    options={this.priorities}
                                                    handleItemClicked={(option) => { handleUniversalChangeAutoComplete(this, 'priority', option) }} />
                                            </Grid>
                                        </Grid>
                                        <Grid item xs={12}>
                                        {this.state.notifyMe ?
                                            <NotifyBefore notifyTypes={this.notifyTypes} timeTypes={this.timeTypes} text={this.UIText} onChange={this.handleNotifyBefore} onDelete={() => this.setState({ notifyMe: false })} />
                                            :
                                            <Button onClick={async (event) => {
                                                event.preventDefault();
                                                event.stopPropagation();
                                                this.setState({ notifyMe: true });
                                            }}>{UIText.notifyMeBefore}</Button>
                                            }
                                        </Grid>
                                    </Grid>
                                </AccordionDetails>
                            </Accordion>
                            <Accordion>
                                <AccordionSummary
                                    expandIcon={<ExpandMoreIcon />}
                                    aria-controls="panel2a-content"
                                    id="panel2a-header"
                                >
                                    <Grid container alignItems='center'>
                                        <Grid item xs={2}>
                                            <img src={require('../../assets/moreFieldsIcon.svg')} className={classes.icon} />
                                        </Grid>
                                        <Grid item xs>
                                            <Typography variant='h6' gutterBottom>{UIText.moreFields}</Typography>
                                        </Grid>
                                    </Grid>
                                </AccordionSummary>
                                <AccordionDetails>
                                    <Grid container spacing={2} >
                                        <Grid item xs={12}>
                                            <UniversalTextField
                                                type='estimatedEffort'
                                                label={UIText.estimatedEffort}
                                                helperText={UIText.estimatedEffortPlcHldr + ' ' + UIText.estimatedEffortEx}
                                                onChange={(event, value) => handleUniversalChangeTextField(this, event, value)}
                                            />
                                        </Grid>
                                        <Grid item xs={12}>
                                            <SearchBox
                                                url={References.usersLookupApi}
                                                apiType='user'
                                                hint={UIText.startTyping}
                                                type='contributors'
                                                title={UIText.contributors}
                                                multiple
                                                handleChangeAutoComplete={(type, value) => {
                                                    handleUniversalChangeAutoComplete(this, type, value);
                                                }} />
                                        </Grid>
                                        <Grid item xs={12}>
                                            <SearchBox
                                                url={References.taskLocationAutoCompleteApi}
                                                apiType='taskLocation'
                                                hint={UIText.startTyping}
                                                type='location'
                                                title={UIText.location}
                                                handleChangeAutoComplete={(type, value) => {
                                                    handleUniversalChangeAutoComplete(this, type, value);
                                                }} />
                                            <Typography>{this.UIText.sharedWith}</Typography>
                                            <SwitchTextField
                                                type='sharedWith'
                                                firstLabel={UIText.everyone}
                                                secondLabel={UIText.private}
                                                onSwitchChange={(event) => this.setState({ sharedWith: event.target.checked })}
                                                onChange={(type, value) => handleUniversalChangeAutoComplete(this, type, value)}
                                                selected={this.state.sharedWith}
                                                searchType='user'
                                                searchUrl={References.usersLookupApi}
                                                label={UIText.sharedWith}
                                                multiple
                                            />
                                        </Grid>                                        
                                    </Grid>
                                </AccordionDetails>
                            </Accordion>
                            <Accordion>
                                <AccordionSummary
                                    expandIcon={<ExpandMoreIcon />}
                                    aria-controls="panel3a-content"
                                    id="panel3a-header"
                                >
                                    <Grid container alignItems='center'>
                                        <Grid item xs={2}>
                                            <img src={require('../../assets/attachmentsIcon.svg')} className={classes.icon} />
                                        </Grid>
                                        <Grid item xs>
                                            <Typography variant='h6' gutterBottom>{UIText.attachments}</Typography>
                                        </Grid>
                                    </Grid>
                                </AccordionSummary>
                                <AccordionDetails>
                                    <AttachmentsArea token={this.props.getAccessToken()} onAddAttachments={this.addAttachments.bind(this)} />
                                </AccordionDetails>
                            </Accordion>
                        </form>
                    </div>
                    <BottomBar onSubmit={this.onSubmit} />
                </div >
            }
        </React.Fragment>
        );
    }
}
const NotifyBefore = (props: any) => {
    return (<Grid container spacing={1} alignItems='center' justifyItems='center'>
        <Grid item container justifyItems='space-between' alignItems='flex-start' xs={12}>
            <Grid item xs={10}>
                <Typography style={{marginTop: '10px'}}>{props.text.notifyMeBefore}</Typography>
            </Grid>
            <Grid item xs={2}>
                <IconButton color='secondary' aria-label="delete" onClick={(e) => {
                    e.preventDefault();
                    e.stopPropagation();
                    props.onDelete();
                }}>
                    <CloseIcon />
                </IconButton>
            </Grid>
        </Grid>
        <Grid item container alignItems='center' xs={12}>
            <Grid item xs={6} style={{marginTop: '-7px'}}>
                <UniversalTextField
                    type='notifyTime'
                    label={props.text.time}
                    onChange={props.onChange} />
            </Grid>
            <Grid item xs={6} >
                <UniversalAutoComplete
                    type='notifyTimeType'
                    options={props.timeTypes}
                    label={props.text.timeType}
                    onChange={props.onChange} />
            </Grid>
        </Grid>
        <Grid item xs={12}>
            <UniversalAutoComplete
                type='notifyType'
                options={props.notifyTypes}
                label={props.text.notifyType}
                onChange={props.onChange} />
        </Grid></Grid>)
}
const mapStateToProps = (state: ReduxState) => ({
    language: state.settings.language,
    runLoader: state.loader.runLoader,
    includeEmail: state.attachments.includeEmail,
    files: state.attachments.files,
    moreEmails: state.attachments.moreEmails,
    includeEmailAttachments: state.attachments.includeEmailAttachments
});
const mapDispatchToProps = (dispatch) => {
    return {
        successAlert: (message: string) => dispatch(actions.successAlert(message)),
        errorAlert: (message: string) => dispatch(actions.errorAlert(message)),
        stopLoader: () => dispatch(actions.stopLoader()),
        startLoader: () => dispatch(actions.startLoader()),
        startSaving: () => dispatch(actions.startSaving()),
        stopSaving: () => dispatch(actions.stopSaving()),
        openModal: () => dispatch(actions.openModal()),
        focusError: (id: string) => dispatch(actions.focusError(id)),
        setAttachments: (files) => dispatch(actions.setAttachments(files)),
        addFile: (file) => dispatch(actions.addFile(file)),
        deleteFile: (index) => dispatch(actions.deleteFile(index)),
        setHasEmailAttachments: (found: boolean) => dispatch(actions.setEmailHasAttachments(found)),
        setFetchingAttachments: (status: boolean) => dispatch(actions.setFetchingAttachments(status))
    }
};
type FormReduxProps = ReturnType<typeof mapStateToProps> & ReturnType<typeof mapDispatchToProps>;
interface IFormState {
    sharedWith: boolean;
    notifyMe: boolean;
    assignToMe: boolean;
    assignee: any;
    errorType,
    requiredFields,
    id: string
}
interface IFormProps extends FormReduxProps, WithStyles<typeof useStyles>, AppProps {

}
const AppContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(Task);
export default withStyles(useStyles, { withTheme: true })(AppContainer);