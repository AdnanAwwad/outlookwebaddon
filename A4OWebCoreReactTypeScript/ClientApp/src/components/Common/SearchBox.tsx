﻿import * as React from 'react';
import TextField from '@material-ui/core/TextField';
import CircularProgress from '@material-ui/core/CircularProgress';
import { AxiosRequest } from '../../Helpers/AxiosRequest';
import * as UIStrings from '../../UIStrings';
import { useSelector, useDispatch } from "react-redux";
import { ReduxState } from '../..';
import SearchIcon from '@material-ui/icons/Search';
import * as GraphService from '../../GraphService';
import { InputAdornment, Typography, Autocomplete, Avatar, Grid, IconButton } from '@material-ui/core';
import PeopleIcon from '@material-ui/icons/People';
import PersonIcon from '@material-ui/icons/Person';
import PersonOutlineIcon from '@material-ui/icons/PersonOutline';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import EmailIcon from '@material-ui/icons/Email';
import { successAlert } from '../../redux/actions';
export default function SearchBox(props) {
    const [open, setOpen] = React.useState(false);
    const [options, setOptions] = React.useState([]);
    const [value, setValue] = React.useState(props.defaultValue);
    const [loading, setLoading] = React.useState(false);
    const [noOptions, setnoOptions] = React.useState('');
    const languageSelected = useSelector((state: ReduxState) => state.settings.language);
    const UIText = UIStrings.getLocaleStrings(languageSelected);
    React.useEffect(() => {
        if (!open) {
            setOptions([]);
        }
    }, [open]);
    React.useEffect(() => {
        setValue(props.defaultValue);
    }, [props.defaultValue]);
    const fetchDataFromMicrosoft = async (text: string) => {
        const token = await props.token;
        switch (props.apiType) {
            case 'outlookContact': {
                if (token) {
                    let contacts = await GraphService.getContactsList(token, text);
                    setOptions(contacts.map((item, index) => contacts[index]));
                    setLoading(false);
                }
                else {
                    Office.context.mailbox.getCallbackTokenAsync({ isRest: true }, async function (result) {
                        if (result.status === Office.AsyncResultStatus.Succeeded) {
                            const token = result.value;
                            let contacts = await GraphService.getContactsList(token, text, true);
                            setOptions(contacts.map((item, index) => contacts[index]));
                            setLoading(false);
                        }
                    });
                }
            } break;
            case 'outlookEmail': {
                let mails = await GraphService.getEmailsList(token, text);
                setOptions(mails.map((item, index) => mails[index]));
                setLoading(false);
            } break;
            default: break;
        }
    }
    const handleChangeTextField = async (event) => {
        var text = event.target.value;
        if (props.url) {
            setLoading(text.legnth >= 3);
            if (text.length >= 3) {
                var dataToPost = 'term=' + text + (props.extraData ? props.extraData : '');
                setLoading(true);
                const response = await AxiosRequest.post(props.url, dataToPost);
                const data = response.data?.success?.data;
                setOptions(data || []);
                setLoading(false);
                setnoOptions(data?.length == 0 ? UIText.noData : null);
            }
            else {
                setnoOptions(UIText.lessCharacters);
            }
        }
        else {
            setLoading(true);
            fetchDataFromMicrosoft(text);
        }
    };
    const inputType = (option, type) => {
        if (option) {
            switch (type) {
                case 'user':
                    return (option.firstName || option.lastName) ? (option.firstName + ' ' + option.lastName) : option.name;
                case 'company': case 'client': case 'contact': case 'companies': case 'contacts':
                    return option.name;
                case 'outlookContact':
                    return option.displayName || option.DisplayName;
                case 'outlookEmail':
                    return option.subject;
                case 'matter':
                    var id: string = option.caseID;
                    var firstLetter = id.substring(0, 1);
                    var matterId = id.substring(1, id.length);
                    var caseId = firstLetter + Number(matterId);
                    return `[${caseId}] ` + option.subject;
                case 'contract':
                    return `[${option.contract_id}] ` + option.name;
                case 'taskLocation':
                    return option.name;
                default: return '';
            }
        }
        else {
            return '';
        }
    }
    const startAdornment = (type) => {
        switch (type) {
            case 'outlookContact':
                return <PeopleIcon />;
            case 'outlookEmail':
                return <EmailIcon />;
            default: return '';
        }
    }
    return (
        <Autocomplete
            fullWidth
            multiple={props.multiple}
            open={open}
            onOpen={() => {
                setOpen(true);
            }}
            onClose={() => {
                setOpen(false);
            }}
            value={props.defaultValue ? value : undefined}
            onChange={(event, value) => {
                setValue(value);
                props.handleChangeAutoComplete(props.type, value)
            }}
            getOptionLabel={(option) => inputType(option, props.apiType)}
            options={options}
            loading={loading}
            loadingText={UIText.loading}
            id={props.type}
            noOptionsText={noOptions}
            renderInput={(params) => (
                props.icon ?
                    <TextField
                        {...params}
                        id={props.type}
                        label={props.title}
                        variant="outlined"
                        helperText={props.hint}
                        onChange={handleChangeTextField}
                        type='search'
                        InputProps={{
                            ...params.InputProps,
                            startAdornment: (
                                <InputAdornment position="start">
                                    {startAdornment(props.apiType)}
                                </InputAdornment>
                            ),
                            endAdornment: (
                                <div style={{ position: 'absolute', right: '10px', top: '16px' }}>
                                    {loading ? <CircularProgress color="inherit" size={20} /> : <SearchIcon />}
                                </div>
                            ),
                        }}
                    />
                    :
                    <TextField
                        {...params}
                        id={props.type}
                        label={props.title}
                        variant="outlined"
                        type='search'
                        helperText={props.hint}
                        onChange={handleChangeTextField}
                        required={props.required}
                        error={props.error}
                        InputProps={{
                            ...params.InputProps,
                            endAdornment: (
                                <div style={{ position: 'absolute', right: '10px', top: '16px' }}>
                                    {loading ? <CircularProgress color="inherit" size={20} /> : <SearchIcon />}
                                </div>
                            ),
                        }}
                    />
            )}
        />
    );
}

export function AttendeesSearchBox(props) {
    const [open, setOpen] = React.useState(false);
    const [options, setOptions] = React.useState([]);
    const [value, setValue] = React.useState(props.defaultValue);
    const [loading, setLoading] = React.useState(false);
    const [noOptions, setnoOptions] = React.useState('');
    const languageSelected = useSelector((state: ReduxState) => state.settings.language);
    const dispatch = useDispatch();
    const UIText = UIStrings.getLocaleStrings(languageSelected);
    React.useEffect(() => {
        if (!open) {
            setOptions([]);
        }
    }, [open]);
    React.useEffect(() => {
        setValue(props.defaultValue);
    }, [props.defaultValue]);
    const fetchDataFromMicrosoft = async (text: string) => {
        const token = await props.token;
        if (token) {
            switch (props.apiType) {
                case 'outlookContact': {
                    let contacts = await GraphService.getContactsList(token, text);
                    setOptions(contacts.map((item, index) => contacts[index]));
                } break;
                default: break;
            }
        }
        setLoading(false);
    }
    const handleChangeTextField = async (event) => {
        var text = event.target.value;
        if (props.url) {
            setLoading(text.legnth >= 3);
            if (text.length >= 3) {
                var dataToPost = 'term=' + text + (props.extraData ? props.extraData : '');
                setLoading(true);
                const response = await AxiosRequest.post(props.url, dataToPost);
                const data = response.data?.success?.data;
                setOptions(data || []);
                setLoading(false);
                setnoOptions(data?.length == 0 ? UIText.noData : null);
            }
            else {
                setnoOptions(UIText.lessCharacters);
            }
        }
        else {
            setLoading(true);
            fetchDataFromMicrosoft(text);
        }
    };
    const inputType = (option, type) => {
        switch (type) {
            case 'user':
                return (option.firstName || option.lastName) ? (option.firstName + ' ' + option.lastName) : option.name;
            case 'company': case 'client': case 'contact': case 'companies': case 'contacts':
                return option.name;
            case 'outlookContact':
                return option.displayName;
            case 'matter':
                var id: string = option.caseID;
                var firstLetter = id.substring(0, 1);
                var matterId = id.substring(1, id.length);
                var caseId = firstLetter + Number(matterId);
                return `[${caseId}] ` + option.subject;
            case 'contract':
                return `[${option.contract_id}] ` + option.name;
            case 'taskLocation':
                return option.name;
            default: return '';
        }
    }
    const handleChangleRequired = (event: React.MouseEvent<HTMLButtonElement>, index: number) => {
        event.preventDefault();
        event.stopPropagation();
        setValue(value.map((v, i) => {
            if (i === index) {
                var required = v.required;
                if (required != null) {
                    v.required = !v.required;
                }
                else {
                    v.required = false;
                }
                return v;
            }
            else {
                return v;
            }
        }));
        props.handleChangeAutoComplete(props.type, value);
    };
    return (
        <Grid container spacing={1}>
            <Grid item xs={12}>
                <Autocomplete
                    fullWidth
                    multiple={props.multiple}
                    open={open}
                    onOpen={() => {
                        setOpen(true);
                    }}
                    onClose={() => {
                        setOpen(false);
                    }}
                    value={props.defaultValue ? value : undefined}
                    onChange={(event, value) => {
                        setValue(value);
                        props.handleChangeAutoComplete(props.type, value);
                    }}
                    getOptionLabel={(option) => inputType(option, props.apiType)}
                    options={options}
                    loading={loading}
                    loadingText={UIText.loading}
                    noOptionsText={noOptions}
                    renderInput={(params) => (
                        <TextField
                            {...params}
                            id={props.type}
                            label={props.title}
                            variant="outlined"
                            type='search'
                            helperText={props.hint}
                            onChange={handleChangeTextField}
                            required={props.required}
                            error={props.error}
                            InputProps={{
                                ...params.InputProps,
                                endAdornment: (
                                    <div style={{ position: 'absolute', right: '10px', top: '16px' }}>
                                        {loading ? <CircularProgress color="inherit" size={20} /> : <SearchIcon />}
                                    </div>
                                ),
                            }}
                        />
                    )}
                />
            </Grid>
            <Grid item xs={12}>
                <List>
                    {value?.map((user, index) => {
                        return (
                            <ListItem>
                                <ListItemAvatar>
                                    <Avatar>
                                        <PersonIcon />
                                    </Avatar>
                                </ListItemAvatar>
                                <ListItemText
                                    primary={user.firstName + ' ' + user.lastName}
                                />
                                <ListItemSecondaryAction>
                                    <IconButton edge="end" onClick={(event) => { dispatch(successAlert(user.required == false ? UIText.markRequired : UIText.markOptional)); handleChangleRequired(event, index) }} aria-label="required">
                                        {user.required == false ?
                                            <PersonOutlineIcon />:
                                            <PersonIcon />
                                        }
                                    </IconButton>
                                </ListItemSecondaryAction>
                            </ListItem>
                        )
                    }
                    )}
                </List>
            </Grid>
        </Grid>
    );
}