﻿/* eslint-disable no-use-before-define */
import * as React from 'react';
import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
import Autocomplete from '@material-ui/core/Autocomplete';
import { TextField, Grid, Typography, withStyles, Switch, ListItemAvatar, Avatar, ListItemText, FormControlLabel, Popover, ButtonGroup, Button } from '@material-ui/core';
import { References } from '../../Helpers/References';
import SearchBox from './SearchBox';
import { useState, FunctionComponent, useEffect, useRef } from 'react';
import { AxiosRequest } from '../../Helpers/AxiosRequest';
import { DatePicker, TimePicker } from '@material-ui/pickers';
import AdapterDateFns from '@material-ui/lab/AdapterDateFns';
import LocalizationProvider from '@material-ui/lab/LocalizationProvider';
import * as PropTypes from 'prop-types';
import { useTheme, alpha } from '@material-ui/core/styles';
import Popper from '@material-ui/core/Popper';
import ClickAwayListener from '@material-ui/core/ClickAwayListener';
import SearchIcon from '@material-ui/icons/Search';
import ButtonBase from '@material-ui/core/ButtonBase';
import InputBase from '@material-ui/core/InputBase';
import MUIRichTextEditor, { TMUIRichTextEditorRef } from 'mui-rte';
import { Helpers } from '../../Helpers/Helpers';
import * as GraphService from '../../GraphService';
import { convertFromRaw } from 'draft-js';
import { stateToHTML } from 'draft-js-export-html';
import { useSelector } from 'react-redux';
import { ReduxState } from '../..';
import * as UIStrings from '../../UIStrings';
import CloseIcon from '@material-ui/icons/Close';
import EmailIcon from '@material-ui/icons/Email';
import HijriUtils from "@date-io/hijri";

export function UniversalTextField(props: ITextFieldProps) {
    return (
        <TextField
            margin="normal"
            required={props.required}
            fullWidth
            error={props.error}
            onChange={props.onChange}
            id={props.id ? props.id : props.type}
            label={props.label}
            name={props.type}
            helperText={props.helperText}
            autoComplete={props.autoComplete}
            autoFocus={props.focus}
            defaultValue={props.defaultValue}
            value={props.controlled ? (props.value || '') : undefined}
            multiline={props.multiline}
            rows={props.rows}            
         />
    );
}
export function DateTextField(props: ITextFieldProps) {
    const [value, setValue] = useState(props.defaultValue);
    return (
        <LocalizationProvider dateAdapter={props.hijri ? HijriUtils : AdapterDateFns}>
            <DatePicker
                dateAdapter={props.hijri ? new HijriUtils() : new AdapterDateFns()}
                label={props.label}
                value={props.value || value}
                minDate='1937-03-14'
                maxDate='2076-11-26'
                disabled={props.disabled}
                onChange={(newValue) => { setValue(newValue); props.onChange(props.type, newValue); }}
                renderInput={(params) => <TextField
                    {...params}
                    fullWidth
                    id={props.type}
                    helperText={props.helperText}
                    required={props.required}
                    error={props.error}
                />}
            />     
        </LocalizationProvider>
    );
}
export function TimeTextField(props: ITextFieldProps) {
    const [value, setValue] = useState(props.defaultValue);
    return (
        <LocalizationProvider dateAdapter={AdapterDateFns}>
            <TimePicker
                dateAdapter={new AdapterDateFns()}
                label={props.label}
                value={value}
                minutesStep={30}
                clearable={props.clearable}
                disabled={props.disabled}
                onChange={(newValue) => { setValue(newValue); props.onChange(props.type, newValue); }}
                renderInput={(params) => <TextField
                    {...params}
                    fullWidth
                    id={props.type}
                    helperText={props.helperText}
                    required={props.required}
                    error={props.error}
                />}
            />
        </LocalizationProvider>
    );
}
export function UniversalAutoComplete(props: ITextFieldProps) {
    const [value, setValue] = useState(props.defaultValue || props.multiple? [] : null);
    const getOptionLabel = (option) => {
        switch (props.type) {
            case 'matterStage':
            case 'practiceArea':
            case 'clientPosition':
            case 'propertyRight':
            case 'propertyClass':
            case 'propertySubCategory':
            case 'propertyName':
            case 'taskType':
            case 'meetingType':
            case 'opponentPosition': return option?.value?.name;
            case 'assignee': return option?.value?.firstName + ' ' + option?.value?.lastName;
            default: return option?.value;
        }
    }
    const getOptionSelected = (option, value) => {
        switch (props.type) {
            case 'matterStage': case 'practiceArea': return option?.value?.name == value?.value?.name;
            default: return option === value;
        }
    }
    const getCurrentValue = () => {
        if (props.value != null) {   
            switch (props.type) {
                case 'assignee': {
                    var filteredOption = props.options?.filter(option => option?.value?.id == props.value?.id);
                    return filteredOption[0];
                }
                case 'language': {
                    return props.options?.filter(option => option?.type == props.value)[0];
                }
            }
            
        }
        return value;

    }
    return (
        <Autocomplete            
            onChange={(event, newValue) => {
                setValue(newValue);
                props.onChange(props.type, newValue);
            }}
            options={props.options}
            id={props.type}
            getOptionLabel={getOptionLabel}
            getOptionSelected={getOptionSelected}
            defaultValue={props.defaultValue}
            fullWidth
            value={props.defaultValue ? undefined : getCurrentValue()}
            multiple={props.multiple}
            renderInput={(params) =>
                <TextField
                    {...params}
                    required={props.required}
                    error={props.error}
                    label={props.label}
                    variant="outlined" />}
        />
    );
}
export function AutoCompleteWithTwoStage(props: ITextFieldProps) {
    const [childOptions, setChildOptions] = useState(props.childOptions || []);
    const [childValue, setChildValue] = useState(props.childDefault || (props.multiple ? [] : null));
    const getParentLabel = (option) => {
        switch (props.type) {
            case 'assign': return option.value?.name;
            default: return '';
        }
    }
    const getChildLabel = (option) => {
        switch (props.type) {
            case 'assign': return option.firstName + ' ' + option.lastName;
            default: return '';
        }
    }
    React.useEffect(() => {
        async function loadChildOptions() {
            await parentOptionChanged(props.parentType, props.parentOptions[0]);
        }
        if (!props.childDefault) {
            loadChildOptions();
        }
    }, []);
    const parentOptionChanged = async (type, value) => {
        switch (type) {
            case 'assignedTeam': {
                if (value != null) {
                    setChildValue(props.multiple ? [] : null);
                    var dataToPost = 'provider_group_id=' + value?.value?.id;
                    var response = await AxiosRequest.post(References.usersLookupApi, dataToPost);
                    if (response?.data?.error === "") {
                        var users = response.data.success.data;
                        setChildOptions(users);
                    }
                }
                else {
                    setChildValue(props.multiple? [] : null);
                    props.onChange(props.childType, props.multiple ? [] :null);
                }
            } break;
            default: break;
        }
    }
    return (
        <Grid item xs={12} container spacing={2}>
            {!props.parentVisible ?
            <Grid item xs={12}>
                    <Autocomplete

                        onChange={(event, newValue) => {
                            parentOptionChanged(props.parentType, newValue);
                            props.onChange(props.parentType, newValue);
                        }}
                        options={props.parentOptions}
                        id={props.parentType}
                        getOptionLabel={getParentLabel}
                        fullWidth
                        defaultValue={props.parentDefault || props.parentOptions[0]}
                        renderInput={(params) =>
                            <TextField
                                {...params}
                                required={props.required}
                                error={props.error}
                                label={props.parentLabel}
                                variant="outlined" />}
                    />
                </Grid>
                : <></>}
            {!props.childVisible ?
                <Grid item xs={12}>
                    <Autocomplete

                        onChange={(event, newValue) => {
                            setChildValue(newValue);
                            props.onChange(props.childType, newValue);
                        }}
                        options={childOptions}
                        id={props.childType}
                        getOptionLabel={getChildLabel}
                        value={childValue}
                        defaultValue={props.childDefault}
                        fullWidth
                        multiple={props.multiple}
                        renderInput={(params) =>
                            <TextField
                                {...params}
                                required={props.required}
                                error={props.error}
                                label={props.childLabel}
                                variant="outlined" />}
                    />
                </Grid>
                : <></>}
        </Grid>
    );
}
export function ContactCompanyComponent(props: ITextFieldProps) {
    const [clientType, setClientType] = React.useState(props.options[0]?.id);
    return (
        <Grid item xs={12} container direction='column' spacing={2}>
            <Grid item xs={12}>
                <Autocomplete

                    onChange={(event, newValue) => {
                        setClientType(newValue?.id);
                        props.onChange(props.parentType, newValue);
                    }}
                    options={props.options}
                    id={props.parentType}
                    getOptionLabel={(option) => option?.value}
                    fullWidth
                    renderInput={(params) =>
                        <TextField
                            {...params}
                            required={props.required}
                            error={props.error}
                            label={props.parentLabel}
                            variant="outlined" />}
                />
            </Grid>
            <Grid item xs={12}>
                <SearchBox
                    url={References.clientsLookupApi}
                    apiType='client'
                    extraData={'&clientType=' + clientType}
                    type={props.childType}
                    title={props.childLabel}
                    hint={props.helperText}
                    handleChangeAutoComplete={props.onChange}
                    multiple={props.multiple} />
            </Grid>
        </Grid>
    );
}
export function SwitchTextField(props: ISwitchTextFieldProps) {
    const language = useSelector((state: ReduxState) => state.settings.language);
    return (
        <Grid container direction='column' alignItems='center' justifyItems='center'>
            <Grid item xs={12}>                                                                                    
                <Typography component="div">
                    <Grid component="label" container alignItems="center" spacing={1}>
                        <Typography>
                            {language === 'ar-AE' ? props.secondLabel : props.firstLabel}
                        </Typography>
                        <FormControlLabel  
                            style={{marginLeft: '7px'}}
                            control={
                                <Switch
                                    onChange={props.onSwitchChange}
                                    name={props.type}
                                    color="primary"
                                />
                            }
                            label={language === 'ar-AE' ? props.firstLabel : props.secondLabel}
                        />
                    </Grid>
                </Typography>
            </Grid>
            <Grid item style={{width: '100%', marginTop: '8px'}}>
                {
                    props.selected ? <SearchBox
                        url={props.searchUrl}
                        apiType={props.searchType}
                        type={props.type}
                        title={props.label}
                        handleChangeAutoComplete={props.onChange}
                        multiple={props.multiple} /> : <div />
                }
            </Grid>
        </Grid>
    );
}

function PopperComponent(props) {
    const { disablePortal, anchorEl, open, ...other } = props;
    return <div {...other} />;
}

PopperComponent.propTypes = {
    anchorEl: PropTypes.any,
    disablePortal: PropTypes.bool,
    open: PropTypes.bool.isRequired,
};

const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
    },
    button: {
        width: '100%',
        paddingBottom: 8,
        color: '#586069',
        textAlign: 'left',
        fontWeight: 600,
        '&:hover,&:focus': {
            color: '#0366d6',
        },
        '& span': {
            width: '100%',
        },
        '& svg': {
            width: 25,
            height: 25,
        },
    },
    tag: {
        marginTop: 3,
        height: 20,
        padding: '.15em 4px',
        fontWeight: 600,
        lineHeight: '15px',
        borderRadius: 2,
    },
    popper: {
        border: '1px solid rgba(27,31,35,.15)',
        boxShadow: '0 3px 12px rgba(27,31,35,.15)',
        borderRadius: 3,
        width: 300,
        zIndex: theme.zIndex.modal,
        fontSize: 13,
        color: '#586069',
        backgroundColor: '#f6f8fa',
    },
    header: {
        borderBottom: '1px solid #e1e4e8',
        padding: '8px 10px',
        fontWeight: 600,
    },
    inputBase: {
        padding: 10,
        width: '100%',
        borderBottom: '1px solid #dfe2e5',
        '& input': {
            borderRadius: 4,
            backgroundColor: theme.palette.common.white,
            padding: 8,
            transition: theme.transitions.create(['border-color', 'box-shadow']),
            border: '1px solid #ced4da',
            fontSize: 14,
            '&:focus': {
                boxShadow: `${alpha(theme.palette.primary.main, 0.25)} 0 0 0 0.2rem`,
                borderColor: theme.palette.primary.main,
            },
        },
    },
    paper: {
        boxShadow: 'none',
        margin: 0,
        color: '#586069',
        fontSize: 13,
    },
    listbox: {
        '& $option': {
            minHeight: 'auto',
            alignItems: 'flex-start',
            padding: 8,
            '&[aria-selected="true"]': {
                backgroundColor: 'transparent',
            },
            '&[data-focus="true"], &[data-focus="true"][aria-selected="true"]': {
                backgroundColor: theme.palette.action.hover,
            },
        },
    },
    option: {},
    popperDisablePortal: {
        position: 'relative',
    },
    iconSelected: {
        width: 17,
        height: 17,
        marginRight: 5,
        marginLeft: -2,
    },
    color: {
        width: 14,
        height: 14,
        flexShrink: 0,
        borderRadius: 3,
        marginRight: 8,
        marginTop: 2,
    },
    text: {
        flexGrow: 1,
    },
    close: {
        opacity: 0.6,
        width: 18,
        height: 18,
    },
}));

export function AutoCompleteWithImage(props) {
    const classes = useStyles();
    const [anchorEl, setAnchorEl] = React.useState(null);
    const [value, setValue] = React.useState(props.options[2]);
    const theme = useTheme();
    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = (newValue) => {
        if (newValue)
            setValue(newValue);
        if (anchorEl) {
            anchorEl.focus();
        }
        setAnchorEl(null);
    };
    const handleColor = (value) => {
        switch (value) {
            case 'medium': return '#2063a2';
            case 'critical': return '#d00b0b';
            case 'high': return '#d32a2a';
            case 'low': return '#4d9b57';
            default: return '#000000';
        }
    }
    const open = Boolean(anchorEl);
    const id = open ? 'completeWithImage' : undefined;

    return (
        <React.Fragment>
            <div className={classes.root}>
                <ButtonBase
                    disableRipple
                    className={classes.button}
                    aria-describedby={id}
                    onClick={handleClick}
                >
                    <span>{props.label}</span>
                    <SearchIcon />
                </ButtonBase>
                {value != null ?
                    <div
                        key={value.value}
                        className={classes.tag}
                        style={{
                            backgroundColor: handleColor(value?.value?.toLowerCase()),
                            color: theme.palette.getContrastText('#123456'),
                        }}
                    >
                        {value.value}
                    </div>
                    : <></>}
            </div>
            <Popper
                id={id}
                open={open}
                anchorEl={anchorEl}
                placement="bottom-start"
                className={classes.popper}
            >
                <ClickAwayListener onClickAway={(event) => { handleClose(null) }}>
                    <div>
                        <Autocomplete
                            open
                            classes={{
                                paper: classes.paper,
                                listbox: classes.listbox,
                                option: classes.option,
                                popperDisablePortal: classes.popperDisablePortal,
                            }}
                            onChange={(event, newValue) => {
                                handleClose(newValue);
                                props.onChange(props.type, newValue);
                            }}
                            PopperComponent={PopperComponent}
                            renderTags={() => null}
                            noOptionsText="No labels"
                            renderOption={(props, option, { selected }) => (
                                <li {...props}>
                                    <img className={classes.color} src={require('../../assets/' + option['image'])} />
                                    <div className={classes.text}>
                                        {option['value']}
                                    </div>
                                </li>
                            )}
                            options={props.options}
                            getOptionLabel={(option: any) => option.value}
                            renderInput={(params) => (
                                <InputBase
                                    ref={params.InputProps.ref}
                                    fullWidth
                                    inputProps={params.inputProps}
                                    autoFocus
                                    className={classes.inputBase}
                                />
                            )}
                        />
                    </div>
                </ClickAwayListener>
            </Popper>
        </React.Fragment>
    );
}

type TAnchor = HTMLElement | null

interface IMyCardPopoverProps {
    anchor: TAnchor
    onSubmit: (index: number) => void
}

type TMyCardPopoverState = {
    anchor: TAnchor
    isCancelled: boolean
}
const cardPopverStyles = makeStyles({
    root: {
        padding: 10,
        maxWidth: 350
    },
    textField: {
        width: "100%"
    }
})
const EmailPopOver: FunctionComponent<IMyCardPopoverProps> = (props) => {
    const classes = cardPopverStyles(props)
    const [state, setState] = useState<TMyCardPopoverState>({
        anchor: null,
        isCancelled: false
    })
    const { language } = useSelector((state: ReduxState) => state.settings);
    const UIText = UIStrings.getLocaleStrings(language);
    useEffect(() => {
        setState({
            anchor: props.anchor,
            isCancelled: false
        })
    }, [props.anchor])
    const handleClick = (index) => {
        setState({
            anchor: null,
            isCancelled: true
        });
        props.onSubmit(index);
    };
    return (
        <Popover
            anchorEl={state.anchor}
            open={state.anchor !== null}
            anchorOrigin={{
                vertical: "bottom",
                horizontal: "right"
            }}
            transformOrigin={{
                vertical: 'top',
                horizontal: 'left',
            }}
        >
            <Grid container spacing={1} className={classes.root}>
                <Grid item xs={12}>
                    <ButtonGroup
                        orientation="vertical"
                        color="primary"
                        aria-label="vertical contained primary button group"
                        variant="contained"
                    >
                        <Button onClick={(event) => handleClick(0)}>{UIText.insertEmailHeader}</Button>
                        <Button onClick={(event) => handleClick(1)}>{UIText.insertEmailTimeStamp}</Button>
                        <Button onClick={(event) => handleClick(2)}>{UIText.insertEmailBody}</Button>
                    </ButtonGroup>
                </Grid>
                <Grid item container xs={12} justifyItems="flex-end">
                    <Button onClick={() => {
                        setState({
                            anchor: null,
                            isCancelled: true
                        })
                    }}
                    >
                        <CloseIcon />
                    </Button>
                </Grid>
            </Grid>
        </Popover>
    )
}
const HtmlContent: FunctionComponent<any> = (props) => {
    const { blockProps } = props;
    useEffect(() => {
        console.log(blockProps);
    }, []);
    return (<div><Typography>{ blockProps.value}</Typography></div>);
}
export function UniversalRichTextEditor(props) {

    const ref = useRef<TMUIRichTextEditorRef>(null);
    const [anchor, setAnchor] = useState<HTMLElement | null>(null)
    const [items, setItems] = useState([]);
    const { language } = useSelector((state: ReduxState) => state.settings);
    const UIText = UIStrings.getLocaleStrings(language);
    const rteStyles = makeStyles((theme) => ({
        root: {
            border: '1px solid ' + (props.error ? 'red' : 'lightGrey'),
            padding: '10px',
            borderRadius: '5px',
            paddingBottom: '40px'
        }
    }));
    type TStaff = {
        job: string
        name: string
        color: string
    }
    React.useEffect(() => {
        getData();
        return () => {
            setItems([]);
        };
    }, []);
    const getData = async () => {
        const token = await props.token(); 
        switch (props.type) {
            case 'contacts': {
                if (token) {
                    var data = await GraphService.getContactsList(token);
                    setItems(data);
                }
                else {
                    Office.context.mailbox.getCallbackTokenAsync({ isRest: true }, async function (result) {
                        if (result.status === Office.AsyncResultStatus.Succeeded) {
                            const token = result.value;
                            let contacts = await GraphService.getContactsList(token, null, true);
                            setItems(contacts);

                        }
                    });
                }
            } break;
            default: break;
        }
    };
    const Staff: FunctionComponent<TStaff> = (props) => {
        return (
            <>
                <ListItemAvatar>
                    <Avatar style={{
                        backgroundColor: props.color
                    }}>{props.name.substr(0, 1)}</Avatar>
                </ListItemAvatar>
                <ListItemText
                    primary={props.name}
                    secondary={props.job}
                />
            </>
        )
    }
    const classes = rteStyles();
    return (<>
        {
            props.isEmail ?
                <EmailPopOver
                    anchor={anchor}
                    onSubmit={(index) => {
                        var insertedData = '';
                        switch (index) {
                            case 0: insertedData = props.data.emailHeader; break;
                            case 1: insertedData = props.data.emailTimeStamp; break;
                            case 2: insertedData = props.data.emailBody; break;
                            default: break;
                        }
                        ref.current?.insertAtomicBlockSync('html_content', insertedData);
                        setAnchor(null);
                    }}
                />:<></>
        }
        <MUIRichTextEditor
            ref={ref}
            label={UIText.startTypingHere}
            onChange={props.onChange}
            classes={classes}
            defaultValue={props.defaultValue}
            onSave={(data) => {
                var obj = JSON.parse(data);
                const html = stateToHTML(convertFromRaw(obj));
                console.log(html);
            }}
            controls={["title", "bold", "italic", "underline", "undo", "redo", "link", "clear"]}
            customControls={[
                {
                    name: "html_content",
                    type: "atomic",
                    atomicComponent: HtmlContent
                },
                {
                    name: "emai_pref",
                    icon: <EmailIcon />,
                    type: "callback",
                    onClick: (_editorState, _name, anchor) => {
                        setAnchor(anchor);
                    }
                }
            ]}
            inlineToolbar={true}
            autocomplete={{
                strategies: [
                    {
                        items: items.map((item) => {
                            switch (props.type) {
                                case 'contacts': {
                                    var firstName = item?.givenName || item?.GivenName ? item?.givenName || item?.GivenName : '...';
                                    var lastName = item?.surname || item?.Surname ? item?.surname || item?.Surname: '...';
                                    var jobTitle = item?.jobTitle || item?.JobTitle ? item?.jobTitle || item?.JobTitle : '...';
                                    return {
                                        keys: [firstName, lastName, jobTitle],
                                        value: firstName + ' ' + lastName,
                                        content: <Staff name={firstName + ' ' + lastName} job={jobTitle} color={Helpers.getRandomColor()} />
                                    }
                                };
                                default: break;
                            }

                        }),
                        triggerChar: '@'
                    }
                ]
            }}
        />
        <Typography variant='caption'>{ UIText.tagPersonHint}</Typography>
    </>);
}
interface ITextFieldProps{
    id?: string;
    clearable?: boolean;
    disabled?: boolean;
    controlled?: boolean;
    type: string;
    label?: string;
    required?: boolean;
    error?: boolean;
    onChange;
    focus?: boolean;
    value?: string | any;
    autoComplete?: string;
    options?: any[];
    helperText?: string;
    multiple?: boolean;
    multiline?: boolean;
    rows?: number;
    childOptions?: [];
    childType?: string;
    childLabel?: string;
    parentVisible?: boolean;
    childVisible?: boolean;
    parentDefault?: any;
    childDefault?: any;
    parentOptions?;
    parentType?: string;
    parentLabel?: string;
    defaultValue?;
    hijri?: boolean;
}
interface ISwitchTextFieldProps {
    type: string;
    firstLabel?: string;
    secondLabel?: string;
    label?: string;
    required?: boolean;
    error?: boolean;
    onChange;
    onSwitchChange;
    helperText?: string;
    multiple?: boolean;
    selected: boolean;
    searchType: string;
    searchUrl: string;
}
const AntSwitch = withStyles((theme: Theme) =>
    createStyles({
        root: {
            width: 45,
            height: 25,
            padding: 0,
            display: 'flex'
        },
        switchBase: {
            padding: 2,
            color: theme.palette.grey[500],
            '&$checked': {
                transform: 'translateX(12px)',
                color: theme.palette.primary.light,
                '& + $track': {
                    opacity: 1,
                    backgroundColor: theme.palette.primary.main,
                    borderColor: theme.palette.primary.main,
                },
            },
        },
        thumb: {
            width: 20,
            height: 20,
            boxShadow: 'none',
        },
        track: {
            border: `1px solid ${theme.palette.grey[500]}`,
            borderRadius: 25 / 2,
            opacity: 1,
            backgroundColor: theme.palette.primary.light,
        },
        checked: {},
    }),
)(Switch);