﻿import * as React from "react";
import { IEmailProps } from "../AddNew/MatterNote";
import { ReduxState } from "../..";
import * as actions from '../../redux/actions';
import { connect as reduxConnect } from "react-redux";

class A4OBase extends React.Component<FormReduxProps, {}> {

    messageID: string;
    attachments: any[];
    data: any;
    emailInfo: IEmailProps;
    constructor(props) {
        super(props);
        this.attachments = [];
        this.messageID = '';
        this.data = {};
    }
    componentDidMount() {
        window.addEventListener('includeEmailAttachment', this.handleChangeEmailAttachment.bind(this));
    }
    componentWillUnmount() {
        window.removeEventListener('includeEmailAttachment', this.handleChangeEmailAttachment);
    }
    handleChangeEmailAttachment(event) {
        if (event) {
            var includeEmailAttachments = event.detail;
            if (includeEmailAttachments) {
                this.props.addFile(this.attachments);
            }
            else {
                var files = this.props.files.filter(file => file.isEmailAttachment == false);
                this.props.setAttachments(files);
            }
        }
    }
}
const mapStateToProps = (state: ReduxState) => ({
    language: state.settings.language,
    runLoader: state.loader.runLoader,
    includeEmail: state.attachments.includeEmail,
    files: state.attachments.files
});
const mapDispatchToProps = (dispatch) => {
    return {
        successAlert: (message: string) => dispatch(actions.successAlert(message)),
        errorAlert: (message: string) => dispatch(actions.errorAlert(message)),
        stopLoader: () => dispatch(actions.stopLoader()),
        startLoader: () => dispatch(actions.startLoader()),
        startSaving: () => dispatch(actions.startSaving()),
        stopSaving: () => dispatch(actions.stopSaving()),
        openModal: () => dispatch(actions.openModal()),
        focusError: (id: string) => dispatch(actions.focusError(id)),
        setAttachments: (files) => dispatch(actions.setAttachments(files)),
        addFile: (file) => dispatch(actions.addFile(file)),
        deleteFile: (index) => dispatch(actions.deleteFile(index)),
        setHasEmailAttachments: (found: boolean) => dispatch(actions.setEmailHasAttachments(found)),
        setFetchingAttachments: (status: boolean) => dispatch(actions.setFetchingAttachments(status))
    }
};
type FormReduxProps = ReturnType<typeof mapStateToProps> & ReturnType<typeof mapDispatchToProps>;
export function connectBase(Component) {
    return reduxConnect(
        (state, props) => ({ mapStateToProps, mapDispatchToProps})
    )(Component);
}
export default A4OBase;