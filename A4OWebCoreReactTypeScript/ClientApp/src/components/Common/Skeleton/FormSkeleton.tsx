﻿import * as React from "react";
import { Snackbar, Alert, AlertTitle, Theme, createStyles, WithStyles, Grid, Container, Skeleton } from "@material-ui/core";

export default function FormSkeleton(props) {
    const getSkeletonShape = (input) => {
        switch (input) {
            case 'settings':
                return <Grid container spacing={2}>
                    <Grid item xs={6}>
                        <Skeleton variant="circular" width={40} height={40} />
                    </Grid>
                    <Grid item xs={6}>
                        <Skeleton variant="text"/>
                    </Grid>
                    <Grid item xs={12}>
                        <Skeleton variant="rectangular" width='100%' height={300} />
                    </Grid>
                </Grid>;
            case 'form':
                return <Grid container spacing={1}>
                    <Grid item xs={12}>
                        <Skeleton variant="text"/>
                    </Grid>
                    <Grid item xs={12}>
                        <Skeleton variant="rectangular" height={500} />
                    </Grid>
                    <Grid item xs={12}>
                        <Skeleton variant="rectangular" height={300} />
                    </Grid>
                    <Grid item xs={12}>
                        <Skeleton variant="rectangular" height={300} />
                    </Grid>
                </Grid>;
            case 'activities':
                return <Grid container spacing={1}>
                    <Grid item xs={12}>
                        <Skeleton variant="rectangular" height={150} />
                    </Grid>
                    <Grid item xs={12}>
                        <Skeleton variant="rectangular" height={150} />
                    </Grid>
                    <Grid item xs={12}>
                        <Skeleton variant="rectangular" height={150} />
                    </Grid>
                    <Grid item xs={12}>
                        <Skeleton variant="rectangular" height={150} />
                    </Grid>
                    <Grid item xs={12}>
                        <Skeleton variant="rectangular" height={150} />
                    </Grid>
                </Grid>;
            default: return <></>;
        }
    };
    return <div style={{ width: '100%'}}>
            {getSkeletonShape(props.input)}
        </div>
}