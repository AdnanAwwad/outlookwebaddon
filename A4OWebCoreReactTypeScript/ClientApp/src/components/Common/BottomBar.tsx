﻿import * as React from "react";
import Button from "@material-ui/core/Button";
import { useSelector } from "react-redux";
import Container from "@material-ui/core/Container";
import Grid from "@material-ui/core/Grid";
import * as Colors from '@material-ui/core/colors';
import * as UIStrings from '../../UIStrings';
import { Toolbar, Autocomplete, Theme, WithStyles, Paper, CircularProgress, makeStyles, withStyles } from "@material-ui/core";
import { Dispatch } from "redux";
import { ReduxState } from "../..";

const useStyles = makeStyles((theme: Theme) => ({
    root: {
        position: 'fixed',
        height: '65px',
        bottom: '0px',
        width: '100%',
        flexGrow: 1,
        boxShadow: 'inset 0 7px 9px -7px rgba(0,0,0,0.4)',
        zIndex: 999
    },
    paper: {
        height: '100%',
        boxShadow: 'inset 0 7px 9px -7px rgba(0,0,0,0.4)'
    },
    toolBar: {
        width: '100%',
        paddingTop: '10px'
    },
    progress: {
        margin: '0 auto'
    }
}));

export default function BottomBar(props) {
    const languageSelected = useSelector((state: ReduxState) => state.settings.language);
    const isSaving = useSelector((state: ReduxState) => state.saving.saving);
    const UIText = UIStrings.getLocaleStrings(languageSelected);
    const classes = useStyles();
    return (
        <div className={classes.root}>
            <Paper className={classes.paper}>
                <Grid container alignItems='center'>
                    <Toolbar className={classes.toolBar}>
                        {isSaving ?
                            <CircularProgress thickness={4.5} className={classes.progress} /> :
                            <Button
                                fullWidth
                                type="submit"
                                variant="contained"
                                color="primary"
                                disabled={props.loading}
                                onClick={props.onSubmit}
                            >
                                {UIText.save}
                            </Button>
                        }
                    </Toolbar>
                </Grid>
            </Paper>
        </div>
    )
}