﻿import * as React from 'react';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import ClickAwayListener from '@material-ui/core/ClickAwayListener';
import Grow from '@material-ui/core/Grow';
import Paper from '@material-ui/core/Paper';
import Popper from '@material-ui/core/Popper';
import MenuItem from '@material-ui/core/MenuItem';
import MenuList from '@material-ui/core/MenuList';
import { withStyles, Theme } from '@material-ui/core/styles';
import * as Colors from '@material-ui/core/colors';
import { Typography, Tooltip, ListItemIcon } from '@material-ui/core';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { useSelector } from 'react-redux';
import { ReduxState } from '../..';
import * as UIStrings from '../../UIStrings';
const CustomButtonBlue = withStyles((theme: Theme) => ({
    root: {
        color: 'white',
        backgroundColor: theme.palette.primary.main,
        '&:hover': {
            backgroundColor: Colors.blue[700],
        },
        width: '130px',
        height: '40px'
    },
}))(Button);
const CustomButtonBordered = withStyles((theme: Theme) => ({
    root: {
        backgroundColor: Colors.common['white'],
        '&:hover': {
            backgroundColor: Colors.grey['A200'],
        },
        border: '1px solid lightGrey',
        width: '130px',
        height: '40px'
    },
}))(Button);
export default function UniversalSplitButton(props) {
    const [open, setOpen] = React.useState(false);
    const anchorRef = React.useRef<any>(null);
    const [selectedIndex, setSelectedIndex] = React.useState(null);
    const language = useSelector((state: ReduxState) => state.settings.language);
    const UIText = UIStrings.getLocaleStrings(language);

    const handleMenuItemClick = (
        event: React.MouseEvent<HTMLLIElement, MouseEvent>,
        index: number,
        id: number
    ) => {
        setSelectedIndex(index);
        setOpen(false);
        props.handleItemClicked(id);
    };

    const handleToggle = () => {
        setOpen((prevOpen) => !prevOpen);
    };

    const handleClose = (target) => {
        if (anchorRef.current && anchorRef.current.contains(target as HTMLElement)) {
            return;
        }

        setOpen(false);
    };
    const getOptionLabel = (option) => {
        switch (props.type) {
            case 'caseStatus': {
                return UIText.sendTo + ' ' +((option.transitionName != null && option.transitionName.length > 0) ?
                    option.transitionName : option.name);
            }
            default: return option;
        }
    }
    return (
        <Grid container alignItems="center">
            <Grid item xs={12}>
                <CustomButtonBlue
                    size="small"
                    aria-controls={open ? 'split-button-menu' : undefined}
                    aria-expanded={open ? 'true' : undefined}
                    aria-label="select merge strategy"
                    aria-haspopup="menu"
                    ref={anchorRef}
                    onClick={handleToggle}                    
                >
                    <Grid container justifyItems='space-between'>
                        <Tooltip title={props.currentValue?.name}>
                            <Typography noWrap style={{ width: '80%' }}>{props.currentValue?.name}</Typography>
                        </Tooltip>
                        <ExpandMoreIcon />
                    </Grid>
                </CustomButtonBlue>
                <Popper open={open} anchorEl={anchorRef.current} role={undefined} transition>
                    {({ TransitionProps, placement }) => (
                        <Grow
                            {...TransitionProps}
                            style={{
                                transformOrigin: placement === 'bottom' ? 'center top' : 'center bottom',
                                width: '100%',
                                overflow: 'hidden'
                            }}
                        >
                            <Paper>
                                <ClickAwayListener onClickAway={(event) => handleClose(event.target)}>
                                    <MenuList id="split-button-menu">
                                        {props.options.map((option, index) => (
                                            <MenuItem
                                                key={option.id}
                                                selected={index === selectedIndex}
                                                onClick={(event) => handleMenuItemClick(event, index, option.id)}
                                            >
                                                <Typography noWrap style={{width: '80%'}}>
                                                    {getOptionLabel(option)}
                                                </Typography>
                                            </MenuItem>
                                        ))}
                                    </MenuList>
                                </ClickAwayListener>
                            </Paper>
                        </Grow>
                    )}
                </Popper>
            </Grid>
        </Grid>
    );
}
export function PriorityButton(props) {
    const [open, setOpen] = React.useState(false);
    const anchorRef = React.useRef<any>(null);
    const [value, setValue] = React.useState(props.options ? props.options.filter( option => option.id === 'medium')[0] : null);
    const handleMenuItemClick = (
        event: React.MouseEvent<HTMLLIElement, MouseEvent>,
        index: number,
        option: any
    ) => {
        event.preventDefault();
        event.stopPropagation();
        props.handleItemClicked(option);
        setValue(option);
        setOpen(false);
    };

    const handleToggle = () => {
        setOpen((prevOpen) => !prevOpen);
    };

    const handleClose = (target) => {
        if (anchorRef.current && anchorRef.current.contains(target as HTMLElement)) {
            return;
        }

        setOpen(false);
    };
    return (
        <Grid container alignItems="center">
            <Grid item xs={12}>
                <CustomButtonBordered
                    size="small"
                    aria-controls={open ? 'split-button-menu' : undefined}
                    aria-expanded={open ? 'true' : undefined}
                    aria-label="select merge strategy"
                    aria-haspopup="menu"
                    ref={anchorRef}                    
                    onClick={handleToggle}
                >
                    {value != null ?
                        <Grid container justifyItems='space-between' alignItems='center'>
                            <Grid item container xs justifyItems='space-between' alignItems='center'>
                                <img src={require('../../assets/' + value.id + '.png')} style={{ width: '16px', height: '16px', margin: '3px' }} />
                                <Typography>{value.value}</Typography>
                            </Grid>
                            <Grid item xs={2}>
                                <ExpandMoreIcon style={{ marginTop: '2px'}} />
                            </Grid>
                        </Grid> : <></>}
                </CustomButtonBordered>
                <Popper open={open} anchorEl={anchorRef.current} role={undefined} transition>
                    {({ TransitionProps, placement }) => (
                        <Grow
                            {...TransitionProps}
                            style={{
                                transformOrigin: placement === 'bottom' ? 'center top' : 'center bottom',
                            }}
                        >
                            <Paper>
                                <ClickAwayListener onClickAway={(event) => handleClose(event.target)}>
                                    <MenuList id="split-button-menu">
                                        {props.options.map((option, index) => (
                                            <MenuItem
                                                key={option.id}
                                                onClick={(event) => handleMenuItemClick(event, index, option)}
                                            >
                                                <ListItemIcon>                                                                                                    
                                                    <img src={require('../../assets/' + option?.id + '.png')} style={{ width: '16px', height: '16px', marginTop: '3px' }} />
                                                </ListItemIcon>                                                
                                                <Typography noWrap>{option?.value}</Typography>
                                            </MenuItem>
                                        ))}
                                    </MenuList>
                                </ClickAwayListener>
                            </Paper>
                        </Grow>
                    )}
                </Popper>
            </Grid>
        </Grid>
    );
}
