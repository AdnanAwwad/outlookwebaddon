﻿import React from 'react';
import Container from "../Login/node_modules/@material-ui/core/Container";
import Paper from '../Login/node_modules/@material-ui/core/Paper';
import CssBaseline from "@material-ui/core/CssBaseline";
import Grid from '../Login/node_modules/@material-ui/core/Grid';
import { withStyles } from "../Login/node_modules/@material-ui/core/styles";

class AppContainer extends React.Component {
    constructor(props, context) {
        super(props, context);
    };
    render() {
        const { classes } = this.props;
        return (
            <Container component="main" maxWidth="xs">
                <CssBaseline />
                <Grid container spacing={0}>
                    <Grid className={classes.root} xs={12}>
                        <Paper className={classes.paper} elevation={0}>
                            {this.props.childerns}
                        </Paper>
                    </Grid>
                </Grid>
            </Container>
        );
    };
};
export default withStyles(useStyles, { withTheme: true })(AppContainer);


const useStyles = (theme) => ({
    root: {
        flexGrow: 1,
        height: '100vh',
    },
    paper: {
        padding: theme.spacing(2),
        //textAlign: 'center',
        color: theme.palette.text.secondary,
        width: '100%',
        height: '100%',
    },
});