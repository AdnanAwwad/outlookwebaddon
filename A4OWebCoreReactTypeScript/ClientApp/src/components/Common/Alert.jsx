﻿/* global UIStrings */
import React from 'react';
import { Snackbar, Alert } from '@material-ui/core';
import { connect } from 'react-redux';
import { AlertTitle } from '@material-ui/core';
import { closeAlert } from '../../redux/actions';
import * as UIStrings from '../../UIStrings';
import { useSelector, useDispatch } from "react-redux";

export default function PopupAlert(props) {
    const { alertMessage, severity, open } = useSelector(state => state.alert);
    const dispatch = useDispatch();
    const language = useSelector(state => state.settings.language);
    const [vertical, setVertical] = React.useState('top');
    const [horizontal, setHorizontal] = React.useState('center');
    const UIText = UIStrings.getLocaleStrings(language);
    const handleClose = () => {
        dispatch(closeAlert());
    };
    return (
        (severity !== "") ?
            <Snackbar anchorOrigin={{ vertical, horizontal }
            } open={open} autoHideDuration={6000}
                onClose={handleClose}
            >
                <Alert onClose={handleClose} severity={severity}>
                    <AlertTitle>{severity === "error" ? UIText.error : UIText.success}</AlertTitle>
                    <div
                        dangerouslySetInnerHTML={{
                            __html: alertMessage
                        }}></div>
                </Alert>
            </Snackbar> : null
    );
}
