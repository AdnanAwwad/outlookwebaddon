﻿
import * as React from "react";
import { makeStyles } from "@material-ui/core/styles";
import { useDispatch, useSelector, connect } from "react-redux";
import Grid from "@material-ui/core/Grid";
import { Typography, createStyles, FormControlLabel, Checkbox, CircularProgress, Button, IconButton, Theme, Modal, Toolbar, Paper, Autocomplete, fade, InputBase, AutocompleteCloseReason, Avatar, Backdrop, ListSubheader, ButtonBase } from "@material-ui/core";
import { DropzoneAreaBase, FileObject } from "material-ui-dropzone";
import { ReduxState } from "../..";
import { Audiotrack, Theaters } from '@material-ui/icons';
import { changeIncludeEmail, deleteMoreEmails, addMoreEmails, changeIncludeEmailAttachments } from "../../redux/actions";
import * as UIStrings from '../../UIStrings';
import * as actions from '../../redux/actions';
import AddIcon from '@material-ui/icons/Add';
import CloseIcon from '@material-ui/icons/Close';
import * as GraphService from '../../GraphService';
import { UniversalTextField } from "../Common/TextFields";
import SearchIcon from '@material-ui/icons/Search';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import { Helpers } from "../../Helpers/Helpers";
import createLog from 'localstorage-logger';

const log = createLog({
    logName: 'appLogs',
    maxLogSizeInBytes: 512 * 1024
});
const useStyles = makeStyles(() =>
    createStyles({
        fileDropzone: {
            "& .MuiDropzonePreviewList-removeButton": {
                top: "35%",
                left: "40%",
            }
        }        
    })
);
export default function AttachmentsArea(props) {
    const dispatch = useDispatch();
    const { language } = useSelector((state: ReduxState) => state.settings);
    const { moreEmails } = useSelector((state: ReduxState) => state.attachments);
    const { files, includeEmail, includeEmailAttachments, hasEmailAttachments, fetchingAttachments } = useSelector((state: ReduxState) => state.attachments);
    const UIText = UIStrings.getLocaleStrings(language);
    const [openModal, setOpenModal] = React.useState(false);
    const [includeEmails, setIncludeEmails] = React.useState(true);
    const [gettingAttachment, setGettingAttachments] = React.useState(false);
    const handleFileAdded = (file: FileObject[]) => {
        var addedFiles = file.map(f => { return ({ fileObject: f, isEmailAttachment: false, isInline: false }) });
        dispatch(actions.addFile(addedFiles));
    };
    const handleFileDeleted = (file: FileObject, index: number) => {
        dispatch(actions.deleteFile(index));
    };
    const handlePreviewIcon = (fileObject: FileObject, classes) => {
        const { type } = fileObject.file
        const iconProps = {
            className: classes.image,
        }
        if (type.startsWith("video/")) return <Theaters {...iconProps} />
        if (type.startsWith("audio/")) return <Audiotrack {...iconProps} />
        if (type.startsWith("image/")) return <img src={fileObject.data.toString()} {...iconProps} />
        switch (type) {
            case "application/msword":
            case "application/vnd.openxmlformats-officedocument.wordprocessingml.document":
                return <img src={require('../../assets/word.svg')} {...iconProps} />
            case "application/vnd.openxmlformats-officedocument.presentationml.presentation":
                return <img src={require('../../assets/powerpoint.svg')} {...iconProps} />
            case "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet":
                return <img src={require('../../assets/excel.svg')} {...iconProps} />
            case "application/pdf":
                return <img src={require('../../assets/pdf.svg')} {...iconProps} />
            case "text/html":
                return <img src={require('../../assets/html.svg')} {...iconProps} />
            default:
                return <img src={require('../../assets/blank.svg')} {...iconProps} />
        }
    };
    const changeIncludeAttachments = (checked) => {
        dispatch(changeIncludeEmailAttachments(checked));
        const event = new CustomEvent('includeEmailAttachment', { detail: checked });
        // Dispatch the event.
        window.dispatchEvent(event);
    }
    const handleAddEmails = async (emails: any[]) => {    
        if (emails && emails.length > 0) {
            dispatch(addMoreEmails(emails));
            for (var i = 0; i < emails.length; i++) {
                const email = emails[i];
                try {
                    if (email.hasAttachments || email.HasAttachments) {
                        setGettingAttachments(true);
                        const token = await props.token;
                        if (token) {
                            var emailFiles: any[] = [];
                            var mailAttachments = await GraphService.getMessageAttachments(token, email.id);
                            if (mailAttachments) {
                                mailAttachments.forEach(attachment => {
                                    if (attachment['@odata.type'] === '#microsoft.graph.fileAttachment') {
                                        var file = {};
                                        var data = new File([`data:${attachment.contentType};base64,${attachment.contentBytes}`],
                                            attachment.name,
                                            { type: attachment.contentType });
                                        file['data'] = `data:${attachment.contentType};base64,${attachment.contentBytes}`;
                                        file['file'] = data;
                                        emailFiles.push({ fileObject: file, isEmailAttachment: true, isInline: attachment.isInline });
                                    }
                                });
                                setGettingAttachments(false);
                                props.onAddAttachments(emailFiles);
                                dispatch(actions.addFile(emailFiles));
                            }
                        }
                        else {
                            const promiseToGetAttachments = new Promise(resolve => {
                                Office.context.mailbox.getCallbackTokenAsync({ isRest: true }, async function (result) {
                                    if (result.status === Office.AsyncResultStatus.Succeeded) {
                                        const token = result.value;
                                        try {
                                            var mailAttachments = await GraphService.getMessageAttachments(token, email.Id, true);
                                            var mailFiles = [];
                                            if (mailAttachments) {
                                                mailAttachments.forEach(attachment => {
                                                    var file = {};
                                                    var data = new File([`data:${attachment.ContentType};base64,${attachment.ContentBytes}`],
                                                        attachment.Name,
                                                        { type: attachment.ContentType });
                                                    file['data'] = `data:${attachment.ContentType};base64,${attachment.ContentBytes}`;
                                                    file['file'] = data;
                                                    mailFiles.push({ fileObject: file, isEmailAttachment: true, isInline: attachment.IsInline });
                                                });
                                                resolve(mailFiles)
                                            }
                                        }
                                        catch (err) {
                                            resolve([]);
                                            log.error('Attachments[handleAddEmails]: ' + err);
                                        }
                                    }
                                });
                            });
                            const attachmentsResolved = await promiseToGetAttachments;
                            setGettingAttachments(false);
                            props.onAddAttachments(attachmentsResolved);
                            dispatch(actions.addFile(attachmentsResolved));
                        }
                    }
                }
                catch (err) {
                    setGettingAttachments(false);
                    log.error('Attachments[handleAddEmails]: ' + err);
                }
            }
        }
    }
    const handleDeleteEmail = (event, index) => {
        if (index >= 0) {
            event.preventDefault();
            event.stopPropagation();
            dispatch(deleteMoreEmails(index));
        }
    }
    const styles = useStyles();
    React.useEffect(() => {
        const mailItem = Office.context.mailbox.item;
        var text: string = mailItem.sender?.displayName;
        if (text) {
            var data = text.split(' ');
            var firstCharfirstString = data[0].charAt(0);
            var firstCharLastString = data.lastItem.charAt(0);
            text = firstCharfirstString + firstCharLastString;
        }
        const currentEmail = {
            subject: mailItem.subject,
            sender: mailItem.sender,
            id: mailItem.itemId,
            receivedDateTime: mailItem.dateTimeCreated,
            avatarAbb: text,
            hasAttachments: mailItem?.attachments?.length > 0,
            isEmail: true
        };
        dispatch(addMoreEmails([currentEmail]));
    }, []);
    return (
        <Grid container spacing={2}>
            <Grid item xs={12} sm={12}>
                <FormControlLabel
                    control={
                        <Checkbox
                            onChange={(event) => changeIncludeAttachments(event.target.checked)}
                            name="includeEmailAttachments"
                            color="primary"
                            value={includeEmailAttachments}
                            defaultChecked
                        />
                    }
                    label={UIText.includeEmailAttachments}
                />
                {fetchingAttachments ? <Grid item container xs alignItems='center' justifyItems='center'>
                    <Grid item xs>
                        <CircularProgress />
                    </Grid>
                    <Grid item xs>
                        <Typography>{UIText.fetchingEmailAttachments}</Typography>
                    </Grid>
                </Grid> : <></>}
                <FormControlLabel
                    control={
                        <Checkbox
                            onChange={(event) => {
                                setIncludeEmails(event.target.checked);
                                dispatch(changeIncludeEmail(event.target.checked));
                            }}
                            name="includeEmail"
                            color="primary"
                            value={includeEmail}
                            defaultChecked
                        />
                    }
                    label={UIText.includeEmail}
                />
                <Grid item container alignItems='center' justifyItems='center' xs={12}>
                    <Button
                        type="button"
                        variant="contained"
                        color="primary"
                        style={{ margin: 'auto' }}
                        startIcon={<AddIcon />}
                        onClick={(event) => {
                            event.preventDefault();
                            event.stopPropagation();
                            setOpenModal(true);
                        }}
                    >
                        {UIText.attachMoreEmails}
                    </Button>
                    {includeEmails ?
                        <List dense style={{ width: '100%', overflow: 'auto' }}>
                            {moreEmails.map((email, index) => {                                
                                return (
                                    <ListItem key={index}>
                                        <ListItemAvatar>
                                            <Avatar
                                                alt={email.subject || email.Subject}
                                                style={{ backgroundColor: Helpers.blendColors(alphabetColor[email.avatarAbb.charAt(0)], alphabetColor[email.avatarAbb?.length > 1 ? email.avatarAbb.charAt(1) : email.avatarAbb.charAt(0)], 0.5) }}
                                            >
                                                {email.avatarAbb}
                                            </Avatar>
                                        </ListItemAvatar>
                                        <ListItemText
                                            primary={email.subject || email.Subject}
                                            secondary={
                                                <React.Fragment>
                                                    {(email.sender?.emailAddress?.name || email.sender?.displayName || email.Sender?.EmailAddress?.Name || email.Sender?.DisplayName) + ' - ' + Helpers.formatDate(email.receivedDateTime || email.ReceivedDateTime, true)}

                                                    <Typography
                                                        component="span"
                                                        variant="body2"
                                                        color="green"
                                                        style={{ display: 'inline' }}
                                                    >
                                                        {email.hasAttachments || email.HasAttachments ? ' - ' + UIText.hasAttachments : ""}
                                                    </Typography>
                                                </React.Fragment>
                                            }
                                        />
                                        <ListItemSecondaryAction>
                                            <IconButton aria-label="delete" onClick={(event) => handleDeleteEmail(event, index)}>
                                                <CloseIcon fontSize="small" />
                                            </IconButton>
                                        </ListItemSecondaryAction>
                                    </ListItem>);
                            })}
                        </List>
                        : <List dense style={{ width: '100%', overflow: 'auto' }}></List>}
                </Grid>
                {gettingAttachment ? <Grid item container alignItems='center' justifyItems='center' xs={12} direction='column'>
                    <div style={{ margin: 'auto' }}>
                        <CircularProgress style={{marginLeft: '45px'}}/>
                        <Typography variant='overline' display="block" gutterBottom>
                            {UIText.gettingAttachments}
                        </Typography>
                    </div>
                </Grid> : <></>
                }
                <div className={styles.fileDropzone}>
                    <DropzoneAreaBase
                        fileObjects={files.map(fileForm => { return fileForm.fileObject })}
                        onAdd={handleFileAdded}
                        onDelete={handleFileDeleted}
                        showAlerts={false}
                        dropzoneText={UIText.dropzoneText}
                        filesLimit={20}
                        maxFileSize={50000000}
                        getFileLimitExceedMessage={
                            (limit) => {
                                dispatch(actions.errorAlert(UIText.filesLimitExceeded));
                                return '';
                            }}
                        showPreviewsInDropzone
                        showFileNames
                        getPreviewIcon={handlePreviewIcon}
                        showFileNamesInPreview
                        previewGridProps={{
                            item: {
                                alignItems: 'center',
                                alignContent: 'center',
                                xs: 6,
                                style: {
                                    overflow: 'hidden',
                                    whiteSpace: 'nowrap',
                                    textOverflow: 'ellipsis'
                                }
                            }
                        }}
                        previewText={UIText.preview}
                    />
                </div>
            </Grid>
            <EmailModal token={props.token} open={openModal} closeModal={() => { setOpenModal(false); }} onClose={null} addEmails={handleAddEmails} />
        </Grid>
    );
}
interface ModalProps {
    open: boolean;
    closeModal;
    onClose;
    token;
    addEmails;
}
const modalStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            overflow: 'auto',
            height: '100%'
        },
        form: {
            paddingTop: '5px',
            paddingBottom: '65px'
        },
        bottomBar: {
            position: 'fixed',
            height: '65px',
            bottom: '0px',
            width: '100%',
            zIndex: 9999
        }
    }),
);
const emailStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            width: 221,
            fontSize: 13,
        },
        button: {
            fontSize: 13,
            width: '100%',
            textAlign: 'left',
            paddingBottom: 8,
            color: '#586069',
            fontWeight: 600,
            '&:hover,&:focus': {
                color: '#0366d6',
            },
            '& span': {
                width: '100%',
            },
            '& svg': {
                width: 16,
                height: 16,
            },
        },
        tag: {
            marginTop: 3,
            height: 20,
            padding: '.15em 4px',
            fontWeight: 600,
            lineHeight: '15px',
            borderRadius: 2,
        },
        popper: {
            border: '1px solid rgba(27,31,35,.15)',
            boxShadow: '0 3px 12px rgba(27,31,35,.15)',
            borderRadius: 3,
            width: 300,
            zIndex: 1,
            fontSize: 13,
            color: '#586069',
            backgroundColor: '#f6f8fa',
        },
        header: {
            borderBottom: '1px solid #e1e4e8',
            padding: '8px 10px',
            fontWeight: 600,
        },
        inputBase: {
            padding: 10,
            width: '100%',
            borderBottom: '1px solid #dfe2e5',
            '& input': {
                borderRadius: 4,
                backgroundColor: theme.palette.common.white,
                padding: 8,
                transition: theme.transitions.create(['border-color', 'box-shadow']),
                border: '1px solid #ced4da',
                fontSize: 14,
                '&:focus': {
                    boxShadow: `${fade(theme.palette.primary.main, 0.25)} 0 0 0 0.2rem`,
                    borderColor: theme.palette.primary.main,
                },
            },
        },
        paper: {
            boxShadow: 'none',
            margin: 0,
            color: '#586069',
            fontSize: 13,
        },
        option: {
            minHeight: 'auto',
            alignItems: 'flex-start',
            padding: 8,
            '&[aria-selected="true"]': {
                backgroundColor: 'transparent',
            },
            '&[data-focus="true"]': {
                backgroundColor: theme.palette.action.hover,
            },
        },
        popperDisablePortal: {
            position: 'relative',
        },
        iconSelected: {
            width: 17,
            height: 17,
            marginRight: 5,
            marginLeft: -2,
        },
        color: {
            width: 14,
            height: 14,
            flexShrink: 0,
            borderRadius: 3,
            marginRight: 8,
            marginTop: 2,
        },
        text: {
            flexGrow: 1,
        },
        close: {
            opacity: 0.6,
            width: 18,
            height: 18,
        },
    }),
);
var emaiInput: string = '';
export function EmailModal(props: ModalProps) {
    const language = useSelector((state: ReduxState) => state.settings.language);
    const UIText = UIStrings.getLocaleStrings(language);
    const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
    const [options, setOptions] = React.useState([]);
    const [loading, setLoading] = React.useState(false);
    const [lazyLoad, setLazyLoad] = React.useState(false);
    const [checked, setChecked] = React.useState([]);
    const [nextLink, setNextLink] = React.useState<string>(null);
    const modalClasses = modalStyles();
    const classes = emailStyles();
    const handleChangeEmails = () => {
        var emails = [];
        checked.forEach(item => {
            emails.push(options[item]);
        })
        props.addEmails(emails);
        setChecked([]);
        props.closeModal();
    };
    const handleToggle = (value: number) => () => {
        const currentIndex = checked.indexOf(value);
        const newChecked = [...checked];

        if (currentIndex === -1) {
            newChecked.push(value);
        } else {
            newChecked.splice(currentIndex, 1);
        }

        setChecked(newChecked);
    };
    const handleClose = (event: React.ChangeEvent<{}>, reason: AutocompleteCloseReason) => {
        if (reason === 'toggleInput') {
            return;
        }
        if (anchorEl) {
            anchorEl.focus();
        }
        setAnchorEl(null);
    };
    const handleChangeTextField = async (event: React.ChangeEvent<HTMLInputElement>) => {
        var text = event.target.value;
        emaiInput = text;
    };
    const fetchData = async (event) => {
        event.preventDefault();
        event.stopPropagation();
        setLoading(true);
        setOptions([]);
        setChecked([]);
        try {
            const token = await props.token;
            if (token) {
                let response = await GraphService.getEmailsList(token, emaiInput);
                if (response) {
                    const mails = response.value;
                    setOptions(mails.map((item, index) => {
                        var mail = mails[index];
                        if (mail) {
                            var text: string = mail.sender?.emailAddress?.name;
                            if (text) {
                                var data = text.split(' ');
                                var firstCharfirstString = data[0].charAt(0);
                                var firstCharLastString = data.lastItem.charAt(0);
                                text = firstCharfirstString + firstCharLastString;
                            }
                            mail.avatarAbb = text;
                        }
                        return mail;
                    }));
                }
                setLoading(false);
                if (response['@odata.nextLink']?.length > 0) {
                    setNextLink(response['@odata.nextLink']);
                }
            }
            else {
                await Office.context.mailbox.getCallbackTokenAsync({ isRest: true }, async function (result) {
                    if (result.status === Office.AsyncResultStatus.Succeeded) {
                        const token = result.value;
                        let response = await GraphService.getEmailsList(token, emaiInput, true);
                        if (response) {                            
                            const mails = response.value;
                            setOptions(mails.map((item, index) => {
                                var mail = mails[index];
                                if (mail) {
                                    var text: string = mail.Sender?.EmailAddress?.Name;
                                    if (text) {
                                        var data = text.split(' ');
                                        var firstCharfirstString = data[0].charAt(0);
                                        var firstCharLastString = data.lastItem.charAt(0);
                                        text = firstCharfirstString + firstCharLastString;
                                    }
                                    mail.avatarAbb = text;
                                }
                                return mail;
                            }));
                        }
                        setLoading(false);
                        if (response['@odata.nextLink']?.length > 0) {
                            setNextLink(response['@odata.nextLink']);
                        }                        
                    }
                });                
            }
        }
        catch (err) {
            setLoading(false);
        }
    };
    const handleScroll = async (e) => {
        const bottom = e.target.scrollHeight - e.target.scrollTop === e.target.clientHeight;
        if (bottom) {
            if (nextLink) {
                setLazyLoad(true);
                try {
                    const token = await props.token;
                    if (token) {
                        let response = await GraphService.getNextPages(token, nextLink);
                        if (response) {
                            const mails = response.value;
                            setOptions(options.concat(mails.map((item, index) => {
                                var mail = mails[index];
                                if (mail) {
                                    var text: string = mail.sender?.emailAddress?.name;
                                    try {
                                        if (text) {
                                            var data = text.split(' ');
                                            var firstCharfirstString = data[0].charAt(0);
                                            var firstCharLastString = data.lastItem.charAt(0);
                                            text = firstCharfirstString + firstCharLastString;
                                        }
                                    }
                                    catch (err) {
                                    }
                                    mail.avatarAbb = text;
                                }
                                return mail;
                            })));
                        }
                        setLazyLoad(false);
                        if (response['@odata.nextLink']?.length > 0) {
                            setNextLink(response['@odata.nextLink']);
                        }
                        else {
                            setNextLink(null);
                        }
                    }
                    else {
                        await Office.context.mailbox.getCallbackTokenAsync({ isRest: true }, async function (result) {
                            if (result.status === Office.AsyncResultStatus.Succeeded) {
                                const token = result.value;
                                let response = await GraphService.getNextPages(token, nextLink, true);
                                if (response) {
                                    const mails = response.value;
                                    setOptions(options.concat(mails.map((item, index) => {
                                        var mail = mails[index];
                                        if (mail) {
                                            var text: string = mail.Sender?.EmailAddress?.Name;
                                            try {
                                                if (text) {
                                                    var data = text.split(' ');
                                                    var firstCharfirstString = data[0].charAt(0);
                                                    var firstCharLastString = data.lastItem.charAt(0);
                                                    text = firstCharfirstString + firstCharLastString;
                                                }
                                            }
                                            catch (err) {
                                            }
                                            mail.avatarAbb = text;
                                        }
                                        return mail;
                                    })));
                                }
                                setLazyLoad(false);
                                if (response['@odata.nextLink']?.length > 0) {
                                    setNextLink(response['@odata.nextLink']);
                                }
                                else {
                                    setNextLink(null);
                                }
                            }
                        });           
                    }
                }
                catch (err) {
                    setLazyLoad(false);
                }
            }
        }
    }
    return (<Modal
        open={props.open}
        onClose={props.onClose}
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
        style={{ backgroundColor: 'rgba(40, 40, 40, 0.4)' }}
    >
        <div className={modalClasses.root} onScroll={handleScroll}>
            <Grid container>
                <Grid item xs={12}>
                    <Paper className={modalClasses.form}>
                        <form id='email' noValidate onSubmit={fetchData}>
                            <Grid container spacing={2} alignItems='center' justifyItems='center'>
                                <Grid item xs={12}>
                                    <div className={classes.header}>{UIText.searchForEmail}</div>
                                </Grid>
                                <Grid item xs={12}>
                                    <UniversalTextField
                                        type='email'
                                        label={UIText.typeHere}
                                        onChange={handleChangeTextField}
                                        focus
                                    />
                                </Grid>
                                <Grid item xs={12} alignItems='center'>
                                    <Button
                                        fullWidth
                                        variant='contained'
                                        color="secondary"
                                        onClick={fetchData}
                                        type="submit"
                                        startIcon={<SearchIcon />}>
                                        {UIText.search}
                                    </Button>
                                </Grid>
                                <Grid item xs={12}>
                                    {loading ?
                                        <Grid item container xs={12} alignItems='center' justifyItems='center'>
                                            <CircularProgress style={{ margin: 'auto', marginTop: '30px' }} />
                                        </Grid> :
                                        <Grid item container xs={12} alignItems='center' justifyItems='center'>
                                            <Grid item xs={12} style={{ marginBottom: '65px' }}>
                                                <List dense style={{ width: '100%' }}>
                                                    {options.map((option, index) => {
                                                        return (
                                                            <ListItem button key={index} onClick={(event) => {
                                                                const currentIndex = checked.indexOf(index);
                                                                const newChecked = [...checked];

                                                                if (currentIndex === -1) {
                                                                    newChecked.push(index);
                                                                } else {
                                                                    newChecked.splice(currentIndex, 1);
                                                                }

                                                                setChecked(newChecked);
                                                            }}>
                                                                <ListItemAvatar>
                                                                    <Avatar
                                                                        alt={option.subject || option.Subject}
                                                                        style={{ backgroundColor: Helpers.blendColors(alphabetColor[option.avatarAbb.charAt(0)], alphabetColor[option.avatarAbb?.length > 1 ? option.avatarAbb.charAt(1) : option.avatarAbb.charAt(0)], 0.5) }}
                                                                    >
                                                                        {option.avatarAbb}
                                                                    </Avatar>
                                                                </ListItemAvatar>
                                                                <ListItemText
                                                                    primary={option.subject || option.Subject}
                                                                    secondary={(option?.sender?.emailAddress?.name || option?.Sender?.EmailAddress?.Name) + ' - ' + Helpers.formatDate(option.receivedDateTime || option.ReceivedDateTime, true)} />
                                                                <ListItemSecondaryAction>
                                                                    <Checkbox
                                                                        edge="end"
                                                                        onChange={handleToggle(index)}
                                                                        checked={checked.indexOf(index) !== -1}
                                                                    />
                                                                </ListItemSecondaryAction>
                                                            </ListItem>);
                                                    })}
                                                </List>
                                            </Grid>
                                            {lazyLoad ? <CircularProgress style={{ margin: 'auto', marginTop: '-45px' }} /> : <></>}
                                        </Grid>
                                    }
                                </Grid>
                            </Grid>
                        </form>
                    </Paper>
                </Grid>
                <Grid item xs={12}>
                    <div className={modalClasses.bottomBar} >
                        <Paper style={{ height: '100%' }}>
                            <Toolbar style={{ width: '100%', paddingTop: '10px' }}>
                                <Grid alignItems='center' justifyItems='center' style={{ width: '100%' }}>
                                    <Grid container alignItems='center' style={{ width: '100%' }}>
                                        <Grid item xs={6}>
                                            <Button
                                                fullWidth
                                                variant="contained"
                                                color="primary"
                                                disabled={checked.length == 0}
                                                onClick={(event) => {
                                                    event.preventDefault();
                                                    event.stopPropagation();
                                                    handleChangeEmails();
                                                }}
                                            >
                                                {UIText.add}
                                            </Button>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Button
                                                fullWidth
                                                variant="contained"
                                                color="secondary"
                                                onClick={(event) => {
                                                    event.preventDefault();
                                                    event.stopPropagation();
                                                    setChecked([]);
                                                    props.closeModal();
                                                }}
                                            >
                                                {UIText.cancel}
                                            </Button>
                                        </Grid>
                                    </Grid>
                                </Grid>
                            </Toolbar>
                        </Paper>
                    </div>
                </Grid>
            </Grid>
        </div>
    </Modal>);
}
const alphabetColor = {
    'A': '#D82656',
    'B': '#D826AA',
    'C': '#B826D8',
    'D': '#7A26D8',
    'E': '#3326D8',
    'F': '#2667D8',
    'G': '#26C8D8',
    'H': '#26D8B5',
    'I': '#26D87C',
    'J': '#26D836',
    'K': '#59D826',
    'L': '#AAD826',
    'M': '#D8BD26',
    'N': '#D88726',
    'O': '#D84C26',
    'P': '#AE553D',
    'Q': '#8FAE3D',
    'R': '#A6AD94',
    'S': '#DAE8B6',
    'T': '#B6E8CB',
    'U': '#5BD890',
    'V': '#0A5A2C',
    'W': '#0A365A',
    'X': '#4A0A5A',
    'Y': '#519F93',
    'Z': '#1E4EFF'
}