﻿import * as React from 'react';
import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
import Backdrop from '@material-ui/core/Backdrop';
import Fade from '@material-ui/core/Fade';
import * as UIStrings from '../../UIStrings';
import { useSelector, useDispatch } from "react-redux";
import CheckCircleTwoToneIcon from '@material-ui/icons/CheckCircleTwoTone';
import { green } from '@material-ui/core/colors';
import { AxiosRequest, RequestHeaders } from '../../Helpers/AxiosRequest';
import Button from '@material-ui/core/Button';
import DialogTitle from '@material-ui/core/DialogTitle';
import Dialog from '@material-ui/core/Dialog';
import Typography from '@material-ui/core/Typography';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import { closeModal } from '../../redux/actions';
import { Grid } from '@material-ui/core';
import { useState } from 'react';
import { ReduxState } from '../..';

export default function Modal(props) {
    const language = useSelector((state: ReduxState) => state.settings.language);
    const UIText = UIStrings.getLocaleStrings(language);
    const openModal = useSelector((state: ReduxState)  => state.modal.openModal);
    const dispatch = useDispatch();
    const handleClose = () => {
        try {
            Office.context.ui.closeContainer();
        }
        catch (err) {

        }
        dispatch(closeModal());
    }
    const reqHeader = new RequestHeaders();
    var url = reqHeader.url;
    const linkToForm = (form: string, id: any) => {
        if (id) {
            switch (form) {
                case 'company': url += 'companies/tab_company/' + id.replace('COM', ''); break;
                case 'contact': url += 'contacts/edit/' + id; break;
                case 'corporateMatter': case 'litigationCase': case 'matterNote': url += 'cases/edit/' + id.replace('M', ''); break;
                case 'contract': url += 'modules/contract/contracts/view/' + id; break;
                case 'intellectualProperty': url += 'intellectual_properties/edit/' + id; break;
                case 'task': url += 'tasks/view/' + id; break;
                case 'hearing': url += 'cases/edit/' + id.matterId ; break;
                default: break;
            }
            return (<a href={url} target='_blank'><Button onClick={handleClose} color="primary">
                {UIText.open}
            </Button></a>)
        }
        else {
            return (<></>)
        }
    }
    const getTitle = () => {
        return UIText.success;
    }
    const getMessage = (type, id) => {
        return (
            <Grid>
                <Typography variant='body1'>{UIText[type] + ' ' + UIText.createdSuccessfully + ' - '}</Typography>
                <Typography variant='body1'>{parseId(id)}</Typography>
            </Grid>
        )
    }
    const parseId = (id: any) => {
        switch (props.type) {
            case 'hearing': return id?.hearingId;
            default: return id;
        }
    }
    return (
        <Dialog onClose={handleClose} aria-labelledby="simple-dialog-title" open={openModal == null ? false : openModal}>
            <DialogTitle id="simple-dialog-title">
                <Grid container justifyItems='center'>
                     <CheckCircleTwoToneIcon style={{ color: green[500] }} />
                     <Typography variant='subtitle1'>{getTitle()}</Typography>
                </Grid>
            </DialogTitle>
            <DialogContent>
                    {getMessage(props.type, props.id)}
            </DialogContent>
            <DialogActions>
                <Button onClick={handleClose} color="primary" autoFocus>
                    {UIText.cancel}
                </Button>
                {linkToForm(props.type, props.id)}
            </DialogActions>
        </Dialog>);
}