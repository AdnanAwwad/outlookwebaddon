﻿/* global UIStrings */
/// <reference types="office-js" />
import * as React from 'react';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import Badge from '@material-ui/core/Badge';
import Avatar from '@material-ui/core/Avatar';
import IconButton from '@material-ui/core/IconButton';
import SettingsIcon from '@material-ui/icons/Settings';
import Typography from '@material-ui/core/Typography';
import { red, blue } from '@material-ui/core/colors';
import { Helpers } from '../../Helpers/Helpers';
import MenuItem from '@material-ui/core/MenuItem';
import { useSelector } from "react-redux";
import * as UIStrings from '../../UIStrings';
import { ReduxState } from '../..';
import { AppBar, Toolbar, Menu, fade, Drawer, Grid, Tooltip, Theme, useScrollTrigger, Slide } from '@material-ui/core';
import MenuIcon from '@material-ui/icons/Menu';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';

const StyledBadge = withStyles((theme) => ({
    badge: {
        backgroundColor: '#44b700',
        color: '#44b700',
        boxShadow: `0 0 0 2px ${theme.palette.background.paper}`,
        '&::after': {
            position: 'absolute',
            top: 0,
            left: 0,
            width: '100%',
            height: '100%',
            borderRadius: '50%',
            animation: '$ripple 1.2s infinite ease-in-out',
            border: '1px solid currentColor',
            content: '""',
        },
    },
    '@keyframes ripple': {
        '0%': {
            transform: 'scale(.8)',
            opacity: 1,
        },
        '100%': {
            transform: 'scale(2.4)',
            opacity: 0,
        },
    },
}))(Badge);
const useStyles = makeStyles((theme: Theme) => ({
    root: {
        flexGrow: 1,
    },
    media: {
        height: 0,
        paddingTop: '56.25%', // 16:9
    },
    expand: {
        transform: 'rotate(0deg)',
        marginLeft: 'auto',
        transition: theme.transitions.create('transform', {
            duration: theme.transitions.duration.shortest,
        }),
    },
    expandOpen: {
        transform: 'rotate(180deg)',
    },
    avatar: {
        backgroundColor: red[500],
    },
    formControl: {
        margin: theme.spacing(1),
        minWidth: 120,
    },
    selectEmpty: {
        marginTop: theme.spacing(2),
    },
    icon: {
        width: '20px',
        height: '20px'
    },
    card: {
        height: '50px',
        cursor: 'pointer'
    },
    grow: {
        flexGrow: 1,
        maxHeight: '50px'
    },
    hide: {
        display: 'none'
    },
    menuButton: {
        marginRight: theme.spacing(2),
    },
    title: {
        //display: 'none',
        //[theme.breakpoints.up('sm')]: {
        //    display: 'block',
        //},
    },
    search: {
        position: 'relative',
        borderRadius: theme.shape.borderRadius,
        backgroundColor: fade(theme.palette.common.white, 0.15),
        '&:hover': {
            backgroundColor: fade(theme.palette.common.white, 0.25),
        },
        marginRight: theme.spacing(2),
        marginLeft: 0,
        width: '100%',
        [theme.breakpoints.up('sm')]: {
            marginLeft: theme.spacing(3),
            width: 'auto',
        },
    },
    searchIcon: {
        padding: theme.spacing(0, 2),
        height: '100%',
        position: 'absolute',
        pointerEvents: 'none',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    inputRoot: {
        color: 'inherit',
    },
    inputInput: {
        padding: theme.spacing(1, 1, 1, 0),
        // vertical padding + font size from searchIcon
        paddingLeft: `calc(1em + ${theme.spacing(4)}px)`,
        transition: theme.transitions.create('width'),
        width: '100%',
        [theme.breakpoints.up('md')]: {
            width: '20ch',
        },
    },
    sectionDesktop: {
        display: 'flex',
        [theme.breakpoints.up('md')]: {
            display: 'flex',
        },
    },
    sectionMobile: {
        display: 'flex',
        [theme.breakpoints.up('md')]: {
            display: 'none',
        },
    },
    image: {
        width: '20px',
        height: '20px',
    },
    header: {
        padding: '5px',
        textAlign: 'center',
        background: theme.palette.background.default
    },
    textHeader: {
        fontWeight: 'bold',
    }
}));
interface HideProps {
    /**
     * Injected by the documentation to work in an iframe.
     * You won't need it on your project.
     */
    window?: () => Window;
    children: React.ReactElement;
}
function HideOnScroll(props: HideProps) {
    const { children, window } = props;
    // Note that you normally won't need to set the window ref as useScrollTrigger
    // will default to window.
    // This is only being set here because the demo is in an iframe.
    const trigger = useScrollTrigger({ target: window ? window() : undefined });

    return (
        <Slide appear={false} direction="down" in={!trigger}>
            {children}
        </Slide>
    );
}
const addNewitems = ['company', 'contact', 'hearing', 'task', 'meeting'];
const logAsItems = ['matterNote', 'corporateMatter', 'contract', 'litigationCase', 'intellectualProperty'];
const moreItems = ['settings', 'activities', 'contactSupport'];
export default function CustomAppBar(props) {
    const languageSelected = useSelector((state: ReduxState) => state.settings.language);
    const UIText = UIStrings.getLocaleStrings(languageSelected);
    const classes = useStyles();
    const [profileName, setProfileName] = React.useState(null);
    const [userImagebase64, setUserImagebase64] = React.useState(null);
    const [toggleDrawer, setToggleDrawer] = React.useState(false);
    const [currentPage, setCurrentPage] = React.useState('');
    const userProfileData = async () => {
        if (props.userProfileInfo) {
            setProfileName(props.userProfileInfo.profileName);
        } else {
            let response = await Helpers.getProfileData();
            if (response.status === 200) {
                if (response.data.error === "") {
                    let userProfileInfo = (response.data.success) ? response.data.success.data : null;
                    setProfileName(userProfileInfo.profileName);
                }
            }
        }
        setUserImagebase64(localStorage.getItem("userImagebase64"));
    }
    React.useEffect(() => {
        (async () => {
            var page = window.location.pathname.replace('/', '');
            if (page === 'matter') {
                page = Helpers.getUrlPar('matterType');
            }
            if (page === 'contactsupport')
                page = 'contactSupport';
            setCurrentPage(page);
            await userProfileData();
        })();
    }, []);
    const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
    const [mobileMoreAnchorEl, setMobileMoreAnchorEl] = React.useState<null | HTMLElement>(null);

    const isMenuOpen = Boolean(anchorEl);
    const isMobileMenuOpen = Boolean(mobileMoreAnchorEl);

    const handleProfileMenuOpen = (event: React.MouseEvent<HTMLElement>) => {
        setAnchorEl(event.currentTarget);
    };
    const handleSettingsClicked = (event: React.MouseEvent<HTMLElement>) => {
        window.location.href = '/settings';
    }
    const handleMobileMenuClose = () => {
        setMobileMoreAnchorEl(null);
    };

    const handleMenuClose = () => {
        setAnchorEl(null);
        handleMobileMenuClose();
    };

    const handleMobileMenuOpen = (event: React.MouseEvent<HTMLElement>) => {
        setMobileMoreAnchorEl(event.currentTarget);
    };
    const handleDrawerItemClicked = (event: React.MouseEvent<HTMLElement>, type: string) => {
        var navToPage = type;
        if (type === 'corporateMatter' || type === 'litigationCase') {
            navToPage = 'matter?matterType=' + navToPage;
        }
        window.location.href = navToPage;
    }
    const handleLogout = (event: React.MouseEvent<HTMLElement>) => {
        props.logout();
        Helpers.logout();
        try {
            var settings = Office.context.roamingSettings;
            settings.remove('url');
            settings.remove('username');
            settings.remove('userKey');
            settings.remove('userId');
            settings.remove('userId');
            settings.remove('categoryId');
            settings.remove('email');
            settings.remove('preferences');
            settings.saveAsync((result) => {
                window.location.reload();
            });
        }
        catch (err) {

        }
    };
    const menuId = 'primary-search-account-menu';
    const renderMenu = (
        <Menu
            anchorEl={anchorEl}
            anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
            id={menuId}
            keepMounted
            transformOrigin={{ vertical: 'top', horizontal: 'right' }}
            open={isMenuOpen}
            onClose={handleMenuClose}
        >
            <MenuItem onClick={handleLogout}>{UIText.logout}</MenuItem>
        </Menu>
    );
    const mobileMenuId = 'primary-search-account-menu-mobile';
    const renderMobileMenu = (
        <Menu
            anchorEl={mobileMoreAnchorEl}
            anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
            id={mobileMenuId}
            keepMounted
            transformOrigin={{ vertical: 'top', horizontal: 'right' }}
            open={isMobileMenuOpen}
            onClose={handleMobileMenuClose}
        >
            <MenuItem onClick={handleSettingsClicked}>
                <IconButton aria-label="show 4 new mails" color="inherit">
                    <Badge badgeContent={4} color="secondary">
                        <SettingsIcon />
                    </Badge>
                </IconButton>
                <p>{UIText.settings}</p>
            </MenuItem>
            <MenuItem onClick={handleProfileMenuOpen}>
                <IconButton
                    aria-label="account of current user"
                    aria-controls="primary-search-account-menu"
                    aria-haspopup="true"
                    color="inherit"
                >
                    <StyledBadge
                        overlap="circular"
                        anchorOrigin={{
                            vertical: 'bottom',
                            horizontal: 'right',
                        }}
                        variant="dot"
                    >
                        <Avatar src={userImagebase64} aria-label="recipe">
                        </Avatar>
                    </StyledBadge>
                </IconButton>
                <p>Profile</p>
            </MenuItem>
        </Menu>
    );
    const handleDrawerToggle = (event: React.MouseEvent<HTMLElement>) => {
        event.preventDefault();
        event.stopPropagation();
        setToggleDrawer(!toggleDrawer);
    }
    return (
        <div className={currentPage != 'start' ? classes.grow : classes.hide}>
            <HideOnScroll>
                <AppBar>
                    <Toolbar>
                        <IconButton
                            edge="start"
                            className={classes.menuButton}
                            color="inherit"
                            aria-label="open drawer"
                            onClick={handleDrawerToggle}
                        >
                            <MenuIcon />
                        </IconButton>
                        <Tooltip title={UIText[currentPage]} style={{ width: '60%' }}>
                            <Typography className={classes.title} variant="body2" noWrap>
                                {UIText[currentPage]}
                            </Typography>
                        </Tooltip>
                        <div className={classes.grow} />
                        <div className={classes.sectionDesktop}>
                            {currentPage !== 'settings' ?
                                <Tooltip title={UIText.settings} style={{ width: '60%' }}>
                                    <IconButton
                                        aria-label="open-settings"
                                        color="inherit"
                                        onClick={(e) => {
                                            e.preventDefault();
                                            window.location.href = 'settings';
                                        }}>
                                        <Badge badgeContent={0} color="secondary">
                                            <SettingsIcon />
                                        </Badge>
                                    </IconButton>
                                </Tooltip>
                                : <></>}
                            <IconButton
                                edge="end"
                                aria-label="account of current user"
                                aria-controls={menuId}
                                aria-haspopup="true"
                                onClick={handleProfileMenuOpen}
                                color="inherit"
                            >
                                <StyledBadge
                                    overlap="circular"
                                    anchorOrigin={{
                                        vertical: 'bottom',
                                        horizontal: 'right',
                                    }}
                                    variant="dot"
                                >
                                    <Avatar src={userImagebase64} aria-label="recipe">
                                    </Avatar>
                                </StyledBadge>
                            </IconButton>
                        </div>
                    </Toolbar>
                </AppBar>
            </HideOnScroll>
            {renderMobileMenu}
            {renderMenu}
            <Drawer open={toggleDrawer} onClose={() => setToggleDrawer(false)}>
                <Divider />
                <Grid container alignItems='center' justifyItems='center' className={classes.header} spacing={1} style={{marginTop: '10px'}}>
                    <Grid item>
                        <img alt='add-new-logo' src={require('../../assets/addNewIcon.svg')} className={classes.image} />
                    </Grid>
                    <Grid item>
                        <Typography className={classes.textHeader}>{UIText.addNew}</Typography>
                    </Grid>
                </Grid>
                <List>
                    {addNewitems.map(item => (
                        <div key={item}>
                            <ListItem button onClick={(event) => handleDrawerItemClicked(event, item)} selected={item === currentPage} >
                                <ListItemIcon><img alt={item} src={require('../../assets/' + item + 'Icon.svg')} className={classes.image} /></ListItemIcon>
                                <ListItemText primary={UIText[item]} />
                            </ListItem>
                        </div>
                    ))}
                </List>
                <Divider />
                <Grid container alignItems='center' justifyItems='center' className={classes.header} spacing={1}>
                    <Grid item style={{ marginTop: '-2px' }}>
                        <img alt='log-as-logo' src={require('../../assets/logAsIcon.svg')} className={classes.image} />
                    </Grid>
                    <Grid item style={{ marginTop: '-2px' }}>
                        <Typography className={classes.textHeader}>{UIText.logAs}</Typography>
                    </Grid>
                </Grid>
                <List>
                    {logAsItems.map(item => (
                        <div key={item}>
                            <ListItem button onClick={(event) => handleDrawerItemClicked(event, item)} selected={item === currentPage}>
                                <ListItemIcon><img alt={item} src={require('../../assets/' + item + 'Icon.svg')} className={classes.image} /></ListItemIcon>
                                <ListItemText primary={UIText[item]} />
                            </ListItem>
                        </div>
                    ))}
                </List>
                <Divider />
                <Grid container alignItems='center' justifyItems='center' className={classes.header} spacing={1}>
                    <Grid item style={{ marginTop: '-2px' }}>
                        <img alt='setup-logo' src={require('../../assets/settingsIcon.png')} className={classes.image} />
                    </Grid>
                    <Grid item style={{ marginTop: '-2px' }}>
                        <Typography className={classes.textHeader}>{UIText.settings}</Typography>
                    </Grid>
                </Grid>
                <List>
                    {moreItems.map(item => (
                        <div key={item}>
                            <ListItem button onClick={(event) => handleDrawerItemClicked(event, item)} selected={item === currentPage}>
                                <ListItemIcon><img alt={item} src={require('../../assets/' + item + 'Icon.svg')} className={classes.image} /></ListItemIcon>
                                <ListItemText primary={UIText[item]} />
                            </ListItem>
                        </div>
                    ))}
                </List>
            </Drawer>
        </div>
    );
}
