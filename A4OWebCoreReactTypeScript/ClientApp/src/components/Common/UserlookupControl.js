﻿import React from 'react';
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';
import CircularProgress from '@material-ui/core/CircularProgress';
import { AxiosRequest } from '../../Helpers/AxiosRequest'
import { References } from '../../Helpers/References'

const UserlookupControl = React.forwardRef((props, ref) => {
    const { register, name, multiple, handleChangeAutoComplete, ...rest } = props;
    const isMulti = multiple ? multiple : false;
    const [open, setOpen] = React.useState(false);
    const [options, setOptions] = React.useState([]);
    const loading = open && options.length === 0;

    React.useEffect(() => {
        let active = true;
        if (!loading) {
            return undefined;
        }
        (async () => {
            const response = await AxiosRequest.post(References.usersLookupApi);
            console.log(response);
            const users = response.data.success.data;
            if (active) {
                setOptions(users);
            }
        })();
        return () => {
            active = false;
        };
    }, [loading]);

    React.useEffect(() => {
        if (!open) {
            setOptions([]);
        }
    }, [open]);

    return (
        <Autocomplete
            id="authorizedSignatory"
            name={name} {...rest}
            fullWidth
            ref={ref}
            onChange={(event, value) => {
                handleChangeAutoComplete(ref, value);
            }}
            multiple={isMulti}
            open={open}
            onOpen={() => {
                setOpen(true);
            }}
            onClose={() => {
                setOpen(false);
            }}
            getOptionSelected={(option, value) => option.fistName === value.firstName}
            getOptionLabel={(option) => (option.firstName + " " + option.lastName)}
            options={options}
            loading={loading}
            renderInput={(params) => (
                <TextField
                    {...params}
                    label={props.label}
                    variant="outlined"
                    InputProps={{
                        ...params.InputProps,
                        endAdornment: (
                            <React.Fragment>
                                {loading ? <CircularProgress color="inherit" size={20} /> : null}
                                {params.InputProps.endAdornment}
                            </React.Fragment>
                        ),
                    }}
                />
            )}
        />
    );
});
export default UserlookupControl;