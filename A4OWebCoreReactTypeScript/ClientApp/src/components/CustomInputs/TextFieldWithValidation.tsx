﻿import * as React from 'react';
import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
import Backdrop from '@material-ui/core/Backdrop';
import Fade from '@material-ui/core/Fade';
import * as UIStrings from '../../UIStrings';
import { useSelector, useDispatch } from "react-redux";
import CheckCircleTwoToneIcon from '@material-ui/icons/CheckCircleTwoTone';
import { green } from '@material-ui/core/colors';
import { AxiosRequest, RequestHeaders } from '../../Helpers/AxiosRequest';
import { Helpers } from '../../Helpers/Helpers';
import Button from '@material-ui/core/Button';
import DialogTitle from '@material-ui/core/DialogTitle';
import Dialog from '@material-ui/core/Dialog';
import Typography from '@material-ui/core/Typography';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import { closeModal } from '../../redux/actions';
import { Grid, TextField, InputAdornment, CircularProgress } from '@material-ui/core';
import { useState } from 'react';
import LinearProgress from '@material-ui/core/LinearProgress';
import { References } from '../../Helpers/References';
import { ReduxState } from '../..';
import { UniversalTextField } from '../Common/TextFields';

export default function TextFieldWithValidation(props: any) {
    const language = useSelector((state : ReduxState) => state.settings.language);
    const [fetching, setFetching] = useState(false);
    const [valid, setValid] = useState(true);
    const UIText = UIStrings.getLocaleStrings(language);
    const [value, setValue] = useState(props.value);
    const handleChangeTextField = async (event) => { 
        event.persist();
        var value = event?.target?.value;
        setValue(value);
        setFetching(true);
        switch (props.apiType) {
            case 'company': {
                var dataToPost = 'term=' + value;
                var response = await AxiosRequest.post(References.companyAutoCompleteApi, dataToPost);
                var data = response?.data?.success?.data;
                var itemExist = false;
                if (data != null) {
                    for (var i = 0; i < data.length; i++) {
                        if (data[i][props.type] == event.target.value) {
                            itemExist = true;
                            break;
                        }
                    }
                }
                setValid(!itemExist);                
            } break;
            case 'corporateMatter': {
                var dataToPost = 'term=' + value;
                var response = await AxiosRequest.post(References.casesLookupApi, dataToPost);
                var data = response?.data?.success?.data;
                var itemExist = false;
                if (data != null) {
                    for (var i = 0; i < data.length; i++) {
                        if (data[i]['subject'] == event.target.value) {
                            itemExist = true;
                            break;
                        }
                    }
                }
                setValid(!itemExist);
            } break;
            case 'email': {
                var emails = [];
                emails = value.split(';');
                var isValidMail = false;
                emails.forEach(email => {
                    isValidMail = Helpers.ValidateEmail(email);
                });
                setValid(isValidMail);
            }; break;
            default: break;
        }
        setFetching(false);
        props.handleChangeTextField(event, { type: props.type, valid: !itemExist});
    };
    const noValidText = () => {
        switch (props.validationType) {
            case 'email': {
                return UIText.notValidEmail;
            }
            default: return UIText.alreadyExists;
        }
    }
    const validText = () => {
        switch (props.validationType) {
            case 'email': {
                return UIText.emailinfo;
            }
            default: return '';
        }
    }
    return (
        <Grid container>
            <UniversalTextField                        
                {...props}
                value={value}
                error={!valid || props.error}
                helperText={valid ? validText() : noValidText()}
                onChange={handleChangeTextField}
                type={props.type}
                id={props.id}
            />
            {fetching ? <LinearProgress color='secondary' style={{width: '100%'}}/> : <div/>}
        </Grid>
        )
}