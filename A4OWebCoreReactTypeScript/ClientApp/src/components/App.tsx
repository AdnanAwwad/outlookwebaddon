﻿/*
 * Copyright (c) Microsoft Corporation.
 * Licensed under the MIT license.
 */

/// <reference types="office-js" />
/* global Excel */ //Required for these to be found.  see: https://github.com/OfficeDev/office-js-docs-pr/issues/691

import * as React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import SignIn from './Login/SignIn';
import StartupPage from './Login/StartupPage';
import Settings from './Settings/Settings';
import ContactSupport from './Settings/ContactSupport';
import Company from './AddNew/Company';
import Progress from './Progress';
import logo from '../assets/SetupHeader.png';
import CssBaseline from '@material-ui/core/CssBaseline';
import Contact from './AddNew/Contact';
import withAuthProvider, { AuthComponentProps } from '../AuthProvider';
import { Backdrop, Grid } from '@material-ui/core';
import Matter from './AddNew/Matter';
import Contract from './AddNew/Contract';
import Hearing from './AddNew/Hearing';
import IntellectualProperty from './AddNew/IntellectualProperty';
import Task from './AddNew/Task';
import Meeting from './AddNew/Meeting';
import MatterNote from './AddNew/MatterNote';
import CustomAppBar from './Common/AppBar';
import Activities from './Activities/Activities';
import PrivacyPolicy from './Settings/PrivacyPolicy';
import TermsConditions from './Settings/TermsConditions';
import createLog from 'localstorage-logger';
import Blank from './Login/Blank';
import ItemsMenu from './Settings/ItemsMenu';

export interface AppProps extends AuthComponentProps {
    title: string;
    isOfficeInitialized: boolean;
    isUserLoggedIn: boolean;
    logger;
}

export interface AppState {
    listItems: [];
}

class App extends React.Component<AppProps, AppState> {
    log;
    constructor(props, context) {
        super(props, context);
        this.state = {
            listItems: [],
        };
        this.log = createLog({
            logName: 'appLogs',
            maxLogSizeInBytes: 512 * 1024
        });
    }

    componentDidMount() {
    }
    render() {
        const { title, isOfficeInitialized, isUserLoggedIn, isAuthenticated, initializing } = this.props;
        if (!isOfficeInitialized) {
            return (
                <BrowserRouter>
                    <Switch>
                        <Route exact path='/'>
                            <Progress
                                title={title}
                                logo={logo}
                                message="Please sideload your addin to see app body."
                            />
                        </Route>
                        <Route path='/privacypolicy'>
                            <PrivacyPolicy />
                        </Route>
                        <Route path='/termsconditions'>
                            <TermsConditions />
                        </Route>
                    </Switch>
                </BrowserRouter>
            );
        }
        const from = window.location.pathname.replace('/', '');
        const isSigningIn = (sessionStorage.getItem('isSigningIn') === 'true');
        return (initializing ?
            <React.Fragment>
                <Backdrop open={true}>
                    <img src={require('../assets/loader.gif')} />
                </Backdrop>
            </React.Fragment>
            :
            < React.Fragment >
                <CssBaseline />
                <BrowserRouter>
                    {isUserLoggedIn && isAuthenticated && !isSigningIn ?
                        <div style={{ flexGrow: 1}}>
                            <Grid container alignItems='center' justifyItems='center' direction='column'>
                                <Grid item xs={12}>
                                    <CustomAppBar {...this.props} />
                                </Grid>
                                <Grid item xs={12} style={{ marginTop: '40px', width: '100%' }}>
                                    <Switch>
                                        <Route exact path='/'>
                                            <div>Welcome To the addin</div>
                                        </Route>
                                        <Route path='/settings' render={(props) =>
                                            <Settings {...this.props} logger={this.log} />
                                        } />
                                        <Route path='/addNew' render={(props) =>
                                            <ItemsMenu {...this.props} logger={this.log} />
                                        } />
                                        <Route path='/logAs' render={(props) =>
                                            <ItemsMenu {...this.props} logger={this.log} />
                                        } />
                                        <Route path='/contactsupport' render={(props) =>
                                            <ContactSupport logger={this.log} />
                                        } />
                                        <Route path='/start'>
                                            <StartupPage />
                                        </Route>
                                        <Route path='/company' render={(props) =>
                                            <Company {...this.props} logger={this.log} />
                                        } />
                                        <Route path='/matterNote' render={(props) =>
                                            <MatterNote {...this.props} logger={this.log} />
                                        } />
                                        <Route path='/intellectualProperty' render={(props) =>
                                            <IntellectualProperty {...this.props} logger={this.log} />
                                        } />
                                        <Route path='/task' render={(props) =>
                                            <Task {...this.props} logger={this.log} />
                                        } />
                                        <Route path='/meeting' render={(props) =>
                                            <Meeting {...this.props} logger={this.log} />
                                        } />
                                        <Route path='/hearing' render={(props) =>
                                            <Hearing {...this.props} logger={this.log} />
                                        } />
                                        <Route path='/contract' render={(props) =>
                                            <Contract {...this.props} logger={this.log} />
                                        } />
                                        <Route path='/contact' render={(props) =>
                                            <Contact {...this.props} logger={this.log} />
                                        } />
                                        <Route path='/matter' render={(props) =>
                                            <Matter {...this.props} logger={this.log} />
                                        } />
                                        <Route path='/activities' render={(props) =>
                                            <Activities {...this.props} logger={this.log} />
                                        } />
                                        <Route path='/blank' render={(props) =>
                                            <Blank/>
                                        } />
                                    </Switch>
                                </Grid>
                            </Grid>
                        </div>
                        :
                        <SignIn from={from} {...this.props} logger={this.log} />}
                </BrowserRouter>
            </React.Fragment >
        );
    }
}
export default withAuthProvider(App);
