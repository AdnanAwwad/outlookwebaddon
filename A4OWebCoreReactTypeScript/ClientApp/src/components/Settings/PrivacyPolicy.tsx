﻿import * as React from 'react';
import { Grid, Typography } from '@material-ui/core';

export default class PrivacyPolicy extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <Grid container alignItems='center' justifyItems='center'>
                <Grid item xs={12} style={{ backgroundColor: '#6264A7'}}>
                    <Grid container justifyItems='center'>
                        <Typography variant='h3' style={{color: 'white', margin: 'auto'}}>Privacy Policy</Typography>
                    </Grid>
                </Grid>
                <Grid item xs={12} style={{ padding: '20px' }}>
                    <Typography>
                        <div>
                            <h3>App4Legal for Outlook - A4O App Privacy Policy [The scope here is for the offering app only "App4Legal for Outlook - A4O"]</h3><br></br>
                            <h4>Confidentiality</h4><br></br>
                            <p>
                                App4Legal for Outlook - A4O app doesn’t collect or save any data that the Customer provide within the add-on inside Microsoft Outlook.
                                About the data you enter when registering for a trial or ordering a subscription for App4Legal:
                            When you register for a trial or order a subscription to App4Legal, you enter your name, e-mail address and other personal data.<br></br>
                            This type of data is used to ensure that our system links you with your subscription/trial version.</p><br></br>
                            <h4>Security</h4><br></br>
                            <p>
                                We value security as much as you do and we monitor our performance around the clock and in the case of any problems, we are ready to act upon them immediately. Your data is always private and protected.</p><br></br>
                            <h4>Backups</h4><br></br>
                            <p>As no data is saved from the App4Legal for Outlook - A4O app on our side, we’ll only be sharing some details for the Backups in regard to the App4Legal Core app itself.
                            Every 24 hours we backup your data and later store it in another physical site. This means that with App4Legal, automatic offsite storage is enabled without the need for you to generate and handle physical backups by yourself. Furthermore, if your hardware is ever damaged, all of your data in our system will still be safe – you only need a computer that is connected to the Internet to access it.
                            We value security as much as you do
                            Privacy is an integral part of everything we do at App4Legal. Privacy isn’t an afterthought or corporate rhetoric – it is how we conduct our business every single day. That is why we follow a strict set of guidelines in order to keep all of your private information safe. Of course, this means that we will never sell, rent or share your private information with third parties. At App4Legal, we prioritize the security of your data, and we can prove it.</p><br></br>
                            <h4>Compliance</h4><br></br>
                            <p>We comply with the most up to date IT management and security policies, meaning that our actions adhere to best practices in the industry. App4Legal works always to stay compliant with international data protection regulations.
                            Available when you need it – anytime, anywhere
                            With App4Legal you are guaranteed 99.9% availability, 24 hours a day, 7 days a week. We make this possible by having redundant facilities and servers in place at all times. This ensures that if one location or server somehow becomes unavailable, your App4Legal service experience will be minimally affected or, most often, not at all.</p><br></br>
                            <h4>Support</h4><br></br>
                            <p>Furthermore, we constantly expand our server facilities in order to keep up with customer growth, which makes it possible for our system to always be ready to handle the increasing number of users.
                            Contact Information For any issue related to the Services, Customer may contact App4Legal’s support by sending an email to <a href='mailto:support@App4Legal.com'>support@App4Legal.com</a>.</p><br></br>
                            <h4>Terms and Conditions</h4><br></br>
                            <p>
                                User Data Storage, User Data Retention, Deletion & Security Controls
                                Regarding App4Legal for Outlook - A4O app in specific, no data stored at our side, thus nothing to share for this topic.
                            As for the App4Legal core app itself, please Refer to <a href='/termsconditions'>Terms and Conditions</a></p>
                        </div>
                    </Typography>
                </Grid>
            </Grid>
        );
    }
}