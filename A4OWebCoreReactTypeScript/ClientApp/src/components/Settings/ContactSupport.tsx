﻿import * as React from 'react';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import Button from '@material-ui/core/Button';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import Badge from '@material-ui/core/Badge';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Collapse from '@material-ui/core/Collapse';
import Avatar from '@material-ui/core/Avatar';
import IconButton from '@material-ui/core/IconButton';
import SettingsIcon from '@material-ui/icons/Settings';
import Typography from '@material-ui/core/Typography';
import { red } from '@material-ui/core/colors';
import { Helpers } from '../../Helpers/Helpers';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import { useDispatch, useSelector } from "react-redux";
import { stopLoader, startLoader, changeLanguage, changeTheme } from '../../redux/actions';
import * as UIStrings from '../../UIStrings';
import { ReduxState } from '../..';
import FormSkeleton from '../Common/Skeleton/FormSkeleton';
import { Grid, ButtonGroup, Paper, Menu, Modal, Backdrop } from '@material-ui/core';
import { UniversalAutoComplete } from '../Common/TextFields';
import ContactSupportIcon from '../../assets/customer-service.svg';
import LanguageIcon from '../../assets/translation.svg';
import LicenseIcon from '../../assets/insurance.svg';
import LogFileIcon from '../../assets/log-file.svg';
import DocumentationIcon from '../../assets/folders.svg';
import ThemeIcon from '../../assets/theme.svg';
import { AxiosRequest } from '../../Helpers/AxiosRequest';
import { References } from '../../Helpers/References';

export default function ContactSupport(props) {
    const language = useSelector((state: ReduxState) => state.settings.language);
    const [url, setUrl] = React.useState<string>(null);
    React.useEffect(() => {
        async function getUserData() {
            const userInfoResponse = await AxiosRequest.get(References.userCheckApi);
            try {
                if (userInfoResponse) {
                    const data = userInfoResponse.data?.success?.data;
                    if (data) {
                        var instanceUrl: string = localStorage.getItem('url');
                        var apiKey: string = localStorage.getItem('userKey');
                        if (instanceUrl) {
                            var arrUrl = instanceUrl.split('/');
                            instanceUrl = arrUrl[arrUrl.length - 2];
                        }
                        let jsonObj = {
                            "x-api-key": apiKey
                        };
                        let iframeEl = document.getElementById('myIframeA') as HTMLFrameElement;
                        if (iframeEl != null) {
                            iframeEl.onload = function () {
                                iframeEl.contentWindow.postMessage({ payload: jsonObj }, "*");
                            };
                        }
                        setUrl(`https://app4legaljiraservicedesk.azurewebsites.net/start?fullName=${encodeURIComponent(data.profileName)}&email=${encodeURIComponent(data.username)}&userId=${data.user_id}&instanceId=${instanceUrl}&organizationName=${data.organizationName}`);
                    }
                }
            } catch (err) {
                props.logger.error('ContactSupport: ' + err);
            }
        }
        getUserData();
    }, []);
    return (
        <div style={{ height: '95vh' }}>
                    <iframe
                        id="myIframeA"
                        frameBorder='0'
                        style={{ width: '100%', height: '100%', borderRadius: ' 3.01px', marginTop: '35px' }}
                        src={url} />
        </div>);
}