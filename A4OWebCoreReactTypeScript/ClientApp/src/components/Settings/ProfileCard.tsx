﻿/* global UIStrings */
/// <reference types="office-js" />
import * as React from 'react';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import Button from '@material-ui/core/Button';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import Badge from '@material-ui/core/Badge';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Collapse from '@material-ui/core/Collapse';
import Avatar from '@material-ui/core/Avatar';
import IconButton from '@material-ui/core/IconButton';
import SettingsIcon from '@material-ui/icons/Settings';
import Typography from '@material-ui/core/Typography';
import { red } from '@material-ui/core/colors';
import { Helpers } from '../../Helpers/Helpers';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import { useDispatch, useSelector } from "react-redux";
import { stopLoader, startLoader, changeLanguage, changeTheme } from '../../redux/actions';
import * as UIStrings from '../../UIStrings';
import { ReduxState } from '../..';
import FormSkeleton from '../Common/Skeleton/FormSkeleton';
import { Grid, ButtonGroup, Paper, Menu, Modal, Backdrop, Tooltip, createMuiTheme, ThemeProvider, ListItemSecondaryAction, Checkbox } from '@material-ui/core';
import { UniversalAutoComplete } from '../Common/TextFields';
import ContactSupportIcon from '../../assets/customer-service.svg';
import LanguageIcon from '../../assets/translation.svg';
import LicenseIcon from '../../assets/insurance.svg';
import LogFileIcon from '../../assets/log-file.svg';
import EmailIcon from '../../assets/email.svg';
import OutlookIcon from '../../assets/outlook.svg';
import GmailIcon from '../../assets/gmail.svg';
import OfficeIcon from '../../assets/office.svg';
import OnPremisesIcon from '../../assets/database.svg';
import DocumentationIcon from '../../assets/folders.svg';
import ThemeIcon from '../../assets/theme.svg';
import PreferencesIcon from '../../assets/tools.svg';
import { AxiosRequest } from '../../Helpers/AxiosRequest';
import { References } from '../../Helpers/References';
import WindowsIconSvg from '../Login/LoginIcons/windows.svg';
import List from '@material-ui/core/List';
import ListItem, { ListItemProps } from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
import NavigateNextIcon from '@material-ui/icons/NavigateNext';
import NavigateBeforeIcon from '@material-ui/icons/NavigateBefore';
import { getUserDetails } from '../../GraphService';
var FileSaver = require('file-saver');
const StyledBadge = withStyles((theme) => ({
    badge: {
        backgroundColor: '#44b700',
        color: '#44b700',
        boxShadow: `0 0 0 2px ${theme.palette.background.paper}`,
        '&::after': {
            position: 'absolute',
            top: 0,
            left: 0,
            width: '100%',
            height: '100%',
            borderRadius: '50%',
            animation: '$ripple 1.2s infinite ease-in-out',
            border: '1px solid currentColor',
            content: '""',
        },
    },
    '@keyframes ripple': {
        '0%': {
            transform: 'scale(.8)',
            opacity: 1,
        },
        '100%': {
            transform: 'scale(2.4)',
            opacity: 0,
        },
    },
}))(Badge);
const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
        borderTop: '3px solid ' + theme.palette.primary['main']

    },
    media: {
        height: 0,
        paddingTop: '56.25%', // 16:9
    },
    expand: {
        transform: 'rotate(0deg)',
        marginLeft: 'auto',
        transition: theme.transitions.create('transform', {
            duration: theme.transitions.duration.shortest,
        }),
    },
    expandOpen: {
        transform: 'rotate(180deg)',
    },
    avatar: {
        backgroundColor: red[500],
    },
    formControl: {
        margin: theme.spacing(1),
        minWidth: 120,
    },
    selectEmpty: {
        marginTop: theme.spacing(2),
    },
    icon: {
        width: '20px',
        height: '20px'
    },
    card: {
        height: '50px',
        cursor: 'pointer',
    },
    paper: {
        padding: '5px',
        "&:hover": {
            backgroundColor: 'red',
        },
    },
    nested: {
        paddingLeft: theme.spacing(4)
    }
}));
interface ISettingsProps {
    status: boolean,
    message: string,
    productVersion: string,
    appVersion: string
}
interface IUserInfoProps {
    name: string,
    mail: string,
    type: string    
}
export default function ProfileCard(props) {
    const dispatch = useDispatch();
    const languageSelected = useSelector((state: ReduxState) => state.settings.language);
    const UIText = UIStrings.getLocaleStrings(languageSelected);
    const isLoading = useSelector((state: ReduxState) => state.loader.runLoader);
    const classes = useStyles();
    const [profileName, setProfileName] = React.useState(null);
    const [settings, setSettings] = React.useState<ISettingsProps>({ status: true, message: '', productVersion: '', appVersion: '1.1' });
    const [instanceUrl, setInstanceUrl] = React.useState(null);
    const [userImagebase64, setUserImagebase64] = React.useState(null);
    const [expanded, setExpanded] = React.useState(true);
    const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
    const [userInfo, setUserInfo] = React.useState<IUserInfoProps>(null);
    const [token, setToken] = React.useState(null);
    const [checked, setChecked] = React.useState([]);
    const [savingChecked, setSavingChecked] = React.useState([]);
    const logs = props.logger.exportToArray();
    const userProfileData = async () => {
        if (props.userProfileInfo) {
            setProfileName(props.userProfileInfo.profileName);
        } else {
            let response = await Helpers.getProfileData();
            if (response.status === 200) {
                if (response.data.error === "") {
                    let userProfileInfo = (response.data.success) ? response.data.success.data : null;
                    setProfileName(userProfileInfo.profileName);
                }
            }
        }
        setInstanceUrl(localStorage.getItem("url"));
        setUserImagebase64(localStorage.getItem("userImagebase64"));
    }
    React.useEffect(() => {
        (async () => {
            dispatch(startLoader());
            await userProfileData();
            await checkOutlookAddinLicence();
            await getProductVersion();
            await getUserInfo();
            const graphToken = await props.getAccessToken();
            setToken(graphToken);
            try {
                const preferencesSettings = Office.context.roamingSettings.get('preferences');
                const initialChecked = [];
                if (preferencesSettings) {
                    Object.keys(preferencesSettings).forEach(key => {
                        if (preferences.includes(key)) {
                            const value = preferencesSettings[key];
                            if (value === 'true') {
                                const index = preferences.indexOf(key);
                                initialChecked.push(index);
                            }
                        }
                    });
                    setChecked(initialChecked);
                }
            }
            catch (err) {
                
            }
            dispatch(stopLoader());
        })();
    }, []);
    const getUserInfo = async () => {
        try {
            const userProfile = Office?.context?.mailbox?.userProfile;
            const token = await props.getAccessToken();
            if (token) {
                const response = await getUserDetails(token);
                if (response) {
                    setUserInfo({
                        name: response.displayName,
                        mail: response.mail || response.userPrincipalName,
                        type: userProfile?.accountType
                    });
                }
            }
            else {
                setUserInfo({
                    name: userProfile?.displayName,
                    mail: userProfile?.emailAddress,
                    type: userProfile?.accountType
                });
            }
        }
        catch (err) {

        }
    }
    const getProductVersion = async () => {
        var response = await AxiosRequest.get(References.productVersion);
        try {
            var resp = response.data;
            var error = resp?.error;
            if (error == '') {
                var data = resp.success?.data;
                setSettings({ ...settings, productVersion: data.productVersion });
            }
        }
        catch (err) {
            console.log(err);
        }
    };
    const handleExpandClick = () => {
        setExpanded(!expanded);
    };
    const handleSignInMicrosoft = (event) => {
        event.preventDefault();
        event.stopPropagation();
        if (!token) {
            props.login();
        }
    }
    const handleLogout = () => {
        props.logout();
        Helpers.logout();
        try {
            var settings = Office.context.roamingSettings;
            settings.remove('url');
            settings.remove('username');
            settings.remove('userKey');
            settings.remove('userId');
            settings.remove('userId');
            settings.remove('categoryId');
            settings.remove('email');
            settings.remove('preferences');
            settings.saveAsync((result) => {
                window.location.reload();
            });
        }
        catch (err) {

        }
    };
    const checkOutlookAddinLicence = async () => {
        let checkAddonLicenseResponse = await Helpers.checkOutlookAddinLicense();
        if (checkAddonLicenseResponse.status === 200) {
            if (checkAddonLicenseResponse.data.error === "") {
                let licenseStatus = checkAddonLicenseResponse.data.status; //true 
                if (licenseStatus === 'valid') {
                    setSettings({ ...settings, status: true, message: checkAddonLicenseResponse.data.message });
                } else {
                    setSettings({ ...settings, status: false, message: checkAddonLicenseResponse.data.message });
                }
            } else {
                // license error
            }
        } else {
            setSettings({ ...settings, status: false, message: "Network Error" });
        }
    }
    const handleLanguageChange = (event, type) => {
        event.preventDefault();
        dispatch(changeLanguage(type));
        setAnchorEl(null);
    };
    const languages = [
        {
            id: 1,
            value: 'English',
            type: 'en-US'
        },
        {
            id: 2,
            value: 'French',
            type: 'fr-fr'
        },
        {
            id: 3,
            value: 'Arabic',
            type: 'ar-AE'
        },
        {
            id: 4,
            value: 'Spanish',
            type: 'sp-sp'
        }
    ];
    const themes = [
        {
            id: 0,
            value: 'light'
        },
        {
            id: 1,
            value: 'dark'
        }
    ];
    const preferences = ['enableHeaderTimeStamp'];
    const isMenuOpen = Boolean(anchorEl?.id === 'menu');
    const isThemeOpen = Boolean(anchorEl?.id === 'theme');
    const isUserInfoOpen = Boolean(anchorEl?.id === 'userInfo');
    const isPreferencesOpen = Boolean(anchorEl?.id === 'preferences');
    const themeSelected = localStorage.getItem('appTheme');
    const handlePreferencesToggle = (value: number) => () => {
        const currentIndex = checked.indexOf(value);
        const newChecked = [...checked];
        const newCheckedSaving = [...savingChecked];
        if (currentIndex === -1) {
            newChecked.push(value);
        } else {
            newChecked.splice(currentIndex, 1);
        }
        newCheckedSaving.push(value);
        setSavingChecked(newCheckedSaving);
        switch (value) {
            case 0: {
                const settings = Office.context.roamingSettings;
                const preferences = settings.get('preferences');
                if (preferences) {
                    const newValue = { ...preferences, enableHeaderTimeStamp: currentIndex === -1 ? 'true' : 'false' }
                    settings.set('preferences', newValue);                    
                }
                else {
                    settings.set('preferences', {enableHeaderTimeStamp: currentIndex === -1 ? 'true' : 'false' });
                }
                settings.saveAsync(response => {
                    const currentIndexSaving = newCheckedSaving.indexOf(value);
                    newCheckedSaving.splice(currentIndexSaving, 1);
                    setSavingChecked(newCheckedSaving);
                    setChecked(newChecked);
                });
            } break;
            default: break;
        }
    };
    return (<div>
        {isLoading ? <FormSkeleton input='settings' /> : <div style={{ flexGrow: 1 }}>
            <Card className={classes.root}>
                <CardHeader
                    avatar={
                        <StyledBadge
                            overlap="circular"
                            anchorOrigin={{
                                vertical: 'bottom',
                                horizontal: 'right',
                            }}
                            variant="dot"
                        >
                            <Avatar src={userImagebase64} aria-label="recipe">
                            </Avatar>
                        </StyledBadge>
                    }
                    title={profileName}
                    action={
                        <Tooltip
                            title={UIText.logout}
                        >
                            <IconButton aria-label="logout" onClick={handleLogout}>
                                <ExitToAppIcon />
                            </IconButton>
                        </Tooltip>
                    }
                    subheader={""}
                />
                <CardContent>
                    <Typography variant="body2" color="textSecondary" component="p">
                        {instanceUrl}
                    </Typography>
                </CardContent>
                {token ? <></> :
                    <CardActions >
                        <ThemeProvider theme={loginADtheme}>
                            <Button variant="contained" color="primary" onClick={handleSignInMicrosoft} style={{ margin: 'auto' }} startIcon={<img src={WindowsIconSvg} />}>{UIText.signinMicrosoftOnly}</Button>
                        </ThemeProvider>
                    </CardActions>
                }
                <CardContent>
                    <div style={{ flexGrow: 1 }}>
                        <List component="nav" aria-label="settings">
                            <ListItem id='preferences' button onClick={(event) => setAnchorEl(isPreferencesOpen ? null : event.currentTarget)}>
                                <ListItemIcon>
                                    <img src={PreferencesIcon} className={classes.icon} />
                                </ListItemIcon>
                                <ListItemText primary={UIText.preferences} />
                                {isPreferencesOpen ? <ExpandLess /> : <ExpandMore />}
                            </ListItem>
                            <Collapse in={isPreferencesOpen} timeout="auto" unmountOnExit>
                                <List component="div" disablePadding>
                                    {preferences.map((value, index) => {
                                        const labelId = `checkbox-list-label-${index}`;

                                        return (
                                            <ListItem key={index} role={undefined} dense button disabled={savingChecked.indexOf(index) !== -1} onClick={handlePreferencesToggle(index)}>
                                                <ListItemIcon>
                                                    <Checkbox
                                                        edge="start"
                                                        checked={checked.indexOf(index) !== -1}
                                                        disabled={savingChecked.indexOf(index) !== -1}
                                                        tabIndex={-1}
                                                        disableRipple
                                                        inputProps={{ 'aria-labelledby': labelId }}
                                                    />
                                                </ListItemIcon>
                                                <ListItemText id={labelId} primary={UIText[value]} />
                                            </ListItem>
                                        );
                                    })}
                                </List>
                            </Collapse>
                            <ListItem id='menu' button onClick={(event) => setAnchorEl(isMenuOpen ? null : event.currentTarget)}>
                                <ListItemIcon>
                                    <img src={LanguageIcon} className={classes.icon} />
                                </ListItemIcon>
                                <ListItemText primary={UIText.language + ' (' + languages.filter(language => language.type === languageSelected)[0].value + ')'} />
                                {isMenuOpen ? <ExpandLess /> : <ExpandMore />}
                            </ListItem>
                            <Collapse in={isMenuOpen} timeout="auto" unmountOnExit>
                                <List component="div" disablePadding>
                                    {languages.map(language => {
                                        return (<ListItem button
                                            selected={languageSelected === language.type}
                                            key={language.id}
                                            className={classes.nested}
                                            onClick={(event) => handleLanguageChange(event, language.type)}>
                                            <ListItemText primary={language.value} /></ListItem>)
                                    })}
                                </List>
                            </Collapse>
                            <ListItem id='theme' button onClick={(event) => setAnchorEl(isThemeOpen ? null : event.currentTarget)}>
                                <ListItemIcon>
                                    <img src={ThemeIcon} className={classes.icon} />
                                </ListItemIcon>
                                <ListItemText primary={UIText.theme} />
                                {isThemeOpen ? <ExpandLess /> : <ExpandMore />}
                            </ListItem>
                            <Collapse in={isThemeOpen} timeout="auto" unmountOnExit>
                                <List component="div" disablePadding>
                                    {themes.map(theme => {
                                        return (<ListItem button
                                            className={classes.nested}
                                            selected={themeSelected === theme.value}
                                            key={theme.id}
                                            onClick={(event) => {
                                                event.preventDefault(); dispatch(changeTheme(theme.value)); setAnchorEl(null);
                                            }}>
                                            <ListItemText primary={UIText[theme.value]} /></ListItem>)
                                    })}
                                </List>
                            </Collapse>
                            <ListItem button onClick={(event) => {
                                event.preventDefault();
                                window.location.href = '/contactsupport';
                            }}>
                                <ListItemIcon>
                                    <img src={ContactSupportIcon} className={classes.icon} />
                                </ListItemIcon>
                                <ListItemText primary={UIText.contactSupport} />
                                {languageSelected === 'ar-AE' ? <NavigateBeforeIcon /> : <NavigateNextIcon />}
                            </ListItem>
                            <ListItem button onClick={(event) => {
                                event.preventDefault();
                                window.open('https://documentation.app4legal.com/x/IICaB');
                            }}>
                                <ListItemIcon>
                                    <img src={DocumentationIcon} className={classes.icon} />
                                </ListItemIcon>
                                <ListItemText primary={UIText.documentation} />
                                {languageSelected === 'ar-AE' ? <NavigateBeforeIcon /> : <NavigateNextIcon />}
                            </ListItem>
                            <ListItem button onClick={(event) => {
                                event.preventDefault();
                                if (logs?.length > 0) {
                                    var logText = "";
                                    logs.map(log => {
                                        logText += log + '\r\n';
                                    });
                                    var blob = new Blob([logText], { type: 'text/plain;charset=utf-8' });
                                    FileSaver.saveAs(blob, 'app4legal-for-outlook-logs.txt');
                                }
                            }}>
                                <ListItemIcon>
                                    <img src={LogFileIcon} className={classes.icon} />
                                </ListItemIcon>
                                <ListItemText primary={UIText.getLogs} />
                                {languageSelected === 'ar-AE' ? <NavigateBeforeIcon /> : <NavigateNextIcon />}
                            </ListItem>  
                            <ListItem>
                                <ListItemIcon>
                                    <img src={LicenseIcon} className={classes.icon} />
                                </ListItemIcon>
                                <ListItemText primary={UIText.license}/>
                                <ListItemSecondaryAction>
                                    <Typography style={{ color: settings.status ? 'green' : 'red' }}>{settings.status ? UIText.valid : UIText.expired}</Typography>
                                </ListItemSecondaryAction>
                            </ListItem>
                            <ListItem id='userInfo' button onClick={(event) => setAnchorEl(isUserInfoOpen ? null : event.currentTarget)}>
                                <ListItemIcon>
                                    <img src={EmailIcon} className={classes.icon} />
                                </ListItemIcon>                                
                                <ListItemText primary={UIText.userInfo} />
                                {isUserInfoOpen ? <ExpandLess /> : <ExpandMore />}
                            </ListItem>
                            <Collapse in={isUserInfoOpen} timeout="auto" unmountOnExit>
                                <List component="div" disablePadding>
                                    <ListItem
                                        className={classes.nested}>
                                        <ListItemText primary={UIText.name}
                                            secondary={
                                                <React.Fragment>
                                                    <Tooltip title={userInfo?.name}>
                                                        <Typography
                                                        >
                                                            {userInfo?.name}
                                                        </Typography>
                                                    </Tooltip>
                                                </React.Fragment>
                                            } />
                                    </ListItem>
                                    <ListItem
                                        className={classes.nested}>
                                        <ListItemText primary={UIText.email}
                                            secondary={
                                                <React.Fragment>
                                                    <Tooltip title={userInfo?.mail}>
                                                        <Typography
                                                        >      
                                                            {userInfo?.mail}
                                                    </Typography>
                                                    </Tooltip>
                                                </React.Fragment>
                                                } />
                                    </ListItem>
                                    <ListItem
                                        className={classes.nested}>
                                        <ListItemText primary={UIText.mailType} />
                                        <ListItemSecondaryAction>
                                            <Tooltip
                                                title={userInfo?.type === 'gmail' ? 'Gmail' : (userInfo?.type === 'office365' ? 'Office 365' : (userInfo?.type === 'outlookCom' ? 'Outlook' : 'On-Premises'))}
                                            >
                                                <img src={userInfo?.type === 'gmail' ? GmailIcon : (userInfo?.type === 'office365' ? OfficeIcon : (userInfo?.type === 'outlookCom' ? OutlookIcon : OnPremisesIcon))} className={classes.icon} />
                                            </Tooltip>
                                        </ListItemSecondaryAction>
                                    </ListItem>
                                </List>
                            </Collapse>
                        </List>
                        <Grid container spacing={3} direction='column'>
                            <Grid item xs={12}>
                                {settings.status === false ? <Typography color="error">
                                    {settings.message}
                                </Typography> : null}
                            </Grid>
                        </Grid>
                    </div>
                </CardContent>
                <CardActions>
                    <Grid container alignItems='center' justifyItems='space-between'>
                        <Grid item xs>
                            <Typography>{UIText.base_company + ' ' + UIText.version + ' ' + settings.productVersion}</Typography>
                        </Grid>
                        <Grid item xs>
                            <Typography style={{ float: languageSelected.includes('ar') ? 'left' : 'right' }}>{'A4O ' + UIText.version + ' ' + settings.appVersion}</Typography>
                        </Grid>
                    </Grid>
                </CardActions>

            </Card>
        </div>
        }
    </div>);
}
const loginADtheme = createMuiTheme({
    components: {
        MuiButton: {
            styleOverrides: {
                root: {
                    fontSize: "11px",
                    fontWeight: 'normal',
                    fontStretch: 'normal',
                    fontStyle: 'normal',
                    "&:hover": {
                        backgroundColor: '#106ebe !important',
                    },
                    height: "33px",
                    width: '190px',
                    borderRadius: '20px',
                    paddingLeft: "20px",
                    paddingRight: "20px",
                    textTransform: 'none'
                }
            }
        },
    },
    palette: {
        primary: {
            light: '#757ce8',
            main: 'rgb(0, 164, 239)',
            dark: '#002884',
            contrastText: '#fff',
        },
    },
});