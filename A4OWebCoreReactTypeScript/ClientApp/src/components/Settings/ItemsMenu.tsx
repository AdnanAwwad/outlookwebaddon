﻿/* global UIStrings */
/// <reference types="office-js" />
import * as React from 'react';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import { red } from '@material-ui/core/colors';
import { Helpers } from '../../Helpers/Helpers';
import { useDispatch, useSelector } from "react-redux";
import ListItem, { ListItemProps } from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Divider from '@material-ui/core/Divider';
import * as UIStrings from '../../UIStrings';
import { ReduxState } from '../..';
import { Grid, List, Typography } from '@material-ui/core';
const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
        borderTop: '3px solid ' + theme.palette.primary['main']

    },
    image: {
        width: '20px',
        height: '20px',
    }
}));

export default function ItemsMenu(props) {
    const languageSelected = useSelector((state: ReduxState) => state.settings.language);
    const UIText = UIStrings.getLocaleStrings(languageSelected);
    const classes = useStyles();
    const [items, setItems] = React.useState<string[]>(null);
    const addNewItems = ['company', 'contact', 'hearing', 'task', 'meeting'];
    const logAsItems = ['matterNote', 'corporateMatter', 'contract', 'litigationCase', 'intellectualProperty'];
    React.useEffect(() => {
        const urlParam = window.location.pathname;        
        if (urlParam === '/addNew') {
            setItems(addNewItems);
        }
        else {
            setItems(logAsItems);
        }
    }, []);
    
    return (<div style={{
        flexGrow: 1, width: '100%', marginTop: '60px'
    }}>
        <Grid container alignItems='center' justifyItems='center' direction='column'>
            <Grid item xs={12}>
                <List component="nav" aria-label="items list" style={{width: '100%'}}>
                    {items?.map(item => {
                        return (
                            <ListItem style={{ width: '100%' }} button onClick={(event) => {
                                var navToPage = item;
                                if (item === 'corporateMatter' || item === 'litigationCase') {
                                    navToPage = 'matter?matterType=' + navToPage;
                                };
                                window.location.href = navToPage;
                            }}>
                                <ListItemText primary={UIText[item]} />
                            </ListItem>);
                    })}
                </List>
            </Grid>
        </Grid>
    </div>);
}