﻿/* global Office,UIStrings */
import * as React from 'react';
import ProfileCard from './ProfileCard';
import logo from '../../assets/SetupHeader.png'
import { withStyles, WithStyles, createStyles, Theme } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import Grid from "@material-ui/core/Grid";
import { withRouter } from "react-router-dom";
import { connect } from 'react-redux';
import { AppProps } from "../App";

const useStyles = (theme: Theme) => createStyles({
    paper: {
        marginTop: theme.spacing(2),
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
    },
    avatar: {
        margin: 'auto'
    },
    form: {
        width: "100%", // Fix IE 11 issue.
        marginTop: theme.spacing(1),
        paddingRight: "5px"
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
    menu: {
        zIndex: 9999,
    },
});
interface ISettingsState {
}
interface ISettingsProps extends WithStyles<typeof useStyles>, AppProps {

}
class Settings extends React.Component<ISettingsProps, ISettingsState> {
    constructor(props, context) {
        super(props, context);
    };
    goBackToLogin = () => {
        window.location.href = '/start';
    }
    render() {
        const { classes } = this.props;
        return (
            <div style={{ marginTop: '30px' }}>
                <Grid container alignItems="center" justifyItems='center'>
                    <Grid item container alignItems="center" justifyItems='center' xs={12} style={{ width: '100%' }}>
                        <img src={logo} alt="Company-Logo" className={classes.avatar} />
                    </Grid>
                    <Grid item xs={12} style={{ width: '100%' }}>
                        <ProfileCard {...this.props}/>
                    </Grid>
                </Grid>
            </div>
        );
    }
};

const AppContainer = connect(
    null,
    null
)(Settings);
export default withStyles(useStyles, { withTheme: true })(AppContainer);
