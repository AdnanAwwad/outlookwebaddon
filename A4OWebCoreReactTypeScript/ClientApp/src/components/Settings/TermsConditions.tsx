﻿import * as React from 'react';
import { Grid, Typography } from '@material-ui/core';

export default class TermsConditions extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <Grid container alignItems='center' justifyItems='center'>
                <Grid item xs={12} style={{ backgroundColor: '#6264A7' }}>
                    <Grid container justifyItems='center'>
                        <Typography variant='h3' style={{ color: 'white', margin: 'auto' }}>Terms & Conditions</Typography>
                    </Grid>
                </Grid>
                <Grid item container xs={12} style={{ padding: '20px' }}>
                    <Typography>
                        <div>
                            <h3>TERMS AND CONDITIONS</h3><br></br>
                            <h4>Last updated: 28 January 2021</h4><br></br>
                            <p>
                                The customer (the “Customer”) has requested access to the legal practice management software, App4Legal, or its additional modules and add-ons (collectively the “Program”). the Customer, and each of its users, agree that the Program is subject to the terms of the following subscription agreement (“Agreement”).</p><br></br>
                            <p>
                                This agreement is entered between App4Legal DWC-LLC, having its registration number 8592 with main office at Dubai World Central, P.O. Box 390667, Dubai, United Arab Emirates, hereinafter referred to as “We” or “App4Legal” and the payer or/and the recipient of services hereunder identified as part of the subscription process for App4Legal Services,  hereinafter referred to as “You” or “Customer”.</p><br></br>
                            <p>
                                By accepting these terms and conditions, the Customer represents and warrants that the Customer is major and thus legally capable to enter into contract and in case Customer is acting on behalf of some business entity, Customer is duly authorized to enter into agreement on behalf of the entity the Customer is representing. The Customer also represents and warrants that the Customer is not a competitor of App4Legal.</p><br></br>
                            <p>
                                Therefore, by signing-up, ordering, and/or using App4Legal’s services, the Customer agrees to be bound by all of the terms and conditions of this agreement (hereinafter referred to as the Agreement).</p><br></br>
                            <p>
                                Now whereas the Customer and App4Legal both hereby agree to the terms & conditions hereinafter mentioned:</p><br></br>
                            <p>
                                1- SCOPE OF SERVICE</p><br></br>
                            <p>
                                App4Legal offers legal practice management software and services (the Services) which includes the following:</p><br></br>
                            <p>
                                The Program and hosting services at the domain managed by App4Legal.</p><br></br>
                            <p>
                                The functional and technical support for the Program.</p><br></br>
                            <p>
                                For any issue related to the Services, Customer may contact App4Legal’s support by sending an email to <a href='mailto:support@App4Legal.com'>support@App4Legal.com</a> or by raising a support ticket from App4Legal Service Desk accessible from the Program used by the Customer. Only the Customer or the Customer’s authorized user may contact App4Legal’s support teams.</p><br></br>
                            <p>
                                2- FREE TRIAL</p><br></br>
                            <p>
                                If the Customer registers for a free trial of the Services, App4Legal will make the Services available on a trial basis and free of charge to the Customer until the earlier of (a) the end of the free trial period or (b) the start date of Customer’s subscription. If App4Legal includes additional terms and conditions on the trial registration Web page, those will apply as well. During the free trial period:</p><br></br>
                            <p>
                                (i) the Services are provided “as is” and without a warranty of any kind</p><br></br>
                            <p>
                                (ii) App4Legal may suspend, limit, or terminate the Services for any reason at any time without notice and</p><br></br>
                            <p>
                                (iii) App4Legal will not be liable towards the Customer for damages of any kind related to the Customer’s use of the Services. Unless the Customer subscribes to the Services before the end of the free trial, all of Customer’s data on the Service will be permanently deleted at the end of the trial and App4Legal may not be able to recover it.</p><br></br>
                            <p>
                                3- LAWFUL USE OF THE SERVICES</p><br></br>
                            <p>
                                The Customer hereby agrees to use the Services only in an authorized manner as per the terms of this Agreement. In case it is found that the Customer’s use of Services violates the terms of this Agreement or any other law, rule or regulation enacted by the concerned authorities from time to time, App4Legal reserves the right to terminate the Agreement with immediate effect.</p><br></br>
                            <p>
                                4- ADDITIONAL USERS POLICY</p><br></br>
                            <p>
                                Each user requires a unique user ID and password to use the Program. Customer’s users are allowed to choose each their own credentials as long as their user IDs are not already in use, aren’t inappropriate or offensive and don’t infringe upon anyone else’s rights. App4Legal doesn’t authorize sharing user IDs. Passwords and credentials are encrypted and shall respect a strong format set at the pages of signup and login to the Program.</p><br></br>
                            <p>
                                5- ACCOUNT ADMINISTRATOR DISCLAIMER</p><br></br>
                            <p>
                                It is truly important that Customer closely monitors the status and identity of each user on its account – particularly users to whom Customer assigns administrative access. Each user has certain abilities and access rights provided by the Program, and App4Legal assumes no responsibility for acts inconsistent with the guidelines below. Generally, the party who initially activated the account (“Account Owner”) from Customer has the authority, during the period for which the party has paid for access to the account, to: (a) cancel the account (b) add, edit, and delete users (including the ability to grant or deny access to “administrative access” and grant or deny access to other functionalities in the Program ); and (c) access any and all data in the account, including the authority to contact App4Legal’s support.</p><br></br>
                            <p>
                                6- App4Legal’S RESPONSIBILITIES, REPRESENTATIONS, WARRANTIES AND LIABILITIES</p><br></br>
                            <p>
                                6.1 In the performance of Services, App4Legal agrees to:</p><br></br>
                            <ol>
                                <li>Perform the Services to the best of its ability and with the degree of care, diligence and skill that a reasonably prudent person would exercise in comparable circumstances;</li>
                                <li>Liaise with the Customer through the Customer’s coordinator on matters related to the use of, and the identification and resolution of errors in the Service;</li>
                                <li>Invoice Customer according to the terms of this Agreement for the Services performed; and,</li>
                                <li>Proceed according to Customer’s instructions for deleting of Customer’s data and supplies on the termination of the Agreement.</li>
                            </ol>
                            <p>
                                6.2 Reasonable attempts to correct errors on notice:</p><br></br>
                            <p>
                                App4Legal warrants that it will, at its expense, make needed efforts, to correct any errors for which App4Legal is directly and solely responsible, provided that the data necessary to correct such errors is available to App4Legal; or at App4Legal’s discretion. It will also provide service credit to the Customer equivalent to the charge that would have been applicable for correcting the portion of the Service that is an error; such service credit will be only for errors solely due to malfunctioning of the Program or any error made by App4Legal’s personnel during the performance of the Services. For App4Legal to correct the errors or obtain the service credit, the Customer must notify App4Legal in writing of such errors within 30 days of receipt of the Services believed to contain the errors.</p><br></br>
                            <p>
                                6.3 App4Legal disclaims all liability and shall not be liable in contract, tort (including negligence), statutory duty, pre-contract or otherwise arising out of or in connection with these Terms and Conditions or the Program for any (a) consequential, indirect or special loss or damage; or (b) any loss of goodwill, reputation or data; or (c) any economic losses (including loss of revenues, profits, contracts, business or anticipated savings). In each case whether advised of the possibility of such loss or damage and howsoever incurred.</p><br></br>
                            <p>
                                6.4 App4Legal is not liable for third party solutions which are available via and/or integrated with the Program. Consequently, App4Legal cannot be held liable for the correctness, completeness, quality and reliability of the information or for the results which are achieved by means of such third party solutions. Moreover, App4Legal cannot be held liable for the availability, security or functionality of such third party solutions, including for any damage and/or loss caused by such third party solutions. The Customer is responsible for proving that a loss or damage suffered by the Customer is not attributable to any third party solutions.</p><br></br>
                            <p>
                                6.5 The maximum liability of App4Legal in contract, tort (including negligence), statutory duty, or otherwise arising out of or in connection with the Terms and Conditions or the Program; shall, in respect of any one or more events or series of events (whether connected or unconnected) taking place within any twelve month period, be limited to the subscription fees paid by Customer in such period.</p><br></br>
                            <p>
                                7- CUSTOMER’S RESPONSIBILITIES, REPRESENTATIONS AND WARRANTIES</p><br></br>
                            <p>
                                7.1 Customer agrees to:</p><br></br>
                            <ol>
                                <li>Provide all necessary information and any special forms or other required materials or information to App4Legal on schedule or in a timely fashion to enable App4Legal to provide the Services;</li>
                                <li>Ensure accuracy, legibility and completeness of all data supplied to App4Legal and be solely responsible for the results obtained from Customer’s use of any of the Services;</li>
                                <li>Liaise with App4Legal through a coordinator the Customer will identify, on matters related to the Services, and authorize that coordinator to make decisions on behalf of Customer in relation to the implementation of this Agreement and the Services and any changes thereto;</li>
                                <li>Control, and be responsible for the use of account information, user ids related to the Services, where required</li>
                            </ol>
                            <p>
                                7.2 Customer Representations: Customer represents and warrants to App4Legal that:</p><br></br>
                            <ol>
                                <li>the information provided by the Customer for the purpose of establishing an account with App4Legal is accurate and</li>
                                <li>Customer has complied with and will continue to comply with all applicable laws, including privacy laws, and has obtained and will continue to obtain the requisite privacy consents in the collection and use of all information that may be collected on any website or maintained on any server hosted by App4Legal.</li>
                            </ol>
                            <p>
                                8- TERM, TERMINATION & SUSPENSION OF SERVICES</p><br></br>
                            <p>
                                8.1 Initial Term:</p><br></br>
                            <p>
                                The initial subscription term shall begin on the effective date of Customer subscription and expire at the end of the period selected during the subscription process.</p><br></br>
                            <p>
                                After placement of order, the Customer is entitled to cancel the order free of charge within a period of 30 days with money back guarantee on subscription fees.</p><br></br>
                            <p>
                                8.2 Renewal of Term:</p><br></br>
                            <p>
                                Unless one of us gives the other a written notice that it does not intend to renew the subscription, the paid subscription and this Agreement will automatically renew for the period selected by the customer in its latest term (“Renewal Subscription Term”). The written notice of non-renewal must be sent at least 15 days before the end of the Subscription Term. The Renewal Subscription Term will be on the current terms and conditions of this Agreement, and subject to the renewal pricing provided in our standard pricing available on App4Legal website <a href='https://App4Legal.com/'>https://App4Legal.com/.</a> Should you decide not to renew, you may send the notice of non-renewal by email to <a href='mailto:bd@App4Legal.com'>bd@App4Legal.com</a> or use the cancellation option within the product interface.</p><br></br>
                            <p>
                                8.3 Termination by Customer:</p><br></br>
                            <p>
                                Customer may terminate this Agreement before the end of the Term without liability (except for amounts due for Services provided up to the effective date of termination) if App4Legal:</p><br></br>
                            <ol>
                                <li>Fails to provide the Services in accordance with the terms of this Agreement, such failure causes material harm to the Customer and App4Legal does not cure the failure within 15 days of receipt of the notice in writing from Customer describing the failure in reasonable detail.</li>
                                <li>Materially violates any other provision of this Agreement and fails to cure the violation within 30 days of receipt of the notice in writing from Customer describing the violation in reasonable detail.</li>
                            </ol>
                            <p>
                                In the event that this Agreement is terminated pursuant to this Section, App4Legal will return the fees paid to it for Services not yet performed pro rata.</p><br></br>
                            <p>
                                8.4 Termination/Suspension by App4Legal:</p><br></br>
                            <p>
                                App4Legal may terminate this Agreement or suspend the Services before the end of the Term without liability:</p><br></br>
                            <ol>
                                <li>On 30 days’ notice to the Customer, if the Customer is overdue on the payment of any amount due under this Agreement;</li>
                                <li>If Customer materially violates any other provision of this Agreement and fails to cure the violation within 15 days’ notice in writing from App4Legal, describing the violation in reasonable detail; or</li>
                                <li>Immediately on written notice upon Customer becoming insolvent or bankrupt within the meaning of the Bankruptcy and Insolvency Laws.</li>
                            </ol>
                            <p>
                                During suspension, the Customer will not be able to access the Services. App4Legal will use reasonable efforts to give Customer an advance notice in writing of suspension of Service unless a law enforcement or governmental agency directs otherwise or suspension without notice is necessary to protect App4Legal or its other customers.</p><br></br>
                            <p>
                                Following suspension, App4Legal shall keep the account of Customer suspended for the reasons stated above for a maximum period of 90 days; thereafter, the account stands deleted and Customer’s data/information with all related backups shall be deleted permanently from the database of App4Legal. But on special request and on being assured in time by Customer about resumption of Service, App4Legal may extend the period of suspension and retain the data/information for further specified period of time as agreed with Customer.</p><br></br>
                            <p>
                                Following termination, Customer’s data and account settings shall be irrevocably deleted within 90 days from the date of termination.</p><br></br>
                            <p>
                                The Customer can also notify App4Legal’s support at any time after termination to delete its data permanently from App4Legal servers; and in this case, App4Legal support shall delete Customer’s Program and data within twenty-four (24) hours.</p><br></br>
                            <p>
                                It shall be Customer’s exclusive responsibility to secure all necessary data from Customer’s account prior to termination.</p><br></br>
                            <p>
                                9- USE OF SUBSCRIPTION</p><br></br>
                            <p>
                                9.1 In accordance with these Terms and Conditions, the Customer is granted a non-exclusive limited right to use the Program which are made available online as a cloud software. The Customer does not acquire the Program or any copy or part of it and is not granted a license to implement the Program in any way other than as a cloud subscription.</p><br></br>
                            <p>
                                9.2 The Customer’s subscription entitles the Customer to use the Program for the number users selected. If the Customer needs to increase the number of users or modules, the subscription will be upgraded and the Customer shall agree to pay the consequential increase in the subscription at the then current rates of such additional users and modules.</p><br></br>
                            <p>
                                9.3 The Program itself or App4Legal’s website may provide a list of subscription types and selected additional modules. Some functionalities, services and additional modules may be subject to separate terms and conditions published per module.</p><br></br>
                            <p>
                                9.4 Only Customer’s users are entitled to use the Program, and the Program may not be used for or on behalf of any other parties or for data processing or the provision of Services for other parties than the Customer. The Customer agrees to be fully responsible and liable for any third parties that are given access to the Program by the Customer or who use the Customer’s log in details.</p><br></br>
                            <p>
                                9.5 Other than as set out in 9.4, the Customer is not entitled to assign the subscription or grant access to the Program, whether in full or in part, to any third party.</p><br></br>
                            <p>
                                9.6 The Customer shall ensure that the Program is not used in any manner which reflects adversely upon the name, reputation and/or goodwill of App4Legal or in breach of any applicable laws or regulations.</p><br></br>
                            <p>
                                10- CUSTOMER DATA AND DATA SECURITY</p><br></br>
                            <p>
                                10.1 As between the parties, the Customer shall own any and all data it provides to App4Legal or the Program. The Program permits the Customer to export records and data held inside the Program’s database and the Customer agrees to export any and all data prior to termination of the subscription. </p><br></br>
                            <p>
                                10.2 App4Legal will not process any Customer data to generate reports outside the database dedicated for the Customer. Customer data entered into the Program will be accessible by the Customer’s users only.</p><br></br>
                            <p>
                                10.3 App4Legal shall take all necessary technical and organisational security and data protection measures to ensure the safe and secure hosting of any Customer data.</p><br></br>
                            <p>
                                10.4 At the Customer’s request App4Legal will provide sufficient information to enable the Customer to ensure that the said technical and organisational measures have been taken. App4Legal shall be permitted to charge the Customer for such work at its standard rates when relevant.</p><br></br>
                            <p>
                                10.5 App4Legal has the obligations to delete Customer data 90 days after termination of the subscription regardless of the reason for termination, and App4Legal will not store any Customer data after such time.</p><br></br>
                            <p>
                                10.6 App4Legal may disclose Customer data to third parties and public authorities where such disclosure is regulated by law, e.g. to avoid a loss of value, including in connection with judgments, public authority orders, the Customer’s bankruptcy, death or the like.</p><br></br>
                            <p>
                                10.7 App4Legal pays serious attention to protect the data of its customers and works to be compliant always with international customers data protection regulations. App4Legal is GDPR compliant.</p><br></br>
                            <p>
                                10.8 App4Legal appointed Prighter as privacy representative. The Customer can make use of its GDPR data privacy rights by visiting this <a href='https://prighter.com/q/19775994'>Public Privacy Dashboard</a></p><br></br>
                            <p>
                                Contact Prighter</p><br></br>
                            <p>
                                Prighter<br></br>
                                Maetzler Rechtsanwalts GmbH & Co KG<br></br>
                                Attorneys at Law<br></br>
                                c/o App4Legal<br></br>
                                Schellinggasse 3/10, 1010 Vienna, Austria<br></br>
                            </p><br></br>
                            <p>
                                The following subject should be added to any correspondence:<br></br>
                                    GDPR-REP ID: 19775994</p><br></br>
                            <p>
                                10.9 For a Customer using a third-party plugin on top of the Program (similar to Zoom for example), App4Legal will retain only the username needed to establish the connection with the third-party plugin as long as the Customer is operational on the Program. Refer to Chapter TERM, TERMINATION & SUSPENSION OF SERVICES.</p><br></br>
                            <p>
                                10.10 App4Legal 3rd Party Integration</p><br></br>
                            <p>
                                Google Calendar</p><br></br>
                            <p>
                                App4Legal integrates with Google Calendar to have a two-way sync between App4Legal calendar & Google’s calendar. App4Legal processes list of calendars in Google, meetings, attendees & timings. App4Legal doesn’t store attendees information.</p><br></br>
                            <p>
                                11- FEES, BILLING, TAXES, CHARGES</p><br></br>
                            <p>
                                11.1 Fees:</p><br></br>
                            <p>
                                The fees set forth in the order form created at the outset of Customer’s account shall be effective for the Initial Term and each renewal Term of this Agreement, provided that App4Legal shall have the right to revise these fees at any time upon thirty (30) days’ written notice to the Customer. In the event that the Customer does not agree with such fee revision, Customer shall have the right to terminate this Agreement upon fifteen (15) days’ written notice, provided that such notice of termination must be received within sixty (60) days from the date of notice of fee increase.</p><br></br>
                            <p>
                                11.2 Billing and Payment Arrangements:</p><br></br>
                            <p>
                                App4Legal will bill the Customer on an annual or any other mutually agreed period basis for all recurring fees published on App4Legal website <a href='https://App4Legal.com/'>https://App4Legal.com/</a>. For recurring fees, no refund or adjustment for plan downgrades, upgrades or elimination of plan features within the current billing term shall be issued. Invoices/payments are irrevocably deemed final and accepted by the Customer unless disputed or sought clarification before subscribing to the Service. The Customer shall at all times provide and keep current and up‐to‐date Customer’s contact, credit card, if applicable, and billing information on the secure administrative control panel of the Program used by the Customer.</p><br></br>
                            <p>
                                11.3 Payment by Credit Card/Wire Transfer/Cheque:</p><br></br>
                            <p>
                                If the Customer is paying by credit card, the Customer shall at all times provide and keep current and updated Customer’s contact, credit card, if applicable, and billing information on the secure administrative control panel. Customer authorizes App4Legal to charge the Customer credit card or bank account for all fees payable at the beginning of the Initial Subscription Term and all subsequent Billing Periods, including upgrades. Customer further authorizes App4Legal to use a third party to process payments and to consent to the disclosure of Customer payment information to such third party.</p><br></br>
                            <p>
                                11.4 Taxes:</p><br></br>
                            <p>
                                Customer acknowledges that the all applicable taxes, duties or government levies whatsoever are not included in the fees and expenses charged under this Agreement. Customer will make timely payment of all such taxes, duties or government levies related to this Agreement</p><br></br>
                            <p>
                                12- OPERATIONAL STABILITY</p><br></br>
                            <p>
                                12.1 App4Legal strives towards the highest possible operational stability, but shall not be responsible or liable for any breakdowns or service interruptions, including interruptions caused by factors beyond App4Legal’s control, such as power failures, defective equipment, Internet connections, telecoms connections or the like. The Program and the Services are provided “as is” and App4Legal expressly disclaims any further representations, warranties, conditions or other terms, express or implied, by statute, collaterally or otherwise, including but not limited to implied warranties, conditions or other terms of satisfactory quality, fitness for a particular purpose or reasonable care and skill.</p><br></br>
                            <p>
                                12.2 In the event of an interruption of service App4Legal will use reasonable endeavors to restore normal operations as soon as possible.</p><br></br>
                            <p>
                                12.3 Planned interruptions will mainly take place based on prior notifications to the Customer.</p><br></br>
                            <p>
                                13- MODIFICATION OF TERMS AND CONDITIONS</p><br></br>
                            <p>
                                App4Legal may update, amend, modify or supplement the terms and conditions of this Agreement from time to time and will use reasonable efforts to notify the Customer regarding the changes. The Customer is responsible for regularly reviewing the most current version of this Agreement at any time. If at any time the Customer does not agree with any amendment, modification or supplement to the terms and conditions of this Agreement, the Customer may terminate this Agreement for convenience, as per Clause 8 mentioned aforesaid. The Customer’s continued use of the Customer’s account and/or the services after the notice period will be conclusively deemed to be acceptance by the Customer of any such modifications or amendment.</p><br></br>
                            <p>
                                14- LIMITED WARRANTY: LIMITATION OF DAMAGES</p><br></br>
                            <p>
                                App4Legal provides the Services “as is”. Customer expressly agrees that use of the Services is at Customer’s sole risk. App4Legal and its subsidiaries, affiliates, officers, employees, agents, partners, vendors and licencors expressly disclaim all warranties of any kind, whether express or implied, including, but not limited to the implied warranties of merchant ability, fitness for a particular purpose and non-infringement. Customer hereby agrees that the terms of this Agreement shall not be altered due to custom or usage or due to the parties’ course of dealing or course of performance under this Agreement.</p><br></br>
                            <p>
                                App4Legal and its subsidiaries, affiliates, officers, employees, agents, partners, vendors and licencors shall not be liable for any indirect, incidental, special, punitive or consequential damages, including but not limited to damages for lost profits, business interruption, loss of programs or information, and the like, that result from the use or inability to use the Service or from mistakes, omissions, interruptions, deletion of files or directories, errors, defects, delays in operation, or transmission, regardless of whether App4Legal has been advised of such damages or their possibility.</p><br></br>
                            <p>
                                Customer is fully responsible for the content of the information and data passing through App4Legal’s network or using the Services and for all activities that Customer conducts with the assistance of the Services.</p><br></br>
                            <p>
                                Notwithstanding anything to the contrary contained in this Agreement, App4Legal’s aggregate liability under or in connection with the Agreement, whether arising from contract, negligence or otherwise, shall in any event not exceed the amount paid by Customer under the Agreement in the preceding 12 months.</p><br></br>
                            <p>
                                15- SOFTWARE AND INTELLECTUAL PROPERTY RIGHTS</p><br></br>
                            <p>
                                15.1 Ownership of Intellectual Property Rights:</p><br></br>
                            <p>
                                All Intellectual Property Rights, including any Software, owned by a party, its licencors or subcontractors as on the effective date of this Agreement shall continue to be owned by such party, its licencors or subcontractors and, except as expressly provided in this Agreement, the other party shall not acquire any right, title or interest in or to such Intellectual Property Rights. App4Legal shall own all rights, titles and interests in and to any materials created or developed by App4Legal or its subcontractors.</p><br></br>
                            <p>
                                The Program and any information provided by it, other than the Customer’s data, is protected by copyright and other intellectual property rights and is owned by or licensed to App4Legal or any of its group companies. Any development or adaptations made to such intellectual property by Customer shall vest in App4Legal. The Customer shall notify App4Legal of any actual or suspected infringement of App4Legal’s intellectual property rights and any unauthorized use of the Program that the Customer is aware of. No intellectual property rights are assigned to the Customer.</p><br></br>
                            <p>
                                The Customer represents and warrants that no uploaded material or Customer data will infringe third party rights or intellectual property rights and will not contain any material that is obscene, offensive, inappropriate or in breach of any applicable law.</p><br></br>
                            <p>
                                15.2 Right to use logo:</p><br></br>
                            <p>
                                Customer agrees to let App4Legal use its organization’s logo in App4Legal’s customer list and at other places on its website.</p><br></br>
                            <p>
                                Customer can notify App4Legal to remove its organization’s logo from App4Legal’s website in a written notice and App4Legal shall remove it within fifteen (15) days.</p><br></br>
                            <p>
                                15.3 License of Customer Software and Intellectual Property:</p><br></br>
                            <p>
                                Customer agrees to grant to App4Legal, solely for App4Legal’s provision of the Services, access to any tool or application used by the Customer and required by App4Legal in order to troubleshoot and perform its Services, license during the Term to use any Intellectual Property Rights, including any Software, owned by or licensed to the Customer by third parties and that is necessary for providing the Services to Customer and otherwise, performing its obligations under this Agreement. With respect to any Intellectual Property Rights and Software used by App4Legal to provide the Services, Customer represents and warrants that: (a) Customer is either the owner of such Intellectual Property Rights or Software or is authorized by its owner to include it under this Agreement; and (b) App4Legal has the right during the Term to use such Intellectual Property Rights and Software for the purpose of providing the Services to Customer as contemplated by this Agreement.</p><br></br>
                            <p>
                                15.4 No Assurance of Compatibility:</p><br></br>
                            <p>
                                Customer acknowledges that App4Legal makes no representation, warranty or assurance that Customer’s equipment and software will be compatible with App4Legal’s equipment, software and systems or the Services.</p><br></br>
                            <p>
                                16- INDEMNIFICATION</p><br></br>
                            <p>
                                Customer and App4Legal shall indemnify, defend and hold harmless each other (and their subsidiaries, affiliates, officers, employees, agents, partners, mandatories, vendors and licencors) of any and all Claims (including third-party Claims) arising as a result of or in relation to any breach of this Agreement or fault by the other party.</p><br></br>
                            <p>
                                17- GOVERNING LAW</p><br></br>
                            <p>
                                This Agreement shall be governed by and construed in accordance with the laws of England. The Customer agrees, in the event any claim or suit is brought in connection with this Agreement, it shall be brought to the non-exclusive jurisdiction of the courts of London.</p><br></br>
                            <p>
                                18- SEVERABILITY</p><br></br>
                            <p>
                                In the event that anyone or more of the provisions contained herein shall, for any reason, be held to be invalid, illegal, or unenforceable in any respect, such invalidity, illegality, or unenforceability shall not affect any of the other provisions of this Agreement; and this Agreement shall be construed as if such provision(s) had never been contained herein, provided that such provision(s) shall be curtailed, limited, or eliminated only to the extent necessary to remove the invalidity, illegality, or unenforceability.</p><br></br>
                            <p>
                                19- WAIVER</p><br></br>
                            <p>
                                No waiver by App4Legal of any breach by the Customer of any of the provisions of this Agreement shall be deemed a waiver of any preceding or succeeding breach of this Agreement. No such waiver shall be effective unless it is in writing signed by the parties hereto and then only to the extent expressly set forth in such writing.</p><br></br>
                            <p>
                                20- ASSIGNMENT</p><br></br>
                            <p>
                                Neither party may assign or transfer this Agreement or any rights or obligations hereunder, in whole or in part, except with the prior written consent of the other party, which shall not be withheld unreasonably; provided that App4Legal may assign or transfer this Agreement, or any rights or obligations hereunder, in whole or in part: (i) to an affiliate of App4Legal; or (ii) in connection with a merger, amalgamation or sale of all or a substantial part of the business of App4Legal, which assignments and/or transfers shall operate novation and discharge App4Legal hereunder. A change of control of the Customer shall be deemed to be an assignment and transfer hereunder and shall be governed by the requirements of this provision.</p><br></br>
                            <p>
                                The terms and conditions along with privacy policies with all references, constitute the sole and entire Agreement of the parties to this Agreement with respect to the subject matter contained herein, and supersede all prior terms and conditions which were agreed by the Customer.</p><br></br>
                        </div>
                    </Typography>
                </Grid>
            </Grid>
        );
    }
}