﻿import * as React from "react";
import { createMuiTheme, ThemeProvider, PaletteOptions } from "@material-ui/core/styles";
import { create } from 'jss';
import rtl from 'jss-rtl';
import { StylesProvider, jssPreset } from '@material-ui/core/styles';
import { useSelector } from "react-redux";
import { createContext, useState, useEffect } from "react";
import { ReduxState } from "../..";
import createTypography from "@material-ui/core/styles/createTypography";
import createPalette from "@material-ui/core/styles/createPalette";
import * as Colors from "@material-ui/core/colors";
import '../../Styles/Fonts.css';
import { PaletteMode } from '@material-ui/core';

// Configure JSS
const jss = create({ plugins: [...jssPreset().plugins, rtl()] });
//jss.use(rtl({ enabled: false }));
export const CustomThemeContext = createContext({
    theme: null,
    setTheme: () => null
});
// Normal or default theme
const light: PaletteOptions = {
    primary: {
        main: Colors.blue[500],
    },
    secondary: {
        main: '#8844cc',
    },
    error: {
        main: Colors.red.A400,
    },
    background: {
        default: '#f5f5f5',
    }
};
// Dark theme
const dark: PaletteOptions = {
    primary: {
        main: '#26292C',
        light: 'rgb(81, 91, 95)',
        dark: 'rgb(26, 35, 39)',
        contrastText: '#ffffff',
    },
    secondary: {
        main: '#FFB74D',
        light: 'rgb(255, 197, 112)',
        dark: 'rgb(200, 147, 89)',
        contrastText: 'rgba(0, 0, 0, 0.87)',
    },
    error: {
        main: Colors.red.A400,
    },
    background: {
        paper: '#424242',
        default: '#121212',
    }
};
const themes = {
    light,
    dark
};
function getTheme(theme): PaletteOptions {
    return themes[theme]
}
function CustomThemeProvider(props) {
    const { direction, theme } = useSelector((state: ReduxState) => state.settings);
    const { children } = props;

    // Retrieve the theme object by theme name
    const cTheme = getTheme(theme)
    
    const generateTheme = () =>
        createMuiTheme({
            palette: { mode: theme as PaletteMode, ...cTheme },
            components: {
                MuiButton: {
                    styleOverrides: {
                        root: {
                            borderRadius: '20px',
                            '&:hover': {
                                backgroundColor: cTheme.primary['main']['A600']
                            },
                        }
                    },                    
                },
                MuiAccordion: {
                    styleOverrides: {
                        root: {
                            borderTop: '3px solid ' + cTheme.primary['main']
                        }
                    }
                },
                MuiBackdrop: {
                    styleOverrides: {
                        root: {
                            backgroundColor: 'transparent'
                        }
                    }
                }
            },            
            direction: direction == 'rtl' ? 'rtl' : 'ltr',
            typography: createTypography(createPalette(getTheme(theme)), {
                fontFamily: "'Source Sans Pro'",
            })
        });
    useEffect(() => {
        document.getElementById("body").setAttribute('dir', direction);
    }, []);
    return (
        <ThemeProvider theme={generateTheme()}>
            <StylesProvider jss={jss}>
                <div dir={direction}>
                    {children}
                </div>
            </StylesProvider>
        </ThemeProvider>
    );
}

export default CustomThemeProvider;