﻿import * as React from "react";
import { withStyles, makeStyles } from "@material-ui/core/styles";
import { connect, useSelector } from "react-redux";
import Grid from "@material-ui/core/Grid";
import PopupAlert from '../Common/Alert';
import { AxiosRequest, RequestHeaders } from '../../Helpers/AxiosRequest';
import { References } from '../../Helpers/References';
import * as actions from '../../redux/actions';
import * as UIStrings from '../../UIStrings';
import { Typography, createStyles, Theme, WithStyles, Paper, Divider, Link, List, ListItem, ListItemIcon, IconButton, Tooltip } from "@material-ui/core";
import FormSkeleton from "../Common/Skeleton/FormSkeleton";
import { AppProps } from "../App";
import { ReduxState } from "../..";
import DeleteIcon from '@material-ui/icons/Delete';
import { grey } from "@material-ui/core/colors";
import { getMessageProperty, deleteMessageProperty } from "../../GraphService";
import { Helpers } from "../../Helpers/Helpers";
import NoActivitiesIcon from '../../assets/noActivities.svg';
import NoActivitiesNoMicrosoftIcon from '../../assets/noActivitiesnoMicrosoft.svg';
const useStyles = (theme: Theme) => createStyles({
    paper: {
        marginTop: theme?.spacing(2),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center'
    },
    avatar: {
        margin: theme?.spacing(1),
        backgroundColor: 'blue',
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme?.spacing(3),
        paddingRight: "5px",
        marginBottom: theme?.spacing(8)
    },
    submit: {
        color: 'red'
    },
    textarea: {
        resize: "both",
        minHeight: "200px"
    },
    formControl: {
        margin: theme?.spacing(1),
        minWidth: 120,
    },
    selectEmpty: {
        marginTop: theme?.spacing(2),
    },
    root: {
        display: 'flex',
        flexDirection: 'column',
        margin: '0 auto'
    },
    main: {
        marginBottom: theme?.spacing(8),
    },
    footer: {
        position: 'fixed',
        bottom: "0",
        marginTop: 'auto',
        width: "100%",
        backgroundColor: '#fff',
    },
    margin: {
        margin: theme?.spacing(1),
    },
    textField: {
        width: '25ch',
    },
    appBar: {
        position: 'fixed',
        height: '55px',
        paddingBottom: '10px',
        bottom: 0,
    },
    addOpponent: {
        margin: theme?.spacing(1),
    },
    emailPref: {
        border: '1px solid lightGrey',
        padding: '10px',
        borderRadius: '5px',
    },
    label: {
        border: '1px solid white',
        padding: '10px',
        borderRadius: '5px',
        backgroundColor: '#4cb0fc'
    },
    icon: {
        width: '20px',
        height: '20px'
    }
});

class Activities extends React.Component<IFormProps, IFormState> {

    messageId: string;
    constructor(props: IFormProps, context: any) {
        super(props, context);
        this.state = {
            activities: []
        };
        this.messageId = '';
        this.handleDeleteActivity = this.handleDeleteActivity.bind(this);
    }
    componentDidMount() {
        this.props.startLoader();
        this.getData();
    }
    async getData() {
        try {
            this.messageId = Office.context.mailbox?.item?.itemId;
            const token = await this.props.getAccessToken();
            if (token) {
                if (this.messageId) {
                    var activities: any[] = await getMessageProperty(token, this.messageId);
                    if (activities) {
                        activities.reverse();
                        this.setState({ activities: activities });
                    }
                }
                this.props.stopLoader();
            }
            else {
                var self = this;
                Office.context.mailbox.getCallbackTokenAsync({ isRest: true }, async function (result) {
                    if (result.status === Office.AsyncResultStatus.Succeeded) {
                        const token = result.value;
                        const properties = await getMessageProperty(token, self.messageId, true);
                        if (properties) {
                            properties.reverse();
                            self.setState({ activities: properties });
                        }
                        else {
                            self.setState({ activities: [] });
                        }
                        self.props.stopLoader();
                    }
                });
            }

        }
        catch (err) {
            this.props.stopLoader();
            this.props.logger.error('Activities: ' + err);
        }
    }
    UIText = UIStrings.getLocaleStrings(this.props.language);
    componentWillUnmount() {
    }
    async handleDeleteActivity(id: string) {
        try {
            if (id) {
                const token = await this.props.getAccessToken();
                if (token) {
                    await deleteMessageProperty(token, this.messageId, id);
                    await this.getData();
                }
                else {
                    var self = this;
                    var deleteDataPromise = new Promise(resolve => {
                        Office.context.mailbox.getCallbackTokenAsync({ isRest: true }, async function (result) {
                            if (result.status === Office.AsyncResultStatus.Succeeded) {
                                const token = result.value;
                                await deleteMessageProperty(token, self.messageId, id, true);
                            }
                            resolve(null);
                        });
                    });
                    await deleteDataPromise;
                    await self.getData();
                }
            }
        }
        catch (err) {
            this.props.logger.error('Activities: ' + err);
        }
    }
    render() {
        const { classes } = this.props;
        return (<React.Fragment>
            <PopupAlert />
            {this.state.activities != null ?
                (this.props.runLoader ?
                    <FormSkeleton input='activities' /> :
                    <div className={classes.root}>
                        {this.state.activities.length == 0 ?
                            <div style={{ flexGrow: 1, margin: '50% auto' }}>
                                <Grid container direction='column' alignItems='center' justifyItems='center'>
                                    <img src={NoActivitiesIcon} style={{ width: '128px', height: '128px' }} />
                                    <Typography style={{ margin: '0 auto', marginTop: '40px' }}>{this.UIText.noActivities}</Typography>
                                </Grid>
                            </div>
                            :
                            <List>
                                {this.state.activities.map((item) => (
                                    <div key={item.guid}>
                                        <Activity {...item} deleteActiviy={this.handleDeleteActivity} />
                                        <Divider />
                                    </div>
                                ))}
                            </List>
                        }
                    </div >)
                :
                <div style={{flexGrow: 1, margin: '50% auto'}}>
                    <Grid container direction='column' alignItems='center' justifyItems='center'>
                        <img src={NoActivitiesNoMicrosoftIcon} style={{ width: '128px', height: '128px' }} />
                        <Typography style={{ margin: '0 auto', marginTop: '40px' }}>{this.UIText.noActivitiesnoMicrosoft}</Typography>
                    </Grid>
                </div>
            }
        </React.Fragment>
        );
    }
}
const mapStateToProps = (state: ReduxState) => ({
    language: state.settings.language,
    runLoader: state.loader.runLoader,
    includeEmail: state.attachments.includeEmail,
    files: state.attachments.files
});
const mapDispatchToProps = (dispatch) => {
    return {
        successAlert: (message: string) => dispatch(actions.successAlert(message)),
        errorAlert: (message: string) => dispatch(actions.errorAlert(message)),
        stopLoader: () => dispatch(actions.stopLoader()),
        startLoader: () => dispatch(actions.startLoader()),
        startSaving: () => dispatch(actions.startSaving()),
        stopSaving: () => dispatch(actions.stopSaving()),
        openModal: () => dispatch(actions.openModal()),
        focusError: (id: string) => dispatch(actions.focusError(id)),
        setAttachments: (files) => dispatch(actions.setAttachments(files)),
        addFile: (file) => dispatch(actions.addFile(file)),
        deleteFile: (index) => dispatch(actions.deleteFile(index)),
        setHasEmailAttachments: (found: boolean) => dispatch(actions.setEmailHasAttachments(found)),
        setFetchingAttachments: (status: boolean) => dispatch(actions.setFetchingAttachments(status))
    }
};
type FormReduxProps = ReturnType<typeof mapStateToProps> & ReturnType<typeof mapDispatchToProps>;
export interface ActivityProps {
    guid: string;
    type: string;
    id: string;
    priority: string;
    subject: string;
    date: string;
    deleteActiviy?;
}
interface IFormState {
    activities: ActivityProps[];
}
interface IFormProps extends FormReduxProps, WithStyles<typeof useStyles>, AppProps {

}
const AppContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(Activities);
export default withStyles(useStyles, { withTheme: true })(AppContainer);

const activityStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            flexGrow: 1,
        },
        paper: {
            padding: theme.spacing(2),
            margin: 'auto',
            maxWidth: 500,
        },
        image: {
            width: 16,
            height: 16,
        }
    }),
);
export function Activity(props: ActivityProps) {
    const languageSelected = useSelector((state: ReduxState) => state.settings.language);
    const UIText = UIStrings.getLocaleStrings(languageSelected);
    const [isDeleting, setIsDeleting] = React.useState(false);
    const classes = activityStyles();
    const handleClick = (event: React.MouseEvent<HTMLAnchorElement>) => {
        event.preventDefault();
        const reqHeader = new RequestHeaders();
        var url = reqHeader.url;
        switch (props.type) {
            case 'corporateMatter': case 'litigationCase': case 'hearing': case 'matterNote': url += 'cases/edit/' + props.id; break;
            case 'contract': url += 'modules/contract/contracts/view/' + props.id; break;
            case 'intellectualProperty': url += 'intellectual_properties/edit/' + props.id; break;
            case 'task': url += 'tasks/view/' + props.id; break;
            default: break;
        }
        window.open(url);
    };
    const handleDelete = async (event: React.MouseEvent<HTMLButtonElement>) => {
        event.preventDefault();
        setIsDeleting(true);
        await props.deleteActiviy(props.guid);
        setIsDeleting(false);
    };
    return (
        <div className={classes.root}>
            <Paper className={classes.paper}>
                <Grid container spacing={1} justifyItems='center' alignItems='center'>
                    <Grid item container xs={12} justifyItems='space-between' alignItems='center'>
                        <Grid item container xs={10} spacing='10px' direction='row' alignItems='center'>
                            <Grid item>
                                <Tooltip title={UIText[props.type]}>
                                    <img className={classes.image} src={require('../../assets/' + props.type + 'Icon.svg')} />
                                </Tooltip>
                            </Grid>
                            <Grid item>
                                <Tooltip title={UIText.open}>
                                    <Link href='#' color='secondary' onClick={handleClick}>
                                        {props.id}
                                    </Link>
                                </Tooltip>
                            </Grid>
                            <Grid item>
                                <Tooltip title={Helpers.jsUcfirst(props.priority.length > 0 ? props.priority : 'medium')}>
                                    <img className={classes.image} src={require('../../assets/' + (props.priority?.length > 0 ? props.priority : 'medium') + '.png')} />
                                </Tooltip>
                            </Grid>
                        </Grid>
                        <Grid item xs={2}>
                            <Tooltip title={UIText.remove}>
                                <IconButton
                                    aria-label="delete"
                                    color="secondary"
                                    disabled={isDeleting}
                                    onClick={handleDelete}>
                                    <DeleteIcon />
                                </IconButton>
                            </Tooltip>
                        </Grid>
                    </Grid>
                    <Grid item container xs={12} style={{ overflow: 'hidden', textOverflow: 'ellipsis' }} alignItems='center' justifyItems='flex-start'>
                        <Typography variant='h6'>
                            {decodeURIComponent(props.subject)}
                        </Typography>
                    </Grid>
                    <Grid item xs={12}>
                        <Grid container alignItems='center' justifyItems='center'>
                            <Typography style={{ margin: 'auto' }} variant='overline'>
                                {UIText.closedOn + ': ' + props.date}
                            </Typography>
                        </Grid>
                    </Grid>
                </Grid>
            </Paper>
        </div>
    );
}