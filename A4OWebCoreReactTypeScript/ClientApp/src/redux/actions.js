﻿import * as Types from './actionTypes';

export const successAlert = (messageContent) => ({
    type: Types.SUCCESS_ALERT, value: messageContent
});
export const openModal = (title, message, id, form) => ({
    type: Types.OPEN_MODAL, value: { title: title, message: message, id: id, form: form }
});
export const closeModal = () => ({
    type: Types.CLOSE_MODAL
});
export const errorAlert = (messageContent) => ({
    type: Types.ERROR_ALERT, value: messageContent
});

export const closeAlert = () => ({
    type: Types.CLOSE_ALERT
});
export const changeLanguage = (language) => ({
    type: Types.CHANGE_LANGUAGE, value: language
});
export const changeTheme = (theme) => ({
    type: Types.CHANGE_THEME, value: theme
});
export const stopLoader = () => ({
    type: Types.STOP_LOADER
});
export const startLoader = () => ({
    type: Types.START_LOADER
});
export const startSaving = () => ({
    type: Types.START_SAVING
});
export const stopSaving = () => ({
    type: Types.STOP_SAVING
});
export const focusError = (id) => ({
    type: Types.FOCUS_ERROR, value: id
});
export const setAttachments = (files) => ({
    type: Types.SET_ATTACHMENTS, value: files
});
export const addFile = (file) => ({
    type: Types.ADD_FILE, value: file
});
export const deleteFile = (file) => ({
    type: Types.DELETE_FILE, value: file
});
export const addMoreEmails = (emails) => ({
    type: Types.ADD_MORE_EMAILS, value: emails
});
export const deleteMoreEmails = (email) => ({
    type: Types.DELETE_MORE_EMAILS, value: email
});
export const changeIncludeEmail = (checked) => ({
    type: Types.CHANGE_INCLUDE_EMAIL, value: checked
});
export const changeIncludeEmailAttachments = (checked) => ({
    type: Types.CHANGE_INCLUDE_EMAIL_ATTACHMENTS, value: checked
});
export const setEmailHasAttachments = (status) => ({
    type: Types.HAS_EMAIL_ATTACHMENTS, value: status
});
export const setFetchingAttachments = (status) => ({
    type: Types.FETCHING_ATTACHMENTS, value: status
});