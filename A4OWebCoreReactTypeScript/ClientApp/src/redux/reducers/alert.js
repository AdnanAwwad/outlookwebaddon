﻿import { SUCCESS_ALERT, ERROR_ALERT, CLOSE_ALERT, START_LOADER, STOP_LOADER, CHANGE_LANGUAGE, START_SAVING, STOP_SAVING, OPEN_MODAL, CLOSE_MODAL, FOCUS_ERROR} from '../actionTypes';
 
const initialAlertState = {
    open: false,
    severity: "",
    alertMessage: "Alert Message",
};

export const alert = (state = initialAlertState, action) => {
    switch (action.type) {
        case SUCCESS_ALERT:
            return {
                alertMessage: action.value, open: true, severity: "success"
            };
        case ERROR_ALERT:
            return {
                alertMessage: action.value, open: true, severity: "error"
            };
        case CLOSE_ALERT:
            return {
                open: false, alertMessage: '', severity: ""
            };
        default:
            return state;
    }
};
const initialSavingState = {
    saving: false
};
export const saving = (state = initialSavingState, action) => {
    switch (action.type) {
        case START_SAVING:
            return {
                saving: true
            };
        case STOP_SAVING:
            return {
                saving: false
            };
        default:
            return state;
    }
};
const initialModalState = {
    openModal: false
};
export const modal = (state = initialModalState, action) => {
    switch (action.type) {
        case OPEN_MODAL:
            return {
                openModal: true
            };
        case CLOSE_MODAL:
            return {
                openModal: false
            };
        default:
            return state;
    }
};
const initialLoaderState = {
    runLoader: false,
};
export const loader = (state = initialLoaderState, action) => {
    switch (action.type) {
        case START_LOADER:
            return {
                runLoader: true,
            };
        case STOP_LOADER:
            return {
                runLoader: false,
            };
        default:
            return state;
    }
};
const initialFocusState = {
    id: '',
};
export const focus = (state = initialFocusState, action) => {
    switch (action.type) {
        case FOCUS_ERROR:
            return {
                id: action.value,
            };
        default:
            return state;
    }
};