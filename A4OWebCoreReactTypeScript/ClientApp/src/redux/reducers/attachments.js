﻿import { ADD_FILE, DELETE_FILE, CHANGE_INCLUDE_EMAIL, HAS_EMAIL_ATTACHMENTS, CHANGE_INCLUDE_EMAIL_ATTACHMENTS, SET_ATTACHMENTS, FETCHING_ATTACHMENTS, ADD_MORE_EMAILS, DELETE_MORE_EMAILS } from "../actionTypes";

const initialAttachmentsState = {
    files: [], moreEmails: [], includeEmail: true, includeEmailAttachments: true, hasEmailAttachments: false, fetchingAttachments: true
};

export const attachments = (state = initialAttachmentsState, action) => {
    switch (action.type) {
        case SET_ATTACHMENTS:
            {
                var files = state.files;
                return {
                    ...state, files: action.value
                };
            }
        case ADD_FILE:
            {
                var files = state.files;
                files = files.concat(action.value);
                return {
                    ...state, files: files
                };
            }
        case DELETE_FILE:
            {
                const files = state.files;
                const index = action.value;
                if (index > -1) {
                    files.splice(index, 1);
                }
                return {
                    ...state, files: files
                };
            }
        case CHANGE_INCLUDE_EMAIL:
            {
                return {
                    ...state, includeEmail: action.value
                };
            }
        case CHANGE_INCLUDE_EMAIL_ATTACHMENTS:
            {
                return {
                    ...state, includeEmailAttachments: action.value
                };
            }
        case HAS_EMAIL_ATTACHMENTS:
            {
                return {
                    ...state, hasEmailAttachments: action.value
                };
            }
        case FETCHING_ATTACHMENTS:
            {
                return {
                    ...state, fetchingAttachments: action.value
                };
            }
        case ADD_MORE_EMAILS:
            {
                const emails = action.value;
                if (emails) {
                    var oldEmails = state.moreEmails;
                    emails.forEach(email => {
                        const existsItem = oldEmails.filter(oldEmail => oldEmail.id ? oldEmail.id === email.id : oldEmail.Id === email.Id)?.length > 0;
                        if (!existsItem) {
                            oldEmails.push(email)
                        }
                    });
                }
                return {
                    ...state, moreEmails: oldEmails
                }
            }
        case DELETE_MORE_EMAILS:
            {
                const index = action.value;
                var oldEmails = state.moreEmails;
                oldEmails.splice(index, 1);
                return {
                    ...state, moreEmails: oldEmails
                }
            }
        default:
            return state;
    }
};