﻿import { CHANGE_LANGUAGE, CHANGE_THEME } from '../actionTypes';
const initialSettingsState = {
    direction: (localStorage.getItem("appLang") ? (localStorage.getItem("appLang") == "ar-AE" ? "rtl" : "ltr") : "ltr"),
    language: localStorage.getItem("appLang") || 'en-US',
    theme: localStorage.getItem("appTheme") || 'light',
};
export const settings = (state = initialSettingsState, action) => {
    switch (action.type) {
        case CHANGE_LANGUAGE:
            {
                var langSelected = action.value;
                if (langSelected) {
                    localStorage.setItem('appLang', langSelected);
                }
                state = {
                    ...state,
                    direction: langSelected ? (langSelected == "ar-AE" ? "rtl" : "ltr") : "ltr",
                    language: langSelected ? langSelected : "en-US"
                };
                return state;
            }
        case CHANGE_THEME:
            {
                const theme = action.value;
                if (theme) {
                    localStorage.setItem('appTheme', theme);
                }
                state = {
                    ...state,
                    theme: theme
                };
                return state;
            }
        default:
            return state;
    }
};