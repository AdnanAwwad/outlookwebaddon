﻿import { combineReducers } from "redux";
import { alert, modal, saving, loader, focus } from "./alert";
import { settings } from "./settings";
import { attachments } from './attachments';
export default combineReducers({ alert, modal, saving, loader, settings, focus, attachments });