﻿import * as moment from 'moment';
import { Event } from 'microsoft-graph';
import { GraphRequestOptions, PageCollection, PageIterator } from '@microsoft/microsoft-graph-client';
import { ActivityProps } from './components/Activities/Activities';
import { Client } from "@microsoft/microsoft-graph-client";
import createLog from 'localstorage-logger';
import { AxiosRequest } from './Helpers/AxiosRequest';
const log = createLog({
    logName: 'appLogs',
    maxLogSizeInBytes: 512 * 1024
});
const propertyId = 'StringArray {66f5a359-4659-4830-9070-00049ec6ac6e} Name A4OCases';
function getAuthenticatedClient(accessToken: string){
    // Initialize Graph client
    const client = Client.init({
        // Use the provided access token to authenticate
        // requests
        authProvider: (done: any) => {
            done(null, accessToken);
        }
    });

    return client;
}

export async function getUserDetails(accessToken: string) {
    const client = getAuthenticatedClient(accessToken);

    const user = await client
        .api('/me')
        .select('displayName,mail,mailboxSettings,userPrincipalName')
        .get();

    return user;
}
export async function getMessageAttachments(accessToken: string, messageId: string, isRest: boolean = false) {
    var request;
    try {
        messageId = messageId.replace(/\//g, '-');
        if (isRest) {
            var restUrl = Office.context.mailbox.restUrl;
            if (!restUrl.endsWith('/'))
                restUrl = restUrl + '/';
            const reqUrl = restUrl + 'v2.0/me/messages/' + messageId + '/attachments';
            const response = await AxiosRequest.getFromServer(reqUrl, accessToken);
            const data = response?.data;
            if (data) {
                var attachments = [];
                attachments = attachments.concat(data?.value);
                var extra = await getPages(accessToken, data['@odata.nextLink'], true);
                attachments = attachments.concat(extra);
                return attachments;
            }
        }
        else {
            const client = getAuthenticatedClient(accessToken);
            request = await client
                .api('/me/messages/' + messageId + '/attachments')
                .get();
            var attachments = [];
            attachments = attachments.concat(request?.value);
            var extra = await getPages(accessToken, request['@odata.nextLink']);
            attachments = attachments.concat(extra);
            return attachments;
        }
    }
    catch (err) {
        log.error('Graph Service[getMessageAttachments]: ', err);
    }
    
    return request;
}
export async function getContactsList(accessToken: string, key = null, isRest = false) {
    var request;
    try {
        if (isRest) {
            var restUrl = Office.context.mailbox.restUrl;
            if (!restUrl.endsWith('/'))
                restUrl = restUrl + '/';            
            const reqUrl = restUrl + 'v2.0/me/contacts' + (key? ('?$search="' + key + '"') : '');
            const response = await AxiosRequest.getFromServer(reqUrl, accessToken);
            const data = response?.data;
            request = data;
        }
        else {
            const client = getAuthenticatedClient(accessToken);

            if (key) {
                request = await client
                    .api('/me/contacts')
                    .search(key)
                    .get();
            }
            else {
                request = await client
                    .api('/me/contacts')
                    .get();
            }
        }
    }
    catch (err) {
        log.error('Graph Service[getContactsList]: ', err);
    }
    var contacts = [];
    contacts = contacts.concat(request?.value);    
    if (request) {
        var extra = await getPages(accessToken, request['@odata.nextLink'], isRest);
        contacts = contacts.concat(extra);
    }
    return contacts;
}
export async function getEmailsList(accessToken: string, key: string = null, isRest = false) {
    var request;
    try {
        if (isRest) {
            var restUrl = Office.context.mailbox.restUrl;
            if (!restUrl.endsWith('/'))
                restUrl = restUrl + '/';
            const reqUrl = restUrl + 'v2.0/me/messages?$select=Sender,Subject,ReceivedDateTime,HasAttachments&$search="' + key + '"';
            const response = await AxiosRequest.getFromServer(reqUrl, accessToken);
            const data = response?.data;           
            return data;            
        }
        else {
            const client = getAuthenticatedClient(accessToken);
            if (key) {
                request = await client
                    .api('/me/messages?$select=sender,subject,receivedDateTime,hasAttachments')
                    .search('"' + key + '"')
                    .get();
            }
            else {
                request = await client
                    .api('/me/messages?$select=sender,subject,receivedDateTime,hasAttachments')
                    .get();
            }
        }
    }
    catch (err) {
        log.error('Graph Service[getEmailsList]: ', err);
    }
    return request;
}
async function getPages(accessToken: string, link: string, isRest = false) {
    var data = [];
    if (link?.length > 0) {
        try {
            if (isRest) {
                var url = link;
                while (url?.length > 0) {
                    const response = await AxiosRequest.getFromServer(url, accessToken);
                    var nextPage = response?.data;
                    if (nextPage) {
                        data = data.concat(nextPage?.value);
                        url = nextPage['@odata.nextLink'];
                    }
                }
            }
            else {
                var url = link.replace('https://graph.microsoft.com/v1.0', '');
                while (url?.length > 0) {
                    const client = getAuthenticatedClient(accessToken);
                    const nextPage = await client
                        .api(url)
                        .get();
                    data = data.concat(nextPage?.value);
                    url = nextPage['@odata.nextLink'];
                }
            }
        }
        catch (err) {
            log.error('Graph Service[getPages]: ', err);
        }
    }
    return data;
}
export async function getNextPages(accessToken: string, link: string, isRest = false) {
    var data;
    if (link?.length > 0) {
        try {
            if (isRest) {
                const response = await AxiosRequest.getFromServer(link, accessToken);
                const dataRes = response?.data;
                return dataRes;
            }
            else {
                var url = link.replace('https://graph.microsoft.com/v1.0', '');
                const client = getAuthenticatedClient(accessToken);
                const nextPage = await client
                    .api(url)
                    .get();
                data = nextPage;
            }
        }
        catch (err) {
            log.error('Graph Service[getPages]: ', err);
        }
    }
    return data;
}
export async function getMIMEMessageContent(accessToken: string, messageId: string, isRest = false) {
    try {
        messageId = messageId.replace(/\//g, '-');
        if (isRest) {
            var restUrl = Office.context.mailbox.restUrl;
            if (!restUrl.endsWith('/'))
                restUrl = restUrl + '/';
            const reqUrl = restUrl + 'v2.0/me/messages/' + messageId + '/$value';
            const response = await AxiosRequest.getFromServer(reqUrl, accessToken);            
            const mimeContent = response?.data;
            return mimeContent;  
        }
        else {
            const client = getAuthenticatedClient(accessToken);

            const mimeContent = await client
                .api(`/me/messages/${messageId}/$value`)
                .get();

            return mimeContent;
        }
    }
    catch (err) {
        log.error('Graph Service[getMIMEMessageContent]: ', err);
        return null;
    }
}
export async function addMessageProperty(accessToken: string, messageId: string, data: ActivityProps) {
    try {
        messageId = messageId.replace(/\//g, '-');
        if (accessToken) {
            const client = getAuthenticatedClient(accessToken);

            client
                .api(`/me/messages/${messageId}/extensions/Com.Activities.App4Legal`)
                .get().then(response => {
                    if (response) {
                        var resp = response.value;
                        var parsedResp = JSON.parse(resp);
                        var parsedData = parsedResp.data;
                        parsedData.push(data);
                        var modifiedData = {
                            data: parsedData
                        };
                        var stringData = JSON.stringify(modifiedData);
                        var updatedProperty = {
                            "@odata.type": "microsoft.graph.openTypeExtension",
                            "value": stringData
                        }
                        client
                            .api(`/me/messages/${messageId}/extensions/Com.Activities.App4Legal`)
                            .patch(updatedProperty).then(response => {
                                addMessageCategory(accessToken, messageId);
                            }).catch(err => { log.error('Graph Service[addMessageProperty]: ', err); });
                    }
                }).catch(async error => {
                    await createMessageProperty(accessToken, messageId, data);
                });
        }
        else {
            await new Promise(resolve => {
                Office.context.mailbox.getCallbackTokenAsync({ isRest: true }, async function (result) {
                    if (result.status === Office.AsyncResultStatus.Succeeded) {
                        const token = result.value;
                        var restUrl = Office.context.mailbox.restUrl;
                        if (!restUrl.endsWith('/'))
                            restUrl = restUrl + '/';
                        const reqUrl = restUrl + "v2.0/me/messages('" + messageId + "')?$expand=MultiValueExtendedProperties($filter=PropertyId%20eq%20'" + encodeURI(propertyId) + "')";
                        const response = await AxiosRequest.getFromServer(reqUrl, token);
                        const messageRes = response?.data;
                        if (messageRes?.MultiValueExtendedProperties) {
                            //Update the property
                            var properties: any[] = messageRes.MultiValueExtendedProperties;
                            var propRes = properties.find(prop => prop.PropertyId === propertyId);
                            if (propRes) {
                                //Property found, update it
                                var propValue: any[] = propRes.Value;
                                if (propValue) {
                                    propValue = propValue.concat(JSON.stringify(data));
                                    //Patch the message
                                    const newRequestData = {
                                        "MultiValueExtendedProperties": [
                                            {
                                                "PropertyId": propertyId,
                                                "Value": propValue
                                            }
                                        ]
                                    };
                                    await AxiosRequest.patchToServer(reqUrl, newRequestData, token);
                                }
                            }        
                            resolve(null);
                        }
                        else {
                            //Create new property
                            var url = restUrl + "v2.0/me/messages('" + messageId + "')";
                            const newRequestData = {
                                "MultiValueExtendedProperties": [
                                    {
                                        "PropertyId": propertyId,
                                        "Value": [JSON.stringify(data)]
                                    }
                                ]
                            };
                            await AxiosRequest.patchToServer(url, newRequestData, token);
                            resolve(null);
                        }
                    }
                });
            });
            
        }
    }
    catch (err) {
        log.error('Graph Service[addMessageProperty-outer]: ', err);
        return null;
    }
}
export async function addMessageCategory(accessToken: string, messageId: string, id: string = null) {
    try {  
        if (accessToken) {
            messageId = messageId.replace(/\//g, '-');
            var categoryId;
            if (id == null) {
                var settings = Office.context.roamingSettings;
                var categoryId = settings.get('categoryId');
            }
            else categoryId = id;
            if (categoryId) {
                var categoryName = await getCategoryNameById(accessToken, categoryId);
                if (categoryName) {
                    const client = getAuthenticatedClient(accessToken);

                    var response = await client
                        .api(`/me/messages/${messageId}`)
                        .get();
                    if (response) {
                        var categories = [];
                        categories = response.categories;
                        if (!categories.includes(categoryName)) {
                            categories.push(categoryName);
                            var patchData = {
                                'categories': categories
                            };
                            await client
                                .api(`/me/messages/${messageId}`)
                                .patch(patchData);
                        }
                    }
                }
                else {
                    var createdId = await createCategory(accessToken);
                    if (createdId)
                        addMessageCategory(accessToken, messageId, createdId);
                }
            }
            else {
                var createdId = await createCategory(accessToken);
                if (createdId)
                    addMessageCategory(accessToken, messageId, createdId);
            }
        }
    }
    catch (err) {
        log.error('Graph Service[addMessageCategory]: ', err);
        return null;
    }
}
export async function getCategoryNameById(accessToken: string, categoryId: string) {
    if (accessToken) {
        const client = getAuthenticatedClient(accessToken);

        try {
            var response = await client.api('/me/outlook/masterCategories/' + categoryId)
                .get();
            if (response) {
                return response.displayName;
            }
            return null;
        }
        catch (err) {
            log.error('Graph Service[getCategoryNameById]: ', err);
            return null;
        }
    }
    return null;
}
export async function createCategory(accessToken: string) {
    if (accessToken) {
        const client = getAuthenticatedClient(accessToken);

        const appCategory = {
            displayName: 'App4Legal',
            color: 'Preset7'
        };
        try {
            var id = "";
            var responseFetching = await client.api('/me/outlook/masterCategories')
                .get();
            const data: any[] = responseFetching?.value;
            if (data && data.length > 0) {
                data.forEach(category => {
                    if (category.displayName === appCategory.displayName) {
                        id = category.id;
                    }
                });
                if(id === ''){
                    var response = await client.api('/me/outlook/masterCategories')
                        .post(appCategory);
                    id = response.id;
                }
            }
            else {
                var response = await client.api('/me/outlook/masterCategories')
                    .post(appCategory);
                id = response.id;
            }
            var settings = Office.context.roamingSettings;
            settings.set('categoryId', id);
            settings.saveAsync();
            return id;
        }
        catch (err) {
            log.error('Graph Service[createCategory]: ', err);
            return null;
        }
    }
    return null;
}
export async function createMessageProperty(accessToken: string, messageId: string, data: ActivityProps) {
    if (accessToken) {
        try {
            messageId = messageId.replace(/\//g, '-');
            const client = getAuthenticatedClient(accessToken);
            var modifiedData = {
                data: [data]
            };
            var stringData = JSON.stringify(modifiedData);
            var extension = {
                '@odata.type': "microsoft.graph.openTypeExtension",
                extensionName: "Com.Activities.App4Legal",
                value: stringData
            }
            await client
                .api(`/me/messages/${messageId}/extensions`)
                .post(extension);
            addMessageCategory(accessToken, messageId);
            return '';
        }
        catch (err) {
            log.error('Graph Service[createMessageProperty]: ', err);
            return null;
        }
    }
    return null;
}
export async function getMessageProperty(accessToken: string, messageId: string, isRest = false) {
    messageId = messageId.replace(/\//g, '-');
    if (!isRest && accessToken) {
        try {
            const client = getAuthenticatedClient(accessToken);

            var response = await client
                .api(`/me/messages/${messageId}/extensions/Com.Activities.App4Legal`)
                .get();
            if (response) {
                var values = response.value;
                if (values) {
                    var parsedResponse = JSON.parse(values);
                    if (parsedResponse)
                        return parsedResponse.data;
                }
            }
        }
        catch (err) {
            log.error('Graph Service[getMessageProperty]: ', err);
            return null;
        }
    }
    else {
        try {
            var restUrl = Office.context.mailbox.restUrl;
            if (!restUrl.endsWith('/'))
                restUrl = restUrl + '/';
            const reqUrl = restUrl + "v2.0/me/messages('" + messageId + "')?$expand=MultiValueExtendedProperties($filter=PropertyId%20eq%20'" + encodeURI(propertyId) + "')";
            const response = await AxiosRequest.getFromServer(reqUrl, accessToken);
            const messageRes = response?.data;
            if (messageRes?.MultiValueExtendedProperties) {
                //Update the property
                var properties: any[] = messageRes.MultiValueExtendedProperties;
                var propValues = [];
                properties.forEach(async prop => {
                    if (prop.PropertyId === propertyId) {
                        //Property found, update it
                        var propValue: any[] = prop.Value;
                        if (propValue) {
                            propValue = propValue.map(value => JSON.parse(value));
                        }
                        propValues = propValue;
                    }
                });
                return propValues;
            }
            return null;
        }
        catch (err) {
            log.error('Graph Service[getMessageProperty - Rest]: ', err);
            return null;
        }
    }
    return null;
}
export async function deleteMessageProperty(accessToken: string, messageId: string, guid: string, isRest = false) {
    messageId = messageId.replace(/\//g, '-');
    if (!isRest && accessToken) {
        try {
            const client = getAuthenticatedClient(accessToken);

            var response = await client
                .api(`/me/messages/${messageId}/extensions/Com.Activities.App4Legal`)
                .get();
            if (response) {
                var values = response.value;
                if (values) {
                    var parsedResponse = JSON.parse(values);
                    var data = [];
                    data = parsedResponse.data;
                    data = data.filter((property: ActivityProps) => property.guid !== guid);
                    await updateMessageProperty(accessToken, messageId, data);
                }
            }
        }
        catch (err) {
            log.error('Graph Service[deleteMessageProperty]: ', err);
            return null;
        }
    }
    else {
        try {
            var restUrl = Office.context.mailbox.restUrl;
            if (!restUrl.endsWith('/'))
                restUrl = restUrl + '/';
            const reqUrl = restUrl + "v2.0/me/messages('" + messageId + "')?$expand=MultiValueExtendedProperties($filter=PropertyId%20eq%20'" + encodeURI(propertyId) + "')";
            const response = await AxiosRequest.getFromServer(reqUrl, accessToken);
            const messageRes = response?.data;
            if (messageRes?.MultiValueExtendedProperties) {
                //Update the property
                var properties: any[] = messageRes.MultiValueExtendedProperties;
                var prop = properties.find(prp => prp.PropertyId === propertyId);
                if (prop) {
                    var propValue: any[] = prop.Value;
                    if (propValue) {
                        propValue = propValue.filter(value => {
                            const parsedValue = JSON.parse(value);
                            const guidd = parsedValue.guid;
                            return guidd !== guid
                        });
                        const url = restUrl + "v2.0/me/messages('" + messageId + "')";
                        const dataToUpdate = {
                            "MultiValueExtendedProperties": [
                                {
                                    "PropertyId": propertyId,
                                    "Value": propValue
                                }
                            ]
                        }
                        await AxiosRequest.patchToServer(url, dataToUpdate, accessToken);
                        return null;
                    }
                }                

            }
            return null;
        }
        catch (err) {
            log.error('Graph Service[getMessageProperty - Rest]: ', err);
            return null;
        }
    }
    return null;
}
export async function updateMessageProperty(accessToken: string, messageId: string, data: ActivityProps[]) {
    if (accessToken) {
        try {
            messageId = messageId.replace(/\//g, '-');
            const client = getAuthenticatedClient(accessToken);
            var modifiedData = {
                data: data
            };
            var stringData = JSON.stringify(modifiedData);
            var updatedProperty = {
                "@odata.type": "microsoft.graph.openTypeExtension",
                "value": stringData
            }
            client
                .api(`/me/messages/${messageId}/extensions/Com.Activities.App4Legal`)
                .patch(updatedProperty).then(response => { }).catch(err => { log.error('Graph Service[updateMessageProperty-inner]: ', err); });

        }
        catch (err) {
            log.error('Graph Service[updateMessageProperty]: ', err);
            return null;
        }
    }
    return null;
}