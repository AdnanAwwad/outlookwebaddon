﻿using Microsoft.Graph;
using OTAWebAppWeb;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace A4OWebCoreReactWebTs.Services
{
    public static class GraphService
    {
        public static GraphServiceClient GetAuthenticatedGraphClient(string accessToken) => 
            new GraphServiceClient(new DelegateAuthenticationProvider(
             requestMessage =>
            {
                requestMessage.Headers.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
                return Task.FromResult(0);
            }));
        public static async Task<List<Attachment>> GetEmailAttachments(string itemId, string accessToken)
        {
            try
            {                
                var graphClient = GetAuthenticatedGraphClient(accessToken);
                List<Attachment> attachments = new List<Attachment>();                   
                var attachPage = await graphClient.Me.Messages[itemId].Attachments.Request().GetAsync();                
                attachments.AddRange(attachPage);
                while (attachPage.NextPageRequest != null)
                {
                    attachPage = await attachPage.NextPageRequest.GetAsync();
                    attachments.AddRange(attachPage);
                }
                return attachments;
            }
            catch(Exception ex)
            {

            }
            return null;
        }
    }
}
